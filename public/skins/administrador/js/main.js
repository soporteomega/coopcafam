$( document ).ready(function() {
	$(".up_table,.down_table").click(function(){
	    var row = $(this).parents("tr:first");
	    var value = row.find("input").val();
	    var content1 =  row.find("input").attr("id");
	    var content2 = 0;
	    if ($(this).is(".up_table")) {
	    	if(row.prev().find("input").val() > 0 ){
	    		row.find("input").val(row.prev().find("input").val());
	    		row.prev().find("input").val(value);
	    		content2 = row.prev().find("input").attr("id");
				row.insertBefore(row.prev());
	    	}
	    } else {
	    	if(row.next().find("input").val() > 0 ){
	    		row.find("input").val(row.next().find("input").val());
	    		row.next().find("input").val(value);
	    		content2 = row.next().find("input").attr("id");
				row.insertAfter(row.next());
	    	}
	    }
	   	var route = $("#order-route").val();
	   	var csrf = $("#csrf").val();
	   	if(route !=""){
	   		 $.post(route, { 'csrf':csrf , 'id1': content1, 'id2': content2 });
	   	}
	});
	$(".switch-form").bootstrapSwitch({
   		"onText" : "Si",
   		"offText" : "No"
   	});



});







function login(){
	var user = $("#login_form #user").val();
	var password = $("#login_form #password").val();
	var csrf = $("#login_form #csrf").val();
	$("#login_form #user").parent().removeClass("has-error");
	$("#login_form #password").parent().removeClass("has-error");
	$("#error_login").removeClass("error-login");
	$("#error_login").html("");
	if(user && password){
		$.post("/administracion/user/login",{"user":user,"password":password,"csrf":csrf},function(res){
			if (res.isError == false) {
				window.location="/administracion/panel";
			} else {
				$("#login_form #user").val("");
				$("#login_form #password").val("");
				$("#error_login").html("El Usuario o Contraseña son incorrectos.");
				$("#error_login").addClass("error-login");
			}
		});
	} else {
		if (!user) {
			$("#login_form #user").parent().addClass("has-error");
		}
		if (!password) {
			$("#login_form #password").parent().addClass("has-error");
		}
		$("#error_login").addClass("error-login");
		$("#error_login").html("El Usuario y la Contraseña son Obligatorios.");
	}
	return false;
}

