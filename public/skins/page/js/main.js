$( document ).ready(function() {
	$(".file-image").fileinput({
   		maxFileSize: 2048,
       	previewFileType: "image",
        allowedFileExtensions: ["jpg", "jpeg", "gif", "png"],
        previewClass: "bg-warning",
        showUpload: false,
        showRemove: false,
        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        language:"es"
    });

});

$( window ).load(function() {
	if($(window).width()>768){
		$(".botonera-lateral").css({height:($("#slider-home").height())+"px"});
	} else {
		$(".botonera-lateral").css({height:"auto"});
	}
	$('[data-toggle="tooltip"]').tooltip();
});
$( window ).resize(function() {
	if($(window).width()>768){
		$(".botonera-lateral").css({height:($("#slider-home").height())+"px"});
	} else {
		$(".botonera-lateral").css({height:"auto"});
	}
});


$(function(){
    $('.view-pdf').on('click',function(){
        var pdf_link = $(this).attr('href');
        var descarga = parseInt( $(this).attr("om-descarga"));
        console.log(descarga);
        if( parseInt(descarga) == 1 ){
        	var iframe = '<div class="iframe-container fondo_descarga" onclick="cerrar_popup();"><div align="right"><a class="x_descarga glyphicon glyphicon-remove" onclick="cerrar_popup();"></a><a href="'+pdf_link+'" download="'+pdf_link+'" class="x_descarga glyphicon glyphicon-download-alt" target="_blank"></a></div><iframe src="http://docs.google.com/viewer?url='+pdf_link+'&embedded=true" style="padding:50px;"></iframe></div>'
        $('#div_popup').html(iframe);
        } else {
        	var iframe = '<div class="iframe-container fondo_descarga" onclick="cerrar_popup();"><div align="right"><a class="x_descarga glyphicon glyphicon-remove" onclick="cerrar_popup();"></a></div><iframe src="http://docs.google.com/viewer?url='+pdf_link+'&embedded=true" style="padding:50px;"></iframe></div>'
        $('#div_popup').html(iframe);
        }
        
        return false;
    });
})

function cerrar_popup(){
	$('#div_popup').html('');
}

function login(){
	var user = $("#login_form #user").val();
	var password = $("#login_form #password").val();
	var csrf = $("#login_form #csrf").val();
	$("#login_form #user").parent().removeClass("has-error");
	$("#login_form #password").parent().removeClass("has-error");
	$("#error_login").removeClass("error-login");
	$("#error_login").html("");
	if(user && password){
		$.post("/page/login/login",{"user":user,"password":password,"csrf":csrf},function(res){
			if (res.isError == false) {
				window.location="/";
			} else {
				$("#login_form #user").val("");
				$("#login_form #password").val("");
				$("#error_login").html("El Usuario o Contraseña son incorrectos.");
				$("#error_login").addClass("error-login");
			}
		});
	} else {
		if (!user) {
			$("#login_form #user").parent().addClass("has-error");
		}
		if (!password) {
			$("#login_form #password").parent().addClass("has-error");
		}
		$("#error_login").addClass("error-login");
		$("#error_login").html("El Usuario y la Contraseña son Obligatorios.");
	}
	return false;
}

function menuresponsive() {
	if($(".nav-responsive").is(":visible")){
		$(".nav-responsive").hide();
	} else{
		$(".nav-responsive").show();
	}
}

function pad (n, length) {
    var  n = n.toString();
    while(n.length < length)
         n = "0" + n;
return n;
}