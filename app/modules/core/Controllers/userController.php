<?php

/**
* Controlador del modulo Administrador que se encarga de adaptar el contenido de la ciudad.
*/
class Core_userController extends Controllers_Abstract
{

    public function validationAction()
    {
        $modelUser = new Core_Model_DbTable_User();
        $user= $this->_getSanitizedParam("user");
        $currentUser= $this->_getSanitizedParam("currentuser");
        $res_user = $modelUser->searchUserByUser($user);
        if ( $res_user != false && ($currentUser=='' ||  $res_user->user_user != $currentUser)) {
            header("HTTP/1.0 400 Usuario ya existe");
        } else {
            http_response_code(200);
        }
    }

    public function actualizarcumpleaniosAction()
    {
        $userModel = new Administracion_Model_DbTable_User();
        $fecha = date("Y-m-d",strtotime ( '-1 month' , strtotime (date('Y-m-d'))));
        $nuevafecha = strtotime ( '+1 year' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
        $usercalendar = $userModel->userCalendar($fecha,$nuevafecha);
        $calendarMoldel = new Administracion_Model_DbTable_Calendar();
        foreach ($usercalendar as $key => $user) {
            if($user->user_date != '0000-00-00'){
                $data = array();
                $data['name'] = $user->user_names." ".$user->user_lastnames;
                $data['startdate'] = $this->fechacumpleanos($user->user_date);
                $data['starttime'] = "";
                $data['enddate'] = "";
                $data['endtime'] = "";
                $data['user'] = $user->user_id;
                $data['description'] ="<p>Coopcafam te felicita por tu cumpleaños</p>";
                $data['type'] = 4;
                $data['tipo'] = "Empleados";
                $data['createdby'] = 0;
                $data['banner'] = "";
                $data['image'] = "";
            }
            $calendarMoldel->insert($data);
        }
    }

    public function  fechacumpleanos($fecha){
        $arrayfecha = explode("-",$fecha);
        $fechaactual = date("Y")."-".$arrayfecha[1]."-".$arrayfecha[2];
        if(date("Y-m-d",strtotime($fechaactual)) < date("Y-m-d",strtotime ( '-1 month' , strtotime (date('Y-m-d'))))){
            $fechaactual = (date("Y")+1)."-".$arrayfecha[1]."-".$arrayfecha[2];
        }
        return $fechaactual;
    }

    public function pruebasAction(){
        echo $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/evento.php');
    }
}