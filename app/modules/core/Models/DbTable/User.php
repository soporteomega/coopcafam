<?php

/**
*
*/

class Core_Model_DbTable_User extends Db_Table
{
	protected $_name = 'user';
    protected $_id = 'user_id';

    public function searchUser($id)
    {
        $res = $this->_conn->query('SELECT * FROM '.$this->_name.' WHERE user_id = "'.$id.'"')->fetchAsObject();
        return $res;
    }

	public function searchUserByUser($user)
    {
        $res = $this->_conn->query('SELECT * FROM '.$this->_name.' WHERE user_user = "'.$user.'"')->fetchAsObject();
        if(isset($res[0])){
            $res = $res[0];
        } else {
            $res = false;
        }
        return $res;
    }

    public function autenticateUser($user,$password){
        $resUser=$this->searchUserByUser($user);
        if ($resUser->user_id) {
            if(password_verify($password,$resUser->user_password)){
                return  true;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }

     public function changeCode($id,$code)
    {
        $res = $this->_conn->query("UPDATE ".$this->_name." SET code = '$code' WHERE user_id = '".$id."'");
    }

    public function changepassword($password,$id){
        $user_password = password_hash($password, PASSWORD_DEFAULT);
        $query = "UPDATE user SET user_password = '$user_password' WHERE user_id= '$id' ";
        $res = $this->_conn->query($query);
    }


}