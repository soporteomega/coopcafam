<?php
/**
*
*/

class Core_Model_Csrf
{
   public function __construct(){
        Session::getInstance()->set('csrf',$this->getRandomCode(20));
   }

   public function getRandomCode($length){
        $chain="";
        $an = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-)(.:,;";
        $su = strlen($an)- 1;
        for($i=0;$i<$length;$i++){
           $chain=$chain.substr($an, rand(0, $su), 1);
        }
        return $chain;
    }
}