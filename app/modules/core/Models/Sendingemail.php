<?php

/**
* Modelo del modulo Core que se encarga de  enviar todos los correos nesesarios del sistema.
*/
class Core_Model_Sendingemail
{
	/**
     * Intancia de la calse emmail
     * @var class
     */
    protected $email;

    protected $_view;

     public function __construct($view)
    {
        $this->email = new Core_Model_Mail();
        $this->_view = $view;
    }


    public function forgotpassword($user)
    {
        if ($user) {
            $code = [];
            $code['user'] = $user->user_id;
            $code['code'] = $user->code;
            $codeEmail = base64_encode(json_encode($code));
           	$this->_view->url = "http://".$_SERVER['HTTP_HOST']."/page/login/changepassword?code=".$codeEmail;
            $this->_view->nombre = $user->user_names." ".$user->user_lastnames;
            $this->_view->usuario = $user->user_user;
            /*fin parametros de la vista */
            //$this->email->getMail()->setFrom("desarrollo4@omegawebsystems.com","Intranet Coopcafam");
            $this->email->getMail()->addAddress($user->user_email,  $user->user_names." ".$user->user_lastnames);
            $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/forgotpassword.php');
            $this->email->getMail()->Subject = "Recuperacion de Contraseña ";
            $this->email->getMail()->msgHTML($content);
            $this->email->getMail()->AltBody = $content;
            if ($this->email->sed()==true) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function solicitudlinea()
    {
        $tipoModel = new Core_Model_DbTable_Tipogestion();
        $tipo = $tipoModel->getById($this->_view->solicitud->solicitud_tipo);
        //$this->email->getMail()->setFrom("notificaciones@intranetcoopcafam.com.co","Intranet Coopcafam");
        if($tipo->tipo_gestion_correos){
            $addresses = explode(',', $tipo->tipo_gestion_correos);
            foreach ($addresses as $address) {
                if($address!='' && $address){
                    $this->email->getMail()->addAddress($address,"Intranet Coopcafam");
                }
            }
        }
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/solicitudenlinea.php');
        $this->email->getMail()->Subject = "Solicitud en Linea ";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return true;
        } else {
            return false;
        }
    }

    public function evento($id)
    {
       $calendarModel= new Administracion_Model_DbTable_Calendar();
        $evento = $calendarModel->getById($id);
        if($evento->calendar_id){
            $this->_view->evento = $evento;
            $this->email->getMail()->addAddress("notificaciones@intranetcoopcafam.com.co","Intranet Coopcafam");
            $this->setUser();
            $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/evento.php');
            $this->email->getMail()->Subject = "Nuevo Evento en el Calendario Coopcafam";
            $this->email->getMail()->msgHTML($content);
            $this->email->getMail()->AltBody = $content;
            if ($this->email->sed()==true) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function noticia($id)
    {
        $contentModel = new Administracion_Model_DbTable_Content();
        $noticia = $contentModel->getById($id);
        if( $noticia->content_id ) {
            $this->_view->noticia = $noticia;
            //$this->email->getMail()->addAddress("proyectos@omegawebsystems.com","pruebas omega");
            $this->email->getMail()->addAddress("notificaciones@intranetcoopcafam.com.co","Intranet Coopcafam");
            $this->setUser();
            $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/noticia.php');
            $this->email->getMail()->Subject = "Nueva Noticia Coopcafam";
            $this->email->getMail()->msgHTML($content);
            $this->email->getMail()->AltBody = $content;
            if ($this->email->sed()==true) {
                echo "envio";
                return true;
            } else {
                echo "no envio";
                return false;
            }
        }
    }
    public function eventos()
    {
        $this->email->getMail()->addAddress("notificaciones@intranetcoopcafam.com.co","Intranet Coopcafam");
        //$this->email->getMail()->addAddress("desarrollo4@omegawebsystems.com","prueba omega");
        $this->setUser();
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/eventos.php');
        $this->email->getMail()->Subject = "Nuevos Eventos en el Calendario Coopcafam";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return true;
        } else {
            return false;
        }
    }

    public function noticias()
    {
        //$this->email->getMail()->addAddress("desarrollo4@omegawebsystems.com","prueba omega");
        $this->email->getMail()->addAddress("notificaciones@intranetcoopcafam.com.co","Intranet Coopcafam");
        $this->setUser();
        $content = $this->_view->getRoutPHP('/../app/modules/core/Views/templatesemail/noticias.php');
        $this->email->getMail()->Subject = "Nuevas Noticias Coopcafam";
        $this->email->getMail()->msgHTML($content);
        $this->email->getMail()->AltBody = $content;
        if ($this->email->sed()==true) {
            return true;
        } else {
            return false;
        }
    }


    public function  setUser(){
        $modelUser =  new Core_Model_DbTable_User();
        $usuarios = $modelUser->getList("( user_level = '2' OR user_level = '3' ) AND user_position = 'Empleado'","");
        foreach ($usuarios as $key => $usuario) {
            if($usuario->user_email){
                $this->email->getMail()->addBCC($usuario->user_email,$usuario->user_names." ".$usuario->user_lastnames);
            }
        }
    }
}