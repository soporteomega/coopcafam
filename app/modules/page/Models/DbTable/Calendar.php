<?php 

/**
* 
*/
class Page_Model_DbTable_Calendar extends Db_Table
{
	protected $_name = 'calendar';
	protected $_id = 'calendar_id';


	public function getList($filters,$order)
    {
        $filter = '';
        if($filters != ''){
            $filter = ' WHERE '.$filters;
        }
        $orders ="";
        if($order != ''){
            $orders = ' ORDER BY '.$order;
        }
        $select = 'SELECT * FROM ('.$this->_name.' LEFT JOIN  type_calendar ON type_calendar_id = calendar_type) LEFT JOIN user ON user_id = calendar_user '.$filter.' '.$orders;
        $res = $this->_conn->query( $select )->fetchAsObject();
        return $res;
    }

    public function getListPages($filters,$order,$page,$amount)
    {
       $filter = '';
        if($filters != ''){
            $filter = ' WHERE '.$filters;
        }
        $orders ="";
        if($order != ''){
            $orders = ' ORDER BY '.$order;
        }
        $select = 'SELECT * FROM ('.$this->_name.' LEFT JOIN  type_calendar ON type_calendar_id = calendar_type) LEFT JOIN user ON user_id = calendar_user '.$filter.' '.$orders.' LIMIT '.$page.' , '.$amount;
        $res = $this->_conn->query($select)->fetchAsObject();
        return $res;
    }
	
}