<?php 

/**
* 
*/
class Page_Model_DbTable_Respuesta extends Db_Table
{
	protected $_name = 'respuesta';
	protected $_id = 'respuesta_id';

	function insert($data)
	{
		$pregunta_id = $data['pregunta'];
		$respuesta_respuesta = $data['respuesta'];
		$respuesta_correcto = $data['correcta'];
		$usuario_id = $data['usuario'];
		$respuesta_fecha = $data['fecha'];
		$respuesta_hora = $data['hora'];
		$respuesta_intentos = $data['intentos'];
		$query = "INSERT INTO respuesta(pregunta_id, respuesta_respuesta, respuesta_correcto, usuario_id,respuesta_fecha,respuesta_hora,respuesta_intentos) VALUES ('$pregunta_id','$respuesta_respuesta','$respuesta_correcto','$usuario_id','
		$respuesta_fecha','$respuesta_hora','$respuesta_intentos')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	public function respondio($usuario,$modulo)
	{
		$query = "SELECT respuesta.* , preguntas.* FROM respuesta LEFT JOIN preguntas ON preguntas.pregunta_id = respuesta.pregunta_id WHERE preguntas.modulo_id = '$modulo' AND respuesta.usuario_id = '$usuario'";
		$res = $this->_conn->query($query)->fetchAsObject();
        return $res;
	}
	public function eliminarRespuestas($usuario,$modulo)
	{
		$query = "DELETE respuesta FROM respuesta LEFT JOIN preguntas ON preguntas.pregunta_id = respuesta.pregunta_id WHERE preguntas.modulo_id = '$modulo' AND respuesta.usuario_id = '$usuario'";
		$this->_conn->query($query);
	}

}