<?php 

/**
* 
*/
class Page_Model_DbTable_Preguntas extends Db_Table
{
	protected $_name = 'preguntas';
	protected $_id = 'pregunta_id';

	public function getTipos()
	{
		$tipos = array();
		$tipos[1] = 'Selección múltiple con única respuesta';
		$tipos[2] = 'Falso o Verdadero';
		$tipos[3] = 'Selección múltiple con múltiple respuesta';

		return $tipos;
	}

}