<?php
/**
*
*/

class Page_Model_DbTable_User extends Db_Table
{
	protected $_name = 'user';
    protected $_id = 'user_id';

    public function update($data,$id){
        $user_names = $data['names'];
        $user_lastnames = $data['lastnames'];
        $user_email = $data['email'];
        $user_idnumber = $data['idnumber'];
        $user_city = $data['city'];
        $user_country = $data['country'];
        $user_phone = $data['phone'];
        $user_address = $data['address'];
        $user_current_user = $data['current_user'];
        $user_photo = $data['photo'];
        $user_position = $data['position'];
        echo $query = "UPDATE user SET user_names = '$user_names',user_lastnames = '$user_lastnames',user_email = '$user_email',user_idnumber = '$user_idnumber',user_city = '$user_city',user_country = '$user_country',user_phone = '$user_phone',user_address = '$user_address', user_current_user = '$user_current_user', user_photo='$user_photo', user_position='$user_position' WHERE user_id= '$id' ";
        $res = $this->_conn->query($query);
    }

    public function changepassword($password,$id){
        $user_password = password_hash($password, PASSWORD_DEFAULT);
        $query = "UPDATE user SET user_password = '$user_password', user_initial = 1 WHERE user_id= '$id' ";
        $res = $this->_conn->query($query);
    }

    public function register($data,$id){
        $user_names = $data['names'];
        $user_lastnames = $data['lastnames'];
        $user_email = $data['email'];
        $user_idnumber = $data['idnumber'];
        $user_city = $data['city'];
        $user_country = $data['country'];
        $user_phone = $data['phone'];
        $user_address = $data['address'];
        $user_level = $data['level'];
        $user_state = $data['state'];
        $user_area = $data['area'];
        $user_user = $data['user'];
        $user_date = $data['date'];
        $user_photo = $data['photo'];
        $changepasword = '';
        if($data['password']!=''){
            $user_password = password_hash($data['password'], PASSWORD_DEFAULT);
            $changepasword = " , user_password = '$user_password'";
        }
        $user_current_user = $data['current_user'];
        $query = "UPDATE user SET user_names = '$user_names',user_lastnames = '$user_lastnames',user_email = '$user_email',user_idnumber = '$user_idnumber',user_city = '$user_city',user_country = '$user_country',user_phone = '$user_phone',user_address = '$user_address',user_level = '$user_level',user_state = '$user_state',user_area = '$user_area',user_user = '$user_user',user_current_user = '$user_current_user'  $changepasword, user_photo = '$user_photo' , user_date = '$user_date' WHERE user_id= '$id' ";
        $res = $this->_conn->query($query);
    }
}