<?php 

/**
* 
*/
class Page_Model_DbTable_Solicitud extends Db_Table
{
	protected $_name = 'solicitud';
	protected $_id = 'solicitud_id';

	public function insert($data)
	{
		$solicitud_fecha = $data['fecha'];
		$solicitud_idusuario = $data['idusuario'];
		$solicitud_nombre = $data['nombre'];
		$solicitud_correo = $data['correo'];
		$solicitud_tipo = $data['tipo'];
		$solicitud_direccion = $data['direccion'];
		$solicitud_ciudad = $data['ciudad'];
		$solicitud_telefono = $data['telefono'];
		$solicitud_celular = $data['celular'];
		$solicitud_solicitud = $data['solicitud'];
		$solicitud_estado = $data['estado'];
		$query = "INSERT INTO solicitud(solicitud_fecha, solicitud_idusuario, solicitud_nombre, solicitud_correo, solicitud_tipo, solicitud_direccion, solicitud_ciudad, solicitud_telefono, solicitud_celular, solicitud_solicitud, solicitud_estado) VALUES ('$solicitud_fecha', '$solicitud_idusuario', '$solicitud_nombre', '$solicitud_correo', '$solicitud_tipo', '$solicitud_direccion', '$solicitud_ciudad', '$solicitud_telefono', '$solicitud_celular', '$solicitud_solicitud', '$solicitud_estado')";
		$this->_conn->query($query);
		return mysqli_insert_id($this->_conn->getConnection());
	}

	public function getList($filters,$order)
    {
        $filter = '';
        if($filters != ''){
            $filter = ' WHERE '.$filters;
        }
        $orders ="";
        if($order != ''){
            $orders = ' ORDER BY '.$order;
        }
        $select = 'SELECT * FROM '.$this->_name.' LEFT JOIN tipo_gestion ON tipo_gestion_id = solicitud_tipo '.$filter.' '.$orders;
        $res = $this->_conn->query( $select )->fetchAsObject();
        return $res;
    }
}