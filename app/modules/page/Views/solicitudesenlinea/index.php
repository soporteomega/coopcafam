<div class="container">
	<div class="padding-content">
	<h1 class="titulo-general">Solicitudes en línea<?php if($this->envio == 1){ ?><span style="display:block;">Su mensaje se ha enviado Correctamente, pronto nos pondremos en contacto con usted. </span><?php } ?></h1>
	<div class="subtitulo2"><?php echo $this->introduccion[0]->content_description; ?></div>
	<div class="caja-solicitud">
		<div class="row">
			<form role="form" action="/page/solicitudesenlinea/insert" method="post" >
				<div class="form-group col-sm-4">
					<label for="nombre">Nombre</label>
					<input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Nombre" value="<?php echo $this->user->user_names." ".$this->user->user_lastnames ?>">
				</div>
				<div class="form-group col-sm-4">
					<label for="correo">Correo</label>
					<input type="email" class="form-control" id="correo" name="correo" placeholder="Correo" value="<?php echo $this->user->user_email; ?>">
				</div>
				<div class="form-group col-sm-4">
					<label for="direccion">Gestion</label>
					<select class="form-control" name="tipo" required>
						<option>Seleccione...</option>
						<?php foreach ($this->gestiones as $key => $gestion): ?>
							<option value="<?php echo $gestion->tipo_gestion_id; ?>"><?php echo $gestion->tipo_gestion_nombre; ?> </option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="form-group col-sm-4">
					<label for="direccion">Dirección</label>
					<input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" value="<?php echo $this->user->user_address; ?>" >
				</div>
				
				<div class="form-group col-sm-2">
					<label for="ciudad">Ciudad</label>
					<input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" value="<?php echo $this->user->user_city; ?>">
				</div>
				<div class="form-group col-sm-3">
					<label for="telefono">Extension</label>
					<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" value="<?php echo $this->user->user_phone; ?>">
				</div>
				<div class="form-group col-sm-3">
					<label for="celular">Celular</label>
					<input type="text" class="form-control" id="celular" name="celular" placeholder="Celular" >
				</div>

				<div class="form-group col-sm-12">
					<label for="solicitud">Solicitud</label>
					<textarea class="form-control" name="solicitud" id="solicitud" placeholder="Solicitud" rows="4" style="resize: none;"></textarea>
				</div>
				<div class="col-sm-12" align="right">
					<button type="submit" class="btn btn-success">Enviar</button>
				</div>
			</form>
		</div>
	</div>
</div>




