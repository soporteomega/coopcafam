<div id="div_popup"></div>
<div>
	<div class="container">
		<h1 class="titulo-general">Buscador</h1>
		<br>
		<br>
		<div class="row">
			<div class="col-sm-3">
				<div class="cajaBuscador">
					<form>
						<div class="form-group">
							<label>Palabra Clave</label>
							<input type="text" placeholder="Palabra Clave" class="form-control" name="buscar" value="<?php echo $this->buscar; ?>">
						</div>
						<?php if(1==2){ ?>
						<div class="form-group">
							<label>Seccion</label>
							<select class="form-control" name="categoria" >
								<option value="">Todas</option>
								<?php foreach ($this->categorias as $key => $categoria): ?>
									<option value="$key" <?php if($this->categoria == $key){ ?> selected <?php } ?>><?php echo $categoria; ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<?php } ?>
						<div class="form-group">
							<button type="submit" class="btn btn-success btn-block"><i></i> Buscar</button>
						</div>

					</form>
				</div>
			</div>
			<div class="col-sm-9">
			<?php $mostrar = 0; ?>
				<?php if($this->cooperativa){ ?>
					<div>
						<h2 class="titulo-buscador">Nuestra Cooperativa</h2>
						<?php foreach ($this->cooperativa as $key => $content): ?>
							<a class="enlace-busqueda" href="/page/cooperativa#<?php echo $content->content_id;?>"><?php echo $content->content_title;?></a>
							<?php $mostrar = 1; ?>
						<?php endforeach ?>
					</div>
				<?php } ?>

				<?php if($this->gestion){ ?>
					<div>
						<h2 class="titulo-buscador">Gestión Humana</h2>
						<?php foreach ($this->gestion as $key => $section): ?>
							<?php if(count($section)>0){ ?>
							<h3 class="subtitulo-buscador"><?php echo $key; ?></h3>
							<?php foreach ($section as $llave => $content): ?>
								<a class="enlace-busqueda" <?php if($key == 'Beneficios'){ ?>href="/page/gestionhumana/beneficios#<?php echo $content->content_id;?>"<?php } else { ?>href="/page/campanias/detalle?id=<?php echo $content->content_id;?>" <?php } ?>><?php echo $content->content_title;?></a>
									<?php $mostrar = 1; ?>
							<?php endforeach ?>
							<?php } ?>
						<?php endforeach ?>
						
					</div>
				<?php } ?>
				<?php if($this->noticias){ ?>
					<div>
						<h2 class="titulo-buscador">Nuestra noticias</h2>
						<?php foreach ($this->noticias as $key => $content): ?>
							<a class="enlace-busqueda" href="/page/noticias/detalle?id=<?php echo $content->content_id;?>"><?php echo $content->content_title;?></a>
								<?php $mostrar = 1; ?>
						<?php endforeach ?>
					</div>
				<?php } ?>
				<?php if($this->asociados){ ?>
					<div>
						<h2 class="titulo-buscador">Nuestra asociados</h2>
						<?php foreach ($this->asociados as $key => $content): ?>
							<a class="enlace-busqueda" href="/page/asociados/detalle?id=<?php echo $content->content_id;?>"><?php echo $content->content_title;?></a>
								<?php $mostrar = 1; ?>
						<?php endforeach ?>
					</div>
				<?php } ?>
				<?php if($this->sgc){ ?>
					<div>
						<h2 class="titulo-buscador">SGC</h2>
						<?php foreach ($this->sgc as $key => $section): ?>
							<?php if(count($section)>0){ ?>
							<h3 class="subtitulo-buscador"><?php echo $section['detalle']->categoria_documento_nombre; ?></h3>
							<?php foreach ($section['documentos'] as $llave => $content): ?>
								<a  <?php echo 'href="http://'.$_SERVER['HTTP_HOST'].'/files/'.$content->documento_documento.'" class="view-pdf enlace-busqueda" om-descarga="'.$content->documento_descarga.'"'; ?> ><?php echo $content->documento_nombre;?></a>
									<?php $mostrar = 1; ?>
							<?php endforeach ?>
							<?php } ?>
						<?php endforeach ?>
					</div>
				<?php } ?>
				<?php if ($this->buscar== ""){?>
					<div class="mensaje-busqueda">
						Te damos la bienvenida a nuestro buscador, ingrese la palabra a buscar.
					</div>
				<?php } ?>
				<?php if ($mostrar == 0 && $this->buscar != "" ){?>
					<div class="mensaje-busqueda">
						 Lo sentimos no encontramos resultados a tu busqueda
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>