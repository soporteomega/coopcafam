<div id="div_popup"></div>
<div class="container">
	<h1 class="titulo-general">Sistema de Gestión de la Calidad SGC</h1>
	<div class="subtitulo2"><?php echo $this->introduccion[0]->content_description; ?></div>
</div>
<div align="center">
	<div class="container">
		<a class="caja_boton" href="/page/sgc/mapa">
			<div class="caption-caja">
				<div class="content-caption">
					<span  class="titulo_caja">Mapa de procesos </span>
					<img src="/skins/page/images/mas-blanco.png" class="boton-mas">
				</div>
			</div>
		</a>
		<?php $modales = '';  ?>
		<?php foreach ($this->contenidos as $key => $content): ?>
			<?php $categoria = $content['detalle'];?>
			<div class="caja_boton" <?php if($categoria->categoria_documento_imagen){?>style="background:url(/images/<?php echo $categoria->categoria_documento_imagen; ?>);"<?php } ?>>
				<div class="caption-caja">
					<div class="content-caption">
						<?php if($categoria->categoria_documento_id!=3){ ?>
							<a href="/page/sgc/detalle?categoria=<?php echo $categoria->categoria_documento_id; ?>" class="titulo_caja"><?php echo  $categoria->categoria_documento_nombre; ?></a>
						<?php } else{?>
							<a href="/page/sgc/detalle2?categoria=<?php echo $categoria->categoria_documento_id; ?>" class="titulo_caja"><?php echo  $categoria->categoria_documento_nombre; ?></a>
						<?php } ?>

						<?php if($categoria->categoria_documento_id!=3){ ?>
						<div class="enlaces-caja">
							<?php $contador = 0;?>
							<?php for ($i=0; $i < 5 ; $i++) {  ?>
								<?php if(isset($content['categorias'][$i])){ ?>
									<?php $contenido = $content['categorias'][$i]; ?>
									<a data-toggle="modal" data-target="#modal<?php echo $contenido['detalle']->categoria_documento_id; ?>"><?php echo $contenido['detalle']->categoria_documento_nombre; ?></a>

									<?php $modales =$modales.'<div class="modal fade" id="modal'.$contenido['detalle']->categoria_documento_id.'" tabindex="-1" role="dialog" aria-labelledby="myModal'.$contenido['detalle']->categoria_documento_id.'">
									  	<div class="modal-dialog" role="document">
									    	<div class="modal-content">
									      		<div class="modal-header">
									        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									        		<h4 class="modal-title" id="myModal'.$contenido['detalle']->categoria_documento_id.'">'.$contenido['detalle']->categoria_documento_nombre.'</h4>
									      		</div>
									      		<div class="modal-body">
										        	'.vercategoria($contenido,$contenido['detalle']->categoria_documento_id).'
									      		</div>
									    	</div>
									  	</div>
									</div>';?>
									<?php $contador ++;?>
								<?php } ?>
							<?php } ?>
							<?php if($contador<5){ ?>
							<?php for ($i=0; $i < 5 ; $i++) {  ?>
								<?php if(isset($content['documentos'][$i])){ ?>
									<?php $contenido = $content['documentos'][$i]; ?>
									<a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/files/<?= $contenido->documento_documento; ?>" class="subtitulo_caja view-pdf" om-descarga="<?php echo  $documento->documento_descarga; ?>"><?= $contenido->documento_nombre; ?></a>
								<?php } ?>
							<?php } ?>
							<?php } ?>
							<?php if((count($content['documentos'])+ count($content['categorias'])) > 5){ ?>
								<a href="/page/sgc/detalle?categoria=<?php echo $categoria->categoria_documento_id; ?>" class="subtitulo_caja ver-todos" om-descarga="<?php echo  $documento->documento_descarga; ?>" ><i class="glyphicon glyphicon-plus-sign"></i> Ver todos</a>
							<?php }?>
						</div>
						<?php }?>
						<img src="/skins/page/images/mas-blanco.png" class="boton-mas">
					</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>

<?php
	echo $modales;
	function vercategoria($array,$id){
		$text = '';
		if(count($array['categorias'])){
			$text = $text.'<div class="panel-group" id="accordion'.$id.'" role="tablist" aria-multiselectable="true">';
			foreach ($array['categorias'] as $key => $categoria) {
				$text = $text.'<div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="heading'.$categoria['detalle']->categoria_documento_id.'">
				      <h4 class="panel-title">
				        <a role="button" data-toggle="collapse" data-parent="#accordion'.$id.'" href="#collapse'.$categoria['detalle']->categoria_documento_id.'"  aria-controls="collapse'.$categoria['detalle']->categoria_documento_id.'">
				          '.$categoria['detalle']->categoria_documento_nombre.'
				        </a>
				      </h4>
				    </div>
				    <div id="collapse'.$categoria['detalle']->categoria_documento_id.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'.$categoria['detalle']->categoria_documento_id.'">
				      <div class="panel-body">
				      '.vercategoria($categoria,$categoria['detalle']->categoria_documento_id).'
				      </div>
				    </div>
				  </div>';
			}
			$text = $text.'</div>';
		}

		if(count($array['documentos'])){
			foreach ($array['documentos'] as $key => $documento) {
				$text = $text.'<a href="http://'.$_SERVER['HTTP_HOST'].'/files/'.$documento->documento_documento.'" class="enlaces-documentos view-pdf" om-descarga="'.$documento->documento_descarga.'"> <i class="glyphicon glyphicon-fullscreen"></i> '.$documento->documento_nombre.'</a>';
			}
		}
		return $text;
	}

?>