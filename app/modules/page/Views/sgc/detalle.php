<div id="div_popup"></div>
<div class="text-center paddin-content">
	<div class="container">
		<h1 class="titulo-general"><?php echo $this->categoria->categoria_documento_nombre; ?></h1>
		<div class="text-right"><a class="btn btn-primary" href="/page/sgc"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a></div>
		<br>
		<?php echo vercategoria($this->content,$this->content['detalle']->categoria_documento_id); ?>
	</div>
</div>
<?php

	function vercategoria($array,$id){
		$text = '';
		if(count($array['categorias'])){
			$text = $text.'<div class="panel-group" id="accordion'.$id.'" role="tablist" aria-multiselectable="true">';
			foreach ($array['categorias'] as $key => $categoria) {
				$text = $text.'<div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="heading'.$categoria['detalle']->categoria_documento_id.'">
				      <h4 class="panel-title">
				        <a role="button" data-toggle="collapse" data-parent="#accordion'.$id.'" href="#collapse'.$categoria['detalle']->categoria_documento_id.'"  aria-controls="collapse'.$categoria['detalle']->categoria_documento_id.'">
				          '.$categoria['detalle']->categoria_documento_nombre.'
				        </a>
				      </h4>
				    </div>
				    <div id="collapse'.$categoria['detalle']->categoria_documento_id.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'.$categoria['detalle']->categoria_documento_id.'">
				      <div class="panel-body">
				      '.vercategoria($categoria,$categoria['detalle']->categoria_documento_id).'
				      </div>
				    </div>
				  </div>';
			}
			$text = $text.'</div>';
		}

		if(count($array['documentos'])){
			foreach ($array['documentos'] as $key => $documento) {
				$text = $text.'<a href="http://'.$_SERVER['HTTP_HOST'].'/files/'.$documento->documento_documento.'" class="enlaces-documentos view-pdf" om-descarga="'.$documento->documento_descarga.'" > <i class="glyphicon glyphicon-fullscreen"></i> '.$documento->documento_nombre.'</a>';
			}
		}
		return $text;
	}

?>