<div class="container">
	<h1 class="titulo-general">Control de Documentos</h1>
	<div class="padding-content">
	<div class="container">
		<?php foreach ($this->contenidos as $key => $contenido): ?>
			<div class="row caja-nosotros">
				<?php if($contenido->content_image){ ?>
				<div class="col-sm-6 <?php if($key%2 ==0){?>col-md-push-6 <?php } ?>">
					<div class="imagen-nosotros">
						<?php if($contenido->content_image){ ?>
						<img src="/images/<?= $contenido->content_image;?>">
						<?php } ?>
					</div>
				</div>

				<div class="col-sm-6 <?php if($key%2 ==0){?>col-md-pull-6 <?php } ?>">
				<?php } else { ?>
					<div class="col-sm-12">
				<?php } ?>
					<h3><?= $contenido->content_title; ?></h3>
					<div class="descripcion"><?= $contenido->content_description; ?></div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>
</div>