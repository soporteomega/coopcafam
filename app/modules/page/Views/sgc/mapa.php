<div id="div_popup"></div>
<div class="container">
	<h1 class="titulo-general">MAPA DE PROCESOS COOPCAFAM</h1>
	<div class="subtitulo2"><?php echo $this->introduccion[0]->content_introduction; ?></div>
	<div class="text-right"><a class="btn btn-primary" href="/page/sgc"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a></div>
	<div class="caja-mapa">
		<div class="titulo-1">NECESIDADES Y EXPECTATIVAS DEL ASOCIADO</div>
		<div class="titulo-2">SATISFACCIÓN DEL ASOCIADO</div>
		<div class="caja-azul">
			<h3 class="titulo-mapa">PROCESOS ESTRATÉGICOS</h3>
			<?php foreach ($this->gerenciales as $key => $content): ?>
				<a class="boton-mapa" data-toggle="modal" data-target="#modal<?php echo $content['detalle']->categoria_documento_id; ?>"><span><?php echo $content['detalle']->categoria_documento_nombre; ?></span></a>
				<div class="modal fade" id="modal<?php echo $content['detalle']->categoria_documento_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel"><?php echo $content['detalle']->categoria_documento_nombre; ?></h4>
				      </div>
				      	<div class="modal-body">
				        	<?php echo vercategoria($content,$content['detalle']->categoria_documento_id); ?>
				      	</div>
				    </div>
				  </div>
				</div>
			<?php endforeach ?>
		</div>
		<div class="caja-verde">
			<h3 class="titulo-mapa">PROCESOS MISIONALES</h3>
			<?php foreach ($this->clave as $key => $content): ?>
				<a class="boton-mapa" data-toggle="modal" data-target="#modal<?php echo $content['detalle']->categoria_documento_id; ?>"><span><?php echo $content['detalle']->categoria_documento_nombre; ?></span></a>
				<div class="modal fade" id="modal<?php echo $content['detalle']->categoria_documento_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel"><?php echo $content['detalle']->categoria_documento_nombre; ?></h4>
				      </div>
				      	<div class="modal-body">
				        	<?php echo vercategoria($content,$content['detalle']->categoria_documento_id); ?>
				      	</div>
				    </div>
				  </div>
				</div>
			<?php endforeach ?>
		</div>
		<div class="caja-azul2">
			<h3 class="titulo-mapa">PROCESOS DE APOYO</h3>
			<?php foreach ($this->apoyo as $key => $content): ?>
				<a class="boton-mapa" data-toggle="modal" data-target="#modal<?php echo $content['detalle']->categoria_documento_id; ?>"><span><?php echo $content['detalle']->categoria_documento_nombre; ?></span></a>
				<div class="modal fade" id="modal<?php echo $content['detalle']->categoria_documento_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel"><?php echo $content['detalle']->categoria_documento_nombre; ?></h4>
				      </div>
				      	<div class="modal-body">
				        	<?php echo vercategoria($content,$content['detalle']->categoria_documento_id); ?>
				      	</div>
				    </div>
				  </div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
	<div class="titulo-3">AMBIENTE DE CONTROL</div>
	<div class="descripcion"><?php echo $this->introduccion[0]->content_description; ?></div>
	<br>
</div>

<?php

	function vercategoria($array,$id){
		$text = '';
		if(count($array['categorias'])){
			$text = $text.'<div class="panel-group" id="accordion'.$id.'" role="tablist" aria-multiselectable="true">';
			foreach ($array['categorias'] as $key => $categoria) {
				$text = $text.'<div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="heading'.$categoria['detalle']->categoria_documento_id.'">
				      <h4 class="panel-title">
				        <a role="button" data-toggle="collapse" data-parent="#accordion'.$id.'" href="#collapse'.$categoria['detalle']->categoria_documento_id.'"  aria-controls="collapse'.$categoria['detalle']->categoria_documento_id.'">
				          '.$categoria['detalle']->categoria_documento_nombre.'
				        </a>
				      </h4>
				    </div>
				    <div id="collapse'.$categoria['detalle']->categoria_documento_id.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'.$categoria['detalle']->categoria_documento_id.'">
				      <div class="panel-body">
				      '.vercategoria($categoria,$categoria['detalle']->categoria_documento_id).'
				      </div>
				    </div>
				  </div>';
			}
			$text = $text.'</div>';
		}

		if(count($array['documentos'])){
			foreach ($array['documentos'] as $key => $documento) {
				$text = $text.'<a href="http://'.$_SERVER['HTTP_HOST'].'/files/'.$documento->documento_documento.'" class="enlaces-documentos view-pdf" om-descarga="'.$documento->documento_descarga.'"> <i class="glyphicon glyphicon-fullscreen"></i> '.$documento->documento_nombre.'</a>';
			}
		}
		return $text;
	}

?>