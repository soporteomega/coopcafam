<div id="div_popup"></div>
<div class="text-center paddin-content">
	<div class="container">
		<h1 class="titulo-general"><?php echo $this->categoria->categoria_documento_nombre; ?></h1>
		<div class="text-right"><a class="btn btn-primary" href="/page/sgc"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a></div>
		<br>
		<?php echo vercategoria($this->content,$this->content['detalle']->categoria_documento_id); ?>
	</div>
</div>
<?php

	function vercategoria($array,$id){
		$text = '';
		if(count($array['categorias'])){
			$text = $text.'<div class="panel-group" id="accordion'.$id.'" role="tablist" aria-multiselectable="true">';
			foreach ($array['categorias'] as $key => $categoria) {
				$text = $text.'<div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="heading'.$categoria['detalle']->categoria_documento_id.'">
				      <h4 class="panel-title">
				        <a role="button" data-toggle="collapse" data-parent="#accordion'.$id.'" href="#collapse'.$categoria['detalle']->categoria_documento_id.'"  aria-controls="collapse'.$categoria['detalle']->categoria_documento_id.'">
				          '.$categoria['detalle']->categoria_documento_nombre.'
				        </a>
				      </h4>
				    </div>
				    <div id="collapse'.$categoria['detalle']->categoria_documento_id.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'.$categoria['detalle']->categoria_documento_id.'">
				      <div class="panel-body">
				      '.vercategoria($categoria,$categoria['detalle']->categoria_documento_id).'
				      </div>
				    </div>
				  </div>';
			}
			$text = $text.'</div>';
		}

		if(count($array['documentos'])){
			$text = $text.'<table class="tabla">
						  <tr class="tabla_th">
						        <td colspan="3"><div align="center">C&Oacute;DIGO</div></td>
						        <td rowspan="2"><div align="center">Nombre del Documento</div></td>
						        <td rowspan="2"><div align="center">Origen del Documento</div></td>
						        <td rowspan="2"><div align="center">Ubicaci&oacute;n</div></td>
						        <td rowspan="2"><div align="center">Cargo Responsable del documento</div></td>
						        <td rowspan="2"><div align="center">Versi&oacute;n Actual o Fecha de Expedici&oacute;n</div></td>
						  </tr>
						  <tr class="tabla_th">
						    <td><div align="center">Tipo de Documento</div></td>
						    <td><div align="center">Proceso</div></td>
						    <td><div align="center">Consecutivo</div></td>
						  </tr>';
			foreach ($array['documentos'] as $key => $documento) {

				$text = $text.'<tr>
				    <td>'.$documento->documento_tipo.'</td>
				    <td>'.$documento->documento_proceso.'</td>
				    <td>'.$documento->documento_consecutivo.'</td>
				    <td>'.utf8_encode($documento->documento_nombre).'</td>
				    <td>'.$documento->documento_origen.'</td>
				    <td>';
				   
				   if($documento->documento_documento) {
				    	$text = $text.'<div style="padding:5px;"><a href="http://'.$_SERVER['HTTP_HOST'].'/files/'.urlencode($documento->documento_documento).'" class="view-pdf" om-descarga="'.$documento->documento_descarga.'" style="font-size:10px;" > http://'.$_SERVER['HTTP_HOST'].'/files/'.urlencode($documento->documento_documento).'</a></div></td>';
					} else if($documento->documento_ubicacion){
				    	if(strpos($documento->documento_ubicacion,"http") === false ){
				    	 $text = $text.'<div style="padding:5px;">'.$documento->documento_ubicacion.'</a></div></td>';
				    	} else {
				    		$text = $text.'<div style="padding:5px;"><a href="'.$documento->documento_ubicacion.'" class="view-pdf" om-descarga="'.$documento->documento_ubicacion.'" style="font-size:10px;" > '.$documento->documento_ubicacion.'</a></div></td>';
				    	}
				    } 


				    $text = $text.'<td>'.utf8_encode($documento->documento_cargo).'</td>
				    <td>'.$documento->documento_version.'</td>
				  </tr>';
			}

			$text = $text.'</table>';
			$text = $text."<div>F-SQ-02  Versión 2  Septiembre de 2016</div>";
		}
		return $text;
	}

?>