<div class="container-fluid fondo_cursos">

	<div id="personaje1" class="col-md-12 text-center">
		<div class="col-md-5 text-right">
			<img src="/corte/imagen1.png">
		</div>
		<div class="col-md-7 text-left">
			<div class="enlinea globo"><img src="/corte/globo.png" width="120" /></div>
			<div class="globo_verde enlinea text-center"><?php echo $this->curso->curso_titulo; ?> 					<span class="subtitulo_curso"><?php echo $this->curso->curso_descripcion; ?></span></div>
		</div>
	</div>
	<div id="piso" class="col-md-12 text-center"><span class="conoce_modulos">CONOCE LOS MODÚLOS</span></div>

	<div class="container">
		<div class="text-right"><a class="btn btn-primary" href="/page/formacionenlinea"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a></div>
		<div>
			<div class="row fila_modulos">
				<?php foreach ($this->modulos as $modulo): ?>
					<?php $i++; ?>
					<div class="col-md-4">
						<div class="titulo_modulos">MÓDULO <?php echo $i; ?></div>
						<a href="/page/cursos/modulo?modulo=<?php echo $modulo->modulo_id; ?>" class="sin_linea"><div class="caja_azul_modulo"><?php echo $modulo->modulo_titulo; ?> <img src="/corte/flecha.png" class="flecha_verde2"></div></a>
					</div>
				<?php endforeach ?>

			</div>
		</div>
	</div>
</div>