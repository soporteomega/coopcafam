<h1 class="titulo_login">
    INICIO DE SESIÓN
    <span>Ingrese sus datos</span>
</h1>
<div class="form_login">
    <?php if($this->message == 1){  ?>
        <div class="text-success bg-success alert_login"> <i class="glyphicon glyphicon-ok-sign"></i> Su registro ha sido exitoso.</div>
    <?php } ?>
    <form id="login_form" autocomplete="off" data-toggle="validator">
                <div class="form-group text-left" >
                    <label class="control-label sr-only">Usuario</label>
                    <div class="inner-addon left-addon">
                        <i class="glyphicon glyphicon-user"></i>
                        <input type="text" class="form-control" id="user" placeholder="Usuario" required>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group text-left">
                    <label class="control-label sr-only">Contraseña</label>
                    <div class="inner-addon left-addon">
                        <i class="glyphicon glyphicon-lock"></i>
                        <input type="password" class="form-control " id="password" placeholder="Contraseña" required>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
                <input type="hidden" id="csrf" value="<?php echo $_SESSION['csrf']; ?>" />
                <div class="text-left">
                    <button  class="btn btn-block  boton_login" onclick="return login()">Entrar</button>
                </div>
                <div id="error_login"></div>
    </form>
    <div class="text-left">
        <a class="olvido_login" href="/page/login/oldpassword"><i class="glyphicon glyphicon-question-sign"></i> Olvidé mi contraseña</a>
    </div>
</div>