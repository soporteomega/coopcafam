<h1 class="titulo_login">Cambiar Contraseña<span><?php echo $this->usuario; ?></span></h1>
<div class="form_login">
<div class="text-right"><a href="/page/login"><i class="glyphicon glyphicon-chevron-left"></i> Volver al login</a></div>
<?php if ($this->error != '') {?>
    <div class="content_right_column">
        <div class=" bg-danger diverror">
            <i class="glyphicon glyphicon-remove-sign"></i> <?= $this->error;?>
        </div>
    </div>
<?php } else { ?>
    <?php if ($this->message != '') { ?>
    <div class="content_right_column">
        <div class="alert_user">
           <i class="glyphicon glyphicon-ok-sign"></i>  <?php echo $this->message; ?>
        </div>
    </div>
    <?php } else { ?>
        <div class="box_password">
            <form data-toggle="validator" role="form" method="post" action="/page/login/changepassword">
                <input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf']; ?>" />
                <input type="hidden" name="code" value="<?php echo $this->code; ?>" />
                <div class="form-group">
                    <label class="control-label">Contraseña:</label>
                    <input type="password" name="password" id="inputPassword" class="form-control" required value="" />
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <label class="control-label">Repita Contraseña:</label>
                    <input type="password" name="re_password" class="form-control" data-match="#inputPassword" data-match-error="Las dos Contraseñas no son iguales"  value="" required/>
                    <div class="help-block with-errors"></div>
                </div>
                <div>
                    <button class="btn btn-block btn-success" type="submit">Cambiar Contraseña</button>
                </div>
            </form>
        </div>
    <?php } ?>
<?php } ?>
</div>