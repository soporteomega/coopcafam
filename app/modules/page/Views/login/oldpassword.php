<h1 class="titulo_login">
    Olvidé mi contraseña
    <span>Ingrese su correo</span>
</h1>
<div class="form_login">
    <div class="text-right"><a href="/page/login"><i class="glyphicon glyphicon-chevron-left"></i> Volver</a></div>
    <?php if($this->message == 1){  ?>
        <div class="text-success bg-success alert_login"> <i class="glyphicon glyphicon-ok-sign"></i> Se ha enviado un mensaje a su correo para restablecer la contraseña</div>
    <?php } ?>
    <form id="login_form" autocomplete="off" data-toggle="validator" method="post" action="/page/login/olvido">
                <div class="form-group text-left" >
                    <label class="control-label sr-only">Correo</label>
                    <div class="inner-addon left-addon">
                        <i class="glyphicon glyphicon-user"></i>
                        <input type="email" class="form-control" name="email" placeholder="correo" required>
                    </div>
                    <div class="help-block with-errors">
                        <?php if($this->message == 2){  ?>
                            Lo sentimos ocurrio un error y no se pudo enviar su mensaje
                         <?php } ?>
                        <?php if($this->message == 3){  ?>
                            Lo sentimos ese correo no se encuentra registrado en nuestro sistema
                         <?php } ?>
                    </div>
                </div>
                <input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf']; ?>" />
                <div class="text-left">
                    <button  class="btn btn-block  boton_login" type="submit">Enviar</button>
                </div>
                <div id="error_login"></div>
    </form>
</div>