<div>
	<div class="padding-content">
		<div class="container">
			<h1 class="titulo-general">Campañas</h1>
			<div class="subtitulo2"><?php echo $this->introduccion[0]->content_description; ?></div>
			<div class="text-right"><a class="btn btn-primary" href="/page/gestionhumana"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a></div>
		</div>
	</div>
	<?php $columna = 0; ?>
	<?php $tipo = ""; ?>
	<?php $carrousel = 1;?>
	<?php foreach ($this->contenidos as $key => $arraycontenido): ?>
		<?php $contenido = $arraycontenido['detalle']; ?>
		<?php
			$adicional= '';
			if(count($arraycontenido['adicionales'])>0){
				$tipoadicional = '';
				foreach ($arraycontenido['adicionales'] as $keyadicional => $adicion) {
					if($tipoadicional == 'Carrousel' && $adicion->adicional_tipo != 'Carrousel' ){
						$adicional= $adicional.'</div></div>';
						$tipoadicional = '';
					}
					if($adicion->adicional_tipo == 'Acordeon'){
							$adicional= $adicional.'<a role="button" data-toggle="collapse"  href="#collapseO'.$adicion->adicional_id.'" aria-expanded="false" aria-controls="collapseOne" class="btn-acordeon" data-parent="adicional_'.$contenido->content_id.'">'.$adicion->adicional_titulo.'</a>';
							$adicional= $adicional.'<div id="collapseO'.$adicion->adicional_id.'" class="panel-acordeon panel-collapse collapse" role="tabpanel" ><div class="descripcion">'.$adicion->adicional_descripcion.'</div></div>';
					} else if($adicion->adicional_tipo == 'Carrousel'){
						if($tipoadicional != 'Carrousel'){
							$adicional= $adicional.'<div id="carousel-'.$carrousel.'" class="carousel-content carousel slide" data-ride="carousel">';
							$adicional= $adicional.' <a class="left carousel-control" href="#carousel-'.$carrousel.'" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="right carousel-control" href="#carousel-'.$carrousel.'" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span></a>';
							$adicional= $adicional.'<div class="carousel-inner" role="listbox"> ';
							$carrousel++;
							$active = 'active';
						}
						$tipoadicional = 'Carrousel';
						if($adicion->adicional_imagen){
							$adicional = $adicional.'<div class="item '.$active.'"><img src="/images/'.$adicion->adicional_imagen.'" alt=""></div>';
							$active='';
						}
					} else {
						$adicional= $adicional.'<div class="descripcion">'.$adicion->adicional_descripcion.'</div>';
					}
				}
			}
		?>
		<?php if ($columna == 1 && $tipo != $contenido->content_disenio){ ?>
			</div></div>
			<?php $columna = 0; ?>
			<?php $tipo = ""; ?>
		<?php }?>
		<?php if($contenido->content_disenio == "Dos Columnas"){ ?>
			<?php $tipo = $contenido->content_disenio; ?>
			<?php if($columna == 0){ ?>
				<div class="padding-content">
					<div class="container">
			<?php } ?>
			<?php $columna ++; ?>
			<div class="col-sm-6" id="<?= $contenido->content_id; ?>">
				<div class="caja-nosotros">
					<h3><?= $contenido->content_title; ?></h3>
					<div class="descripcion"><?= $contenido->content_description; ?></div>
					<div id="adicional_<?php echo $contenido->content_id ?>" aria-multiselectable="true"  class="panel-group"><?php echo $adicional; ?></div>
				</div>
			</div>
			<?php if($columna == 2){ ?>
				</div></div>
				<?php $columna = 0; ?>
				<?php $tipo = ""; ?>
			<?php } ?>
		<?php } else if($contenido->content_disenio == "Dos Columnas Gris"){ ?>
			<?php $tipo = $contenido->content_disenio; ?>
			<?php if($columna == 0){ ?>
				<div class="padding-content fondo-gris" id="<?= $contenido->content_id; ?>">
					<div class="container">
			<?php } ?>
			<?php $columna++; ?>
			<div class="col-sm-6" id="<?= $contenido->content_id; ?>">
				<div class="caja-nosotros">
					<h3><?= $contenido->content_title; ?></h3>
					<div class="descripcion"><?= $contenido->content_description; ?></div>
					<div id="adicional_<?php echo $contenido->content_id ?>" aria-multiselectable="true"  class="panel-group"><?php echo $adicional; ?></div>
					<div class="divisor_azul"></div>
				</div>
			</div>
			<?php if($columna == 2){ ?>
				</div></div>
				<?php $columna = 0; ?>
				<?php $tipo = ""; ?>
			<?php } ?>
		<?php } else if($contenido->content_disenio == "Banner"){ 
				$tipo = "Banner";
			?>
			<div class="banner-full" id="<?= $contenido->content_id; ?>" <?php if($contenido->content_banner){ ?>style="background-image:url(/images/<?= $contenido->content_banner; ?>);"<?php } ?>>
				<div class="container">
					<h3><?= $contenido->content_title; ?></h3>
					<div class="descripcion"><?= $contenido->content_description; ?></div>
					<div id="adicional_<?php echo $contenido->content_id ?>" aria-multiselectable="true"  class="panel-group"><?php echo $adicional; ?></div>
				</div>
			</div>
		<?php } else { ?>
			<div class="padding-content<?php if ($contenido->content_disenio == 'Default Gris'){ echo " fondo-gris"; }?>" id="<?= $contenido->content_id; ?>">
				<div class="container">
					<div class="contenido-general">
						<h3><?= $contenido->content_title; ?></h3>
						<div class="descripcion"><?= $contenido->content_description; ?></div>
						<div id="adicional_<?php echo $contenido->content_id ?>" aria-multiselectable="true"  class="panel-group"><?php echo $adicional; ?></div>
					</div>
				</div>
			</div>
		<?php } ?>
	<?php endforeach ?>
</div>

<script type="text/javascript">
	var targetOffset = parseInt($(window.location.hash).offset().top )- 150;
    $('html,body').animate({scrollTop: targetOffset}, 1000);
</script>