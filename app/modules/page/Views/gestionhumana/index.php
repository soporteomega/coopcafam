<div class="container">
	<h1 class="titulo-general">Gestión Humana</h1>
	<div class="subtitulo2"><?php echo $this->introduccion[0]->content_description; ?></div>
</div>
<div align="center">
	<div class="container">
		<div class="caja_boton" style="background-image: url(/skins/page/images/beneficios.jpg)">
			<div class="caption-caja">
				<div class="content-caption">
					<a href="/page/gestionhumana/beneficios" class="titulo_caja">Beneficios</a>
					<div class="enlaces-caja">
						<?php foreach ($this->contenidos as $key => $contenido): ?>
							<a href="/page/gestionhumana/beneficios#<?= $contenido->content_id; ?>" class="subtitulo_caja"><?= $contenido->content_title; ?></a>
						<?php endforeach ?>
					</div>
					<img src="/skins/page/images/mas-blanco.png" class="boton-mas">
				</div>
			</div>
		</div>
		<?php if(1==2){ ?>
		<div class="caja_boton" style="background-image: url(/skins/page/images/campana.jpg)">
			<div class="caption-caja">
				<div class="content-caption">
					<a href="/page/campanias" class="titulo_caja">Campañas</a>
					<div class="enlaces-caja">
						<?php foreach ($this->contenidos2 as $key => $contenido): ?>
							<a href="/page/campanias/detalle?id=<?= $contenido->content_id; ?>" class="subtitulo_caja"><?= $contenido->content_title; ?></a>
						<?php endforeach ?>
					</div>
					<img src="/skins/page/images/mas-blanco.png" class="boton-mas">
				</div>
			</div>
		</div>
		<div class="caja_boton" style="background-image: url(/skins/page/images/comunicados.jpg)" >
			<div class="caption-caja">
				<div class="content-caption">
					<a href="/page/gestionhumana/comunicados" class="titulo_caja">Comunicados</a>
					<div class="enlaces-caja">
						<?php foreach ($this->contenidos3 as $key => $contenido): ?>
							<a href="/page/gestionhumana/comunicados#<?= $contenido->content_id; ?>" class="subtitulo_caja"><?= $contenido->content_title; ?></a>
						<?php endforeach ?>
					</div>
					<img src="/skins/page/images/mas-blanco.png" class="boton-mas">
				</div>
			</div>
		</div>
		<?php } ?>
		<a class="caja_boton" href="/page/gestionhumana/galeria" style="background-image: url(/skins/page/images/galeria.jpg)"> 
			<div class="caption-caja">
				<div class="content-caption">
					<span  class="titulo_caja">Galería</span>
					<img src="/skins/page/images/mas-blanco.png" class="boton-mas">
				</div>
			</div>
		</a>
		<a class="caja_boton" href="/page/felicitaciones" style="background-image: url(/skins/page/images/felicitaciones.jpg)">
			<div class="caption-caja">
				<div class="content-caption">
					<span  class="titulo_caja">Felicitamos A </span>
					<img src="/skins/page/images/mas-blanco.png" class="boton-mas">
				</div>
			</div>
		</a>
	</div>
</div>