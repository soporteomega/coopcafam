<div class="container">
	<h1 class="titulo-general">Galería</h1>
	<div class="subtitulo2"><?php echo $this->introduccion[0]->content_description; ?></div>
	<div class="text-right"><a class="btn btn-primary" href="/page/gestionhumana"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a></div>
	<div class="padding-content">
	<div class="container">
		<div class="row">
		<?php foreach ($this->albumes as $array) { ?>
			<?php $album = $array['detalle']; ?>
			<div class="col-sm-4"  id ="album_<?php echo $album->album_id; ?>">
				<div class="caja_album">
					<div class="contenedor_album" style="background-image: url(/images/<?php echo str_replace(" ","%20",$album->album_imagen); ?>);">
						<div class="caption_album">
							<div><?php echo $album->album_titulo; ?></div>
							<div class="descripcion"><?php echo $album->album_descripcion; ?></div>
						</div>
					</div>
				</div>
				<?php foreach ($array['fotos'] as $key => $foto) { ?>
					<a href="/images/<?php echo $foto->foto_imagen; ?>" data-gallery title="<?php echo $foto->foto_titulo; ?>" ></a>
				<?php } ?>
			</div>
			<script type="text/javascript">
				document.getElementById('album_<?php echo $album->album_id; ?>').onclick = function (event) {
				    event = event || window.event;
				    var target = event.target || event.srcElement;
				    var link = target.src ? target.parentNode : target;
				    var options = {index: link, event: event};
				    var links = this.getElementsByTagName('a');
				    blueimp.Gallery(links, options);
				};
			</script>
		<?php } ?>
	</div>
	</div>
</div>
</div>