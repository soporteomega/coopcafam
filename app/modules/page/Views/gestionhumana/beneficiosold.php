<div class="container">
	<div class="regresar"><a href="index">Regresar</a></div>
	<h1 class="titulo-general">Beneficios</h1>
	<div class="subtitulo2">Por ser parte de nuestra Cooperativa, conoce aquí todos los beneficios que te otorgamos, recuerda que aplican si tienes contrato a término indefinido</div>
	<div class="padding-content">
	<div class="container">
		<?php foreach ($this->contenidos as $key => $contenido): ?>
			<?php if($_GET['id']==$contenido->content_id){ ?>
			<div class="row caja-nosotros">
				<?php if($contenido->content_image){ ?>
				<div class="col-sm-6 <?php if($key%2 ==0){?>col-md-push-6 <?php } ?>">
					<div class="imagen-nosotros">
						<?php if($contenido->content_image){ ?>
						<img src="/images/<?= $contenido->content_image;?>">
						<?php } ?>
					</div>
				</div>

				<div class="col-sm-6 <?php if($key%2 ==0){?>col-md-pull-6 <?php } ?>">
				<?php } else { ?>
					<div class="col-sm-12">
				<?php } ?>
					<h3><?= $contenido->content_title; ?></h3>
					<div class="descripcion"><?= $contenido->content_description; ?></div>
				</div>
			</div>
			<?php } ?>
		<?php endforeach ?>

		<div class="alto"></div>
		<h3>Otros Beneficios</h3>
		<?php foreach ($this->contenidos as $key => $contenido): ?>
			<?php if($_GET['id']!=$contenido->content_id){ ?>
					<div class="col-sm-4">
					<a class="leer" href="?id=<?= $contenido->content_id; ?>"><?= $contenido->content_title; ?></a>
					</div>
			<?php } ?>
		<?php endforeach ?>

	</div>
</div>
</div>