<div id="slider-home">
	<div class="botonera-lateral">
		<a href="/page/buscador"><img src="/skins/page/images/buscador.png"><span>Buscador</span></a><br>
		<a href="/page/cifras"><img src="/skins/page/images/directorio.png"><span>Coopcafam<br>en Cifras</span></a><br>
		<a href="/page/directorio"><img src="/skins/page/images/telefono.png"><span>Directorio<br> Coopcafam</span></a><br>
		<a href="/page/solicitudesenlinea"><img src="/skins/page/images/opinion.png"><span>Escríbenos</span></a>
	</div>
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<?php foreach ($this->banners as $key => $banner): ?>
				<div class="item <?php if($key ==0){ ?>active<?php } ?>">
				<div class="item-carousel">
					<?php if($banner->content_link){ ?>
						<a target="blank" href="<?php echo $banner->content_link; ?>"><img src="/images/<?= $banner->content_banner; ?>" alt="<?= $banner->content_title; ?>"></a>
					<?php } else {?>
						<img src="/images/<?= $banner->content_banner; ?>" alt="<?= $banner->content_title; ?>">
					<?php } ?>
				</div>
				</div>
			<?php endforeach ?>
		</div>
		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
<div class="padding-content">
	<div class="container">
		<div class="decripcion-frase-home">
			<?php echo $this->introduccion->content_description; ?>
		</div>
	</div>
</div>
<div class="padding-content">
	<div class="container">
		<div class="cajas-home">
			<a href="/page/gestionhumana/beneficios" style="background-image: url(/skins/page/images/beneficios.jpg);">
				<div class="caption">
					Tus beneficios
					<div class="mas"><img src="/skins/page/images/mas.png"></div>
				</div>
			</a>
			<a href="/page/gestionhumana/galeria" style="background-image: url(/skins/page/images/galeria.jpg);">
				<div class="caption" >
					Galería
					<div class="mas"><img src="/skins/page/images/mas.png"></div>
				</div>
			</a>
			<a href="/page/sgc/mapa" style="background-image: url(/skins/page/images/procesos1.jpg);">
				<div class="caption" >
					Mapa de procesos
					<div class="mas"><img src="/skins/page/images/mas.png"></div>
				</div>
			</a>
			<a style="background-image: url(/skins/page/images/noticias1.jpg);" href="/page/noticias">
				<div class="caption">
					Noticias para <br>nuestros colaboradores
					<div class="mas"><img src="/skins/page/images/mas.png"></div>
				</div>
			</a>
			<a style="background-image: url(/skins/page/images/informacion1.jpg);" href="/page/asociados">
				<div class="caption">
					Información para nuestros asociados
					<div class="mas"><img src="/skins/page/images/mas.png"></div>
				</div>
			</a>
		</div>
	</div>
</div>
<div class="padding-content">
	<div class="container">
		<div class="col-md-7">
			<div id='carousel_noticias'>
				<div class="titulo-caroucel">
					<h4>Lo más reciente</h4>
					<div class="flechas-carrousel">
						<div id='left_scroll'><i class="glyphicon glyphicon-chevron-left"></i></div>
						<div id='right_scroll'><i class="glyphicon glyphicon-chevron-right"></i></div>
					</div>
					<div style="clear: both;"></div>
				</div>
				<div class="carousel_inner">
					<ul>

						<?php foreach ($this->noticias as $key => $noticia): ?>
							<li>
								<div class="caja-noticia">
									<div class="imagen-noticia" style="background-image: url(/images/<?php echo str_replace(" ","%20",$noticia->content_image); ?>);"></div>
									<h3><?php echo $noticia->content_title; ?></h3>
									<div class="descripcion"><?php echo $noticia->content_introduction; ?></div>
									<a class="leer" href="/page/noticias/detalle?id=<?php echo $noticia->content_id; ?>" >Leer Más</a>
								</div>
							</li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>
			<a class="direccionamiento" data-toggle="modal" data-target="#myModal">
				<h2><?php echo $this->direccionamiento->content_title; ?></h2>
				<div><?php echo $this->direccionamiento->content_introduction; ?></div>
			</a>
		</div>
		<div class="col-md-5">
			<h4 class="titulo-linea">Calendario</h4>
			<div class="caja-calendario">
				<div class="row">
					<div class="col-sm-6">
						<a class="enlace-calendario" href="/page/actividadesempleados">
							<h4>Actividades Empleados</h4>
							<img src="/skins/page/images/interno.png">
						</a>
					</div>
					<div class="col-sm-6">
						<a class="enlace-calendario" href="/page/actividadesasociados">
							<h4>Actividades Asociados</h4>
							<img src="/skins/page/images/externo.png">
						</a>
					</div>
				</div>
			</div>
			<h4 class="titulo-felicitamos" >Felicitamos a: <img src="/skins/page/images/felicitamos.png"></h4>
			<?php $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio',
               'Agosto','Septiembre','Octubre','Noviembre','Diciembre'); ?>
			<div class="caja-calendario">
				<div class="fecha-calendario" onclick="window.location='/page/felicitaciones';"><?php echo $meses[(int)date("m")]." ".date("Y"); ?></div>
				<?php foreach ($this->events as $key => $event): ?>
					<a href="/page/felicitaciones/detail?id=<?php echo $event->calendar_id; ?>" class="caja-felicitacion">
						<div class="persona-felicitacion">
							<img src="/skins/page/images/persona.png">
							<span><?php echo $event->calendar_name?></span>
							<img src="/images/<?php echo $event->type_calendar_icon; ?>">
						</div>
						<div class="tarjeta-felicitacion">
							<span>Enviar tarjeta</span>
							<img src="/skins/page/images/tarjeta.png">
						</div>
						<div style="clear: both;"></div>
					</a>
				<?php endforeach ?>
				<a href="/page/felicitaciones" class="caja-felicitacion">
					<div class="tarjeta-felicitacion">
						<span>Ver Todos</span>
					</div>
					<div style="clear: both;"></div>
				</a>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="myModal" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $this->direccionamiento->content_title; ?></h4>
      </div>
      <div class="modal-body">
      	<?php if($this->direccionamiento->content_image){ ?>
        	<img src="/images/<?php echo $this->direccionamiento->content_image; ?>" style="max-width: 100%;">
        <?php } ?>
		<div class="descripcion"><?php echo $this->direccionamiento->content_description; ?></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$("#carousel_noticias").carousel({
	 		quantity : 3,
	 		sizes : {
	 			'1200' : 2,
	 			'500' : 1
	 		}
	 	});
</script>