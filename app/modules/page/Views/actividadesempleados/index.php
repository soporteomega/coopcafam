
<div class="container">
<h1 class="titulo-general" >Actividades Empleados</h1>
<div class="row">
	<div class="div_calendar col-sm-9">
	<?php
	# definimos los valores iniciales para nuestro calendario
	$month=$this->month;
	$year=$this->year;
	if($this->day != ''){
		$diaActual = $this->day;
	}
	# Obtenemos el dia de la semana del primer dia
	# Devuelve 0 para domingo, 6 para sabado
	$diaSemana=date("w",mktime(0,0,0,$month,1,$year))+7;
	# Obtenemos el ultimo dia del mes
	$ultimoDiaMes=date("d",(mktime(0,0,0,$month+1,1,$year)-1));
	$meses=array(1=>"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
	"Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	?>
	<table id="calendar" width="100%">
		<caption>
			<a class="glyphicon glyphicon-chevron-left calendar-left" href="/page/actividadesempleados?year=<?= $this->lastyear ?>&month=<?= $this->lastmonth ?>"></a>
			<a data-toggle="modal" data-target="#exampleModal" style="color:#FFFFFF;"><?php echo $meses[$month]." ".$year?></a>
			<a class="glyphicon glyphicon-chevron-right calendar-right" href="/page/actividadesempleados?year=<?= $this->nextyear ?>&month=<?= $this->nextmonth ?>"></a>
		</caption>
		<tr>
			<th>Lun</th><th>Mar</th><th>Mie</th><th>Jue</th>
			<th>Vie</th><th>Sab</th><th>Dom</th>
		</tr>
		<tr bgcolor="silver">
			<?php
			$last_cell=$diaSemana+$ultimoDiaMes;
			// hacemos un bucle hasta 42, que es el máximo de valores que puede
			// haber... 6 columnas de 7 dias
			for($i=1;$i<=42;$i++)
			{
				if($i==$diaSemana)
				{
					// determinamos en que dia empieza
					$day=1;
				}
				if($i<$diaSemana || $i>=$last_cell)
				{
					// celca vacia
					echo "<td class='blanco'>&nbsp;</td>";
				}else{
					// mostramos el dia
					$class = "";
					if($day==$diaActual){
						$class = "hoy";
					}
						echo "<td class='$class'><div>$day</div>";
							if(isset($this->events[$day])){
								foreach ($this->events[$day] as $event) {
									if ($event['tipo'] == 'Felicitacion') {
										echo '<a href="/page/felicitaciones/detail?id='.$event['id'].'" class="event_calendar" style="border-color:'.$this->types[$event['type']]['color'].'">'.$event['name'].'</a>';
									} else {
									echo '<a href="/page/actividadesempleados/detail?id='.$event['id'].'" class="event_calendar" style="border-color:'.$this->types[$event['type']]['color'].'">'.$event['name'].'</a>';
									}
								}
							}
						echo "</td>";
					$day++;
				}
				// cuando llega al final de la semana, iniciamos una columna nueva
				if($i%7==0)
				{
					echo "</tr><tr>\n";
				}
			}
		?>
		</tr>
	</table>
	</div>

	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="exampleModalLabel">Selecionar fecha</h4>
	      </div>
	      <div class="modal-body">
	      <div class="row">
	        <form method="get" action="/page/actividadesempleados">
	          <div class="form-group col-sm-4">
	            <label for="recipient-name" class="control-label">Mes</label>
	             <select class="form-control" name="month" >
	            	<?php foreach ($meses as $key => $mes): ?>
	            		<option value="<?php echo $key;  ?>" <?php if($month == $key){ ?>selected<?php } ?>><?php echo $mes;  ?></option>
	            	<?php endforeach ?>
	            </select>
	          </div>
	          <div class="form-group col-sm-4">
	            <label for="message-text" class="control-label">Año</label>
	            <input type="number" class="form-control" name="year" value=<?php echo date("Y"); ?>>
	          </div>
	          <div class="form-group col-sm-4">
	           		<label for="message-text" class="control-label">&nbsp;</label>
	           		<button type="submit" class="btn btn-block btn-success">Ir</button>
	          </div>
	        </form>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="col-sm-3">
		<div class="caja-dia-evento">
			<h3 >Eventos del dia</h3>
			<?php foreach ($this->eventosdia as $key => $hora): ?>
				<div class="dia-mes">
					<div class="row">
						<div class="col-sm-4">
							<?php echo $key;  ?>
						</div>
						<div class="col-sm-8">
							<?php foreach ($hora as  $evento): ?>
								<a href="/page/actividadesempleados/detail?id=<?php echo $evento['id']; ?>" class="event_calendar" style="border-color:<?php echo $this->types[$evento['type']]['color']; ?>"><?php echo $evento['accion']; ?> <?php echo $evento['name']; ?></a>
							<?php endforeach ?>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
		<div class="tipos-evento">
			<?php foreach ($this->types as $key => $tipo): ?>
				<div class="row">
					<div class="col-sm-2"><span style="background:<?php echo $tipo['color']; ?> "></span></div>
					<div class="col-sm-10"><?php echo $tipo['name']; ?></div>
				</div>
			<?php endforeach ?>
		</div>
	</div>

</div>
</div>