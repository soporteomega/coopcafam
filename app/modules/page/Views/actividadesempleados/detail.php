
<div class="container">
<?php if(file_exists($this->content->calendar_banner)){?>
	<img src="/<?= $this->content->calendar_banner?>" style="width: 100%;">
<?php } ?>
	<h1 class="titulo-general" style="border-color:<?= $this->type->type_calendar_color; ?>; text-transform: uppercase;"><?= $this->content->calendar_name?>
        <?php if($this->type->type_calendar_icon){?>
            <img src="/images/<?php echo $this->type->type_calendar_icon  ?>">
        <?php } ?>
    </h1>
    <div class="text-right"><a class="btn btn-primary" href="/page/actividadesempleados"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a></div>
	<?php if(1==2){ ?><div class="text-right">
		<?php
			$ruta = $this->route;
			$titulo = $this->content->calendar_name;
			$fecha1 = date("Ymd",strtotime($this->content->calendar_startdate))."T".str_replace(":","",$this->content->calendar_starttime);
			$fecha2 = date("Ymd",strtotime($this->content->calendar_enddate))."T".str_replace(":","",$this->content->calendar_endtime);
			$ubicacion = " ";
			$descripcion = $this->content->calendar_description;

            $ics = "
            BEGIN:VCALENDAR
            VERSION:2.0
            PRODID:-//hacksw/handcal//NONSGML v1.0//EN
            BEGIN:VEVENT
            DTSTART:$fecha1
            DTEND:$fecha2
            SUMMARY:$titulo
            LOCATION:$ubicacion
            DESCRIPTION:$descripcion
            END:VEVENT
            END:VCALENDAR
            ";

            $archivo = "tmp/event_".$this->content->calendar_id.".ics";
            $file = fopen($archivo, "w");
            fputs($file, $ics ."".PHP_EOL);
            fclose($file);

        ?>
        <strong>Agregar a calendario :</strong>
        <a class="btn btn-sm btn-primary" href="/<?php echo $archivo; ?>" target="_blank">Outlook</a>
        <a  class="btn btn-sm btn-danger" href="http://www.google.com/calendar/event?action=TEMPLATE&amp;text=<?php echo $titulo; ?>&amp;dates=<?php echo $fecha1 ?>/<?php echo $fecha2 ?>&amp;details=<?php echo htmlentities($descripcion); ?><?php echo $ruta; ?>/index.php?mod=detalle%26id=<?php echo $_GET['id']; ?>&amp;location=&amp;trp=true&amp;sprop=www.oraclecalendaronline.com&amp;sprop=name:" target="_blank">Google</a>

	</div>
    <?php } ?>

	<div class="description-detail descripcion">
        <div class="row">
            <div class="<?php if($this->content->calendar_user){?>col-sm-8<?php } else { ?> col-sm-12 <?php } ?>">
        		<?php if($this->content->calendar_image){?>
        			<img src="/images/<?= $this->content->calendar_image?>" class="image_content">
        		<?php } ?>
                <?php if($this->type->type_calendar_type == 'Calendario'){?>
        		<div><strong>Fecha y hora de Inicio:</strong> <?= $this->content->calendar_startdate;?> <?= $this->content->calendar_starttime;?> </div>
        		<div><strong>Fecha y hora de finalización:</strong> <?= $this->content->calendar_enddate;?> <?= $this->content->calendar_endtime;?>  </div>
                <?php } else { ?>
                    <div><strong>Fecha:</strong> <?= $this->content->calendar_startdate;?> </div>
                <?php } ?>
        		<?= $this->content->calendar_description;?>
        		<div style="clear: both;"></div>
            </div>
            <?php if($this->content->calendar_user){?>
                <?php if($this->envio == 1){ ?>
                    <div class="mensaje-envio">Tu mensaje se ha enviado correctamente</div>
                <?php } ?>
                <div class="col-sm-4" style="border-left: 1px #CCCCCC solid;">
                    <form action="/page/actividadesempleados/felicitar" method="post" >
                    <input type="hidden" name="calendar" value="<?= $this->content->calendar_id;?>">
                        <div class="form-group">
                            <div><strong>De:</strong> <?php echo $this->user->user_names." ".$this->user->user_lastnames ?></div>
                        </div>
                        <div class="form-group">
                          <label>Mensaje:</label>
                          <textarea class="form-control" name="mensaje" rows="5" style="resize:none;"></textarea>
                        </div>
                         <div class="form-group">
                            <button class="btn btn-success btn-block" type="submit"><i class="glyphicon glyphicon-heart"></i> Enviar Tarjeta</button>
                        </div>
                    </form>
                </div>
            <?php } ?>
        </div>
	</div>
</div>