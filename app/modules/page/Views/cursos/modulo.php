<div class="container-fluid fondo_cursos">

	<div id="personaje2" class="col-md-12 text-center">
		<div class="col-md-4 text-right">
			<img src="/corte/indice.png">
		</div>
		<div class="col-md-6 text-left">
			<div class="titulo_azul"><?php echo $this->curso->curso_titulo; ?></div>
		</div>
		<div class="col-md-2">
			<div class="text-right margen_regresar"><a class="btn btn-primary" href="/page/cursos/curso?curso=<?php echo $this->curso->cursos_id; ?>"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a></div>
		</div>
	</div>
	<div id="piso2" class="col-md-12 text-center"><?php echo $this->modulo->modulo_titulo; ?></div>

	<div class="container-fluid">
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="col-md-12">
						<div class="globo_azul col-md-9"><?php echo $this->modulo->modulo_descripcion; ?></div>
					</div>
					<div class="text-center col-md-12"><img src="/corte/globo-azul2.png"></div>
					<div id="personaje3" class="col-md-12"><img src="/corte/imagen2.png" width="100%"></div>
				</div>
			</div>
		</div>
		<div id="piso3"></div>
		<br>
		<div class="container">
			<div class="titulo_secciones text-center">SECCIONES</div>
			<br>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<?php foreach ($this->secciones as $seccion): ?>
					<div class="col-sm-12 <?php echo $this->columnas; ?>">
						<div class="panel panel-default">
							<div class="panel-heading pointer" role="tab" id="tab<?php echo $seccion->seccion_modulo_id; ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $seccion->seccion_modulo_id; ?>" aria-expanded="false" aria-controls="<?php echo $seccion->seccion_modulo_id; ?>">
							     <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $seccion->seccion_modulo_id; ?>" aria-expanded="false" aria-controls="<?php echo $seccion->seccion_modulo_id; ?>" class="button-acordeon" ><?php echo $seccion->seccion_modulo_nombre; ?></a>
							</div>
							<div id="<?php echo $seccion->seccion_modulo_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="tab<?php echo $seccion->seccion_modulo_id; ?>">
							  	<div class="panel-body">
							    	<?php echo $seccion->seccion_modulo_descripcion; ?>
							  	</div>
							</div>
						</div>
						<div class="col-md-12"><br></div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
			<br>
			<div align="center" class="fondo_verde margen_abajo"><a class="btn btn-success " href="/page/cursos/evaluacion?modulo=<?php echo $this->modulo->modulo_id; ?>" > Realizar Evaluación  </a></div>
	</div>

</div>