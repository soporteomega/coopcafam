<div class="container-fluid fondo_cursos">

	<div id="personaje2" class="col-md-12 text-center">
		<div class="col-md-3 text-right">
			<img src="/corte/indice.png">
		</div>
		<div class="col-md-7 text-left">
			<div class="titulo_azul"><?php echo $this->curso->curso_titulo; ?></div>
		</div>
		<div class="col-md-2">
			<div class="text-right margen_regresar"><a class="btn btn-primary" href="/page/cursos/modulo?modulo=<?php echo $this->modulo->modulo_id; ?>"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a></div>
		</div>
	</div>
	<div id="piso2" class="col-md-12 text-center margen_abajo">Evaluación <?php echo $this->modulo->modulo_titulo; ?></div>

	<div class="container">

		<?php if($this->respondido != true){  ?>

		<?php
			//$relojes = array("Reloj-Arena.gif","Reloj-pendulo.gif");
			//$aleatorio = mt_rand(0,1);
			//$reloj = $relojes[$aleatorio];
			$reloj = "/corte/Reloj-Arena.gif";
			if($this->gif_reloj!=""){
				$reloj = "/images/".$this->gif_reloj;
			}
		?>
		<form data-toggle="validator" method="post" action="/page/cursos/responder" id="formevaluacion" >
		<div id="alarma" class="text-center"><b>Se termina el tiempo</b><br><img src="<?php echo $reloj; ?>" height="110"></div>
		<div class="relojfijo">Tiempo Restante<div id="relojregresivo"></div></div>
			<div class="description" >
				<div class="container-fluid">
					<?php
						$fila = 1;
						$contador = 2;
					?>
					<?php foreach ($this->preguntas as $pregunta): ?>
						<?php if ($contador == 2){?>
							<?php if($fila!=1){ ?></div><?php } else { $fila = 2; } ?>
							<div class="row">
							<?php $contador = 0;  ?>
						<?php } ?>
						<?php $contador++; ?>
						<?php
							$respuestas = array();
							$respuestas[1] =  $pregunta->pregunta_respuesta1;
							$respuestas[2] =  $pregunta->pregunta_respuesta2;
							$respuestas[3] =  $pregunta->pregunta_respuesta3;
							$respuestas[4] =  $pregunta->pregunta_respuesta4;
							$respuestas[5] =  $pregunta->pregunta_respuesta5;
							$respuestas[6] =  $pregunta->pregunta_respuesta6;
							$respuestas[7] =  $pregunta->pregunta_respuesta7;
							$respuestas[8] =  $pregunta->pregunta_respuesta8;
							$respuestas[9] =  $pregunta->pregunta_respuesta9;
							$respuestas[10] =  $pregunta->pregunta_respuesta10;
							$respuestas[11] =  $pregunta->pregunta_respuesta11;
							$respuestas[12] =  $pregunta->pregunta_respuesta12;
							$respuestas[13] =  $pregunta->pregunta_respuesta13;
							$respuestas[14] =  $pregunta->pregunta_respuesta14;
							if($this->orden_respuestas=="2"){
								shuffle($respuestas);
							}
						?>
						<div class="col-sm-6 ">
							<input type="hidden" name="pregunta[]" value="<?php echo $pregunta->pregunta_id; ?>"></input>
							<div class="caja_pregunta form-group">
								<div class="pregunta"><?php echo $pregunta->pregunta_pregunta; ?></div>
								<div class="pregunta_tipo"><?php echo $this->tipos[$pregunta->pregunta_tipo]; ?></div>
								<?php foreach ($respuestas as $key => $respuesta): ?>
									<?php if($respuesta!=''){ ?>
										<?php if($pregunta->pregunta_tipo=="1" or $pregunta->pregunta_tipo=="2"){ ?>
											<div class="respuesta">
												<input type="radio" name="pregunta_<?php echo $pregunta->pregunta_id; ?>" id="pregunta_<?php echo $pregunta->pregunta_id; ?>_<?php echo $key; ?>" value="<?php echo $respuesta; ?>" required></input>
												<label for="pregunta_<?php echo $pregunta->pregunta_id; ?>_<?php echo $key; ?>"><?php echo $respuesta; ?></label>
											</div>
										<?php } ?>
										<?php if($pregunta->pregunta_tipo=="3"){ ?>
											<div class="respuesta">
												<input type="checkbox" name="pregunta_<?php echo $pregunta->pregunta_id; ?>_<?php echo $key; ?>" id="pregunta_<?php echo $pregunta->pregunta_id; ?>_<?php echo $key; ?>" value="<?php echo $respuesta; ?>" ></input>
												<label for="pregunta_<?php echo $pregunta->pregunta_id; ?>_<?php echo $key; ?>"><?php echo $respuesta; ?></label>
											</div>
										<?php } ?>
									<?php } ?>
								<?php endforeach ?>
								<div></div>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					<?php endforeach ?>
					</div>
					<div class="row form-group fondo_verde margen_abajo margen_arriba oculto">
						<div class="col-sm-12 text-center">
							<button id="enviar" type="submit" class="btn btn-success">Enviar evaluación</button>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="intentos" value="<?php echo $this->intentos; ?>">
		</form>
		<script type="text/javascript">
		var segundos= Number('<?php echo $this->tiempo*60; ?>');
		//var segundos= 61;
		var interval;
		function cuentaatras(){
			if(parseInt(segundos)>=0){
				var minuto = pad(parseInt(segundos / 60),2);
				var segundo = pad (parseInt(segundos % 60),2);
				$("#relojregresivo").html(minuto+":"+segundo);
				segundos= parseInt(segundos)-1;
				if(minuto=="00"){
					$("#alarma").show();
				}
			} else {
				clearInterval(interval);
				$("#formevaluacion").submit();
			}
		}
		$( window ).load(function() {
			interval = setInterval(cuentaatras,1000);
		});
	</script>
		<?php } else { ?>

			<?php
				$bien = 0;
				$total = 0;

			?>
			<?php foreach ($this->preguntas as $pregunta): ?>
				<?php
					if($pregunta->respuesta_correcto == 1){
						$bien++;
					}
					$total++;

					$porse = (100/$total)*$bien;
					//$porse = 100;
					if($porse >= 65){
						$estado = "Aprobado";
						$class = "bg-success";
					} else {
						$estado = "Reprobado";
						$class = "bg-danger";
					}
				?>
			<?php endforeach ?>
			<div class="description" >

				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-12 ">
						<?php if($this->mostrarBotones != 1 and $this->intentos<$this->oportunidades){ ?>
							<div class="text-right margen_abajo oculto"><a href="/page/cursos/evaluacion?modulo=<?php echo $this->modulo->modulo_id;?>&nuevo=1" class="btn btn-info">Hacer de nuevo</a></div>
						<?php } ?>
							<div class="text-right oculto" ><h3 class="estado <?php echo $class; ?>" >Total: <?php echo $bien."/".$total." - Porcentaje ".round($porse,2).'% - '.$estado;  ?></h3></div>
						</div>

						<?php if($estado == "Aprobado" or $_GET['aprobado']==1){ ?>

							<div class="text-right oculto" style="padding-right: 30px;"><a href="/page/cursos/certificado?modulo=<?php echo $this->modulo->modulo_id;?>&user=<?php echo $this->iduser;?>&descarga=1" class="btn btn-primary" target="_blank" ><i class="glyphicon glyphicon-download-alt"></i> Descarga Certificado</a></div>

							<?php
								$gif_aprobado = "/corte/Premio.gif";
								if($this->gif_aprobado!=""){
									$gif_aprobado = "/images/".$this->gif_aprobado;
								}
							?>

							<div class="col-md-12 margen_abajo">
								<?php if ($this->mostrar_certificado=="1"): ?>
									<div class="col-xs-12 col-md-7 pos_imagen4">
										<div class="col-md-3 imagen4"><img src="/corte/imagen4.png"></div>
										<div class="col-md-5">
											<div class="col-md-3 flecha_azul"><img src="/corte/globo-azul1.png"></div>
											<div class="globo_azul text-center col-md-9">
												<div>Total: <?php echo $bien."/".$total;?></div>
												<div>Porcentaje <?php echo round($porse,2).'%'; ?></div>
												<div class="resultado"><?php echo $estado; ?></div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="text-center" style="margin-bottom:-30px;"><img src="<?php echo $gif_aprobado; ?>"></div>
											<div class="globo_frase col-md-12" style="background: <?php echo $this->frases_aprobado[0]->cursos_mensajes_color; ?>; color:<?php echo $this->frases_aprobado[0]->cursos_mensajes_color2; ?>"><?php echo $this->frases_aprobado[0]->cursos_mensajes_mensaje; ?></div>
										</div>
									</div>
									<div class="col-xs-12 col-md-5">
										<div  class="text-center">
											<img src="/page/cursos/certificado?modulo=<?php echo $this->modulo->modulo_id;?>&user=<?php echo $this->iduser;?>" width="80%" class="certificado" />
										</div>
									</div>
								<?php endif ?>
								<?php if ($this->mostrar_certificado=="0"): ?>
									<div class="col-md-2"></div>
									<div class="col-xs-12 col-md-8 pos_imagen4">
										<div class="col-md-3 imagen4"><img src="/corte/imagen4.png"></div>
										<div class="col-md-5">
											<div class="col-md-3 flecha_azul"><img src="/corte/globo-azul1.png"></div>
											<div class="globo_azul text-center col-md-9">
												<div>Total: <?php echo $bien."/".$total;?></div>
												<div>Porcentaje <?php echo round($porse,2).'%'; ?></div>
												<div class="resultado"><?php echo $estado; ?></div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="text-center" style="margin-bottom:-30px;"><img src="<?php echo $gif_aprobado; ?>"></div>
											<div class="globo_frase col-md-12" style="background: <?php echo $this->frases_aprobado[0]->cursos_mensajes_color; ?>; color:<?php echo $this->frases_aprobado[0]->cursos_mensajes_color2; ?> "><?php echo $this->frases_aprobado[0]->cursos_mensajes_mensaje; ?></div>
										</div>
									</div>
								<?php endif ?>
									<div class="fondo_azul col-md-12 text-center descarga_certificado margen_abajo">
										<?php if ($this->mostrar_certificado=="1"): ?>
											<a href="/page/cursos/certificado?modulo=<?php echo $this->modulo->modulo_id;?>&user=<?php echo $this->iduser;?>&descarga=1" class="btn btn-primary" target="_blank" > Descarga Certificado</a>
										<?php endif ?>
										<?php if ($this->mostrar_certificado=="0"): ?>
											<br>
										<?php endif ?>
									</div>
							</div>

						<?php } else{ ?>

							<?php
								$gif_reprobado = "/corte/Robot.gif";
								if($this->gif_reprobado!=""){
									$gif_reprobado = "/images/".$this->gif_reprobado;
								}
							?>

							<div class="col-md-12 margen_abajo">
								<div class="col-xs-12 col-md-8 pos_imagen4">
									<div class="col-md-5"></div>
									<div class="col-md-3 imagen4 text-center"><img src="/corte/imagen3.png"></div>
									<div class="col-md-4">
										<div class="col-md-3 flecha_azul"><img src="/corte/globo-azul1.png"></div>
										<div class="globo_azul text-center col-md-9">
											<div>Total: <?php echo $bien."/".$total;?></div>
											<div>Porcentaje <?php echo round($porse,2).'%'; ?></div>
											<div class="resultado"><?php echo $estado; ?></div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="text-center" style="margin-bottom: -50px;"><img src="<?php echo $gif_reprobado; ?>"></div>
									<div class="globo_frase col-md-12 pos_imagen4" style="background: <?php echo $this->frases_reprobado[0]->cursos_mensajes_color; ?>; color:<?php echo $this->frases_reprobado[0]->cursos_mensajes_color2; ?> "><?php echo $this->frases_reprobado[0]->cursos_mensajes_mensaje; ?></div>
								</div>
							</div>
								<div class="fondo_azul col-md-12 text-center descarga_certificado2 alto80 margen_abajo">
									<?php if($this->mostrarBotones != 1 and $this->intentos<$this->oportunidades){ ?>
										<a href="/page/cursos/evaluacion?modulo=<?php echo $this->modulo->modulo_id;?>&nuevo=1" class="btn btn-primary" >Hacer de nuevo</a>
									<?php } else {?>
										<br>
									<?php } ?>
								</div>
						<?php } ?>

					</div>
					<?php
						$fila = 1;
						$contador = 2;
					?>
					<?php foreach ($this->preguntas as $pregunta): ?>
						<?php if ($contador == 2){?>
							<?php if($fila!=1){ ?></div><?php } else { $fila = 2; } ?>
							<div class="row">
							<?php $contador = 0;  ?>
						<?php } ?>
						<?php $contador++; ?>
						<?php
							$respuestas = array();
							$respuestas[1] =  $pregunta->pregunta_respuesta1;
							$respuestas[2] =  $pregunta->pregunta_respuesta2;
							$respuestas[3] =  $pregunta->pregunta_respuesta3;
							$respuestas[4] =  $pregunta->pregunta_respuesta4;
							$respuestas[5] =  $pregunta->pregunta_respuesta5;
							$respuestas[6] =  $pregunta->pregunta_respuesta6;
							$respuestas[7] =  $pregunta->pregunta_respuesta7;
							$respuestas[8] =  $pregunta->pregunta_respuesta8;
							$respuestas[9] =  $pregunta->pregunta_respuesta9;
							$respuestas[10] =  $pregunta->pregunta_respuesta10;
							$respuestas[11] =  $pregunta->pregunta_respuesta11;
							$respuestas[12] =  $pregunta->pregunta_respuesta12;
							$respuestas[13] =  $pregunta->pregunta_respuesta13;
							$respuestas[14] =  $pregunta->pregunta_respuesta14;
							//shuffle($respuestas);
						?>
						<div class="col-sm-6 ">
							<div class="caja_pregunta form-group">
								<div class="pregunta"><?php echo $pregunta->pregunta_pregunta; ?></div>
								<?php foreach ($respuestas as $key => $respuesta): ?>
									<?php if($respuesta!=''){ ?>
										<?php if($pregunta->pregunta_tipo=="1" or $pregunta->pregunta_tipo=="2"){ ?>
											<div class="respuesta">
												<span class="respuesta_check mal">
													<?php if($pregunta->respuesta_respuesta == $respuesta ){?>
														<?php if($pregunta->respuesta_correcto == 1){ ?>
															<i class=" text-success glyphicon glyphicon-ok-circle"></i>
														<?php } else {  ?>
															<i class=" text-danger glyphicon glyphicon-remove-circle"></i>
														<?php } ?>
													<?php } else {  ?>
													<i class="glyphicon glyphicon-ban-circle"></i>
													<?php } ?>
												</span>
												<label for="pregunta_<?php echo $pregunta->pregunta_id; ?>_<?php echo $key; ?>"><?php echo $respuesta; ?></label>
											</div>
										<?php } ?>
										<?php if($pregunta->pregunta_tipo=="3"){ ?>
											<div class="respuesta">
												<span class="respuesta_check mal">
													<?php $multiples_respuestas = explode("|",$pregunta->respuesta_respuesta); ?>
													<?php if(in_array($respuesta, $multiples_respuestas)){ ?>
														<?php if($pregunta->respuesta_correcto == 1){ ?>
															<i class=" text-success glyphicon glyphicon-ok-circle"></i>
														<?php } else {  ?>
															<i class=" text-danger glyphicon glyphicon-remove-circle"></i>
														<?php } ?>
													<?php } else {  ?>
														<i class="glyphicon glyphicon-ban-circle"></i>
													<?php } ?>
												</span>
												<label for="pregunta_<?php echo $pregunta->pregunta_id; ?>_<?php echo $key; ?>"><?php echo $respuesta; ?></label>
											</div>
										<?php } ?>
									<?php } ?>
								<?php endforeach ?>
								<div></div>
								<div class="help-block with-errors"></div>
							</div>
						</div>
					<?php endforeach ?>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>

	<?php if($this->respondido != true){  ?>
		<div class="row form-group fondo_verde margen_abajo margen_arriba alto80">
			<div class="col-sm-12 text-center">
				<button type="button" class="btn btn-success" onclick="document.getElementById('enviar').click();">Enviar evaluación</button>
			</div>
		</div>
	<?php } ?>

</div>