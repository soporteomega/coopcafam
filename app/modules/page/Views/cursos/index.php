<img src="/skins/page/images/banner2-cursos.jpg" style="width: 100%;" class="imagen-curso">
<div class="container">
	<h1 class="title-main cursos" >Formación en línea</h1>
	<?php foreach ($this->cursos as $curso): ?>
		<a class="caja_curso" href="/page/cursos/curso?curso=<?php echo $curso->cursos_id; ?>">
			<div class="contenedor_curso" style="<?php if($curso->cursos_imagen ){ ?>background-image: url(/<?php echo str_replace(" ","%20",$curso->cursos_imagen); ?>); <?php }  ?>">
				<div class="caption_curso"><?php echo $curso->curso_titulo; ?></div>
				</div>
		</a>
	<?php endforeach ?>
</div>