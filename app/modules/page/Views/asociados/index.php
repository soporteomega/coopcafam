<div class="container">
	<h1 class="titulo-general">Información para nuestros asociados</h1>
	<div class="subtitulo2"><?php echo $this->introduccion[0]->content_description; ?></div>
	<br>
</div>
<div align="center">
	<div class="container">
		<div class="row">
			<?php foreach ($this->noticias as $key => $noticia): ?>
				<div class="col-sm-3">
					<div class="caja-noticia">
						<div class="imagen-noticia" style="background-image: url(/images/<?php echo str_replace(" ","%20",$noticia->content_image); ?>);"></div>
						<h3><?php echo $noticia->content_title; ?></h3>
						<div class="descripcion"><?php echo $noticia->content_introduction; ?></div>
						<a class="leer" href="/page/asociados/detalle?id=<?php echo $noticia->content_id; ?>">Leer Más</a>
					</div>
				</div>
				<?php if(($key+1)%4 == 0){ ?>
					<div class="clearfix"></div>
				<?php } ?>
			<?php endforeach ?>
		</div>
	</div>
</div>