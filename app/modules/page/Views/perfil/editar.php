 <div class="container">
	<h1 class="titulo-general">Actualiza tu Perfíl </h1>
	<div class="caja-perfil">
	<div class="row">
		<div class="col-sm-8 form-group">
			<form action="/page/perfil/actualizar" data-toggle="validator"  method="post"  enctype="multipart/form-data" >
				<div class="row">
					<div class="col-sm-4 form-group">
						<label class="label_perfil control-label">Nombres</label>
						<input value="<?php echo $this->user->user_names; ?>" type="text" name="names" class="form-control" required>
					</div>
					<div class="col-sm-4 form-group">
						<label class="label_perfil control-label">Apellidos</label>
						<input value="<?php echo $this->user->user_lastnames; ?>" type="text" name="lastnames" class="form-control" required>
					</div>
					<div class="col-sm-4 form-group">
						<label class="label_perfil control-label">Identificación</label>
						<input value="<?php echo $this->user->user_idnumber; ?>" type="text" name="idnumber" class="form-control" readonly="readonly" required>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8 form-group">
						<label class="label_perfil control-label">Correo</label>
						<input value="<?php echo $this->user->user_email; ?>" type="email" name="email" class="form-control" required>
					</div>
					<div class="col-sm-4 form-group">
						<label class="label_perfil control-label">Extensión</label>
						<input value="<?php echo $this->user->user_phone; ?>" type="text" name="phone" class="form-control" required>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 form-group">
						<label class="label_perfil control-label">Direccion</label>
						<input value="<?php echo $this->user->user_address; ?>" type="text" name="address" class="form-control" required>
					</div>
					<div class="col-sm-3 form-group">
						<label class="label_perfil control-label">Ciudad</label>
						<input value="<?php echo $this->user->user_city; ?>" type="text" name="city" class="form-control" required>
					</div>
					<div class="col-sm-3 form-group">
						<label class="label_perfil control-label">Pais</label>
						<input value="<?php echo $this->user->user_country; ?>" type="text" name="country" class="form-control" required>
					</div>
					<div class="col-xs-8 form-group">
		                <label for="photo" >Fotografia</label>
		                <input type="file" value="" name="photo" id="photo" class="form-control file-image" placeholder="Fotografia" >
		            </div>
		            <div class="col-xs-4 form-group">
		                <label for="photo" >Cargo</label>
		                <select required name="position" class="form-control">
		                	<option value="">Seleccione...</option>
		                	<?php foreach ($this->cargos as $key => $cargo): ?>
		                		<option value="<?php echo $cargo->cargo_nombre; ?>" <?php if ($cargo->cargo_nombre == $this->user->user_position ): ?> selected <?php endif ?>><?php echo $cargo->cargo_nombre; ?></option>
		                	<?php endforeach ?>
		                </select>
		            </div>
				</div>

				<div class="row">
					<div class="col-sm-6 form-group">
						<button class="btn btn-block btn-perfil" type="submit">Actualizar</button>
					</div>
					<div class="col-sm-6 form-group">
						<a href="/page/perfil" class="btn btn-block   btn-primary" >Cancelar</a>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm-4 form-group">
			<br>
			<?php if($this->user->user_photo){  ?>
				<img src="/images/<?php echo $this->user->user_photo; ?>" style="width: 100%;" />
			<?php } else { ?>
				<img src="/skins/page/images/imagen-perfil.jpg" style="width: 100%;" />
			<?php } ?>
		</div>
	</div>
	</div>
</div>
