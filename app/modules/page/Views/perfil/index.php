<div class="container">
	<h1 class="titulo-general">Bienvenido a tu perfíl </h1>
	<div class="caja-perfil">
	<div class="row">
		<div class="col-sm-8">
			<?php if($this->actualizo == 1){ ?>
				<div class="text-center mensaje-perfil"> Tu perfíl se ha actualizado correctamente.</div>
			<?php }?>
			<?php if($this->actualizo == 2){ ?>
				<div class="text-center mensaje-perfil"> Tu contraseña se ha actualizado correctamente.</div>
			<?php }?>
			<br>
			<div class="text-right">
				<a class="btn btn-perfil" href="/page/perfil/editar"> <i class="glyphicon glyphicon-pencil"></i> Editar Perfíl</a>
				<a class="btn btn-perfil" href="/page/perfil/password"> <i class="glyphicon glyphicon-lock"></i> Cambiar Contraseña </a>
			</div>
			<br>
				<div class="row">
					<div class="col-sm-6 form-group">
						<label class="label_perfil">Nombres</label>
						<div><?php echo $this->user->user_names; ?> <?php echo $this->user->user_lastnames; ?></div>
					</div>
					<div class="col-sm-3 form-group">
						<label class="label_perfil">Identificación</label>
						<div><?php echo $this->user->user_idnumber; ?></div>
					</div>
					<div class="col-sm-3 form-group">
						<label class="label_perfil">Cargo</label>
						<div><?php echo $this->user->user_position; ?></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 form-group">
						<label class="label_perfil">Correo</label>
						<div><?php echo $this->user->user_email; ?></div>
					</div>
					<div class="col-sm-4 form-group">
						<label class="label_perfil">Extensión</label>
						<div><?php echo $this->user->user_phone; ?></div>
					</div>
					<div class="col-sm-4 form-group">
						<label class="label_perfil">Fecha de Nacimiento</label>
						<div><?php echo $this->user->user_date; ?></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 form-group">
						<label class="label_perfil">Direccion</label>
						<div><?php echo $this->user->user_address; ?></div>
					</div>
					<div class="col-sm-4 form-group">
						<label class="label_perfil">Ciudad</label>
						<div><?php echo $this->user->user_city; ?></div>
					</div>
					<div class="col-sm-4 form-group">
						<label class="label_perfil">Pais</label>
						<div><?php echo $this->user->user_country; ?></div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<?php if($this->user->user_photo){  ?>
					<img src="/images/<?php echo $this->user->user_photo; ?>" style="width: 100%;" />
				<?php } else { ?>
					<img src="/skins/page/images/imagen-perfil.jpg" style="width: 100%;" />
				<?php } ?>
			</div>
		</div>
	</div>
</div>