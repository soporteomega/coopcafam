
<div class="container">
	<h1 class="titulo-general">Cambiar Contraseña</h1>
	<div class="caja-perfil">
	<div class="row">
		<div class="col-sm-8 form-group">
			<br>
			<?php if($this->error == 1){ ?>
				<div class="text-center mensaje-perfil"> La contraseña Actual no es correcta.</div>
			<?php } else if($this->error!='') {?>
				<div class="text-center mensaje-perfil"><?php echo $this->error ;?></div>
			<?php } ?>
			<form action="/page/perfil/changepassword" data-toggle="validator"  method="post" >
				<div class="row">
					<div class="col-sm-4 form-group">
						<label class="label_perfil control-label">Contraseña Actual</label>
						<input value="" type="password" name="current_password" class="form-control" required>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 form-group">
						<label class="label_perfil control-label">Nueva Contraseña</label>
						<input value="" type="password" name="password" id="password" class="form-control" required>
					</div>
					<div class="col-sm-6 form-group">
						<label class="label_perfil control-label">Repita Contraseña</label>
						<input value="" type="password" name="re-password" class="form-control" required data-match="#password" data-match-error="Las dos contraseñas no son iguales">
						<div class="help-block with-errors"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 form-group">
						<button class="btn btn-block btn-perfil" type="submit">Actualizar</button>
					</div>
					<div class="col-sm-6 form-group">
						<a href="/page/perfil" class="btn btn-block   btn-primary" >Cancelar</a>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm-4 form-group">
			<br>
			<img src="/skins/page/images/imagen-perfil.jpg" style="width: 100%;" />
		</div>
	</div>
	</div>
</div>