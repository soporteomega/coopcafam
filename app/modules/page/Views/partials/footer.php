<div class="container">
	<div class="row">
		<div class="col-sm-5">
			<div class="row enlaces-footer">
				<h3>Sedes</h3>
				<div class="row">
				<?php foreach ($this->sedes as $key => $sede): ?>
					<a class="col-sm-12" <?php if( $sede->content_link != '' ){ ?> href="<?php echo $sede->content_link; ?>" <?php } ?> target="_blank" ><?php echo $sede->content_title?> <?php echo $sede->content_subtitle?></a>
				<?php endforeach ?>
				</div>
			</div>
		</div>
		<div class="col-sm-4 text-right">
			<?php foreach ($this->redes as $key => $red): ?>
				<a href="<?php echo $red->content_link; ?>" target="_blank" data-toggle="tooltip" data-placement="top" title="<?php echo $red->content_title; ?>"><img src="/images/<?php echo $red->content_image; ?>" class="spin"></a>
			<?php endforeach ?>
		</div>
		<div class="col-sm-3 text-right enlaces-footer">
			<h3 >Links de interés</h3>
				<?php foreach ($this->enlaces as $key => $sede): ?>
					<a class="col-sm-12" <?php if( $sede->content_link != '' ){ ?> href="<?php echo $sede->content_link; ?>" <?php } ?> target="_blank" ><?php echo $sede->content_title?> <?php echo $sede->content_subtitle?></a>
				<?php endforeach ?></div>
		<div class="col-sm-12 text-center">Todos los derechos reservados</div>
	</div>
</div>