<div class="text-right">
	<a href="/page/perfil">
		<?php if($this->user->user_photo) { ?>
			<img src="/images/<?php echo $this->user->user_photo; ?>" class="foto-header">
		<?php } ?>
	 <span>Hola <?php echo $this->user->user_names; ?></span></a>
	<a class="salir" href="/page/login/logout"><i class="glyphicon glyphicon-off"></i> Salir</a>
</div>