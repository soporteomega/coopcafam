<div class="container">
	<h1 class="titulo-general">Campañas</h1>
	<div class="subtitulo2"><?php echo $this->introduccion[0]->content_description; ?></div>
	<br>
</div>
<div align="center">
	<div class="container">
		<div class="row">
			<?php foreach ($this->noticias as $key => $noticia): ?>
				<div class="col-sm-3">
					<a href="/page/campanias/detalle?id=<?php echo $noticia->content_id; ?>" class="caja-campania" <?php if($noticia->content_image!=''){ ?>style="background-image: url(/images/<?php echo str_replace(" ","%20",$noticia->content_image); ?>);"<?php } ?>>
						<div class="caption-campania" >
							<h3><?php echo $noticia->content_title; ?></h3>
							<div class="descripcion"><?php echo $noticia->content_introduction; ?></div>
						</div>
					</a>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>