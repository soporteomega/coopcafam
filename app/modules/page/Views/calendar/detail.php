<?php echo $this->getRoutPHP('modules/page/Views/partials/botonera.php');?>
<div>
<?php if(file_exists($this->content->calendar_banner)){?>
	<img src="/<?= $this->content->calendar_banner?>" style="width: 100%;">
<?php } ?>
	<h1 class="title-main company" style="color:<?= $this->type->type_calendar_color; ?>"><?= $this->content->calendar_name?></h1>
	<div class="text-right" style="margin-right: 30px; margin-top: 20px; font-size:20px;">
		<?php
			$ruta = $this->route;
			$titulo = $this->content->calendar_name;
			$fecha1 = date("Ymd",strtotime($this->content->calendar_startdate))."T".str_replace(":","",$this->content->calendar_starttime);
			$fecha2 = date("Ymd",strtotime($this->content->calendar_enddate))."T".str_replace(":","",$this->content->calendar_endtime);
			$ubicacion = " ";
			$descripcion = $this->content->calendar_description;

            $ics = "
            BEGIN:VCALENDAR
            VERSION:2.0
            PRODID:-//hacksw/handcal//NONSGML v1.0//EN
            BEGIN:VEVENT
            DTSTART:$fecha1
            DTEND:$fecha2
            SUMMARY:$titulo
            LOCATION:$ubicacion
            DESCRIPTION:$descripcion
            END:VEVENT
            END:VCALENDAR
            ";

            $archivo = "tmp/event_".$this->content->calendar_id.".ics";
            $file = fopen($archivo, "w");
            fputs($file, $ics ."".PHP_EOL);
            fclose($file);

        ?>
        <strong>Agregar a calendario :</strong>
        <a class="btn btn-sm btn-primary" href="/<?php echo $archivo; ?>" target="_blank">Outlook</a>
        <a  class="btn btn-sm btn-danger" href="http://www.google.com/calendar/event?action=TEMPLATE&amp;text=<?php echo $titulo; ?>&amp;dates=<?php echo $fecha1 ?>/<?php echo $fecha2 ?>&amp;details=<?php echo htmlentities($descripcion); ?><?php echo $ruta; ?>/index.php?mod=detalle%26id=<?php echo $_GET['id']; ?>&amp;location=&amp;trp=true&amp;sprop=www.oraclecalendaronline.com&amp;sprop=name:" target="_blank">Google</a>

	</div>
	<div class="description-detail">
		<?php if(file_exists($this->content->calendar_image)){?>
			<img src="/<?= $this->content->calendar_image?>" class="image_content">
		<?php } ?>
		<div><strong>Fecha y hora de Inicio:</strong> <?= $this->content->calendar_startdate;?> <?= $this->content->calendar_starttime;?> </div>
		<div><strong>Fecha y hora de finalización:</strong> <?= $this->content->calendar_enddate;?> <?= $this->content->calendar_endtime;?>  </div>
		<?= $this->content->calendar_description;?>
		<div style="clear: both;"></div>
	</div>
</div>