<div class="container">
	<div class="row">
	<?php foreach ($this->array as $key => $value): ?>
		<?php $eventos = $value['eventos']; ?>
		<div class="col-sm-12">	<h1 class="titulo-general" ><?php echo $value['detalle']->type_calendar_name; ?>
			<?php if($value['detalle']->type_calendar_icon){?>
            	<img src="/images/<?php echo $value['detalle']->type_calendar_icon  ?>">
        	<?php } ?>
		</h1></div>
		<?php foreach ($eventos as $key => $event): ?>
			<div class="col-sm-6">
			<a href="/page/felicitaciones/detail?id=<?php echo $event->calendar_id; ?>" class="caja-felicitacion2">
				<div class="row">
					<div class="col-sm-5">
						<?php if($event->user_photo){ ?>
							<img src="/images/<?php echo $event->user_photo; ?>" class="foto">
						<?php } else { ?>
							<img src="/skins/page/images/persona.png"  class="foto">
						<?php } ?>
					</div>
					<div class="col-sm-7">
						<div class="fecha"><?php echo $event->calendar_startdate; ?></div>
						<div class="name"><?php echo $event->calendar_name?></div>
						<div class="tarjeta-felicitacion2">
							<span>Enviar tarjeta</span>
							<img src="/skins/page/images/tarjeta.png">
						</div>
					</div>
				</div>
			</a>
			</div>
		<?php endforeach ?>
	<?php endforeach ?>
	</div>
</div>
<br>
<br>