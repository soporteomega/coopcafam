<div class="container">
	<h1 class="title-main cursos" ><?php echo $this->modulo->modulo_titulo; ?></h1>
	<h2 class="subtitle-main" ><?php echo $this->curso->curso_titulo; ?></h2>
	<div class="description" >
		<div class="container-fluid">
			<div class="col-sm-4 col-md-push-8">
				<div><?php echo $this->modulo->modulo_descripcion; ?></div>
				<div align="right"><a class="btn btn-success " href="/page/cursos/evaluacion?modulo=<?php echo $this->modulo->modulo_id; ?>" > <span class="badge"><i class="glyphicon glyphicon-question-sign"></i></span> Realizar Evaluación  </a></div>
				<br><br>
			</div>
			<div class="col-sm-8 col-md-pull-4">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<?php foreach ($this->secciones as $seccion): ?>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="tab<?php echo $seccion->seccion_modulo_id; ?>">
							    	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $seccion->seccion_modulo_id; ?>" aria-expanded="false" aria-controls="<?php echo $seccion->seccion_modulo_id; ?>" class="button-acordeon" >
							      		<?php echo $seccion->seccion_modulo_nombre; ?>
							    	</a>
							</div>
							<div id="<?php echo $seccion->seccion_modulo_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="tab<?php echo $seccion->seccion_modulo_id; ?>">
							  	<div class="panel-body">
							    	<?php echo $seccion->seccion_modulo_descripcion; ?>
							  	</div>
							</div>
						</div>
					<?php endforeach ?>
				</div>
			</div>

		</div>
	</div>
</div>