<img src="/skins/page/images/banner2-cursos.jpg" style="width: 100%;" class="imagen-curso">
<div class="container">
	<h1 class="title-main cursos" ><?php echo $this->curso->curso_titulo; ?></h1>
	<div class="description-detail">
		<div class="container-fluid">

			<div class="col-sm-5">
				<?php if(file_exists(PUBLIC_PATH.$this->curso->cursos_imagen)){ ?>
						<img src="/images/<?php echo $this->curso->cursos_imagen; ?>" class="img-thumbnail img-curso">
				<?php } ?>
				<div><?php echo $this->curso->curso_descripcion; ?></div>
				<div style="clear: both;"></div>
			</div>
			<div class="col-sm-7">
				<div class="list-group">
					<?php foreach ($this->modulos as $modulo): ?>
						<a href="/page/cursos/modulo?modulo=<?php echo $modulo->modulo_id; ?>" class="list-group-item"><?php echo $modulo->modulo_titulo; ?></a>
					<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
</div>