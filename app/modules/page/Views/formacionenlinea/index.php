<div class="container-fluid fondo_cursos">
	<div class="fondo_verde titulo_formacion col-md-12 text-center">Formación en LÍNEA</div>
	<div class="container">

		<div class="col-md-12">
			<div class="col-md-3"></div>
			<div class="col-md-2"><img src="/corte/imagen4.png" width="180" class="z1"></div>
			<div class="col-md-4">
				<div class="col-md-3 flecha_azul"><img src="/corte/globo-azul1.png"></div>
				<div class="globo_azul text-center col-md-9 resultado">
					<?php echo $this->introduccion[0]->content_description; ?>
				</div>
			</div>
			<div class="fondo_azul col-md-12 pos_fondo1"><br></div>
		</div>

		<br>
		<?php $total_cursos=count($this->cursos); ?>
		<div class="col-md-12 text-center">
			<?php foreach ($this->cursos as $curso): ?>
				<?php if ($total_cursos==1): ?>
					<div class="col-md-3"></div>
				<?php endif ?>
				<a class="caja_curso2 col-sm-12 col-md-6" href="/page/cursos/curso?curso=<?php echo $curso->cursos_id; ?>">
					<div class="contenedor_curso">
						<div class="caption_curso"><?php echo $curso->curso_titulo; ?></div>
						<div class="separador"></div>
						<div class="texto_curso text-justify"><?php echo $curso->curso_descripcion; ?></div>
						<div class="btn btn-success">Entrar al curso</div>
					</div>
				</a>
			<?php endforeach ?>
		</div>
	</div>
</div>