<img src="/skins/page/images/banner2-cursos.jpg" style="width: 100%;" class="imagen-curso">
<div class="container">
	<h1 class="title-main cursos" >Evalución <?php echo $this->modulo->modulo_titulo; ?></h1>
	<h2 class="subtitle-main" ><?php echo $this->curso->curso_titulo; ?></h2>
	<?php if($this->respondido != true){  ?>
	<form data-toggle="validator" method="post" action="/page/cursos/responder" >
		<div class="description" >
			<div class="container-fluid">
				<?php
					$fila = 1;
					$contador = 2;
				?>
				<?php foreach ($this->preguntas as $pregunta): ?>
					<?php if ($contador == 2){?>
						<?php if($fila!=1){ ?></div><?php } else { $fila = 2; } ?>
						<div class="row">
						<?php $contador = 0;  ?>
					<?php } ?>
					<?php $contador++; ?>
					<?php
						$respuestas = array();
						$respuestas[1] =  $pregunta->pregunta_respuesta1;
						$respuestas[2] =  $pregunta->pregunta_respuesta2;
						$respuestas[3] =  $pregunta->pregunta_respuesta3;
						$respuestas[4] =  $pregunta->pregunta_respuesta4;
						$respuestas[5] =  $pregunta->pregunta_respuesta5;
						shuffle($respuestas);
					?>
					<div class="col-sm-6 ">
						<input type="hidden" name="pregunta[]" value="<?php echo $pregunta->pregunta_id; ?>"></input>
						<div class="caja_pregunta form-group">
							<div class="pregunta"><?php echo $pregunta->pregunta_pregunta; ?></div>
							<?php foreach ($respuestas as $key => $respuesta): ?>
								<?php if($respuesta!=''){ ?>
									<div class="respuesta">
									<input type="radio" name="pregunta_<?php echo $pregunta->pregunta_id; ?>" id="pregunta_<?php echo $pregunta->pregunta_id; ?>_<?php echo $key; ?>" value="<?php echo $respuesta; ?>" required></input>
										<label for="pregunta_<?php echo $pregunta->pregunta_id; ?>_<?php echo $key; ?>"><?php echo $respuesta; ?></label>
									</div>
								<?php } ?>
							<?php endforeach ?>
							<div></div>
							<div class="help-block with-errors"></div>
						</div>
					</div>
				<?php endforeach ?>
				</div>
				<div class="row form-group">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-success btn-block">Enviar Respuestas</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<?php } else { ?>

		<?php
			$bien = 0;
			$total = 0;

		?>
		<?php foreach ($this->preguntas as $pregunta): ?>
			<?php
				if($pregunta->respuesta_correcto == 1){
					$bien++;
				}
				$total++;

				$porse = (100/$total)*$bien;
				if($porse >= 65){
					$estado = "Aprobado";
					$class = "bg-success";
				} else {
					$estado = "Reprobado";
					$class = "bg-danger";
				}
			?>
		<?php endforeach ?>
		<div class="description" >

			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12 ">
					<div class="text-right"><a href="/page/cursos/evaluacion?modulo=<?php echo $this->modulo->modulo_id;?>&nuevo=1" class="btn btn-info">Hacer de nuevo</a></div>
						<div class="text-right"><h3 class="estado <?php echo $class; ?>" >Total: <?php echo $bien."/".$total." - Porsentaje ".$porse.'% - '.$estado;  ?></h3></div>
					</div>
				</div>
				<?php
					$fila = 1;
					$contador = 2;
				?>
				<?php foreach ($this->preguntas as $pregunta): ?>
					<?php if ($contador == 2){?>
						<?php if($fila!=1){ ?></div><?php } else { $fila = 2; } ?>
						<div class="row">
						<?php $contador = 0;  ?>
					<?php } ?>
					<?php $contador++; ?>
					<?php
						$respuestas = array();
						$respuestas[1] =  $pregunta->pregunta_respuesta1;
						$respuestas[2] =  $pregunta->pregunta_respuesta2;
						$respuestas[3] =  $pregunta->pregunta_respuesta3;
						$respuestas[4] =  $pregunta->pregunta_respuesta4;
						$respuestas[5] =  $pregunta->pregunta_respuesta5;
						shuffle($respuestas);
					?>
					<div class="col-sm-6 ">
						<div class="caja_pregunta form-group">
							<div class="pregunta"><?php echo $pregunta->pregunta_pregunta; ?></div>
							<?php foreach ($respuestas as $key => $respuesta): ?>
								<?php if($respuesta!=''){ ?>
									<div class="respuesta">
										<span class="respuesta_check mal">
											<?php if($pregunta->respuesta_respuesta == $respuesta ){?>
												<?php if($pregunta->respuesta_correcto == 1){ ?>
													<i class=" text-success glyphicon glyphicon-ok-circle"></i>
												<?php } else {  ?>
													<i class=" text-danger glyphicon glyphicon-remove-circle"></i>
												<?php } ?>
											<?php } else {  ?>
											<i class="glyphicon glyphicon-ban-circle"></i>
											<?php } ?>
										</span>
										<label for="pregunta_<?php echo $pregunta->pregunta_id; ?>_<?php echo $key; ?>"><?php echo $respuesta; ?></label>
									</div>
								<?php } ?>
							<?php endforeach ?>
							<div></div>
							<div class="help-block with-errors"></div>
						</div>
					</div>
				<?php endforeach ?>
				</div>
			</div>
		</div>
	<?php } ?>
</div>