<?php 

/**
*
*/

class Page_sgcController extends Page_mainController
{

	public function indexAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->introduccion = $contentModel->getList("content_section = 'Introduccion SGC'"," orden ASC");
		$this->_view->contenidos = $this->getDocuments(0,'SGC General');
	}

	public function detalleAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$categoriasModel = new Page_Model_DbTable_Categoriadocumentos();
		$documentoModel = new Page_Model_DbTable_Documento();
		$id = $this->_getSanitizedParam("categoria");
		$this->_view->categoria =  $categoriasModel->getById($id);
		$this->_view->content =  $this->getDocuments($id,"orden ASC");
	}

	public function detalle2Action()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$categoriasModel = new Page_Model_DbTable_Categoriadocumentos();
		$documentoModel = new Page_Model_DbTable_Documento();
		$id = $this->_getSanitizedParam("categoria");
		$this->_view->categoria =  $categoriasModel->getById($id);
		$this->_view->content =  $this->getDocuments($id,"documento_nombre ASC");
	}

	public function mapaAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->introduccion = $contentModel->getList("content_section = 'Introduccion Mapa'"," orden ASC");
		$this->_view->gerenciales = $this->getDocuments(0,'Mapa - Procesos Estrategicos');
		$this->_view->clave = $this->getDocuments(0,'Mapa - Procesos Misionales');
		$this->_view->apoyo = $this->getDocuments(0,'Mapa - Procesos de Apoyo');
	}

	public function getDocuments($id,$seccion){
		$categoriasModel = new Page_Model_DbTable_Categoriadocumentos();
		$documentoModel = new Page_Model_DbTable_Documento();
		$arrayCategorias = array();
		if($id>0){
			$categoria = $categoriasModel->getById($id);
			$categorias = $categoriasModel->getList("categoria_documento_padre = '$id' ","orden ASC");
			$arrayCategorias['detalle'] =  $categoria;
			foreach ($categorias as $key => $cate) {
				$arrayCategorias['categorias'][$key] = $this->getDocuments($cate->categoria_documento_id,"");
			}
			$arrayCategorias['documentos'] =  $documentoModel->getList("documento_categoria = '$id'","documento_nombre ASC, orden ASC");
		} else if($seccion!=""){
			$categorias = $categoriasModel->getList("categoria_documento_tipo = '$seccion' AND ( categoria_documento_padre = 0 OR categoria_documento_padre IS NULL) ","orden ASC");
			foreach ($categorias as $key => $cate) {
				$idp =$cate->categoria_documento_id;
				$arrayCategorias[$key] = $this->getDocuments($cate->categoria_documento_id,"");
			}
		}
		return $arrayCategorias;
	}

	public function samAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->contenidos = $contentModel->getList("content_section = 'Control SAM'"," orden ASC");

	}
	public function documentosAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->contenidos = $contentModel->getList("content_section = 'Control de Documentos'"," orden ASC");
	}
}