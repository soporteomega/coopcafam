<?php

/**
*
*/

class Page_cursosController extends Page_mainController
{

	public function indexAction()
	{
		$hoy = date("Y-m-d H:i:s");
		$modelcursos = new Page_Model_DbTable_Cursos();
		$this->_view->cursos = $modelcursos->getList(" cursos_ocultar <> 1 AND cursos_inicio <='$hoy' AND cursos_fin >='$hoy'  "," orden ASC");
	}
	public function cursoAction()
	{
		$modelcursos = new Page_Model_DbTable_Cursos();
		$modelModulos = new Page_Model_DbTable_Modulos();
		$curso = $this->_getSanitizedParam("curso");
		$this->_view->curso = $modelcursos->getById($curso);
		$this->_view->modulos = $modelModulos->getList(" cursos_id = '$curso' "," orden ASC ");
	}

	public function moduloAction()
	{
		$modelcursos = new Page_Model_DbTable_Cursos();
		$modelModulos = new Page_Model_DbTable_Modulos();
		$modelSecciones = new Page_Model_DbTable_Seccionesmodulo();
		$modulo = $this->_getSanitizedParam("modulo");
		$this->_view->modulo = $modelModulos->getById($modulo);
		$this->_view->curso = $modelcursos->getById($this->_view->modulo->cursos_id);
		$this->_view->secciones = $modelSecciones->getList(" modulo_id = '$modulo' "," orden ASC ");

		$modulo_list = $modelModulos->getById($modulo);
		$columna = $modulo_list->modulo_columnas;
		$columna_estilo = "col-md-12";
		if($columna=="1"){
			$columna_estilo = "col-md-12";
		}
		if($columna=="2"){
			$columna_estilo = "col-md-6";
		}
		if($columna=="3"){
			$columna_estilo = "col-md-4";
		}
		$this->_view->columnas = $columna_estilo;

	}
	public function certificadoAction()
	{
		$modelcursos = new Page_Model_DbTable_Cursos();
		$modelModulos = new Page_Model_DbTable_Modulos();
		$modelUser = new Page_Model_DbTable_User();
		$modulo = $this->_getSanitizedParam("modulo");
		$user = $this->_getSanitizedParam("user");
		$this->_view->modulo = $modelModulos->getById($modulo);
		$micurso = $modelcursos->getById($this->_view->modulo->cursos_id);

		$usuario = $modelUser->getById($user);
		header("Content-type: image/png");
		if($this->_getSanitizedParam("descarga")==1){
			header('Content-disposition: attachment; filename=Certificado.jpg');;
		}
		$nombre = $usuario->user_names." ".$usuario->user_lastnames;
		$cedula = $usuario->user_idnumber;
		$dia =date("d");
		$mes= date("m");
		$anio = date("y");
		$curso = html_entity_decode($micurso->curso_titulo);
		$im     = imagecreatefrompng(PUBLIC_PATH."/skins/page/images/certificado.png");
		$fuente = PUBLIC_PATH.'skins/page/fuentes/OpenSans-Bold-webfont.ttf';
		$verde = imagecolorallocate($im, 0, 164, 95);
		$azul = imagecolorallocate($im, 43, 57,125);
		$px     = (imagesx($im)  / 2) - ((strlen($nombre)*12) / 2);
		imagettftext($im,30, 0, $px,220, $verde, $fuente, $nombre);
		imagettftext($im,18, 0, 580,266, $azul, $fuente, $cedula);
		$px     = ((imagesx($im) / 2)+100) - ((strlen($curso)*12) / 2);
		imagettftext($im,25, 0, $px,340, $verde, $fuente, $curso);
		imagettftext($im,14, 0, 596,408, $azul, $fuente, $dia);
		imagettftext($im,14, 0, 683,408, $azul, $fuente, $mes);
		imagettftext($im,14, 0, 767,408, $azul, $fuente, $anio);

		imagepng($im);
		imagedestroy($im);


	}
	public function evaluacionAction()
	{
		$modelRespuestas = new Page_Model_DbTable_Respuesta();
		$modelcursos = new Page_Model_DbTable_Cursos();
		$modelModulos = new Page_Model_DbTable_Modulos();
		$modelPreguntas = new Page_Model_DbTable_Preguntas();
		$modelConfig = new Page_Model_DbTable_Preguntasconfig();
		$modelFrases = new Administracion_Model_DbTable_Frases();
		$modulo = $this->_getSanitizedParam("modulo");

		//config
		$configuracion = $modelConfig->getList(" preguntas_config_modulo_id = '$modulo' ","");
		$orden = $configuracion[0]->preguntas_config_orden;
		if($orden=="1"){
			$orden1 = " pregunta_tipo ASC, orden ASC ";
		}
		if($orden=="2" or $orden==""){
			$orden1 = " pregunta_tipo ASC, RAND() ";
		}
		$cantidad = 10;
		if($configuracion[0]->preguntas_config_cantidad>0){
			$cantidad = $configuracion[0]->preguntas_config_cantidad;
		}
		$tiempo = 5;
		if($configuracion[0]->preguntas_config_tiempo>0){
			$tiempo = $configuracion[0]->preguntas_config_tiempo;
		}
		$this->_view->tiempo=$tiempo;
		$this->_view->oportunidades = $configuracion[0]->preguntas_config_oportunidades*1;
		$this->_view->mostrar_certificado = $configuracion[0]->preguntas_mostrar_certificado*1;
		$this->_view->gif_aprobado = $configuracion[0]->preguntas_config_gif_aprobado;
		$this->_view->gif_reprobado = $configuracion[0]->preguntas_config_gif_reprobado;
		$this->_view->gif_reloj = $configuracion[0]->preguntas_config_gif_reloj;
		$this->_view->orden_respuestas = $configuracion[0]->preguntas_config_orden_respuestas;

		if($this->_getSanitizedParam("usuario")){
			$usuario = $this->_getSanitizedParam("usuario");
			$this->_view->mostrarBotones = 1;
		}else {
			$usuario = Session::getInstance()->get("kt_login_id");
		}
		$this->_view->modulo = $modelModulos->getById($modulo);
		$this->_view->curso = $modelcursos->getById($this->_view->modulo->cursos_id);
		$respuestas = $modelRespuestas->respondio($usuario,$modulo);
		$intentos = $respuestas[0]->respuesta_intentos*1;

		if(isset($respuestas[0]->respuesta_id) && ($this->_getSanitizedParam("nuevo")!=1 or $intentos>=$this->_view->oportunidades) ){
			$this->_view->respondido = true;
			$this->_view->preguntas = $respuestas;
		} else {
			$this->_view->preguntas = $modelPreguntas->getListPages("modulo_id = '$modulo' AND pregunta_activo='1' "," $orden1 ",0,$cantidad);
		}
		$this->_view->iduser = $usuario;
		$this->_view->intentos = $intentos;
		$this->_view->frases_aprobado = $modelFrases->getList(" cursos_mensajes_tipo='1' "," rand() ");
		$this->_view->frases_reprobado= $modelFrases->getList(" cursos_mensajes_tipo='2' "," rand() ");
		$this->_view->tipos = $modelPreguntas->getTipos();


	}
	public function responderAction()
	{
		$modelPreguntas = new Page_Model_DbTable_Preguntas();
		$modelRespuestas = new Page_Model_DbTable_Respuesta();
		$preguntas = $this->getRequest()->_getParam("pregunta");
		$modulo = 0;
		$intentos = $this->_getSanitizedParam("intentos");
		$intentos++;
		foreach ($preguntas as $pregunta) {
			$preguntaactual = $modelPreguntas->getById($pregunta);
			$respuesta = $this->_getSanitizedParam("pregunta_".$pregunta);
			$tipo = $preguntaactual->pregunta_tipo;

			if($tipo=="3"){
				$res="";
				for($i=0;$i<=4;$i++){
					$aux = $this->_getSanitizedParam("pregunta_".$pregunta."_".$i);
					if($aux!=""){
						$multiples_respuestas[]=$aux;
						$res.= $aux."|";
					}
				}
				$res = substr($res,0,-1);
				$respuesta = $res;
			}
			if(isset($preguntaactual->pregunta_id)){
				if($modulo == 0){
					$modulo = $preguntaactual->modulo_id;
					$modelRespuestas->eliminarRespuestas(Session::getInstance()->get("kt_login_id"),$modulo);
				}
				$data = array();
				$data['pregunta'] = $preguntaactual->pregunta_id;
				$data['respuesta'] = $respuesta;

				//validar respuestas
				if($tipo=="1" or $tipo=="2"){;
					$correcto  = '0';
					if($respuesta == $preguntaactual->pregunta_respuesta1 and $preguntaactual->pregunta_correcta1=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta2 and $preguntaactual->pregunta_correcta2=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta3 and $preguntaactual->pregunta_correcta3=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta4 and $preguntaactual->pregunta_correcta4=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta5 and $preguntaactual->pregunta_correcta5=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta6 and $preguntaactual->pregunta_correcta6=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta7 and $preguntaactual->pregunta_correcta7=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta8 and $preguntaactual->pregunta_correcta8=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta9 and $preguntaactual->pregunta_correcta9=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta10 and $preguntaactual->pregunta_correcta10=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta11 and $preguntaactual->pregunta_correcta11=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta12 and $preguntaactual->pregunta_correcta12=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta13 and $preguntaactual->pregunta_correcta13=="1"){
						$correcto = '1';
					}
					if($respuesta == $preguntaactual->pregunta_respuesta14 and $preguntaactual->pregunta_correcta14=="1"){
						$correcto = '1';
					}
				}
				if($tipo=="3"){
					$correcto  = '1';
					if(in_array($preguntaactual->pregunta_respuesta1,$multiples_respuestas) and $preguntaactual->pregunta_correcta1!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta2,$multiples_respuestas) and $preguntaactual->pregunta_correcta2!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta3,$multiples_respuestas) and $preguntaactual->pregunta_correcta3!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta4,$multiples_respuestas) and $preguntaactual->pregunta_correcta4!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta5,$multiples_respuestas) and $preguntaactual->pregunta_correcta5!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta6,$multiples_respuestas) and $preguntaactual->pregunta_correcta6!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta7,$multiples_respuestas) and $preguntaactual->pregunta_correcta7!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta8,$multiples_respuestas) and $preguntaactual->pregunta_correcta8!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta9,$multiples_respuestas) and $preguntaactual->pregunta_correcta9!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta10,$multiples_respuestas) and $preguntaactual->pregunta_correcta10!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta11,$multiples_respuestas) and $preguntaactual->pregunta_correcta11!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta12,$multiples_respuestas) and $preguntaactual->pregunta_correcta12!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta13,$multiples_respuestas) and $preguntaactual->pregunta_correcta13!="1"){
						$correcto = '0';
					}
					if(in_array($preguntaactual->pregunta_respuesta14,$multiples_respuestas) and $preguntaactual->pregunta_correcta14!="1"){
						$correcto = '0';
					}
					if(count($multiples_respuestas)==0){
						$correcto = '0';
					}
				}

				$data['correcta'] = $correcto;
				$data['fecha'] = date("Y:m:d");
				$data['hora'] = date("H:i:s");
				$data['usuario'] = Session::getInstance()->get("kt_login_id");
				$data['intentos']=$intentos;
				$modelRespuestas->insert($data);
				//print_r($data);
			}
		}
		header('Location: /page/cursos/evaluacion?modulo='.$modulo);
	}
}