<?php 

/**
*
*/

class Page_mainController extends Controllers_Abstract
{

	public $noaplica;
	public function init()
	{
		$modelContent = new Page_Model_DbTable_Content();
		$this->setLayout('page_page');
		if(Session::getInstance()->get("kt_login_id") < 0 || Session::getInstance()->get("kt_login_id","") == ''){
			header('Location: /page/login');
		} else {
			$modelUser = new Page_Model_DbTable_User();
			$user = $modelUser->getById(Session::getInstance()->get("kt_login_id"));
			$this->_view->user = $user;
			if($user->user_initial !=1 && $this->noaplica != 1){
				header('Location: /page/perfil/password?initial=1');
			}
		}
		$this->_view->sedes = $modelContent->getList("content_section = 'Sedes'"," orden ASC");
		$this->_view->enlaces = $modelContent->getList("content_section = 'Enlaces'"," orden ASC");
		$this->_view->redes = $modelContent->getList("content_section = 'Red Social'"," orden ASC");
		$perfil = $this->_view->getRoutPHP('modules/page/Views/partials/perfil.php');
		$this->getLayout()->setData("perfil",$perfil);
		$footer = $this->_view->getRoutPHP('modules/page/Views/partials/footer.php');
		$this->getLayout()->setData("footer",$footer);
	}

}