<?php 

/**
*
*/

class Page_gestionhumanaController extends Page_mainController
{

	public function indexAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->introduccion = $contentModel->getList("content_section = 'Introduccion Gestion' "," orden ASC");
		$this->_view->contenidos = $contentModel->getList("content_section = 'Beneficios'"," orden ASC");
		$this->_view->contenidos2 = $contentModel->getList("content_section = 'Campanias'"," orden ASC");
		$this->_view->contenidos3 = $contentModel->getList("content_section = 'Comunicados'"," orden ASC");
	}
	public function beneficiosAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$adicionalModel = new Page_Model_DbTable_Adicional();
		$this->_view->introduccion = $contentModel->getList("content_section = 'Introduccion Beneficios'"," orden ASC");
		$contenidos = $contentModel->getList("content_section = 'Beneficios'"," orden ASC");
		$arrayContenidos = array();
		foreach ($contenidos as $key => $contenido) {
			$id = $contenido->content_id;
			$arrayContenidos[$key]= array();
			$arrayContenidos[$key]['detalle'] = $contenido;
			$arrayContenidos[$key]['adicionales'] = $adicionalModel->getList("content_id= '$id' ","orden ASC");
		}
		$this->_view->contenidos = $arrayContenidos;
	}
	public function campaniasAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->introduccion = $contentModel->getList("content_section = 'Introduccion Campanias'"," orden ASC");
		$adicionalModel = new Page_Model_DbTable_Adicional();
		$contenidos = $contentModel->getList("content_section = 'Campanias'"," orden ASC");
		$arrayContenidos = array();
		foreach ($contenidos as $key => $contenido) {
			$id = $contenido->content_id;
			$arrayContenidos[$key]= array();
			$arrayContenidos[$key]['detalle'] = $contenido;
			$arrayContenidos[$key]['adicionales'] = $adicionalModel->getList("content_id= '$id' ","orden ASC");
		}
		$this->_view->contenidos = $arrayContenidos;

	}
	public function comunicadosAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$adicionalModel = new Page_Model_DbTable_Adicional();
		$contenidos = $contentModel->getList("content_section = 'Comunicados'"," orden ASC");
		$arrayContenidos = array();
		foreach ($contenidos as $key => $contenido) {
			$id = $contenido->content_id;
			$arrayContenidos[$key]= array();
			$arrayContenidos[$key]['detalle'] = $contenido;
			$arrayContenidos[$key]['adicionales'] = $adicionalModel->getList("content_id= '$id' ","orden ASC");
		}
		$this->_view->contenidos = $arrayContenidos;

	}
	public function galeriaAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->introduccion = $contentModel->getList("content_section = 'Introduccion Galeria'"," orden ASC");
		$modelAlbum = new Page_Model_DbTable_Album();
		$modelFotos = new Page_Model_DbTable_Fotos();
		$albumes = $modelAlbum->getList("","album_fecha DESC , orden ASC");
		$array = array();
		foreach ($albumes as $key => $album) {
			$array[$key]= array();
			$array[$key]['detalle'] = $album;
			$array[$key]['fotos'] = $modelFotos->getList("album_id = '".$album->album_id."'"," orden ASC ");

		}
		$this->_view->albumes = $array;
	}
	public function felicitamosAction()
	{
		
	}

}