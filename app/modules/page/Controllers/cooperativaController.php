<?php 

/**
*
*/

class Page_cooperativaController extends Page_mainController
{

	public function indexAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$adicionalModel = new Page_Model_DbTable_Adicional();
		$this->_view->introducion = $contentModel->getList("content_section = 'Introduccion Cooperativa'"," orden ASC")[0];
		$contenidos = $contentModel->getList("content_section = 'Nuestra Cooperativa'"," orden ASC");
		$arrayContenidos = array();
		foreach ($contenidos as $key => $contenido) {
			$id = $contenido->content_id;
			$arrayContenidos[$key]= array();
			$arrayContenidos[$key]['detalle'] = $contenido;
			$arrayContenidos[$key]['adicionales'] = $adicionalModel->getList("content_id= '$id' ","orden ASC");
		}
		$this->_view->contenidos = $arrayContenidos;
	}

}