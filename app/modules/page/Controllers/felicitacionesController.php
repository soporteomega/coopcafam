<?php 

/**
*
*/

class Page_felicitacionesController extends Page_mainController
{

	private $mainModel;

	public function init()
	{
		$this->mainModel = new Page_Model_DbTable_Calendar();
		parent::init();
	}

	public function indexAction()
	{
		$modelType = new Administracion_Model_DbTable_TypeCalendar();
		$modelDias = new Core_Model_Dias();
		$month = date("n");
		$year = date("Y");
		$fecha = "01-".$month."-".$year;
		$fechainicial = date ( 'Y-m-d', strtotime ( $modelDias->primerDiaMes($fecha)));
		$fechaifinal = date ( 'Y-m-d', strtotime ( $modelDias->ultimoDiaMes($fecha)));
		$tipos = $modelType->getList("type_calendar_type = 'Felicitacion'","");
		$array = array();
		$contador = 0;
		foreach ($tipos as $key => $tipo) {
			$type = $tipo->type_calendar_id;
			$events = $this->mainModel->getList(" calendar_startdate >= '$fechainicial' AND calendar_startdate <= '$fechaifinal' AND calendar_type_user = 'Empleados' AND calendar_type = '$type' ","calendar_startdate ASC");
			if(count($events)>0){
				$array[$contador] = array();
				$array[$contador]['detalle'] = $tipo;
				$array[$contador]['eventos'] = $events;
				$contador++;
			}
		}
		$this->_view->array = $array;
	}

	public function detailAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$id = $this->_getSanitizedParam("id");
		$this->mainModel->getById($id);
		$this->_view->content = $this->mainModel->getById($id);
		$idtype =$this->_view->content->calendar_type;
		$modelType = new Administracion_Model_DbTable_TypeCalendar();
		$type = $modelType->getById($idtype);
		$this->_view->type = $type;
		$this->_view->tarjetas = $contentModel->getList(" content_section = 'Tarjetas' "," orden ASC ");
		$this->_view->envio = $this->_getSanitizedParam("envio");
	}

	public function eventosdia($fecha)
	{
		
		$modelType = new Administracion_Model_DbTable_TypeCalendar();
		$fechainicial = $fecha;
		$events = $this->mainModel->getList(" calendar_startdate = '$fechainicial'  OR calendar_enddate = '$fechaifinal' ","");
		$arrayEvent = array();
		foreach ($events as $event) {
			if($event->calendar_startdate == $fechainicial ){
				$day = $event->calendar_starttime;
				$value = "inicio";
			} else {
				$day = $event->calendar_starttime;
				$value = "fin";
			}
			if(isset($arrayEvent[$day]) == false  ){
				$arrayEvent[$day] = array();
			}
			$infoday = $arrayEvent[$day];

			$calendar = array();
			$calendar['id'] = $event->calendar_id;
			$calendar['name'] = $event->calendar_name;
			$calendar['type'] = $event->calendar_type;
			$calendar['accion'] = $value;
			array_push($infoday,$calendar);
			$arrayEvent[$day] = $infoday;

			if($event->calendar_startdate == $fechainicial && $event->calendar_enddate == $fechainicial){
				$day = $event->calendar_endtime;
				$value = "fin";
				if(isset($arrayEvent[$day]) == false  ){
					$arrayEvent[$day] = array();
				}
				$infoday = $arrayEvent[$day];

				$calendar = array();
				$calendar['id'] = $event->calendar_id;
				$calendar['name'] = $event->calendar_name;
				$calendar['type'] = $event->calendar_type;
				$calendar['accion'] = $value;
				array_push($infoday,$calendar);
				$arrayEvent[$day] = $infoday;
				}
		}
		ksort($arrayEvent);
		$this->_view->eventosdia = $arrayEvent;
	}

	public function felicitarAction(){
		$data = array();
		$data['user'] = Session::getInstance()->get("kt_login_id");
		$data['mensaje'] = $this->_getSanitizedParam("mensaje");
		$data['calendar'] = $this->_getSanitizedParam("calendar");
		$data['tarjeta'] = $this->_getSanitizedParam("tarjeta");
		$data['date'] = date("Y-m-d");
		$modelFelicitacion = new Page_Model_DbTable_Felicitacion();
		$id = $modelFelicitacion->insert($data);
		$this->enviarFelicitacion($id);
		header("location: /page/felicitaciones/detail?id=".$this->_getSanitizedParam("calendar")."&envio=1");
	}

	public function correoAction(){
		$id = 5;
		$felicitacionModel = new Page_Model_DbTable_Felicitacion();
		$contentModel = new Page_Model_DbTable_Content();
		$userModel = new Page_Model_DbTable_User();
		$felicitacion = $felicitacionModel->getById($id);
		$this->_view->felicitacion = $felicitacion;
		$this->_view->usuario = $userModel->getById($felicitacion->felicitacion_user);
		$this->_view->tarjeta = $contentModel->getById($felicitacion->felicitacion_tarjeta);
		$this->_view->calendario = $this->mainModel->getList(" calendar_id = '".$felicitacion->felicitacion_calendar."' ","")[0];
		echo $content = $this->_view->getRoutPHP('/../app/modules/page/Views/templatesemail/felicitacion.php');
	}

	public function enviarFelicitacion($id)
	{
		$this->_view->url = "http://intranetcoopcafam.com.co";
		$email = new Core_Model_Mail();
		$felicitacionModel = new Page_Model_DbTable_Felicitacion();
		$contentModel = new Page_Model_DbTable_Content();
		$userModel = new Page_Model_DbTable_User();
		$felicitacion = $felicitacionModel->getById($id);
		$this->_view->felicitacion = $felicitacion;
		$this->_view->usuario = $userModel->getById($felicitacion->felicitacion_user);
		$this->_view->tarjeta = $contentModel->getById($felicitacion->felicitacion_tarjeta);
		$this->_view->calendario = $this->mainModel->getList(" calendar_id = '".$felicitacion->felicitacion_calendar."' ","")[0];
		$usuario = $userModel->getById($this->_view->calendario->calendar_user);
		$content = $this->_view->getRoutPHP('/../app/modules/page/Views/templatesemail/felicitacion.php');
        /*fin parametros de la vista */
        //$email->getMail()->addCC("desarrollo4@omegawebsystems.com","Omega");
        $email->getMail()->addAddress($usuario->user_email,  $usuario->user_names." ".$usuario->user_lastnames);
        $email->getMail()->Subject = "Felicitaciones Coopcafam";
        $email->getMail()->msgHTML($content);
        $email->getMail()->AltBody = $content;
        if ($email->sed()==true) {
        	echo "envio el mensaje";
            return true;
        } else {
        	echo "no envio el mensaje";
            return false;
        }
	}

}