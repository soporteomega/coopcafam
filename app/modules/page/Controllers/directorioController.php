<?php 

/**
*
*/

class Page_directorioController extends Page_mainController
{

	public function indexAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$adicionalModel = new Page_Model_DbTable_Adicional();
		$contenidos = $contentModel->getList("content_section = 'Directorio'"," orden ASC");
		$arrayContenidos = array();
		foreach ($contenidos as $key => $contenido) {
			$id = $contenido->content_id;
			$arrayContenidos[$key]= array();
			$arrayContenidos[$key]['detalle'] = $contenido;
			$arrayContenidos[$key]['adicionales'] = $adicionalModel->getList("content_id= '$id' ","orden ASC");
		}
		$this->_view->contenidos = $arrayContenidos;
	}

}