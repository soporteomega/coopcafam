<?php 

/**
*
*/

class Page_asociadosController extends Page_mainController
{

	public function indexAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->noticias = $contentModel->getList("content_section = 'Asociados'"," orden ASC");
		$this->_view->introduccion = $contentModel->getList("content_section = 'Introduccion Asociados'"," orden ASC");

	}
	public function detalleAction()
	{
		$id = $this->_getSanitizedParam("id");
		$contentModel = new Page_Model_DbTable_Content();
		$adicionalModel = new Page_Model_DbTable_Adicional();
		$contenidos = $contentModel->getList("content_id = '$id'"," orden ASC");
		$arrayContenidos = array();
		foreach ($contenidos as $key => $contenido) {
			$id = $contenido->content_id;
			$arrayContenidos[$key]= array();
			$arrayContenidos[$key]['detalle'] = $contenido;
			$arrayContenidos[$key]['adicionales'] = $adicionalModel->getList("content_id= '$id' ","orden ASC");
		}
		$this->_view->contenidos = $arrayContenidos;
	}
}