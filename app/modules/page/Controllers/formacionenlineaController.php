<?php 

/**
*
*/

class Page_formacionenlineaController extends Page_mainController
{

	public function indexAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->introduccion = $contentModel->getList("content_section = 'Introduccion Formacion'"," orden ASC");
		$hoy = date("Y-m-d H:i:s");
		$modelcursos = new Page_Model_DbTable_Cursos();
		$this->_view->cursos = $modelcursos->getList(" cursos_ocultar <> 1 AND cursos_inicio <='$hoy' AND cursos_fin >='$hoy'  "," orden ASC");
	}
	public function cursoAction()
	{
		$modelcursos = new Page_Model_DbTable_Cursos();
		$modelModulos = new Page_Model_DbTable_Modulos();
		$curso = $this->_getSanitizedParam("curso");
		$this->_view->curso = $modelcursos->getById($curso);
		$this->_view->modulos = $modelModulos->getList(" cursos_id = '$curso' "," orden ASC ");
	}

	public function moduloAction()
	{
		$modelcursos = new Page_Model_DbTable_Cursos();
		$modelModulos = new Page_Model_DbTable_Modulos();
		$modelSecciones = new Page_Model_DbTable_Seccionesmodulo();
		$modulo = $this->_getSanitizedParam("modulo");
		$this->_view->modulo = $modelModulos->getById($modulo);
		$this->_view->curso = $modelcursos->getById($this->_view->modulo->cursos_id);
		$this->_view->secciones = $modelSecciones->getList(" modulo_id = '$modulo' "," orden ASC ");
	}
	public function evaluacionAction()
	{
		$modelRespuestas = new Page_Model_DbTable_Respuesta();
		$modelcursos = new Page_Model_DbTable_Cursos();
		$modelModulos = new Page_Model_DbTable_Modulos();
		$modelPreguntas = new Page_Model_DbTable_Preguntas();
		$modulo = $this->_getSanitizedParam("modulo");
		$usuario = Session::getInstance()->get("kt_login_id");
		$this->_view->modulo = $modelModulos->getById($modulo);
		$this->_view->curso = $modelcursos->getById($this->_view->modulo->cursos_id);
		$respuestas = $modelRespuestas->respondio($usuario,$modulo);
		if(isset($respuestas[0]->respuesta_id) && $this->_getSanitizedParam("nuevo")!= 1 ){
			$this->_view->respondido = true;
			$this->_view->preguntas = $respuestas;
		} else {
			$this->_view->preguntas = $modelPreguntas->getListPages("modulo_id = '$modulo'"," RAND() ",0,10);
		}
	}
	public function responderAction()
	{
		$modelPreguntas = new Page_Model_DbTable_Preguntas();
		$modelRespuestas = new Page_Model_DbTable_Respuesta();
		$preguntas = $this->getRequest()->_getParam("pregunta");
		$modulo = 0;
		foreach ($preguntas as $pregunta) {
			$preguntaactual = $modelPreguntas->getById($pregunta);
			$respuesta = $this->_getSanitizedParam("pregunta_".$pregunta);
			if($respuesta!='' && isset($preguntaactual->pregunta_id)){
				if($modulo == 0){
					$modulo = $preguntaactual->modulo_id;
					$modelRespuestas->eliminarRespuestas(Session::getInstance()->get("kt_login_id"),$modulo);
				}
				$data = array();
				$data['pregunta'] = $preguntaactual->pregunta_id;
				$data['respuesta'] = $respuesta;
				if($respuesta == $preguntaactual->pregunta_respuesta1){
					$correcto = '1';
				} else {
					$correcto  = '0';
				}
				$data['correcta'] = $correcto;
				$data['usuario'] = Session::getInstance()->get("kt_login_id");
				$modelRespuestas->insert($data);
				
			}
		}
		header('Location: /page/cursos/evaluacion?modulo='.$modulo);
	}
}