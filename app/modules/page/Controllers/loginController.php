<?php 

/**
*
*/

class Page_loginController extends Controllers_Abstract
{

	public function indexAction()
	{
		$this->setLayout('page_login');
		$this->_view->message = $this->_getSanitizedParam("message");
		if(Session::getInstance()->get("kt_login_id") > 0 || Session::getInstance()->get("kt_login_id","") != ''){
			header('Location: /page');
		}
	}

	public function oldpasswordAction()
	{
		$this->setLayout('page_login');
		$this->_view->message = $this->_getSanitizedParam("message");
		if(Session::getInstance()->get("kt_login_id") > 0 || Session::getInstance()->get("kt_login_id","") != ''){
			header('Location: /page');
		}
	}


	public function olvidoAction()
	{
		$emailModel = new Core_Model_Mail();
        $userModel = new Core_Model_DbTable_User;
        $sendemailModel = new Core_Model_Sendingemail($this->_view);
        $email = $this->_getSanitizedParam("email");
        $csrf = $this->_getSanitizedParam("csrf");
        if ($csrf == Session::getInstance()->get('csrf') ) {
        	$users =  $userModel->getList("user_email='$email'","");
        	if(isset($users[0])){
        		$userModel->changeCode($users[0]->user_id,$csrf);
        		$user = $userModel->getById($users[0]->user_id);
		     	if ($sendemailModel->forgotpassword($user)==true) {
                    $message = 1;
                } else {
                    $message = 2;
                }
	    	} else {
	    		$message = 3;
	    	}
	    } else {
	    	$message =3;
	    }
	    header('Location: /page/login/oldpassword?message='.$message);
	}

	public function changepasswordAction()
    {
        $this->setLayout('page_login');
        $user = $this->validateCode();
        if (isset($user['error'])) {
            if ($user['error'] == 1) {
                $this->_view->error = "Lo sentimos este codigo ya fue utilizado.";
            } else {
                $this->_view->error = "La información Suministrada es invalida.";
            }
        } else {
            $csrf = $this->_getSanitizedParam("csrf");
            $password = $this->_getSanitizedParam("password");
            $re_password = $this->_getSanitizedParam("re_password");
            if ($this->getRequest()->isPost() == true && $csrf == Session::getInstance()->get('csrf') && $password == $re_password) {
                $id_user = $user['user']->user_id;
                $modelUser = new Core_Model_DbTable_User();
                $modelUser->changepassword($password,$id_user);
                $modelUser->changeCode($id_user, $csrf);
                $this->_view->message = "Sea cambiado su contraseña satisfactoriamente.";
            } else {
                $this->_view->code = $this->getRequest()->_getParam("code");
                $this->_view->usuario = $user['user']->user_user;
                $this->_view->csrf = Session::getInstance()->get('csrf');
            }
        }
    }

	public function loginAction()
	{
		$this->setLayout('blanco');
		header('Content-Type:application/json');
		$isPost = $this->getRequest()->isPost();
		$user= $this->_getSanitizedParam("user");
		$password = $this->_getSanitizedParam("password");
		$csrf = $this->_getSanitizedParam("csrf");
		$isError = false;
		$busco = "no";
		if($user && $password && Session::getInstance()->get('csrf') == $csrf ){
			$userModel = new Core_Model_DbTable_User();
			$busco = "si";
			if ($userModel->autenticateUser($user,$password) == true) {
				$resUser = $userModel->searchUserByUser($user);
				Session::getInstance()->set("kt_login_id",$resUser->user_id);
				Session::getInstance()->set("kt_login_level",$resUser->user_level);
				Session::getInstance()->set("kt_login_user",$resUser->user_user);
				Session::getInstance()->set("kt_login_name",$resUser->user_names." ".$resUser->user_lastnames);
                $logModel = new Core_Model_DbTable_Log();
                $logModel->log($resUser->user_id);
			} else {
				$isError = true;
			}
		} else {
			$isError = true;
		}
		echo $this->_view->array = json_encode(array(
			"isError" => $isError,
			"Busco"  => $busco
		));
	}

	public function logoutAction()
	{
		Session::getInstance()->set("kt_login_id","");
		Session::getInstance()->set("kt_login_level","");
		Session::getInstance()->set("kt_login_user","");
		Session::getInstance()->set("kt_login_name","");
		header('Location: /page/login');
	}

	 /**
     * valida el codigo del usuario
     * @return int  valida el codigo del usuario
     */
    private function validateCode()
    {
        $res = [];
        $code =  base64_decode($this->getRequest()->_getParam("code"));
        if (isset($code) && $this->isJson($code)== true) {
            $code = json_decode($code, true);
            $modelUser = new Core_Model_DbTable_User();
            if (isset($code['user'])) {
                $user = $modelUser->searchUser($code['user']);
                if (isset($user[0])) {
                    if ($user[0]->code == $code['code']) {
                        $res['user'] = $user[0];
                    } else {
                        $res['error'] =  1;
                        $res['user'] = $user[0];
                    }
                } else {
                    $res['error'] =  2;
                }
            } else {
                $res['error'] =  3;
            }
        } else {
            $res['error'] =  4;
        }
        return $res;
    }

    /**
     * verifica si una cadena es de tipo json
     * @param  string  $string cadena a evaluar
     * @return boolean    resultado de la evaluacion
     */
    private function isJson($string)
    {
        return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string))))) ? true : false;
    }

}