<?php 

/**
*
*/

class Page_perfilController extends Page_mainController
{

	private $mainModel;
	public $noaplica;

	public function init()
	{
		$this->edituser = 1;
		$this->noaplica = 1;
		$this->mainModel = new Page_Model_DbTable_User();
		parent::init();
	}

	public function indexAction()
	{
		$this->_view->user = $this->mainModel->getById(Session::getInstance()->get("kt_login_id"));
		$this->_view->actualizo = $this->_getSanitizedParam("actualizo");
	}

	public function editarAction()
	{
		$cargoModel = new Page_Model_DbTable_Cargo();
		$this->_view->user = $this->mainModel->getById(Session::getInstance()->get("kt_login_id"));
		$this->_view->cargos = $cargoModel->getList("","");

	}

	public function actualizarAction()
	{
		$user = $this->mainModel->getById(Session::getInstance()->get("kt_login_id"));
		$uploadImage =  new Core_Model_Upload_Image();
		$this->edituser =1;
		$data = array();
		$data['names'] = $this->_getSanitizedParam("names");
		$data['lastnames'] = $this->_getSanitizedParam("lastnames");
		$data['email'] = $this->_getSanitizedParam("email");
		$data['idnumber'] = $this->_getSanitizedParam("idnumber");
		$data['city'] = $this->_getSanitizedParam("city");
		$data['country'] = $this->_getSanitizedParam("country");
		$data['phone'] = $this->_getSanitizedParam("phone");
		$data['address'] = $this->_getSanitizedParam("address");
		$data['position'] = $this->_getSanitizedParam("position");
		$data['current_user'] = Session::getInstance()->get("kt_login_id");
		$data['photo'] = '';
		if($_FILES['photo']['name'] != ''){
			if($user->user_photo !=''){
				$uploadImage->delete($user->user_photo);
			}
			$data['photo'] = $uploadImage->upload("photo");
		} else if($user->user_photo){
			$data['photo'] = $user->user_photo;
		}
		$this->mainModel->update($data,Session::getInstance()->get("kt_login_id"));
		header('Location: /page/perfil?actualizo=1');
	}

	public function passwordAction()
	{
		if($this->_getSanitizedParam("initial") == 1){
			$this->_view->error = "Por ser tu primer ingreso debes de cambiar la contraseña. ";
		} else {
			$this->_view->error = $this->_getSanitizedParam("error");
		}
		
	}
	public function changepasswordAction(){
		$user = $this->mainModel->getById(Session::getInstance()->get("kt_login_id"));
		$password = $this->_getSanitizedParam("current_password");
		if(password_verify($password,$user->user_password)){
			$newpassword = $this->_getSanitizedParam("password");
			$this->mainModel->changepassword($newpassword,$user->user_id);
			header("location: /page/perfil?actualizo=2");
        }else{
            header("location: /page/perfil/password?error=1");
        }
	}

}