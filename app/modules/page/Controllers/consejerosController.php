<?php 

/**
*
*/

class Page_consejerosController extends Page_mainController
{

	public function indexAction()
	{
		$contentModel = new Page_Model_DbTable_Consejero();
		$adicionalModel = new Page_Model_DbTable_Adicionalconsejero();
		$this->_view->introducion = $contentModel->getList("content_section = 'Introduccion'"," orden ASC")[0];
		$contenidos = $contentModel->getList("content_section = 'Consejero'"," orden ASC");
		$arrayContenidos = array();
		foreach ($contenidos as $key => $contenido) {
			$id = $contenido->content_id;
			$arrayContenidos[$key]= array();
			$arrayContenidos[$key]['detalle'] = $contenido;
			$arrayContenidos[$key]['adicionales'] = $adicionalModel->getList("content_id='$id' ","orden ASC");
		}
		$this->_view->contenidos = $arrayContenidos;
	}

}