<?php 

/**
*
*/

class Page_indexController extends Page_mainController
{

	public function indexAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->banners = $contentModel->getList("content_section = 'Banner Home'"," orden ASC");
		$this->_view->introduccion = $contentModel->getList("content_section = 'Introduccion Home'"," orden ASC")[0];
		$this->_view->direccionamiento = $contentModel->getList("content_section = 'Direccionamiento'"," orden ASC")[0];
		$this->_view->noticias = $contentModel->getList("content_section = 'Noticias' OR content_section = 'Asociados'"," content_date DESC ");
		$modelDias = new Core_Model_Dias();
		$month = date("n");
		$year = date("Y");
		$fecha = "01-".$month."-".$year;
		$fechainicial = date ( 'Y-m-d', strtotime ( $modelDias->primerDiaMes($fecha)));
		$fechaifinal = date ( 'Y-m-d', strtotime ( $modelDias->ultimoDiaMes($fecha)));
		$modelCalendar = new Page_Model_DbTable_Calendar();
		$this->_view->events = $modelCalendar->getListPages(" calendar_startdate >= '$fechainicial' AND calendar_startdate <= '$fechaifinal' AND calendar_type_user = 'general' AND type_calendar_type = 'Felicitacion' ","rand()",0,3);
	}

}