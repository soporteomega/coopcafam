<?php 

/**
*
*/

class Page_calendarController extends Page_mainController
{

	private $mainModel;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Calendar();
		parent::init();
	}

	public function indexAction()
	{
		$modelType = new Administracion_Model_DbTable_TypeCalendar();
		$types = $modelType->getList("","");
		$arrayTypes = array();
		foreach ($types as $value) {
			$type = array();
			$type['id'] = $value->type_calendar_id;
			$type['name'] = $value->type_calendar_name;
			$type['color'] = $value->type_calendar_color;
			$arrayTypes[$type['id']] = $type;
		}
		$this->_view->types = $arrayTypes;
		$month = date("n");
		$year = date("Y");
		if($this->_getSanitizedParam("month")!='' && $this->_getSanitizedParam("year")!='' && $this->_getSanitizedParam("month") >= 1 && $this->_getSanitizedParam("month") <=12){
			$month = $this->_getSanitizedParam("month");
			$year = $this->_getSanitizedParam("year");
		}
		$this->_view->month = $month;
		$this->_view->year = $year;
		if($month == date('n') && $year == date("Y")){
			$this->_view->day = date("j");
		}
		$modelDias = new Core_Model_Dias();
		$fecha = "01-".$month."-".$year;
		$mesanterior= strtotime ( '-1 month' , strtotime ( $fecha )) ;
		$this->_view->lastyear = date ( 'Y' , $mesanterior );
		$this->_view->lastmonth = date ( 'n' , $mesanterior );
		$messiguiente = strtotime ( '+1 month' , strtotime ( $fecha )) ;
		$this->_view->nextyear = date ( 'Y' , $messiguiente );
		$this->_view->nextmonth = date ( 'n' , $messiguiente );
		$messiguiente = date ( 'Y' , $nuevafecha );
		$fechainicial = date ( 'Y-m-d', strtotime ( $modelDias->primerDiaMes($fecha)));
		$fechaifinal = date ( 'Y-m-d', strtotime ( $modelDias->ultimoDiaMes($fecha)));
		$events = $this->mainModel->getList(" calendar_startdate >= '$fechainicial' AND calendar_startdate <= '$fechaifinal' ","");
		$arrayEvent = array();
		foreach ($events as $event) {
			$day = (int) date ('d',strtotime ($event->calendar_startdate));
			if(isset($arrayEvent[$day]) == false  ){
				$arrayEvent[$day] = array();
			}
			$infoday = $arrayEvent[$day];

			$calendar = array();
			$calendar['id'] = $event->calendar_id;
			$calendar['name'] = $event->calendar_name;
			$calendar['type'] = $event->calendar_type;
			array_push($infoday,$calendar);
			$arrayEvent[$day] = $infoday;
		}
		$this->_view->events = $arrayEvent;
		$this->eventosdia(date("Y-m-d"));
	}

	public function detailAction()
	{
		$id = $this->_getSanitizedParam("id");
		$this->mainModel->getById($id);
		$this->_view->content = $this->mainModel->getById($id);
		$idtype =$this->_view->content->calendar_type;
		$modelType = new Administracion_Model_DbTable_TypeCalendar();
		$type = $modelType->getById($idtype);
		$this->_view->type = $type;
	}

	public function eventosdia($fecha)
	{
		
		$modelType = new Administracion_Model_DbTable_TypeCalendar();
		$fechainicial = $fecha;
		$events = $this->mainModel->getList(" calendar_startdate = '$fechainicial'  OR calendar_enddate = '$fechaifinal' ","");
		$arrayEvent = array();
		foreach ($events as $event) {
			if($event->calendar_startdate == $fechainicial ){
				$day = $event->calendar_starttime;
				$value = "inicio";
			} else {
				$day = $event->calendar_starttime;
				$value = "fin";
			}
			if(isset($arrayEvent[$day]) == false  ){
				$arrayEvent[$day] = array();
			}
			$infoday = $arrayEvent[$day];

			$calendar = array();
			$calendar['id'] = $event->calendar_id;
			$calendar['name'] = $event->calendar_name;
			$calendar['type'] = $event->calendar_type;
			$calendar['accion'] = $value;
			array_push($infoday,$calendar);
			$arrayEvent[$day] = $infoday;

			if($event->calendar_startdate == $fechainicial && $event->calendar_enddate == $fechainicial){
				$day = $event->calendar_endtime;
				$value = "fin";
				if(isset($arrayEvent[$day]) == false  ){
					$arrayEvent[$day] = array();
				}
				$infoday = $arrayEvent[$day];

				$calendar = array();
				$calendar['id'] = $event->calendar_id;
				$calendar['name'] = $event->calendar_name;
				$calendar['type'] = $event->calendar_type;
				$calendar['accion'] = $value;
				array_push($infoday,$calendar);
				$arrayEvent[$day] = $infoday;
				}
		}
		ksort($arrayEvent);
		$this->_view->eventosdia = $arrayEvent;
	}

}