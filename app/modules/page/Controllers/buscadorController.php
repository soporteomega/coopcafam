<?php 

/**
*
*/

class Page_buscadorController extends Page_mainController
{

	public function indexAction()
	{
		$categoriasModel = new Page_Model_DbTable_Categoriadocumentos();
		$categorias = $categoriasModel->getList("","");
		$busqueda = $this->_getSanitizedParam("buscar");
		$this->_view->buscar= $busqueda;
		if($busqueda !=''){
			$this->_view->categorias = $this->getCategoria();

			$this->_view->cooperativa = $this->buscarContenidos("Nuestra Cooperativa",$busqueda);

			$gestion =array();
			$gestion['Beneficios'] = $this->buscarContenidos("Beneficios",$busqueda);
			$gestion['Campañas'] = $this->buscarContenidos("Campanias",$busqueda);

			if(count($gestion['Beneficios'])>0 || count($gestion['Campañas'])>0 ){
				$this->_view->gestion = $gestion;
			}
			$this->_view->noticias = $this->buscarContenidos("Noticias",$busqueda);
			$this->_view->asociados = $this->buscarContenidos("Asociados",$busqueda);
			$arraysgc = array();
			foreach ($categorias as $key => $categoria) {
				$documentos = $this->buscarDocumentos($busqueda,$categoria->categoria_documento_id);
				if($documentos[0]->documento_id){
					$arraysgc[$categoria->categoria_documento_id] = array();
					$arraysgc[$categoria->categoria_documento_id]['detalle'] = $categoria;
					$arraysgc[$categoria->categoria_documento_id]['documentos'] = $documentos;
				}
			}
			$this->_view->sgc = $arraysgc;
		}
	}

	public function getCategoria()
	{
		$categorias = array();
		$categorias[1] = "Nuestra Cooperativa";
		$categorias[2] = "Gestión Humana";
		$categorias[3] = "SGC";
		$categorias[4] = "Formacion en línea";
		$categorias[5] = "Noticias";
		$categorias[6] = "Información para nuestros asociados";
		return $categorias;
	}

	public function buscarContenidos($section,$palabra){
		$contentModel = new Page_Model_DbTable_Content();
		$filter = "content_section = '$section' AND ( content_title LIKE '%$palabra%' OR content_subtitle LIKE '%$palabra%' OR content_introduction LIKE '%$palabra%' OR content_description LIKE '%$palabra%' OR EXISTS( SELECT * FROM adicional WHERE adicional.content_id = content.content_id AND adicional_titulo LIKE '%$palabra%' ) )";
		$content = $contentModel->getList($filter,"orden ASC");
		return $content;
	}

	public function buscarDocumentos($palabra,$categoria){
		$documentoModel = new Page_Model_DbTable_Documento();
		$documentos = $documentoModel->getList("documento_nombre LIKE '%$palabra%' AND documento_categoria = '$categoria' ","orden ASC");
		return $documentos;
	}

}