<?php

/**
*
*/

class Page_solicitudesenlineaController extends Page_mainController
{

	public function indexAction()
	{
		$contentModel = new Page_Model_DbTable_Content();
		$this->_view->introduccion = $contentModel->getList("content_section = 'Introduccion Solicitud'"," orden ASC");
		$tipoModel = new Page_Model_DbTable_Tipogestion();
		$this->_view->envio= $this->_getSanitizedParam("envio");
		$this->_view->gestiones = $tipoModel->getList("","tipo_gestion_nombre ASC");
	}

	public function insertAction()
	{
		$this->setLayout('blanco');
		$modelSolicitud = new Page_Model_DbTable_Solicitud();
		$data = $this->getData();
		$id = $modelSolicitud->insert($data);
		$sendemailModel = new Core_Model_Sendingemail($this->_view);
		$this->_view->solicitud = $modelSolicitud->getList("solicitud_id = '".$id."'","")[0];
		$sendemailModel->solicitudlinea();
		header('Location: /page/solicitudesenlinea?envio=1');
	}

	protected function getData()
	{
		$data = array();
		$data['fecha']= date("Y:m:d");
		$data['idusuario']= Session::getInstance()->get("kt_login_id");
		$data['nombre']= $this->_getSanitizedParam('nombre');
		$data['correo']= $this->_getSanitizedParam('correo');
		$data['tipo']= $this->_getSanitizedParam('tipo');
		$data['direccion']= $this->_getSanitizedParam('direccion');
		$data['ciudad']= $this->_getSanitizedParam('ciudad');
		$data['telefono']= $this->_getSanitizedParam('telefono');
		$data['celular']= $this->_getSanitizedParam('celular');
		$data['solicitud']= $this->_getSanitizedParam('solicitud');
		$data['estado']= 0;
		return $data;
	}

}