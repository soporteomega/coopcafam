<?php 

/**
*
*/

class generator_indexController extends Controllers_Abstract
{

	protected $_csrf_section = "login_admin";

	public function init(){
		$this->_view->namedatabase = Config_Config::getInstance()->getValue('db/name');
	}

	public function indexAction()
	{
		$this->setLayout('generator');
		$modelTables = new Generator_Model_DbTable_Tables();
		$this->_view->tablas = $modelTables->getTables();
		$table = $this->_getSanitizedParam("table");
		$this->_view->table = $table;
		$this->_view->campos = $modelTables->getCampos($table);
		$this->_view->tipos = $this->getTipos();
	}
	
	public function getTipos(){
		$data = [];
		$data[1] = 'Input';
		$data[2] = 'Textarea';
		$data[3] = 'Select';
		$data[4] = 'Check Box (Si,No)';
		$data[5] = 'Upload Image';
		$data[6] = 'Upload File';
		$data[7] = 'Oculto';
		return $data;
	}

	public function crearAction()
	{
		$modelTables = new Generator_Model_DbTable_Tables();
		$table = $this->_getSanitizedParam("table");
		$campos = $modelTables->getCampos($table);
		$data = $this->getDatacolum($campos);
		$info = $this->_getSanitizedParam("controlador");
		$this->crearControlador($data,$campos);
		$this->crearModelo($data,$campos);
		$this->crearVistaindex($data,$campos);
		$this->crearVistamanage($data,$campos);
		header('Location: /generator');
	}

	public function getDatacolum($campos){
		$data = [];
		$orden = [];
		foreach ($campos as $key => $campo) {
			if($campo->Key != 'PRI'){
				$data[$key] = [];
				$data[$key]['nombre'] = $campo->Field;
				$data[$key]['tipo_dato'] = $campo->Type;
				$data[$key]['requerido'] = $this->_getSanitizedParam("requerido_".$campo->Field);
				$data[$key]['titulo'] = $this->_getSanitizedParam("titulo_".$campo->Field);
				$data[$key]['tipo'] = $this->_getSanitizedParam("tipo_".$campo->Field);
				$data[$key]['en_listado'] = $this->_getSanitizedParam("en_listado_".$campo->Field);
				$data[$key]['ancho'] = $this->_getSanitizedParam("ancho_".$campo->Field);
				$data[$key]['orden'] = $this->_getSanitizedParam("orden_".$campo->Field);
				$orden[$key] = $data[$key]['orden'];
				if($campo->Field == 'orden' ){
					$data[$key]['ordenar'] = 1; 
				}
			} 
		}
		array_multisort($orden, SORT_ASC, $data);
		return $data;
	}

	public function crearControlador($data,$campos){
		$controlador = strtolower($this->_getSanitizedParam('controlador'));
		$modelo = strtolower($this->_getSanitizedParam("ruta"));
		$ruta = APP_PATH."/modules/".$modelo."/Controllers/".$controlador."Controller.php";
		$nombrecontroller = ucwords($modelo)."_".$controlador."Controller";
		$nuevoarchivo = fopen($ruta, "w+");
		$ordenar = $this->getorden($data);
		$titulolistar = $this->_getSanitizedParam('titulo_listado'); 
		$tituloeditar = $this->_getSanitizedParam('titulo_edicion');
		$images = $this->getFilesimages($data);
		$documents = $this->getFilesdocument($data);
		$identificador = $this->getKey($campos)->Field;
		$php = "<?php
/**
* Controlador de ".ucwords($controlador)." que permite la  creacion, edicion  y eliminacion de los ".$titulolistar." del Sistema
*/
class ".$nombrecontroller." extends ".ucwords($modelo)."_mainController
{
	/**
	 * \$mainModel  instancia del modelo de  base de datos ".$titulolistar."
	 * @var modeloContenidos
	 */
	private \$mainModel;

	/**
	 * \$route  url del controlador base
	 * @var string
	 */
	private \$route;

	/**
	 * \$pages cantidad de registros a mostrar por pagina]
	 * @var integer
	 */
	private \$pages ;

	/**
	 * \$namefilter nombre de la variable a la fual se le van a guardar los filtros
	 * @var string
	 */
	protected \$namefilter;

	/**
	 * \$_csrf_section  nombre de la variable general csrf  que se va a almacenar en la session
	 * @var string
	 */
	protected \$_csrf_section = \"".$modelo."_".$controlador."\";

	/**
	 * \$namepages nombre de la pvariable en la cual se va a guardar  el numero de seccion en la paginacion del controlador
	 * @var string
	 */
	protected \$namepages;



	/**
     * Inicializa las variables principales del controlador ".$controlador." .
     *
     * @return void.
     */
	public function init()
	{
		\$this->mainModel = new ".ucwords($modelo)."_Model_DbTable_".ucwords($controlador)."();
		\$this->namefilter = \"parametersfilter".$controlador."\";
		\$this->route = \"/".$modelo."/".$controlador."\";
		\$this->namepages =\"pages_".$controlador."\";
		\$this->_view->route = \$this->route;
		if(Session::getInstance()->get(\$this->namepages)){
			\$this->pages = Session::getInstance()->get(\$this->namepages);
		} else {
			\$this->pages = 20;
		}
		parent::init();
	}


	/**
     * Recibe la informacion y  muestra un listado de  ".$titulolistar." con sus respectivos filtros.
     *
     * @return void.
     */
	public function indexAction()
	{
		\$this->setLayout('administracion_panel');
		\$this->getLayout()->setTitle(\"Listar ".$titulolistar."\");
		\$this->filters();
		\$this->_view->csrf = Session::getInstance()->get('csrf')[\$this->_csrf_section];
		\$filters =(object)Session::getInstance()->get(\$this->namefilter);
        \$this->_view->filters = \$filters;
		\$filters = \$this->getFilter();
		\$order = \"".$ordenar."\";
		\$list = \$this->mainModel->getList(\$filters,\$order);
		\$amount = \$this->pages;
		\$page = \$this->_getSanitizedParam(\"page\");
		if (!\$page) {
		   	\$start = 0;
		   	\$page=1;
		}
		else {
		   	\$start = (\$page - 1) * \$amount;
		}
		\$this->_view->register_number = count(\$list);
		\$this->_view->pages = \$this->pages;
		\$this->_view->totalpages = ceil(count(\$list)/\$amount);
		\$this->_view->page = \$page;
		\$this->_view->lists = \$this->mainModel->getListPages(\$filters,\$order,\$start,\$amount);
		\$this->_view->csrf_section = \$this->_csrf_section;";
		foreach ($data as $key => $value) {
		if($value['tipo'] == 3 && $value['en_listado'] == 1){
			$nombreSelect = ucwords(str_replace("_","",$value['nombre']));
			$php = $php."
		\$this->_view->list_".$value['nombre']." = \$this->get".$nombreSelect."();";
			}
		}
		$php= $php."
	}

	/**
     * Genera la Informacion necesaria para editar o crear un  ".$tituloeditar."  y muestra su formulario
     *
     * @return void.
     */
	public function manageAction()
	{
		\$this->_view->route = \$this->route;
		\$this->setLayout('administracion_panel');
		\$this->_csrf_section = \"manage_".$controlador."_\".date(\"YmdHis\");
		\$this->_csrf->generateCode(\$this->_csrf_section);
		\$this->_view->csrf_section = \$this->_csrf_section;
		\$this->_view->csrf = Session::getInstance()->get('csrf')[\$this->_csrf_section];";
		foreach ($data as $key => $value) {
		if($value['tipo'] == 3 ){
			$nombreSelect = ucwords(str_replace("_","",$value['nombre']));
			$php = $php."
		\$this->_view->list_".$value['nombre']." = \$this->get".$nombreSelect."();";
			}
		}
		$php = $php."
		\$id = \$this->_getSanitizedParam(\"id\");
		if (\$id > 0) {
			\$content = \$this->mainModel->getById(\$id);
			if(\$content->".$identificador."){
				\$this->_view->content = \$content;
				\$this->_view->routeform = \$this->route.\"/update\";
				\$this->getLayout()->setTitle(\"Actualizar ".$tituloeditar."\");
			}else{
				\$this->_view->routeform = \$this->route.\"/insert\";
				\$this->getLayout()->setTitle(\"Crear ".$tituloeditar."\");
			}
		} else {
			\$this->_view->routeform = \$this->route.\"/insert\";
			\$this->getLayout()->setTitle(\"Crear ".$tituloeditar."\");
		}
	}

	/**
     * Inserta la informacion de un ".$tituloeditar."  y redirecciona al listado de ".$titulolistar.".
     *
     * @return void.
     */
	public function insertAction(){
		\$csrf = \$this->_getSanitizedParam(\"csrf\");
		if (Session::getInstance()->get('csrf')[\$this->_getSanitizedParam(\"csrf_section\")] == \$csrf ) {	
			\$data = \$this->getData();";
		if(count($images)>0){
			$php = $php."
			\$uploadImage =  new Core_Model_Upload_Image();";
			foreach ($images as $key => $image) {
				$php = $php."
			if(\$_FILES['".$image."']['name'] != ''){
				\$data['".$image."'] = \$uploadImage->upload(\"".$image."\");
			}";
			}
		}
		if(count($documents)>0){
			$php = $php."
			\$uploadDocument =  new Core_Model_Upload_Document();";
			foreach ($documents as $key => $document) {
				$php = $php."
			if(\$_FILES['".$document."']['name'] != ''){
				\$data['".$document."'] = \$uploadDocument->upload(\"".$document."\");
			}";
			}
		}

			$php = $php."
			\$id = \$this->mainModel->insert(\$data);
			";
			if($ordenar == "orden ASC" ){
			$php = $php."\$this->mainModel->changeOrder(\$id,\$id);";
			}
			$php = $php."
		}
		header('Location: '.\$this->route);
	}

	/**
     * Recibe un identificador  y Actualiza la informacion de un ".$tituloeditar."  y redirecciona al listado de ".$titulolistar.".
     *
     * @return void.
     */
	public function updateAction(){
		\$csrf = \$this->_getSanitizedParam(\"csrf\");
		if (Session::getInstance()->get('csrf')[\$this->_getSanitizedParam(\"csrf_section\")] == \$csrf ) {
			\$id = \$this->_getSanitizedParam(\"id\");
			\$content = \$this->mainModel->getById(\$id);
			if (\$content->".$identificador.") {
				\$data = \$this->getData();
				";
			if(count($images)>0){
			$php = $php."\$uploadImage =  new Core_Model_Upload_Image();";
			foreach ($images as $key => $image) {
				$php = $php."
				if(\$_FILES['".$image."']['name'] != ''){
					if(\$content->".$image."){
						\$uploadImage->delete(\$content->".$image.");
					}
					\$data['".$image."'] = \$uploadImage->upload(\"".$image."\");
				} else {
					\$data['".$image."'] = \$content->".$image.";
				}
			";
			}
		}
		if(count($documents)>0){
			$php = $php."	\$uploadDocument =  new Core_Model_Upload_Document();";
			foreach ($documents as $key => $document) {
				$php = $php."
				if(\$_FILES['".$document."']['name'] != ''){
					if(\$content->".$document."){
						\$uploadDocument->delete(\$content->".$document.");
					}
					\$data['".$document."'] = \$uploadDocument->upload(\"".$document."\");
				} else {
					\$data['".$document."'] = \$content->".$document.";
				}
			";
			}
		}
				$php = $php."	\$this->mainModel->update(\$data,\$id);
			}
		}
		header('Location: '.\$this->route);
	}

	/**
     * Recibe un identificador  y elimina un ".$tituloeditar."  y redirecciona al listado de ".$titulolistar.".
     *
     * @return void.
     */
	public function deleteAction()
	{
		\$csrf = \$this->_getSanitizedParam(\"csrf\");
		if (Session::getInstance()->get('csrf')[\$this->_csrf_section] == \$csrf ) {
			\$id =  \$this->_getSanitizedParam(\"id\");
			if (isset(\$id) && \$id > 0) {
				\$content = \$this->mainModel->getById(\$id);
				if (isset(\$content)) {
					";
				if(count($images)>0){
					$php = $php."\$uploadImage =  new Core_Model_Upload_Image();";
					foreach ($images as $key => $image) {
					$php = $php."
					if (isset(\$content->".$image.") && \$content->".$image." != '') {
						\$uploadImage->delete(\$content->".$image.");
					}
					";
					}
				}
				if(count($documents)>0){
					$php = $php."\$uploadDocument =  new Core_Model_Upload_Document();";
					foreach ($documents as $key => $document) {
					$php = $php."
					if (isset(\$content->".$document.") && \$content->".$document." != '') {
						\$uploadDocument->delete(\$content->".$document.");
					}
					";
					}
				}
					$php = $php."\$this->mainModel->deleteRegister(\$id);
				}
			}
		}
		header('Location: '.\$this->route);
	}

	/**
     * Recibe la informacion del formulario y la retorna en forma de array para la edicion y creacion de ".ucwords($controlador).".
     *
     * @return array con toda la informacion recibida del formulario.
     */
	private function getData()
	{
		\$data = array();";
		foreach ($data as $key => $value) {
			if($value['ordenar']!= 1){
				if($value['tipo']== 5 || $value['tipo']== 6){
			$php = $php."
		\$data['".$value['nombre']."'] = \"\";";
				} else if ($value['tipo']== 2 ){
					$php = $php."
		\$data['".$value['nombre']."'] = \$this->_getSanitizedParamHtml(\"".$value['nombre']."\");";
				} else {
					if(strpos($value['tipo_dato'],"int") !== false || strpos($value['tipo_dato'],"float") !== false || strpos($value['tipo_dato'],"double") !== false   ){
						$php = $php."
		if(\$this->_getSanitizedParam(\"".$value['nombre']."\") == '' ) {
			\$data['".$value['nombre']."'] = '0';
		} else {
			\$data['".$value['nombre']."'] = \$this->_getSanitizedParam(\"".$value['nombre']."\");
		}";
					} else {
						$php = $php."
		\$data['".$value['nombre']."'] = \$this->_getSanitizedParam(\"".$value['nombre']."\");";
					}
				}
			}
		}
		$php = $php."
		return \$data;
	}";

		foreach ($data as $key => $value) {
			if($value['tipo'] == 3){
				$nombreSelect = ucwords(str_replace("_","",$value['nombre']));
		$php = $php."

	/**
     * Genera los valores del campo ".$value['titulo'].".
     *
     * @return array cadena con los valores del campo ".$value['titulo'].".
     */
	private function get".$nombreSelect."()
	{
		\$array = array();
		\$array['Data'] = 'Data';
		return \$array;
	}
";
			}
		}

		$php = $php."
	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {

        \$filtros = \" 1 = 1 \";
        if (Session::getInstance()->get(\$this->namefilter)!=\"\") {
            \$filters =(object)Session::getInstance()->get(\$this->namefilter);";
			foreach ($data as $key => $value) {
				if($value['en_listado']== 1){
					if( $value['tipo']== 3){
 			$php= $php."
            if (\$filters->".$value['nombre']." != '') {
                \$filtros = \$filtros.\" AND ".$value['nombre']." ='\".\$filters->".$value['nombre'].".\"'\";
            }";
					} else {
            $php= $php."
            if (\$filters->".$value['nombre']." != '') {
                \$filtros = \$filtros.\" AND ".$value['nombre']." LIKE '%\".\$filters->".$value['nombre'].".\"%'\";
            }";
       				}
        		}
        	}
	$php= $php."
		}
        return \$filtros;
    }

 /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        if (\$this->getRequest()->isPost()== true) {
            \$parramsfilter = array();";

            foreach ($data as $key => $value) {
    if($value['en_listado']== 1){
     $php = $php."\$parramsfilter['".$value['nombre']."'] =  \$this->_getSanitizedParam(\"".$value['nombre']."\");";
          }
         }
            $php = $php."Session::getInstance()->set(\$this->namefilter, \$parramsfilter);
        }
        if (\$this->_getSanitizedParam(\"cleanfilter\") == 1) {
             Session::getInstance()->set(\$this->namefilter, '');
        }
    }
}";
		fwrite($nuevoarchivo,$php);
		fclose($nuevoarchivo);
	}


	public function crearModelo($data,$campos){
		$controlador = strtolower($this->_getSanitizedParam('controlador'));
		$modelo = strtolower($this->_getSanitizedParam("ruta"));
		$table = strtolower($this->_getSanitizedParam("table"));
		$identificador = $this->getKey($campos)->Field;
		$ruta = APP_PATH."/modules/".$modelo."/Models/DbTable/".ucwords($controlador).".php";
		$nuevoarchivo = fopen($ruta, "w+");
		$titulolistar = $this->_getSanitizedParam('titulo_listado'); 
		$tituloeditar = $this->_getSanitizedParam('titulo_edicion');
		$php = "<?php 
/**
* clase que genera la insercion y edicion  de ".$titulolistar." en la base de datos
*/
class ".ucwords($modelo)."_Model_DbTable_".ucwords($controlador)." extends Db_Table
{
	/**
	 * [$_name nombre de la tabla actual]
	 * @var string
	 */
	protected \$_name = '".$table."';

	/**
	 * [$_id identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected \$_id = '".$identificador."';

	/**
	 * insert recibe la informacion de un ".$tituloeditar." y la inserta en la base de datos
	 * @param  array $data array con la informacion con la cual se va a realizar la insercion en la base de datos
	 * @return integer      identificador del  registro que se inserto
	 */
	public function insert(\$data){";
		$campos = "";
		$variables = "";
		foreach ($data as $key => $value) {
			if($value['ordenar']!= 1){
				
		$php= $php."
		\$".$value['nombre']." = \$data['".$value['nombre']."'];";
			$campos =  $campos." ".$value['nombre'].",";
			$variables =  $variables." "."'\$".$value['nombre']."',";
			}
		}
		$campos = substr($campos, 0, -1);
		$variables = substr($variables, 0, -1);
		$php= $php."
		\$query = \"INSERT INTO ".$table."(".$campos.") VALUES (".$variables.")\";
		\$res = \$this->_conn->query(\$query);
        return mysqli_insert_id(\$this->_conn->getConnection());
	}

	/**
	 * update Recibe la informacion de un ".$tituloeditar."  y actualiza la informacion en la base de datos
	 * @param  array $data Array con la informacion con la cual se va a realizar la actualizacion en la base de datos
	 * @param  integer $id   identificador al cual se le va a realizar la actualizacion
	 * @return void
	 */
	public function update(\$data,\$id){
		";
		$campos = "";
		foreach ($data as $key => $value) {
			if($value['ordenar']!= 1){
		$php= $php."
		\$".$value['nombre']." = \$data['".$value['nombre']."'];";
			$campos = $campos." ".$value['nombre']." = '\$".$value['nombre']."',";
			}
		}
		$campos = substr($campos, 0, -1);
	$php= $php." \$query = \"UPDATE ".$table." SET ".$campos." WHERE ".$identificador." = '\".\$id.\"'\";
		\$res = \$this->_conn->query(\$query);
	}
}";
fwrite($nuevoarchivo,$php);
		fclose($nuevoarchivo);
	}


	public function crearVistaindex($data,$campos){
		$controlador = strtolower($this->_getSanitizedParam('controlador'));
		$modelo = strtolower($this->_getSanitizedParam("ruta"));
		$table = strtolower($this->_getSanitizedParam("table"));
		$identificador = $this->getKey($campos)->Field;
		$carpeta = APP_PATH."/modules/".$modelo."/Views/".$controlador."/";
		$ruta = APP_PATH."/modules/".$modelo."/Views/".$controlador."/index.php";
		if (!file_exists($carpeta)) {
		    mkdir($carpeta, 0777, true);
		}
		$nuevoarchivo = fopen($ruta, "w+");
		$titulolistar = $this->_getSanitizedParam('titulo_listado'); 
		$tituloeditar = $this->_getSanitizedParam('titulo_edicion');
		$php = "<div class=\"container-fluid\">
	<div class=\"text-right\">
		<a class=\"btn btn-success\" href=\"<?php echo \$this->route.\"\manage\"; ?>\">Crear Nuevo</a>
	</div>
	<form action=\"<?php echo \$this->route; ?>\" method=\"post\">
        <div class=\"container-fluid\">
            <div class=\"row\">";
            foreach ($data as $key => $value) {
				if($value['en_listado'] == 1){
					if($value['tipo'] == 3){
				$php =$php."
                 <div class=\"form-group col-xs-3\">
                    <label>".$value['titulo']."</label>
                    <select class=\"form-control\" name=\"".$value['nombre']."\">
                        <option value=\"\">Todas</option>
                        <?php foreach (\$this->list_".$value['nombre']." as \$key => \$value) : ?>
                            <option value=\"<?= \$key; ?>\" <?php if (\$this->getObjectVariable(\$this->filters, '".$value['nombre']."') ==  \$key) { echo \"selected\";} ?>><?= \$value; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>";
					} else {
						$php =$php."
                <div class=\"form-group col-xs-3\">
                    <label>".$value['titulo']."</label>
                    <input type=\"text\" class=\"form-control\" name=\"".$value['nombre']."\" value=\"<?php echo \$this->getObjectVariable(\$this->filters, '".$value['nombre']."') ?>\"></input>
                </div>
                ";
					}
            	
            	}
            }
                $php =$php."<div class=\"form-group col-xs-3\">
                    <label> </label>
                    <button type=\"submit\" class=\"btn btn-block btn-primary\">Filtrar</button>
                </div>
                <div class=\"form-group col-xs-3\">
                    <label> </label>
                    <a class=\"btn btn-block btn-info\" href=\"<?php echo \$this->route; ?>?cleanfilter=1\" >Limpiar Filtro</a>
                </div>
            </div>
        </div>
    </form>
    <div align=\"center\">
		<ul class=\"pagination\">
	    <?php
	    	\$url = \$this->route;
	        if (\$this->totalpages > 1) {
	            if (\$this->page != 1)
	                echo '<li><a href=\"'.\$url.'?page='.(\$this->page-1).'\"> &laquo; Anterior </a></li>';
	            for (\$i=1;\$i<=\$this->totalpages;\$i++) {
	                if (\$this->page == \$i)
	                    echo '<li class=\"active\"><a class=\"paginaactual\">'.\$this->page.'</a></li>';
	                else
	                    echo '<li><a href=\"'.\$url.'?page='.\$i.'\">'.\$i.'</a></li>  ';
	            }
	            if (\$this->page != \$this->totalpages)
	                echo '<li><a href=\"'.\$url.'?page='.(\$this->page+1).'\">Siguiente &raquo;</a></li>';
	        }
	  	?>
	  	</ul>
	</div>
    <div class=\"franja-paginas\">
	    <div class=\"row\">
	    	<div class=\"col-xs-6\"><div class=\"titulo-registro\">Se encontraron <?php echo \$this->register_number; ?> Registros</div></div>
	    	<div class=\"col-xs-4 text-right\">
	    		<div class=\"texto-paginas\">Registros por pagina:</div>
	    	</div>
	    	<div class=\"col-xs-2\">
	    		<select class=\"form-control selectpagination\">
	    			<option value=\"20\" <?php if(\$this->pages == 20){ echo 'selected'; } ?>>20</option>
	    			<option value=\"30\" <?php if(\$this->pages == 30){ echo 'selected'; } ?>>30</option>
	    			<option value=\"50\" <?php if(\$this->pages == 50){ echo 'selected'; } ?>>50</option>
	    			<option value=\"100\" <?php if(\$this->pages == 100){ echo 'selected'; } ?>>100</option>
	    		</select>
	    	</div>
	    </div>
    </div>
	<div>
		<table class=\" table table-striped  table-hover table-administrator text-left\">
			<thead>
				<tr>";
				foreach ($data as $key => $value) {
					if($value['en_listado'] == 1){
						$php = $php."
					<td>".$value['titulo']."</td>";
					}
				}
				if($this->getorden($data) == "orden ASC"){
					$php = $php."
					<td width=\"100\">Orden</td>";
				}
				$php = $php."
					<td width=\"250\"></td>
				</tr>
			</thead>
			<tbody>
				<?php foreach(\$this->lists as \$content){ ?>
				<?php \$id =  \$content->".$identificador."; ?>
					<tr>";
						foreach ($data as $key => $value) {
							if($value['en_listado'] == 1){
								if($value['tipo'] == 3){
									$php = $php."
						<td><?= \$this->list_".$value['nombre']."[\$content->".$value['nombre']."];?></td>";
								} else {
									$php = $php."
						<td><?=\$content->".$value['nombre'].";?></td>";
								}
								
							}
						}
						if($this->getorden($data) == "orden ASC"){
							$php = $php."
						<td>
							<input type=\"hidden\" id=\"<?= \$id; ?>\" value=\"<?= \$content->orden; ?>\"></input>
							<button class=\"up_table btn btn-primary btn-xs\"><i class=\"glyphicon glyphicon-chevron-up\"></i></button>
							<button class=\"down_table btn btn-primary btn-xs\"><i class=\"glyphicon glyphicon-chevron-down\"></i></button>
						</td>";
						}

						$php = $php."
						<td class=\"text-right\">
							<div>
								<a class=\"btn btn-primary btn-xs\" href=\"<?php echo \$this->route;?>/manage?id=<?= \$id ?>\"><i class=\"glyphicon glyphicon-pencil\"></i> Editar</a>
								<a class=\"btn btn-danger btn-xs\" data-toggle=\"modal\" data-target=\"#modal<?= \$id ?>\" ><i class=\"glyphicon glyphicon-trash\"></i> Eliminar</a>
							</div>
							<!-- Modal -->
							<div class=\"modal fade text-left\" id=\"modal<?= \$id ?>\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
							  	<div class=\"modal-dialog\" role=\"document\">
							    	<div class=\"modal-content\">
							      		<div class=\"modal-header\">
							        		<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
							        		<h4 class=\"modal-title\" id=\"myModalLabel\">Eliminar Registro</h4>
							      	</div>
							      	<div class=\"modal-body\">
							        	<div class=\"\">¿Esta seguro de eliminar este registro?</div>
							      	</div>
								      <div class=\"modal-footer\">
								        	<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>
								        	<a class=\"btn btn-danger\" href=\"<?php echo \$this->route;?>/delete?id=<?= \$id ?>&csrf=<?= \$this->csrf;?>\" >Eliminar</a>
								      </div>
							    	</div>
							  	</div>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<input type=\"hidden\" id=\"csrf\" value=\"<?php echo \$this->csrf ?>\">";
	if($this->getorden($data) == "orden ASC"){
	$php = $php."<input type=\"hidden\" id=\"order-route\" value=\"<?php echo \$this->route; ?>/order\">";
	}
	$php = $php."<input type=\"hidden\" id=\"page-route\" value=\"<?php echo \$this->route; ?>/changepage\">
	<div align=\"center\">
		<ul class=\"pagination\">
	    <?php
	    	\$url = \$this->route;
	        if (\$this->totalpages > 1) {
	            if (\$this->page != 1)
	                echo '<li><a href=\"'.\$url.'?page='.(\$this->page-1).'\"> &laquo; Anterior </a></li>';
	            for (\$i=1;\$i<=\$this->totalpages;\$i++) {
	                if (\$this->page == \$i)
	                    echo '<li class=\"active\"><a class=\"paginaactual\">'.\$this->page.'</a></li>';
	                else
	                    echo '<li><a href=\"'.\$url.'?page='.\$i.'\">'.\$i.'</a></li>  ';
	            }
	            if (\$this->page != \$this->totalpages)
	                echo '<li><a href=\"'.\$url.'?page='.(\$this->page+1).'\">Siguiente &raquo;</a></li>';
	        }
	  ?>
	  </ul>
	</div>
</div>";
		fwrite($nuevoarchivo,$php);
		fclose($nuevoarchivo);
	}

	public function crearVistamanage($data,$campos){
		$controlador = strtolower($this->_getSanitizedParam('controlador'));
		$modelo = strtolower($this->_getSanitizedParam("ruta"));
		$table = strtolower($this->_getSanitizedParam("table"));
		$identificador = $this->getKey($campos)->Field;
		$carpeta = APP_PATH."/modules/".$modelo."/Views/".$controlador."/";
		$ruta = APP_PATH."/modules/".$modelo."/Views/".$controlador."/manage.php";
		if (!file_exists($carpeta)) {
		    mkdir($carpeta, 0777, true);
		}
		$nuevoarchivo = fopen($ruta, "w+");
		$titulolistar = $this->_getSanitizedParam('titulo_listado');
		$tituloeditar = $this->_getSanitizedParam('titulo_edicion');

		$php = "<div class=\"container-fluid\">
<form class=\"text-left\" enctype=\"multipart/form-data\" method=\"post\" action=\"<?php echo \$this->routeform;?>\" data-toggle=\"validator\">
	<input type=\"hidden\" name=\"csrf\" id=\"csrf\" value=\"<?php echo \$this->csrf ?>\">
	<input type=\"hidden\" name=\"csrf_section\" id=\"csrf_section\" value=\"<?php echo \$this->csrf_section ?>\">
	<?php if (\$this->content->".$identificador.") { ?>
		<input type=\"hidden\" name=\"id\" id=\"id\" value=\"<?= \$this->content->".$identificador."; ?>\" />
	<?php }?>
	<div class=\"row\">";
	foreach ($data as $key => $value) {
		if($value['ordenar']!= 1){
			if($value['requerido'] == 1){
				$requerido = "required";
			} else {
				$requerido = "";
			}
		if($value['tipo'] == 7){
			$php = $php."
		<input type=\"hidden\" name=\"".$value['nombre']."\"  value=\"<?php echo \$this->content->".$value['nombre']." ?>\">";
		} else if($value['tipo'] == 6 ){
			$php = $php."
		<div class=\"".$value['ancho']." form-group\">
			<label for=\"".$value['nombre']."\" >".$value['titulo']."</label>
			<input type=\"file\" name=\"".$value['nombre']."\" id=\"".$value['nombre']."\" class=\"form-control  file-image\" data-buttonName=\"btn-primary\" onchange=\"validardocumento('".$value['nombre']."');\" accept=\"application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf\" ".$requerido.">
		</div>";
		} else if($value['tipo'] == 5 ){
			$php = $php."
		<div class=\"".$value['ancho']." form-group\">
			<label for=\"".$value['nombre']."\" >".$value['titulo']."</label>
			<input type=\"file\" name=\"".$value['nombre']."\" id=\"".$value['nombre']."\" class=\"form-control  file-image\" data-buttonName=\"btn-primary\" onchange=\"validarimagen('".$value['nombre']."');\" accept=\"image/gif, image/jpg, image/jpeg, image/png\"  ".$requerido.">
			<?php if(\$this->content->".$value['nombre'].") { ?>
				<div id=\"".$value['nombre']."\">
					<img src=\"/images/<?= \$this->content->".$value['nombre']."; ?>\"  class=\"img-thumbnail thumbnail-administrator\" />
					<div><button class=\"btn btn-danger btn-xs\" type=\"button\" onclick=\"eliminarImagen('".$value['nombre']."','<?php echo \$this->route.\"/deleteimage\"; ?>')\"><i class=\"glyphicon glyphicon-remove\" ></i> Eliminar Imagen</button></div>
				</div>
			<?php } ?>
		</div>";
		} else if($value['tipo'] == 4 ){
			$php = $php."
<div class=\"".$value['ancho']." form-group text-right\">
	<label   class=\"control-label\">".$value['titulo']."</label>
		<input type=\"checkbox\" name=\"".$value['nombre']."\" value=\"1\" class=\"form-control switch-form \" <?php if (\$this->getObjectVariable(\$this->content, '".$value['nombre']."') == 1) { echo \"checked\";} ?>  ".$requerido." ></input>
</div>";
		}  else if($value['tipo'] == 3 ){
			$php = $php."
		<div class=\"".$value['ancho']." form-group\">
			<label class=\"control-label\">".$value['titulo']."</label>
			<select class=\"form-control\" name=\"".$value['nombre']."\"  ".$requerido." >
				<option value=\"\">Seleccione...</option>
				<?php foreach (\$this->list_".$value['nombre']." AS \$key => \$value ){?>
					<option <?php if(\$this->getObjectVariable(\$this->content,\"".$value['nombre']."\") == \$key ){ echo \"selected\"; }?> value=\"<?php echo \$key; ?>\" /> <?= \$value; ?></option>
				<?php } ?>
			</select>
		</div>";
		}  else if($value['tipo'] == 2 ){
			$php= $php."
		<div class=\"".$value['ancho']." form-group\">
			<label for=\"".$value['nombre']."\" class=\"form-label\" >".$value['titulo']."</label>
			<textarea name=\"".$value['nombre']."\" id=\"".$value['nombre']."\"   class=\"form-control tinyeditor\" rows=\"10\"  ".$requerido." ><?= \$this->content->".$value['nombre']."; ?></textarea>
		</div>";
		} else {
			$php = $php."
		<div class=\"".$value['ancho']." form-group\">
			<label for=\"".$value['nombre']."\"  class=\"control-label\">".$value['titulo']."</label>
			<input type=\"text\" value=\"<?= \$this->content->".$value['nombre']."; ?>\" name=\"".$value['nombre']."\" id=\"".$value['nombre']."\" class=\"form-control\"  ".$requerido." >
		</div>";
		}
		}
	}

	$php = $php."
	</div>
	<div class=\"row\">
		<div class=\"col-xs-6\">
			<button class=\"btn btn-success btn-block\" type=\"submit\">Guardar</button>
		</div>
		<div class=\"col-xs-6\">
			<a class=\"btn btn-primary btn-block\"  href=\"<?php echo \$this->route; ?>\">Cancelar</a>
		</div>
	</div>
</form>
</div>";
		fwrite($nuevoarchivo,$php);
		fclose($nuevoarchivo);

	}


	public function getorden($data){
		foreach ($data as $key => $value) {
			if($value['ordenar']== 1){
				return $value['nombre']." ASC";
			}
		}
		return "";
	}

	public function getKey($campos){
		foreach ($campos as $key => $campo) {
			if($campo->Key == 'PRI'){
				return $campo;
			} 
		}
		return false;
	}

	public function getFilesimages($data){
		$images = [];
		foreach ($data as $key => $value) {
			if($value['tipo'] == 5){
				array_push($images,$value['nombre']);
			}
		}
		return $images;
	}

	public function getFilesdocument($data){
		$document = [];
		foreach ($data as $key => $value) {
			if($value['tipo'] == 6){
				array_push($document,$value['nombre']);
			}
		}
		return $document;
	}
}