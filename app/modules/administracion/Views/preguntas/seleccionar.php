<script type="text/javascript">
function todos(){
	var i=0;
	for(i=0;i<=1000;i++){
		if(document.getElementById('activo'+i)){
			document.getElementById('activo'+i).checked=true;
		}
	}
}
function ninguno(){
	var i=0;
	for(i=0;i<=1000;i++){
		if(document.getElementById('activo'+i)){
			document.getElementById('activo'+i).checked=false;
		}
	}
}
</script>


<form method="post" action="/administracion/preguntas/guardarseleccionadas/">
	<div class="container-fluid">
		<div>
			<table class=" table table-striped  table-hover table-administrator text-left">
				<thead>
					<tr>
						<td>Pregunta</td>
						<td class="text-right" width="20%">Seleccionar <br> <a onclick="todos();">Todas</a> | <a onclick="ninguno();">Ninguno</a></td>
					</tr>
				</thead>
				<tbody>
					<?php foreach($this->lists as $content){ ?>
					<?php $id =  $content->pregunta_id; ?>
						<tr>
							<td><div class="imagen_pregunta"><?=$content->pregunta_pregunta;?></div></td>
							<td class="text-right">
								<div>
									<input type="checkbox" name="activo<?= $id; ?>" id="activo<?= $id; ?>" value="1" <?php if($content->pregunta_activo=="1"){ echo 'checked'; } ?> >
									<a class="btn btn-primary btn-xs oculto" href="<?php echo $this->route;?>/manage?id=<?= $id ?>&modulo=<?php echo $this->modulo->modulo_id; ?>"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
									<a class="btn btn-danger btn-xs oculto" data-toggle="modal" data-target="#modal<?= $id ?>" ><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
								</div>
								<!-- Modal -->
								<div class="modal fade text-left" id="modal<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								  	<div class="modal-dialog" role="document">
								    	<div class="modal-content">
								      		<div class="modal-header">
								        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								        		<h4 class="modal-title" id="myModalLabel">Eliminar Registro</h4>
								      	</div>
								      	<div class="modal-body">
								        	<div class="">¿Esta seguro de eliminar este registro?</div>
								      	</div>
									      <div class="modal-footer">
									        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
									        	<a class="btn btn-danger" href="<?php echo $this->route;?>/delete?id=<?= $id ?>&csrf=<?= $this->csrf;?>&modulo=<?php echo $this->modulo->modulo_id; ?>" >Eliminar</a>
									      </div>
								    	</div>
								  	</div>
								</div>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<input type="hidden" id="csrf" value="<?php echo $this->csrf ?>">
		<input type="hidden" id="order-route" value="<?php echo $this->route; ?>/order">
		<div align="center">
			<ul class="pagination">
		    <?php
		    	$url = $this->route;
		        if ($this->totalpages > 1) {
		            if ($this->page != 1)
		                echo '<li><a href="'.$url.'?page='.($this->page-1).'"> &laquo; Anterior </a></li>';
		            for ($i=1;$i<=$this->totalpages;$i++) {
		                if ($this->page == $i)
		                    echo '<li class="active"><a class="paginaactual">'.$this->page.'</a></li>';
		                else
		                    echo '<li><a href="'.$url.'?page='.$i.'">'.$i.'</a></li>  ';
		            }
		            if ($this->page != $this->totalpages)
		                echo '<li><a href="'.$url.'?page='.($this->page+1).'">Siguiente &raquo;</a></li>';
		        }
		  ?>
		  </ul>
		</div>
		<input type="hidden" name="curso" value="<?php echo $_GET['curso'] ?>">
		<input type="hidden" name="modulo" value="<?php echo $_GET['modulo'] ?>">
		<div><button type="submit" class="btn btn-primary">Guardar</button></div>
	</div>
</form>