﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<input type="hidden" name="modulo" value="<?php echo $this->modulo->modulo_id; ?>">
	<?php if ($this->content->pregunta_id) { ?>
		<input type="hidden" name="id" value="<?= $this->content->pregunta_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="pregunta"  class="control-label">Pregunta</label>
			<textarea name="pregunta" id="pregunta"  placeholder="Pregunta" class="form-control" rows="10" required><?= $this->content->pregunta_pregunta; ?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label class="control-label">Tipo pregunta</label>
			<select class="form-control" id="tipo" name="tipo" onchange="validar_tipo();" required>
				<option value="">Seleccione...</option>
				<?php foreach ($this->tipos AS $key => $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"pregunta_tipo") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-9 form-group">
			<label for="respuesta1"  class="control-label">Respuesta 1</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta1'); ?>" name="respuesta1" id="respuesta1" class="form-control" placeholder="Respuesta 1" required>
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta1"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta1" id="correcta1" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta1')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row">
		<div class="col-xs-9 form-group">
			<label for="respuesta2"  class="control-label">Respuesta 2</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta2'); ?>" name="respuesta2" id="respuesta2" class="form-control" placeholder="Respuesta 2" required>
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta2"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta2" id="correcta2" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta2')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila3">
		<div class="col-xs-9 form-group">
			<label for="respuesta3"  class="control-label">Respuesta 3</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta3'); ?>" name="respuesta3" id="respuesta3" class="form-control" placeholder="Respuesta 3" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta3"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta3" id="correcta3" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta3')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila4">
		<div class="col-xs-9 form-group">
			<label for="respuesta4"  class="control-label">Respuesta 4</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta4'); ?>" name="respuesta4" id="respuesta4" class="form-control" placeholder="Respuesta 4" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta4"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta4" id="correcta4" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta4')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila5">
		<div class="col-xs-9 form-group">
			<label for="respuesta5"  class="control-label">Respuesta 5</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta5'); ?>" name="respuesta5" id="respuesta5" class="form-control" placeholder="Respuesta 5" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta5"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta5" id="correcta5" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta5')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila6">
		<div class="col-xs-9 form-group">
			<label for="respuesta6"  class="control-label">Respuesta 6</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta6'); ?>" name="respuesta6" id="respuesta6" class="form-control" placeholder="Respuesta 6" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta6"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta6" id="correcta6" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta6')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila7">
		<div class="col-xs-9 form-group">
			<label for="respuesta7"  class="control-label">Respuesta 7</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta7'); ?>" name="respuesta7" id="respuesta7" class="form-control" placeholder="Respuesta 7" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta7"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta7" id="correcta7" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta7')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila8">
		<div class="col-xs-9 form-group">
			<label for="respuesta8"  class="control-label">Respuesta 8</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta8'); ?>" name="respuesta8" id="respuesta8" class="form-control" placeholder="Respuesta 8" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta8"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta8" id="correcta8" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta8')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila9">
		<div class="col-xs-9 form-group">
			<label for="respuesta9"  class="control-label">Respuesta 9</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta9'); ?>" name="respuesta9" id="respuesta9" class="form-control" placeholder="Respuesta 9" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta9"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta9" id="correcta9" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta9')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila10">
		<div class="col-xs-9 form-group">
			<label for="respuesta10"  class="control-label">Respuesta 10</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta10'); ?>" name="respuesta10" id="respuesta10" class="form-control" placeholder="Respuesta 10" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta10"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta10" id="correcta10" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta10')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila11">
		<div class="col-xs-9 form-group">
			<label for="respuesta11"  class="control-label">Respuesta 11</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta11'); ?>" name="respuesta11" id="respuesta11" class="form-control" placeholder="Respuesta 11" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta11"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta11" id="correcta11" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta11')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila12">
		<div class="col-xs-9 form-group">
			<label for="respuesta12"  class="control-label">Respuesta 12</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta12'); ?>" name="respuesta12" id="respuesta12" class="form-control" placeholder="Respuesta 12" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta12"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta12" id="correcta12" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta12')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila13">
		<div class="col-xs-9 form-group">
			<label for="respuesta13"  class="control-label">Respuesta 13</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta13'); ?>" name="respuesta13" id="respuesta13" class="form-control" placeholder="Respuesta 13" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta13"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta13" id="correcta13" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta13')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row" id="fila14">
		<div class="col-xs-9 form-group">
			<label for="respuesta14"  class="control-label">Respuesta 14</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'pregunta_respuesta14'); ?>" name="respuesta14" id="respuesta14" class="form-control" placeholder="Respuesta 14" >
		</div>
		<div class="col-xs-3 form-group text-center">
			<label for="correcta14"  class="control-label">Correcta?</label>
			<input type="checkbox" value="1" name="correcta14" id="correcta14" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_correcta14')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 form-group text-center">
			<label for="activo"  class="control-label">Pregunta activa</label>
			<input type="checkbox" value="1" name="activo" id="activo" class="form-control" <?php if($this->getObjectVariable($this->content,'pregunta_activo')=="1"){ echo 'checked'; } ?> >
		</div>
	</div>


	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/preguntas?modulo=<?php echo $this->modulo->modulo_id; ?>">Cancelar</a>
		</div>
	</div>
</form>
</div>
<!-- tiny -->
<script language="javascript" type="text/javascript">
tinyMCE.init({
  mode : "textareas",
  theme: "modern",
  language_url: "/scripts/tinymce/langs/es.js",
  language: "es",
  plugins:"link , responsivefilemanager, table ,  visualblocks, code,paste, media" ,
  external_filemanager_path:"/scripts/tinymce/plugins/filemanager/",
  filemanager_title:"Responsive Filemanager" ,
  external_plugins: {
        "filemanager": "/scripts/tinymce/plugins/filemanager/plugin.min.js",
        "responsivefilemanager": "/scripts/tinymce/plugins/responsivefilemanager/plugin.min.js"
  },
  theme_modern_toolbar_location : "bottom",
  paste_auto_cleanup_on_paste : true,
  toolbar: "bold,italic,underline,|,alignleft, aligncenter, alignright, alignjustify |,bullist,numlist,|,link,unlink,|,table,|,responsivefilemanager,visualblocks,|,removeformat,media,code",
  menubar: false,
  resize: true,
  browser_spellcheck : true ,
  statusbar: true,
  relative_urls: false
});
$(":file").filestyle({buttonName: "btn-primary",buttonText:' Cargar Archivo'});

function validar_tipo(){
	var tipo = $("#tipo").val();
	if(tipo=="2"){
		$("#fila3").hide();
		$("#fila4").hide();
		$("#fila5").hide();
		$("#fila6").hide();
		$("#fila7").hide();
		$("#fila8").hide();
		$("#fila9").hide();
		$("#fila10").hide();
		$("#fila11").hide();
		$("#fila12").hide();
		$("#fila13").hide();
		$("#fila14").hide();
		$("#respuesta1").val("Falso");
		$("#respuesta2").val("Verdadero");
		$("#respuesta3").val("");
		$("#respuesta4").val("");
		$("#respuesta5").val("");
		$("#respuesta6").val("");
		$("#respuesta7").val("");
		$("#respuesta9").val("");
		$("#respuesta10").val("");
		$("#respuesta11").val("");
		$("#respuesta12").val("");
		$("#respuesta13").val("");
		$("#respuesta14").val("");
		$("#correcta3").prop('checked',false);
		$("#correcta4").prop('checked',false);
		$("#correcta5").prop('checked',false);
		$("#correcta6").prop('checked',false);
		$("#correcta7").prop('checked',false);
		$("#correcta8").prop('checked',false);
		$("#correcta9").prop('checked',false);
		$("#correcta10").prop('checked',false);
		$("#correcta11").prop('checked',false);
		$("#correcta12").prop('checked',false);
		$("#correcta13").prop('checked',false);
		$("#correcta14").prop('checked',false);
	}else{
		$("#fila3").show();
		$("#fila4").show();
		$("#fila5").show();
		$("#fila5").show();
		$("#fila6").show();
		$("#fila7").show();
		$("#fila8").show();
		$("#fila9").show();
		$("#fila10").show();
		$("#fila11").show();
		$("#fila12").show();
		$("#fila13").show();
		$("#fila14").show();
	}
}
validar_tipo();
</script>
<!-- Fin tiny -->