<div class="container-fluid">
	<div class="text-right">
		<a class="btn btn-success" href="<?php echo $this->route;?>/manage">Crear Nuevo</a>
	</div>
	<form action="<?php echo $this->route; ?>" method="post">
        <div class="container-fluid">
            <div class="row">
                <div class="form-group col-xs-2">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="nombre" value="<?php echo $this->getObjectVariable($this->filters, 'nombre') ?>"></input>
                </div>
                <div class="form-group col-xs-2">
                    <label>Nivel</label>
                    <select class="form-control" name="level">
                        <option value="">Todos</option>
                        <?php foreach ($this->levels as $key=>$level) : ?>
                            <option value="<?= $key; ?>" <?php if ($this->getObjectVariable($this->filters, 'level') ==  $key) { echo "selected";} ?>><?= $level; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group col-xs-2">
                    <label>Cargo</label>
                    <select class="form-control" name="cargo">
                        <option value="">Todos</option>
                        <?php foreach ($this->cargos as $key=>$cargo) : ?>
                            <option value="<?= $cargo->cargo_nombre; ?>" <?php if ($this->getObjectVariable($this->filters, 'cargo') ==  $cargo->cargo_nombre) { echo "selected";} ?>><?= $cargo->cargo_nombre; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group col-xs-2">
                    <label>Estado</label>
                    <select class="form-control" name="state">
                        <option value="">Todos</option>
                            <option value="1" <?php if ($this->getObjectVariable($this->filters, 'state') ==  1) { echo "selected";} ?>>Activo</option>
                            <option value="2" <?php if ($this->getObjectVariable($this->filters, 'state') ==  2) { echo "selected";} ?>>Inactivo</option>
                    </select>
                </div>
                <div class="form-group col-xs-2">
                    <label> </label>
                    <button type="submit" class="btn btn-block btn-primary">Filtrar</button>
                </div>
                <div class="form-group col-xs-2">
                    <label> </label>
                    <a class="btn btn-block btn-info" href="<?php echo $this->route; ?>?cleanfilter=1" >Limpiar Filtro</a>
                </div>
            </div>
        </div>
    </form>
	<div>
		<table class=" table table-striped  table-hover table-administrator text-left">
			<thead>
				<tr>
					<td>Nombre</td>
					<td width="150">Nivel</td>
					<td width="100">Estado</td>
					<td width="250"></td>
				</tr>
			</thead>
			<tbody>
				<?php foreach($this->lists as $content){ ?>
				<?php $id =  $content->user_id; ?>
					<tr>
						<td><?=$content->user_names?> <?=$content->user_lastnames?></td>
						<td><?= $this->levels[$content->user_level];?></td>
						<td>
							<?php if ( $content->user_state == 1){ $estado = "Activo"; } else { $estado = "Inactivo"; } ?>
							<?= $estado; ?>
						</td>
						<td class="text-right">
							<div>
								<a class="btn btn-primary btn-xs" href="<?php echo $this->route;?>/manage?id=<?= $id ?>"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
								<a class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal<?= $id ?>" ><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
							</div>
							<!-- Modal -->
							<div class="modal fade text-left" id="modal<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  	<div class="modal-dialog" role="document">
							    	<div class="modal-content">
							      		<div class="modal-header">
							        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        		<h4 class="modal-title" id="myModalLabel">Eliminar Registro</h4>
							      	</div>
							      	<div class="modal-body">
							        	<div class="">¿Esta seguro de eliminar este registro?</div>
							      	</div>
								      <div class="modal-footer">
								        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
								        	<a class="btn btn-danger" href="<?php echo $this->route;?>/delete?id=<?= $id ?>&csrf=<?= $this->csrf;?>" >Eliminar</a>
								      </div>
							    	</div>
							  	</div>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<input type="hidden" id="csrf" value="<?php echo $this->csrf ?>">
	<input type="hidden" id="order-route" value="<?php echo $this->route; ?>/order">
	<div align="center">
		<ul class="pagination">
	    <?php
	    	$url = $this->route;
	        if ($this->totalpages > 1) {
	            if ($this->page != 1)
	                echo '<li><a href="'.$url.'?page='.($this->page-1).'"> &laquo; Anterior </a></li>';
	            for ($i=1;$i<=$this->totalpages;$i++) {
	                if ($this->page == $i)
	                    echo '<li class="active"><a class="paginaactual">'.$this->page.'</a></li>';
	                else
	                    echo '<li><a href="'.$url.'?page='.$i.'">'.$i.'</a></li>  ';
	            }
	            if ($this->page != $this->totalpages)
	                echo '<li><a href="'.$url.'?page='.($this->page+1).'">Siguiente &raquo;</a></li>';
	        }
	  ?>
	  </ul>
	</div>
</div>