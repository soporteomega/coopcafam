<table border="1" cellpadding="10" cellspacing="0">
	<tr style="text-align: center; background: #CCCCCC;">
		<td>Nivel</td>
		<td>Nombres</td>
		<td>Apellidos</td>
		<td>Identificacion</td>
		<td>correo</td>
		<td>Ciudad</td>
		<td>Pais</td>
		<td>Telefono</td>
		<td>Fecha de Nacimiento</td>
		<td>Usuario</td>
		<td>Estado</td>
	</tr>

	<?php foreach ($this->listado as $key => $value): ?>
		<tr>
			<td><?php echo $this->levels[$value->user_level]; ?></td>
			<td><?php echo $value->user_names; ?></td>
			<td><?php echo $value->user_lastnames; ?></td>
			<td><?php echo $value->user_idnumber; ?></td>
			<td><?php echo $value->user_email; ?></td>
			<td><?php echo $value->user_city; ?></td>
			<td><?php echo $value->user_country; ?></td>
			<td><?php echo $value->user_phone; ?></td>
			<td><?php echo $value->user_date; ?></td>
			<td><?php echo $value->user_user; ?></td>
			<td><?php if($value->user_state == 1){echo "Activo" ;} else { echo "Inactivo";} ?></td>
		</tr>
	<?php endforeach ?>
</table>