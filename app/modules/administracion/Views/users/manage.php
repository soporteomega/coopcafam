﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<?php if ($this->content->user_id) { ?>
		<input type="hidden" name="id" value="<?= $this->content->user_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="col-xs-12 form-group text-right">
			<label   class="control-label">Estado</label>
			<input type="checkbox" name="state" value="1" class="form-control switch-form " <?php if ($this->getObjectVariable($this->content, 'user_state') == 1) { echo "checked";} ?>></input>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-4 form-group">
			<label class="control-label">Nivel</label>
			<select class="form-control" name="level" required>
				<option value="">Seleccione...</option>
				<?php foreach ($this->levels AS $key => $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"user_level") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-4 form-group">
			<label class="control-label">Cargo</label>
			<select class="form-control" name="position" required>
				<option value="">Seleccione...</option>
				<?php foreach ($this->cargos AS $key => $cargo ){?>
					<option <?php if($this->getObjectVariable($this->content,"user_position") == $cargo->cargo_nombre ){ echo "selected"; }?> value="<?php echo $cargo->cargo_nombre; ?>" /> <?= $cargo->cargo_nombre; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-4 form-group">
			<label for="date"  class="control-label">Fecha Nacimiento</label>
			<input type="text" value="<?= $this->content->user_date; ?>" name="date" id="date" class="form-control" placeholder="0000-00-00" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-language="es">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-2 form-group">
			<label for="idnumber"  class="control-label">Identificación</label>
			<input type="text" value="<?= $this->content->user_idnumber; ?>" name="idnumber" id="idnumber" class="form-control" placeholder="Identificación">
		</div>
		<div class="col-xs-5 form-group">
			<label for="names"  class="control-label">Nombres</label>
			<input type="text" value="<?= $this->content->user_names; ?>" name="names" id="names" class="form-control" placeholder="Nombres" required>
		</div>
		<div class="col-xs-5 form-group">
			<label for="lastnames" >Apellidos</label>
			<input type="text" value="<?= $this->content->user_lastnames; ?>" name="lastnames" id="lastnames" class="form-control" placeholder="Apellidos" required>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-8 form-group">
			<label for="email"  class="control-label">Correo</label>
			<input type="email" value="<?= $this->content->user_email; ?>" name="email" id="email" class="form-control" placeholder="Correo" required>
		</div>
		<div class="col-xs-4 form-group">
			<label for="phone"  class="control-label">Telefono</label>
			<input type="text" value="<?= $this->content->user_phone; ?>" name="phone" id="phone" class="form-control" placeholder="Telefono" required>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label for="user"  class="control-label">Dirección</label>
			<input type="text" value="<?= $this->content->user_address; ?>" name="address" id="address" class="form-control" placeholder="Dirección" required>
		</div>
		<div class="col-xs-3 form-group">
			<label for="city"  class="control-label">Ciudad</label>
			<input type="text" value="<?= $this->content->user_city; ?>" name="city" id="city" class="form-control" placeholder="Ciudad" required>
		</div>
		<div class="col-xs-3 form-group">
			<label for="country"  class="control-label">Pais</label>
			<input type="text" value="<?= $this->content->user_country; ?>" name="country" id="country" class="form-control" placeholder="Pais" required>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label for="user"  class="control-label">Usuario</label>
			<input type="text" value="<?= $this->content->user_user; ?>" name="user" id="user" class="form-control" placeholder="Usuario" required>
		</div>
		<div class="col-xs-3 form-group">
			<label for="password"  class="control-label">Contraseña</label>
			<input type="password" value="" name="password" id="password" class="form-control" placeholder="Contraseña" <?php if (!$this->content->user_id) { ?>required<?php } ?>>
		</div>
		<div class="col-xs-3 form-group">
			<label for="re_password"  class="control-label">Repita Contraseña</label>
			<input type="password" value="" name="re_password" id="re_password" class="form-control" placeholder="Repita Contraseña"  data-match="#password" data-match-error="las dos contraseñas no son iguales" >
			<div class="help-block with-errors"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/users">Cancelar</a>
		</div>
	</div>
</form>
</div>