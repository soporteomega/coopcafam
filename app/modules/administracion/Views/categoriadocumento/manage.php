﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<?php if ($this->content->categoria_documento_id) { ?>
		<input type="hidden" name="id" value="<?= $this->content->categoria_documento_id; ?>" />
	<?php }?>
	<input type="hidden" name="content" value="<?php if( $this->id_content > 0 ){ echo $this->id_content; } else { echo $this->getObjectVariable($this->content,'content_id'); } ?>" />
	<input type="hidden" name="padre" value="<?php if( $this->padre > 0 ){ echo $this->padre; } else { echo 0; } ?>" />
	<div class="row">
		<div class="col-xs-6 form-group">
			<label class="control-label">Tipo</label>
			<select class="form-control" name="tipo" required>
				<option value="">Seleccione...</option>
				<?php foreach ($this->sections AS $key => $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"categoria_documento_tipo") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-6 form-group">
			<label for="nombre"  class="control-label">Título</label>
			<input type="text" value="<?= $this->content->categoria_documento_nombre; ?>" name="nombre" id="nombre" class="form-control" placeholder="Título" required>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="descripcion" >Descripción</label>
			<textarea name="descripcion" id="descripcion"  placeholder="Descripción" class="form-control" rows="10" ><?= $this->content->categoria_documento_descripcion; ?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="imagen" >Imagen</label>
			<input type="file" name="imagen" id="imagen" class="form-control  filestyle" data-buttonName="btn-primary">
			<?php if($this->content->categoria_documento_imagen) { ?>
				<img src="/images/<?= $this->content->categoria_documento_imagen; ?>" class="img-thumbnail thumbnail-administrator" />
			<?php } ?>
		</div>
	</div>

	
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/content">Cancelar</a>
		</div>
	</div>
</form>
</div>
<!-- tiny -->
<script language="javascript" type="text/javascript">
tinyMCE.init({
  mode : "textareas",
  theme: "modern",
  language_url: "/scripts/tinymce/langs/es.js",
  language: "es",
  plugins:"link , responsivefilemanager, table ,  visualblocks, code,paste ,tiny_bootstrap_elements_light,fullscreen" ,
  external_filemanager_path:"/scripts/tinymce/plugins/filemanager/",
  filemanager_title:"Responsive Filemanager" ,
  external_plugins: {
        "filemanager": "/scripts/tinymce/plugins/filemanager/plugin.min.js",
        "responsivefilemanager": "/scripts/tinymce/plugins/responsivefilemanager/plugin.min.js",
        "tiny_bootstrap_elements_light": "/scripts/tinymce/plugins/tiny_bootstrap_elements_light/plugin.js"
  },
  theme_modern_toolbar_location : "bottom",
  paste_auto_cleanup_on_paste : true,
  toolbar: "bold,italic,underline,|,alignleft, aligncenter, alignright, alignjustify, |,bullist,numlist,|,link,unlink,|,table,|,responsivefilemanager,visualblocks,|,removeformat,code,|,fullscreen",
  menubar: false,
  resize: true,
  browser_spellcheck : true ,
  statusbar: true,
  relative_urls: false
});
$(":file").filestyle({buttonName: "btn-primary",buttonText:' Cargar Archivo'});
</script>
<!-- Fin tiny -->