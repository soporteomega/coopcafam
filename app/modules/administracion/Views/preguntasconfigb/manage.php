<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" id="csrf" value="<?php echo $this->csrf ?>">
	<input type="hidden" name="csrf_section" id="csrf_section" value="<?php echo $this->csrf_section ?>">
	<?php if ($this->content->preguntas_config_id) { ?>
		<input type="hidden" name="id" id="id" value="<?= $this->content->preguntas_config_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="col-xs-1 form-group">
			<label for="preguntas_config_modulo_id"  class="control-label">preguntas_config_modulo_id</label>
			<input type="text" value="<?= $this->content->preguntas_config_modulo_id; ?>" name="preguntas_config_modulo_id" id="preguntas_config_modulo_id" class="form-control"   >
		</div>
		<div class="col-xs-1 form-group">
			<label for="preguntas_config_orden"  class="control-label">preguntas_config_orden</label>
			<input type="text" value="<?= $this->content->preguntas_config_orden; ?>" name="preguntas_config_orden" id="preguntas_config_orden" class="form-control"   >
		</div>
		<div class="col-xs-1 form-group">
			<label for="preguntas_config_cantidad"  class="control-label">preguntas_config_cantidad</label>
			<input type="text" value="<?= $this->content->preguntas_config_cantidad; ?>" name="preguntas_config_cantidad" id="preguntas_config_cantidad" class="form-control"   >
		</div>
		<div class="col-xs-1 form-group">
			<label for="preguntas_config_tiempo"  class="control-label">preguntas_config_tiempo</label>
			<input type="text" value="<?= $this->content->preguntas_config_tiempo; ?>" name="preguntas_config_tiempo" id="preguntas_config_tiempo" class="form-control"   >
		</div>
		<div class="col-xs-1 form-group">
			<label for="preguntas_config_oportunidades"  class="control-label">preguntas_config_oportunidades</label>
			<input type="text" value="<?= $this->content->preguntas_config_oportunidades; ?>" name="preguntas_config_oportunidades" id="preguntas_config_oportunidades" class="form-control"   >
		</div>
		<div class="col-xs-1 form-group">
			<label for="preguntas_mostrar_certificado"  class="control-label">preguntas_mostrar_certificado</label>
			<input type="text" value="<?= $this->content->preguntas_mostrar_certificado; ?>" name="preguntas_mostrar_certificado" id="preguntas_mostrar_certificado" class="form-control"   >
		</div>
		<div class="col-xs-1 form-group">
			<label for="preguntas_config_gif_aprobado" >preguntas_config_gif_aprobado</label>
			<input type="file" name="preguntas_config_gif_aprobado" id="preguntas_config_gif_aprobado" class="form-control  file-image" data-buttonName="btn-primary" onchange="validarimagen('preguntas_config_gif_aprobado');" accept="image/gif, image/jpg, image/jpeg, image/png"  >
			<?php if($this->content->preguntas_config_gif_aprobado) { ?>
				<div id="preguntas_config_gif_aprobado">
					<img src="/images/<?= $this->content->preguntas_config_gif_aprobado; ?>"  class="img-thumbnail thumbnail-administrator" />
					<div><button class="btn btn-danger btn-xs" type="button" onclick="eliminarImagen('preguntas_config_gif_aprobado','<?php echo $this->route."/deleteimage"; ?>')"><i class="glyphicon glyphicon-remove" ></i> Eliminar Imagen</button></div>
				</div>
			<?php } ?>
		</div>
		<div class="col-xs-1 form-group">
			<label for="preguntas_config_gif_reprobado" >preguntas_config_gif_reprobado</label>
			<input type="file" name="preguntas_config_gif_reprobado" id="preguntas_config_gif_reprobado" class="form-control  file-image" data-buttonName="btn-primary" onchange="validarimagen('preguntas_config_gif_reprobado');" accept="image/gif, image/jpg, image/jpeg, image/png"  >
			<?php if($this->content->preguntas_config_gif_reprobado) { ?>
				<div id="preguntas_config_gif_reprobado">
					<img src="/images/<?= $this->content->preguntas_config_gif_reprobado; ?>"  class="img-thumbnail thumbnail-administrator" />
					<div><button class="btn btn-danger btn-xs" type="button" onclick="eliminarImagen('preguntas_config_gif_reprobado','<?php echo $this->route."/deleteimage"; ?>')"><i class="glyphicon glyphicon-remove" ></i> Eliminar Imagen</button></div>
				</div>
			<?php } ?>
		</div>
		<div class="col-xs-1 form-group">
			<label for="preguntas_config_gif_reloj" >preguntas_config_gif_reloj</label>
			<input type="file" name="preguntas_config_gif_reloj" id="preguntas_config_gif_reloj" class="form-control  file-image" data-buttonName="btn-primary" onchange="validarimagen('preguntas_config_gif_reloj');" accept="image/gif, image/jpg, image/jpeg, image/png"  >
			<?php if($this->content->preguntas_config_gif_reloj) { ?>
				<div id="preguntas_config_gif_reloj">
					<img src="/images/<?= $this->content->preguntas_config_gif_reloj; ?>"  class="img-thumbnail thumbnail-administrator" />
					<div><button class="btn btn-danger btn-xs" type="button" onclick="eliminarImagen('preguntas_config_gif_reloj','<?php echo $this->route."/deleteimage"; ?>')"><i class="glyphicon glyphicon-remove" ></i> Eliminar Imagen</button></div>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block"  href="<?php echo $this->route; ?>">Cancelar</a>
		</div>
	</div>
</form>
</div>