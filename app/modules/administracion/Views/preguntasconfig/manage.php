<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" id="csrf" value="<?php echo $this->csrf ?>">
	<input type="hidden" name="csrf_section" id="csrf_section" value="<?php echo $this->csrf_section ?>">
	<?php if ($this->content->preguntas_config_id) { ?>
		<input type="hidden" name="id" id="id" value="<?= $this->content->preguntas_config_id; ?>" />
	<?php }?>
	<div class="row">
		<input type="hidden" name="preguntas_config_modulo_id"  value="<?php echo $this->content->preguntas_config_modulo_id; ?>">
		<div class="col-xs-6 form-group">
			<label class="control-label">orden de las preguntas</label>
			<select class="form-control" name="preguntas_config_orden"  required >
				<option value="">Seleccione...</option>
				<?php foreach ($this->list_preguntas_config_orden AS $key => $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"preguntas_config_orden") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-6 form-group">
			<label class="control-label">orden de las respuestas</label>
			<select class="form-control" name="preguntas_config_orden_respuestas"  required >
				<option value="">Seleccione...</option>
				<?php foreach ($this->list_preguntas_config_orden AS $key => $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"preguntas_config_orden_respuestas") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-6 form-group">
			<label for="preguntas_config_cantidad"  class="control-label">cantidad de preguntas</label>
			<input type="text" value="<?= $this->content->preguntas_config_cantidad; ?>" name="preguntas_config_cantidad" id="preguntas_config_cantidad" class="form-control" required  >
		</div>
		<div class="col-xs-6 form-group">
			<label for="preguntas_config_tiempo"  class="control-label">tiempo en minutos</label>
			<input type="text" value="<?= $this->content->preguntas_config_tiempo; ?>" name="preguntas_config_tiempo" id="preguntas_config_tiempo" class="form-control" required  >
		</div>
		<div class="col-xs-6 form-group">
			<label for="preguntas_config_oportunidades"  class="control-label">cantidad de oportunidades</label>
			<input type="text" value="<?= $this->content->preguntas_config_oportunidades; ?>" name="preguntas_config_oportunidades" id="preguntas_config_oportunidades" class="form-control" required  >
		</div>
		<div class="col-xs-6 form-group text-center">
			<label for="preguntas_mostrar_certificado"  class="control-label">mostrar certificado?</label>
			<input type="checkbox" value="1" name="preguntas_mostrar_certificado" id="preguntas_mostrar_certificado" class="form-control" <?php if($this->content->preguntas_mostrar_certificado=="1"){ echo 'checked'; } ?>  >
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label for="preguntas_config_gif_aprobado" >Gif aprobado</label>
			<input type="file" name="preguntas_config_gif_aprobado" id="preguntas_config_gif_aprobado" class="form-control  file-image" data-buttonName="btn-primary" onchange="validarimagen('preguntas_config_gif_aprobado');" accept="image/gif, image/jpg, image/jpeg, image/png"  >
			<?php if($this->content->preguntas_config_gif_aprobado) { ?>
				<div id="preguntas_config_gif_aprobado">
					<img src="/images/<?= $this->content->preguntas_config_gif_aprobado; ?>"  class="img-thumbnail thumbnail-administrator" />
					<div><button class="btn btn-danger btn-xs" type="button" onclick="eliminarImagen('preguntas_config_gif_aprobado','<?php echo $this->route."/deleteimage"; ?>')"><i class="glyphicon glyphicon-remove" ></i> Eliminar Imagen</button></div>
				</div>
			<?php } ?>
		</div>
		<div class="col-xs-6 form-group">
			<label for="preguntas_config_gif_reprobado" >Gif reprobado</label>
			<input type="file" name="preguntas_config_gif_reprobado" id="preguntas_config_gif_reprobado" class="form-control  file-image" data-buttonName="btn-primary" onchange="validarimagen('preguntas_config_gif_reprobado');" accept="image/gif, image/jpg, image/jpeg, image/png"  >
			<?php if($this->content->preguntas_config_gif_reprobado) { ?>
				<div id="preguntas_config_gif_reprobado">
					<img src="/images/<?= $this->content->preguntas_config_gif_reprobado; ?>"  class="img-thumbnail thumbnail-administrator" />
					<div><button class="btn btn-danger btn-xs" type="button" onclick="eliminarImagen('preguntas_config_gif_reprobado','<?php echo $this->route."/deleteimage"; ?>')"><i class="glyphicon glyphicon-remove" ></i> Eliminar Imagen</button></div>
				</div>
			<?php } ?>
		</div>
		<div class="col-xs-6 form-group">
			<label for="preguntas_config_gif_reloj" >Gif Reloj</label>
			<input type="file" name="preguntas_config_gif_reloj" id="preguntas_config_gif_reloj" class="form-control  file-image" data-buttonName="btn-primary" onchange="validarimagen('preguntas_config_gif_reloj');" accept="image/gif, image/jpg, image/jpeg, image/png"  >
			<?php if($this->content->preguntas_config_gif_reloj) { ?>
				<div id="preguntas_config_gif_reloj">
					<img src="/images/<?= $this->content->preguntas_config_gif_reloj; ?>"  class="img-thumbnail thumbnail-administrator" />
					<div><button class="btn btn-danger btn-xs" type="button" onclick="eliminarImagen('preguntas_config_gif_reloj','<?php echo $this->route."/deleteimage"; ?>')"><i class="glyphicon glyphicon-remove" ></i> Eliminar Imagen</button></div>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block"  href="/administracion/modulos/?curso=<?php echo $_GET['curso']; ?>">Cancelar</a>
		</div>
	</div>
	<input type="hidden" name="curso" id="curso" value="<?php echo $_GET['curso'] ?>" />
</form>
</div>