﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<?php if ($this->content->solicitud_id) { ?>
		<input type="hidden" name="id_content" value="<?= $this->content->solicitud_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="form-group col-sm-4">
			<label for="nombre">Fecha</label>
			<input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-language="es" id="fecha" name="fecha"  placeholder="Fecha"   type="text" value="<?php if ($this->content->solicitud_fecha){?><?= $this->content->solicitud_fecha; ?><?php } else { echo date("Y-m-d"); } ?>">
		</div>
		<div class="form-group col-sm-4">
			<label for="nombre">Estado</label>
			<select class="form-control" name="estado">
				<option value="0">No Realizado</option>
				<option value="1" <?php if(1 == $this->content->solicitud_estado ){ ?> selected <?php } ?>>Realizado</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-sm-4">
			<label for="nombre">Nombre</label>
			<input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Nombre" value="<?= $this->content->solicitud_nombre; ?>">
		</div>
		<div class="form-group col-sm-4">
			<label for="correo">Correo</label>
			<input type="email" class="form-control" id="correo" name="correo" placeholder="Correo" value="<?= $this->content->solicitud_correo; ?>">
		</div>
		<div class="form-group col-sm-4">
			<label for="direccion">Gestion</label>
			<select class="form-control" name="tipo" required>
				<option>Seleccione...</option>
				<?php foreach ($this->gestiones as $key => $gestion): ?>
					<option value="<?php echo $gestion->tipo_gestion_id; ?>" <?php if($gestion->tipo_gestion_id == $this->content->solicitud_tipo ){ ?> selected <?php } ?>><?php echo $gestion->tipo_gestion_nombre; ?> </option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group col-sm-4">
			<label for="direccion">Dirección</label>
			<input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" value="<?= $this->content->solicitud_direccion; ?>">
		</div>
		<div class="form-group col-sm-2">
			<label for="ciudad">Ciudad</label>
			<input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" value="<?= $this->content->solicitud_ciudad; ?>">
		</div>
		<div class="form-group col-sm-3">
			<label for="telefono">Extencion</label>
			<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" value="<?= $this->content->solicitud_telefono; ?>">
		</div>
		<div class="form-group col-sm-3">
			<label for="celular">Celular</label>
			<input type="text" class="form-control" id="celular" name="celular" placeholder="Celular" value="<?= $this->content->solicitud_celular; ?>">
		</div>

		<div class="form-group col-sm-12">
			<label for="solicitud">Solicitud</label>
			<textarea class="form-control" name="solicitud" id="solicitud" placeholder="Solicitud" rows="4" style="resize: none;"><?= $this->content->solicitud_solicitud; ?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/solicitudes">Cancelar</a>
		</div>
	</div>
</form>
</div>
<!-- tiny -->
<script language="javascript" type="text/javascript">
tinyMCE.init({
  mode : "textareas",
  theme: "modern",
  language_url: "/scripts/tinymce/langs/es.js",
  language: "es",
  plugins:"link , responsivefilemanager, table ,  visualblocks, code,paste,fullscreen" ,
  external_filemanager_path:"/scripts/tinymce/plugins/filemanager/",
  filemanager_title:"Responsive Filemanager" ,
  external_plugins: {
        "filemanager": "/scripts/tinymce/plugins/filemanager/plugin.min.js",
        "responsivefilemanager": "/scripts/tinymce/plugins/responsivefilemanager/plugin.min.js"
  },
  theme_modern_toolbar_location : "bottom",
  paste_auto_cleanup_on_paste : true,
  toolbar: "bold,italic,underline,|,alignleft, aligncenter, alignright, alignjustify, |,bullist,numlist,|,link,unlink,|,table,|,responsivefilemanager,visualblocks,|,removeformat,code,|,fullscreen",
  menubar: false,
  resize: true,
  browser_spellcheck : true ,
  statusbar: true,
  relative_urls: false
});
</script>
<!-- Fin tiny -->