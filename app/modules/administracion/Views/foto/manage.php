﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<input type="hidden" name="album" value="<?php echo $this->album; ?>">
	<?php if ($this->content->foto_id) { ?>
		<input type="hidden" name="id" value="<?= $this->content->foto_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="titulo"  class="control-label">Título</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'foto_titulo'); ?>" name="titulo" id="titulo" class="form-control" placeholder="Título" required>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="descripcion" >Descripción</label>
			<textarea name="descripcion" id="descripcion"  placeholder="Descripción" class="form-control" rows="3" ><?= $this->getObjectVariable($this->content,'foto_descripcion'); ?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="imagen" >Imagen</label>
			<input type="file" name="imagen[]" id="imagen" class="form-control" <?php if(!$this->content->foto_imagen) { ?>required<?php } ?> multiple class="file-loading">
			<?php if($this->content->foto_imagen) { ?>
				<img src="/images/<?= $this->content->foto_imagen; ?>" class="img-thumbnail thumbnail-administrator" />
			<?php } ?>
		</div>
	</div>

	
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/foto">Cancelar</a>
		</div>
	</div>
</form>
</div>

 <script type="text/javascript">
           $("#imagen").fileinput({
           	 	uploadUrl: '/upload-image/',
                showUpload: false,
                showRemove: false,
                browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
                browseLabel: 'Seleccionar',
                allowedFileExtensions: ["jpg", "gif", "png", "jpeg"],
                maxFilePreviewSize: 10240,
                actions: '<div class="file-actions">\n' +
        '    <div class="file-footer-buttons">\n' +
        '        {delete} {zoom} {other}' +
        '    </div>\n' +
        '    {drag}\n' +
        '    <div class="file-upload-indicator" title="{indicatorTitle}">{indicator}</div>\n' +
        '    <div class="clearfix"></div>\n' +
        '</div>',
            });
    </script>
 <style type="text/css">
 	.kv-file-upload{
 		display: none;
 	}
 </style>