<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" id="csrf" value="<?php echo $this->csrf ?>">
	<input type="hidden" name="csrf_section" id="csrf_section" value="<?php echo $this->csrf_section ?>">
	<?php if ($this->content->cursos_mensajes_id) { ?>
		<input type="hidden" name="id" id="id" value="<?= $this->content->cursos_mensajes_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label for="cursos_mensajes_mensaje" class="form-label" >mensaje</label>
			<textarea name="cursos_mensajes_mensaje" id="cursos_mensajes_mensaje"   class="form-control tinyeditor" rows="10"  required ><?= $this->content->cursos_mensajes_mensaje; ?></textarea>
		</div>
		<div class="col-xs-6 form-group">
			<label class="control-label">tipo</label>
			<select class="form-control" name="cursos_mensajes_tipo"  required >
				<option value="">Seleccione...</option>
				<?php foreach ($this->list_cursos_mensajes_tipo AS $key => $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"cursos_mensajes_tipo") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-6 form-group">
			<label for="cursos_mensajes_color" class="form-label" >color de fondo</label>
			<input name="cursos_mensajes_color" id="cursos_mensajes_color" value="<?php if($this->content->cursos_mensajes_color){ echo $this->content->cursos_mensajes_color; }else{ echo '#00663D'; } ?>" required >
		</div>
		<div class="col-xs-6 form-group">
			<label for="cursos_mensajes_color2" class="form-label" >color del texto</label>
			<input name="cursos_mensajes_color2" id="cursos_mensajes_color2" value="<?php if($this->content->cursos_mensajes_color2){ echo $this->content->cursos_mensajes_color2; }else{ echo '#FFFFFF'; } ?>" required >
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block"  href="<?php echo $this->route; ?>">Cancelar</a>
		</div>
	</div>
</form>
</div>

  <script>
    $(function () {
      $('#cursos_mensajes_color').colorpicker();
      $('#cursos_mensajes_color2').colorpicker();
    });
  </script>