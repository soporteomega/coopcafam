<div class="botones-registros">
	<a href="/administracion/registros/log" class="btn btn-primary"> Log de Usuario </a>
	<a href="/administracion/registros/triggersolicitud" class="btn btn-primary"> Solicitudes </a>
	<a href="/administracion/registros/triggertipogestion" class="btn btn-primary"> Tipos de Gestion</a>
	<a href="/administracion/registros/triggercontent" class="btn btn-primary"> Contenidos</a>
	<a href="/administracion/registros/triggeradicional" class="btn btn-primary"> Adicionales Contenido</a>
	<a href="/administracion/registros/triggertypecalendar" class="btn btn-primary"> Tipos de Calendario</a>
	<a href="/administracion/registros/triggercalendar" class="btn btn-primary"> Calendario</a>
	<a href="/administracion/registros/triggercategoriadocumento" class="btn btn-primary"> Categorias Documentos</a>
	<a href="/administracion/registros/triggerdocumento" class="btn btn-primary"> Documentos</a>
	<a href="/administracion/registros/triggercursos" class="btn btn-primary"> Cursos</a>
	<a href="/administracion/registros/triggermodulo" class="btn btn-primary"> Modulos Cursos</a>
	<a href="/administracion/registros/triggerseccionmodulo" class="btn btn-primary"> Seccion Modulo Curso</a>
	<a href="/administracion/registros/triggerpreguntas" class="btn btn-primary"> Preguntas Modulo Curso</a>
	<a href="/administracion/registros/triggerrespuesta" class="btn btn-primary"> Respuestas Modulo Curso</a>
	<a href="/administracion/registros/triggeralbum" class="btn btn-primary"> Albumes</a>
	<a href="/administracion/registros/triggerfoto" class="btn btn-primary"> Fotografias</a>
	<a href="/administracion/registros/triggeruser" class="btn btn-primary"> Usuarios</a>
</div>