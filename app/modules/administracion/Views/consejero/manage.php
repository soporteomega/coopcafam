﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<?php if ($this->content->content_id) { ?>
		<input type="hidden" name="id_content" value="<?= $this->content->content_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label class="control-label">Seccion</label>
			<select class="form-control" name="section" required>
				<option value="">Seleccione...</option>
				<?php foreach ($this->sections AS $key => $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"content_section") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-6 form-group">
			<label for="title"  class="control-label">Fecha</label>
			<input data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-language="es" type="text" value="<?php if ($this->content->content_date){?><?= $this->content->content_date; ?><?php } else { echo date("Y-m-d"); } ?>" name="date" id="date" class="form-control"  required>
		</div>
		<div class="col-xs-6 form-group">
			<label for="title"  class="control-label">Título</label>
			<input type="text" value="<?= $this->content->content_title; ?>" name="title" id="title" class="form-control" placeholder="Título" required>
		</div>
		<div class="col-xs-6 form-group">
			<label for="subtitle" >Subtítulo</label>
			<input type="text" value="<?= $this->content->content_subtitle; ?>" name="subtitle" id="subtitle" class="form-control" placeholder="Subtítulo">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label for="introduction" class="form-label" >Introdución</label>
			<textarea name="introduction" id="introduction"  placeholder="Introdución" class="form-control" rows="10" ><?= $this->content->content_introduction; ?></textarea>
		</div>
		<div class="col-xs-6 form-group">
			<label for="description" >Descripción</label>
			<textarea name="description" id="description"  placeholder="Descripción" class="form-control" rows="10" ><?= $this->content->content_description; ?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label for="image" >Imagen</label>
			<input type="file" name="image" id="image" class="form-control filestyle" >
			<?php if($this->content->content_image) { ?>
				<img src="/images/<?= $this->content->content_image; ?>" class="img-thumbnail thumbnail-administrator" />
			<?php } ?>
		</div>
		<div class="col-xs-6 form-group">
			<label for="banner" >Banner</label>
			<input type="file" name="banner" id="banner" class="form-control filestyle"/>
			<?php if($this->content->content_banner) { ?>
				<img src="/images/<?= $this->content->content_banner; ?>" class="img-thumbnail thumbnail-administrator" />
			<?php } ?>
		</div>
		<div class="col-xs-6 form-group">
			<label for="subtitle" >Disenio</label>
			<select class="form-control" name="disenio">
				<option value="">Default</option>
				<?php foreach ($this->disenios AS $key => $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"content_disenio") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-6 form-group">
			<label for="subtitle" >Enlace</label>
			<div class="input-group">
				<span class="input-group-addon">http://</span>
				<input type="text" value="<?= $this->content->content_link; ?>" name="link" id="link" class="form-control" >
			</div>
		</div>
		<div class="col-xs-12 form-group">
			<label for="documento">Documento</label>
			<input type="file" name="documento" id="documento" class="form-control filestyle"/>
			<?php if($this->content->content_documento) { ?>
				<a href="/files/<?= $this->content->content_documento; ?>" target="_blank"><?= $this->content->content_documento; ?></a>
			<?php } ?>
		</div>
	</div>

	
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/content">Cancelar</a>
		</div>
	</div>
</form>
</div>
<!-- tiny -->
<script language="javascript" type="text/javascript">
tinyMCE.init({
  mode : "textareas",
  theme: "modern",
  language_url: "/scripts/tinymce/langs/es.js",
  language: "es",
  plugins:"link , responsivefilemanager, table ,  visualblocks, code,paste,fullscreen" ,
  external_filemanager_path:"/scripts/tinymce/plugins/filemanager/",
  filemanager_title:"Responsive Filemanager" ,
  external_plugins: {
        "filemanager": "/scripts/tinymce/plugins/filemanager/plugin.min.js",
        "responsivefilemanager": "/scripts/tinymce/plugins/responsivefilemanager/plugin.min.js"
  },
  theme_modern_toolbar_location : "bottom",
  paste_auto_cleanup_on_paste : true,
  toolbar: "bold,italic,underline,|,alignleft, aligncenter, alignright, alignjustify, |,bullist,numlist,|,link,unlink,|,table,|,responsivefilemanager,visualblocks,|,removeformat,code,|,fullscreen",
  menubar: false,
  resize: true,
  browser_spellcheck : true ,
  statusbar: true,
  relative_urls: false
});
</script>
<!-- Fin tiny -->