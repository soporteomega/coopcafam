﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<?php if ($this->content->type_calendar_id) { ?>
		<input type="hidden" name="id" value="<?= $this->content->type_calendar_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label for="name"  class="control-label">Nombre</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,"type_calendar_name"); ?>" name="name" id="name" class="form-control" placeholder="Título" required>
		</div>
		<div class="col-xs-6 form-group">
			<label for="name"  class="control-label">Color</label>
			<div id="clp" class="input-group colorpicker-component ">
				<input type="text" value="<?= $this->getObjectVariable($this->content,"type_calendar_color"); ?>" name="color" id="color" class="form-control"  required>
			    <span class="input-group-addon"><i></i></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label class="control-label">Tipo</label>
			<select class="form-control" name="tipo" required>
				<option value="">Seleccione...</option>
				<?php foreach ($this->sections AS $key => $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"type_calendar_type") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-6 form-group">
			<label for="icono" >Icono</label>
			<input type="file" name="icono" id="icono" class="form-control  filestyle" data-buttonName="btn-primary">
			<?php if($this->content->type_calendar_icon) { ?>
				<img src="/images/<?= $this->content->type_calendar_icon; ?>" class="img-thumbnail thumbnail-administrator" />
			<?php } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/typecalendar">Cancelar</a>
		</div>
	</div>
</form>
</div>
<script>
    $(function() {
        $('#clp').colorpicker();
    });
    $(":file").filestyle({buttonName: "btn-primary",buttonText:' Cargar Archivo'});
</script>
