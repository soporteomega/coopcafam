﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<?php if ($this->content->cargo_id) { ?>
		<input type="hidden" name="id_content" value="<?= $this->content->cargo_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="form-group col-sm-12">
			<label for="nombre">Nombre</label>
			<input type="text" class="form-control" id="nombre" name="nombre"  placeholder="Nombre" value="<?= $this->content->cargo_nombre; ?>">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/solicitudes">Cancelar</a>
		</div>
	</div>
</form>
</div>
<!-- tiny -->
<script language="javascript" type="text/javascript">
tinyMCE.init({
  mode : "textareas",
  theme: "modern",
  language_url: "/scripts/tinymce/langs/es.js",
  language: "es",
  plugins:"link , responsivefilemanager, table ,  visualblocks, code,paste,fullscreen" ,
  external_filemanager_path:"/scripts/tinymce/plugins/filemanager/",
  filemanager_title:"Responsive Filemanager" ,
  external_plugins: {
        "filemanager": "/scripts/tinymce/plugins/filemanager/plugin.min.js",
        "responsivefilemanager": "/scripts/tinymce/plugins/responsivefilemanager/plugin.min.js"
  },
  theme_modern_toolbar_location : "bottom",
  paste_auto_cleanup_on_paste : true,
  toolbar: "bold,italic,underline,|,alignleft, aligncenter, alignright, alignjustify, |,bullist,numlist,|,link,unlink,|,table,|,responsivefilemanager,visualblocks,|,removeformat,code,|,fullscreen",
  menubar: false,
  resize: true,
  browser_spellcheck : true ,
  statusbar: true,
  relative_urls: false
});
</script>
<!-- Fin tiny -->