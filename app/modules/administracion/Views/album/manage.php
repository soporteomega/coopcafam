﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<?php if ($this->content->album_id) { ?>
		<input type="hidden" name="id" value="<?= $this->content->album_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label for="title"  class="control-label">Fecha</label>
			<input data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-language="es" type="text" value="<?php if ($this->content->album_fecha){?><?= $this->content->album_fecha; ?><?php } else { echo date("Y-m-d"); } ?>" name="fecha" id="fecha" class="form-control"  required>
		</div>
		<div class="col-xs-6 form-group">
			<label for="titulo"  class="control-label">Título</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'album_titulo'); ?>" name="titulo" id="titulo" class="form-control" placeholder="Título" required>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="descripcion" >Descripción</label>
			<textarea name="descripcion" id="descripcion"  placeholder="Descripción" class="form-control" rows="10" ><?= $this->getObjectVariable($this->content,'album_descripcion'); ?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="imagen" >Imagen</label>
			<input type="file" name="imagen" id="imagen" class="form-control  filestyle" data-buttonName="btn-primary">
			<?php if($this->content->album_imagen) { ?>
				<img src="/<?= $this->content->album_imagen; ?>" class="img-thumbnail thumbnail-administrator" />
			<?php } ?>
		</div>
	</div>

	
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/album">Cancelar</a>
		</div>
	</div>
</form>
</div>
<!-- tiny -->
<script language="javascript" type="text/javascript">
tinyMCE.init({
  mode : "textareas",
  theme: "modern",
  language_url: "/scripts/tinymce/langs/es.js",
  language: "es",
  plugins:"link , responsivefilemanager, table ,  visualblocks, code,paste" ,
  external_filemanager_path:"/scripts/tinymce/plugins/filemanager/",
  filemanager_title:"Responsive Filemanager" ,
  external_plugins: {
        "filemanager": "/scripts/tinymce/plugins/filemanager/plugin.min.js",
        "responsivefilemanager": "/scripts/tinymce/plugins/responsivefilemanager/plugin.min.js"
  },
  theme_modern_toolbar_location : "bottom",
  paste_auto_cleanup_on_paste : true,
  toolbar: "bold,italic,underline,|,alignleft, aligncenter, alignright, alignjustify, |,bullist,numlist,|,link,unlink,|,table,|,responsivefilemanager,visualblocks,|,removeformat,code",
  menubar: false,
  resize: true,
  browser_spellcheck : true ,
  statusbar: true,
  relative_urls: false
});
$(":file").filestyle({buttonName: "btn-primary",buttonText:' Cargar Archivo'});
</script>
<!-- Fin tiny -->