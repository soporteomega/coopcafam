﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<?php if ($this->content->documento_id) { ?>
		<input type="hidden" name="id" value="<?= $this->content->documento_id; ?>" />
	<?php }?>
	<input type="hidden" name="categoria" value="<?php if( $this->id_categoria > 0 ){ echo $this->id_categoria; } else { echo $this->getObjectVariable($this->content,'documento_categoria'); } ?>" />
	<div class="row">
		<div class="col-xs-5 form-group">
			<label for="nombre"  class="control-label">Nombre</label>
			<input type="text" value="<?= $this->content->documento_nombre; ?>" name="nombre" id="nombre" class="form-control" placeholder="Nombre" required>
		</div>
		<div class="col-xs-5 form-group">
			<label for="archivo" >Archivo: <a href="/files/<?= $this->content->documento_documento; ?>" target="_blank">/files/<?= $this->content->documento_documento; ?></a></label><br>
			
			<input type="file" name="archivo" id="archivo" class="form-control  filestyle" data-buttonName="btn-primary">
		</div>
		<div class="col-xs-2 form-group">
			<label   class="control-label">Descarga</label><br>
			<input type="checkbox" name="descarga" value="1" class="form-control switch-form " <?php if ($this->getObjectVariable($this->content, 'documento_descarga') == 1) { echo "checked";} ?>></input>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-4 form-group">
			<label for="tipo"  class="control-label">Tipo de documento</label>
			<input type="text" value="<?= $this->content->documento_tipo; ?>" name="tipo" id="tipo" class="form-control" placeholder="Tipo de documento" >
		</div>
		<div class="col-xs-4 form-group">
			<label for="proceso"  class="control-label">Proceso</label>
			<input type="text" value="<?= $this->content->documento_proceso; ?>" name="proceso" id="proceso" class="form-control" placeholder="Proceso" >
		</div>
		<div class="col-xs-4 form-group">
			<label for="consecutivo"  class="control-label">Consecutivo</label>
			<input type="text" value="<?= $this->content->documento_consecutivo; ?>" name="consecutivo" id="consecutivo" class="form-control" placeholder="Consecutivo" >
		</div>
	</div>

	<div class="row">
		<div class="col-xs-4 form-group">
			<label for="origen"  class="control-label">Origen de documento</label>
			<input type="text" value="<?= $this->content->documento_origen; ?>" name="origen" id="origen" class="form-control" placeholder="Origen de documento" >
		</div>
		<div class="col-xs-4 form-group">
			<label for="ubicacion"  class="control-label">Ubicación</label>
			<input type="text" value="<?= $this->content->documento_ubicacion; ?>" name="ubicacion" id="ubicacion" class="form-control" placeholder="Ubicación" >
		</div>
		<div class="col-xs-4 form-group">
			<label for="cargo"  class="control-label">Responsable del documento</label>
			<input type="text" value="<?= $this->content->documento_cargo; ?>" name="cargo" id="cargo" class="form-control" placeholder="Cargo del responsable" >
		</div>
	</div>

	<div class="row">
		<div class="col-xs-4 form-group">
			<label for="version"  class="control-label">Versión</label>
			<input type="text" value="<?= $this->content->documento_version; ?>" name="version" id="version" class="form-control" placeholder="Versión" >
		</div>
	</div>

	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/documento?categoria=<?php echo $this->id_categoria; ?>">Cancelar</a>
		</div>
	</div>
</form>
</div>