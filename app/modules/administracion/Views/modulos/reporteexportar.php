<table width="100%" border="1" cellpadding="10" cellspacing="0" class="tabla-resultados">
	<thead>
		<tr>
			<td>Fecha</td>
			<td>Hora</td>
			<td>Usuario</td>
			<td>Respuestas Bien</td>
			<td>Respuestas Mal</td>
			<td>Estado</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($this->contenido as $key => $value): ?>
			<tr>
				<td><?= $value['detalle']->respuesta_fecha;?></td>
				<td><?= $value['detalle']->respuesta_hora;?></td>
				<td><?= $value['detalle']->user_names." ".$value['detalle']->user_lastnames;?></td>
				<td><?= $value['bien'];?></td>
				<td><?= $value['mal'];?></td>
				<td><?php
					$porse = (100/($value['bien']+$value['mal']))*$value['bien'];
					if($porse >= 65){
						$estado = "Aprobado";
					} else {
						$estado = "Reprobado";
					}
					echo $estado;
				?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
