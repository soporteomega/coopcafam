<div class="text-right"><a class="btn btn-primary " target="_blank" href="/administracion/modulos/reporteexportar?modulo=<?= $this->modulo; ?>"> Exportar</a></div>
<br>
<table width="100%" border="1" cellpadding="10" cellspacing="0" class="tabla-resultados">
	<thead>
		<tr>
			<td>Fecha</td>
			<td>Hora</td>
			<td>Usuario</td>
			<td>Respuestas Bien</td>
			<td>Respuestas Mal</td>
			<td>Estado</td>
			<td></td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($this->contenido as $key => $value): ?>
			<?php
				$fecha = $value['detalle']->respuesta_fecha;
				$aux = explode("-",$fecha);
				$dia = $aux[2]*1;
				$mes = $aux[1]*1;
				$anio1 = $aux[0];

				$porse = (100/($value['bien']+$value['mal']))*$value['bien'];
				if($porse >= 65){
					$estado = "Aprobado";
				} else {
					$estado = "Reprobado";
				}
			?>
			<?php if($_GET['anio']=="" or ($_GET['anio']!="" and $_GET['anio']==$anio1)){ ?>
			<?php if($_GET['mes']=="" or ($_GET['mes']!="" and $_GET['mes']==$mes)){ ?>
			<?php if($_GET['estado']=="" or ($_GET['estado']!="" and $_GET['estado']==$estado)){ ?>
				<tr>
					<td><?= $value['detalle']->respuesta_fecha;?></td>
					<td><?= $value['detalle']->respuesta_hora;?></td>
					<td><?= $value['detalle']->user_names." ".$value['detalle']->user_lastnames;?></td>
					<td><?= $value['bien'];?></td>
					<td><?= $value['mal'];?></td>
					<td><?php echo $estado; ?></td>
					<td>
						<a class="btn btn-info btn-xs" href="/page/cursos/evaluacion?modulo=<?= $value['detalle']->modulo_id;?>&usuario=<?= $value['detalle']->user_id;?>" target="_blank"><i class="glyphicon glyphicon-eye-open"></i> Detalle</a>
					</td>
				</tr>
			<?php } ?>
			<?php } ?>
			<?php } ?>
		<?php endforeach ?>
	</tbody>
</table>
