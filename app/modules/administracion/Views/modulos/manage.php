﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<input type="hidden" name="curso" value="<?php echo $this->getObjectVariable($this->content,'cursos_id'); ?>">
	<?php if ($this->content->modulo_id) { ?>
		<input type="hidden" name="id" value="<?= $this->content->modulo_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="titulo"  class="control-label">Título</label>
			<input type="text" value="<?= $this->getObjectVariable($this->content,'modulo_titulo'); ?>" name="titulo" id="titulo" class="form-control" placeholder="Título" required>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="descripcion" >Descripción</label>
			<textarea name="descripcion" id="descripcion"  placeholder="Descripción" class="form-control" rows="10" ><?= $this->getObjectVariable($this->content,'modulo_descripcion'); ?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label for="columnas" >Columnas secciones</label>
			<select name="columnas" id="columnas" class="form-control">
				<?php for($i=1;$i<=3;$i++){ ?>
					<option value="<?php echo $i; ?>" <?php if($this->getObjectVariable($this->content,'modulo_columnas')==$i){ echo 'selected'; } ?> ><?php echo $i; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="/administracion/modulos?curso=<?= $this->getObjectVariable($this->content,'cursos_id'); ?>">Cancelar</a>
		</div>
	</div>
</form>
</div>
<!-- tiny -->
<script language="javascript" type="text/javascript">
tinyMCE.init({
  mode : "textareas",
  theme: "modern",
  language_url: "/scripts/tinymce/langs/es.js",
  language: "es",
  plugins:"link , responsivefilemanager, table ,  visualblocks, code,paste" ,
  external_filemanager_path:"/scripts/tinymce/plugins/filemanager/",
  filemanager_title:"Responsive Filemanager" ,
  external_plugins: {
        "filemanager": "/scripts/tinymce/plugins/filemanager/plugin.min.js",
        "responsivefilemanager": "/scripts/tinymce/plugins/responsivefilemanager/plugin.min.js"
  },
  theme_modern_toolbar_location : "bottom",
  paste_auto_cleanup_on_paste : true,
  toolbar: "bold,italic,underline,|,alignleft, aligncenter, alignright, alignjustify, |,bullist,numlist,|,link,unlink,|,table,|,responsivefilemanager,visualblocks,|,removeformat,code",
  menubar: false,
  resize: true,
  browser_spellcheck : true ,
  statusbar: true,
  relative_urls: false
});
$(":file").filestyle({buttonName: "btn-primary",buttonText:' Cargar Archivo'});
</script>
<!-- Fin tiny -->