<div class="row">
	<div class="col-md-10">
		<h2>Reporte preguntas</h2>
		<table width="100%" border="1" cellpadding="10" cellspacing="0" class="tabla-resultados tabla_pad">
			<tr>
				<td><b>No</b></td>
				<td><b>Pregunta</b></td>
				<td><b>Respuestas</b></td>
				<td><b>Correctas</b></td>
				<td><b>%</b></td>
				<td><b>Incorrectas</b></td>
				<td><b>%</b></td>
			</tr>
			<?php foreach($this->preguntas as $pregunta): ?>
				<?php
					$pregunta_id =  $pregunta->pregunta_id;
					foreach ($this->respuestas as $respuesta) {
						if($respuesta->pregunta_id == $pregunta_id){
							$totales[$pregunta_id]++;
							if($respuesta->respuesta_correcto){
								$totales2[$pregunta_id]['correcto']++;
							}else{
								$totales2[$pregunta_id]['incorrecto']++;
							}
						}
					}
					$j++;
				?>
				<tr>
					<td><?php echo $j; ?></td>
					<td class="text-left"><div class="imagen_pregunta"><?php echo $pregunta->pregunta_pregunta;?></div></td>
					<td><?php echo $totales[$pregunta_id]; ?></td>
					<td><?php echo $totales2[$pregunta_id]['correcto']*1; ?></td>
					<td><?php echo number_format($totales2[$pregunta_id]['correcto']*1/$totales[$pregunta_id]*100,1); ?></td>
					<td><?php echo $totales2[$pregunta_id]['incorrecto']; ?></td>
					<td><?php echo number_format($totales2[$pregunta_id]['incorrecto']*1/$totales[$pregunta_id]*100,1); ?></td>
				</tr>
			<?php endforeach ?>
		</table>

	</div>
</div>