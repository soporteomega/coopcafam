<?php
$anio = date("Y");
$anio_actual = date("Y");
if($_GET['anio']!=""){
	$anio = $_GET['anio'];
}
$meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
$modulo = $_GET['modulo'];
?>


<table width="100%" border="1" cellpadding="10" cellspacing="0" class="tabla-resultados oculto">
	<thead>
		<tr>
			<td>Fecha</td>
			<td>Hora</td>
			<td>Usuario</td>
			<td>Respuestas Bien</td>
			<td>Respuestas Mal</td>
			<td>Estado</td>
			<td></td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($this->contenido as $key => $value): ?>
			<tr>
				<td><?= $value['detalle']->respuesta_fecha;?></td>
				<td><?= $value['detalle']->respuesta_hora;?></td>
				<td><?= $value['detalle']->user_names." ".$value['detalle']->user_lastnames;?></td>
				<td><?= $value['bien'];?></td>
				<td><?= $value['mal'];?></td>
				<td><?php
					$porse = (100/($value['bien']+$value['mal']))*$value['bien'];
					if($porse >= 65){
						$estado = "Aprobado";
					} else {
						$estado = "Reprobado";
					}
					echo $estado;
				?></td>
				<td>
				</td>
			</tr>
			<?php
				$fecha = $value['detalle']->respuesta_fecha;
				$aux = explode("-",$fecha);
				$dia = $aux[2]*1;
				$mes = $aux[1]*1;
				$anio1 = $aux[0];
				$totales[$mes][$anio1]++;
				$totales2[$mes][$anio1][$estado]++;
			?>
		<?php endforeach ?>
	</tbody>
</table>

<h2>Aprobados y reprobados por mes</h2>

	<form method="get">
		Año <select name="anio">
			<?php for($i=$anio_actual;$i>=2016;$i--){ ?>
				<option value="<?php echo $i; ?>" <?php if($anio==$i){ echo 'selected'; } ?>><?php echo $i; ?></option>
			<?php } ?>
		</select>
		<button type="submit">Filtrar</button>
		<input type="hidden" name="modulo" value="<?php echo $_GET['modulo']; ?>">
	</form>
	<br>

<table width="100%" border="1" cellpadding="10" cellspacing="0" class="tabla-resultados">
	<tr>
		<td><b>Mes</b></td>
		<td><b>Usuarios</b></td>
		<td><b>Aprobados</b></td>
		<td><b>%</b></td>
		<td><b>Reprobados</b></td>
		<td><b>%</b></td>
	</tr>
	<?php for($i=1;$i<=12;$i++){ ?>
		<?php
			$porcentaje_aprobados = number_format($totales2[$i][$anio]['Aprobado']/$totales[$i][$anio]*100,1);
			$porcentaje_reprobados = number_format($totales2[$i][$anio]['Reprobado']/$totales[$i][$anio]*100,1);
		?>
	<tr>
		<td><?php echo $meses[$i]; ?></td>
		<td><a href="/administracion/modulos/reporte/?modulo=<?php echo $modulo; ?>&anio=<?php echo $anio ?>&mes=<?php echo $i; ?>" class="btn btn-sm btn-success"><?php echo $totales[$i][$anio]*1; $total1+=$totales[$i][$anio]*1; ?></a></td>
		<td><a href="/administracion/modulos/reporte/?modulo=<?php echo $modulo; ?>&anio=<?php echo $anio ?>&mes=<?php echo $i; ?>&estado=Aprobado" class="btn btn-sm btn-success"><?php echo $totales2[$i][$anio]['Aprobado']*1; $total2+=$totales2[$i][$anio]['Aprobado']*1; ?></a></td>
		<td><?php echo $porcentaje_aprobados; ?></td>
		<td><a href="/administracion/modulos/reporte/?modulo=<?php echo $modulo; ?>&anio=<?php echo $anio ?>&mes=<?php echo $i; ?>&estado=Reprobado" class="btn btn-sm btn-success"><?php echo $totales2[$i][$anio]['Reprobado']*1; $total3+=$totales2[$i][$anio]['Reprobado']*1; ?></a></td>
		<td><?php echo $porcentaje_reprobados; ?></td>
	</tr>
		<?php
			$titulos[$i]=$meses[$i];
			$valores[$i]=$totales[$i][$anio]*1;
			$valores2[$i]=$totales2[$i][$anio]['Aprobado']*1;
			$valores3[$i]=$totales2[$i][$anio]['Reprobado']*1;
		?>
	<?php } ?>
	<tr>
		<td><b>Total</b></td>
		<td><a href="/administracion/modulos/reporte/?modulo=<?php echo $modulo; ?>&anio=<?php echo $anio ?>" class="btn btn-sm btn-primary"><b><?php echo $total1; ?></b></a></td>
		<td><a href="/administracion/modulos/reporte/?modulo=<?php echo $modulo; ?>&anio=<?php echo $anio ?>&estado=Aprobado" class="btn btn-sm btn-primary"><b><?php echo $total2; ?></b></a></td>
		<td><?php echo number_format($total2/$total1*100,1); ?></td>
		<td><a href="/administracion/modulos/reporte/?modulo=<?php echo $modulo; ?>&anio=<?php echo $anio ?>&estado=Reprobado" class="btn btn-sm btn-primary"><b><?php echo $total3; ?></b></a></td>
		<td><?php echo number_format($total3/$total1*100,1); ?></td>
	</tr>
</table>



<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Mes', 'Usuarios','Aprobados','Reprobados'],

		<?php for($x=1;$x<=$i;$x++){?>
			<?php
				$titulo = $titulos[$x];
				$valor = $valores[$x];
				$valor2 = $valores2[$x];
				$valor3 = $valores3[$x];
				if($titulo!=""){
					echo "['".$titulo."', ".$valor.", ".$valor2.", ".$valor3."],";
				}
			?>
		<?php }//for?>
    ]);

    var options = {
      colors: ['#55BA9A','#348DA7','#EE4846'],
      title: '',
        vAxis: {
          viewWindowMode:'pretty'
        }
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }
</script>
<div id="chart_div" style="width:100%; height:400px;"></div>