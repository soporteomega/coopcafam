<div class="container-fluid">
	<div class="text-right">
		<a class="btn btn-primary" href="<?php echo $this->route;?>/enviareventos"> <i class="glyphicon glyphicon-envelope"></i> Enviar Correo de Nuevos Eventos </a><br><br>
		<a class="btn btn-success" href="<?php echo $this->route;?>/manage">Crear Nuevo</a>

	</div>
	<div>
		<table class=" table table-striped  table-hover table-administrator text-left">
			<thead>
				<tr>
					<td>Fecha</td>
					<td>Titulo</td>
					<td>Tipo Calendario</td>
					<td>Tipo Usuario</td>
					<td width="250"></td>
				</tr>
			</thead>
			<tbody>
				<?php foreach($this->lists as $content){ ?>
				<?php $id =  $content->calendar_id; ?>
					<tr>
						<td><?=$content->calendar_startdate;?></td>
						<td><?=$content->calendar_name;?></td>
						<td><?=$content->type_calendar_name;?></td>
						<td><?=$content->calendar_type_user;?></td>
						<td class="text-right">
							<div>
								<a class="btn btn-primary btn-xs" href="<?php echo $this->route;?>/manage?id=<?= $id ?>"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
								<a class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal<?= $id ?>" ><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
							</div>
							<!-- Modal -->
							<div class="modal fade text-left" id="modal<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  	<div class="modal-dialog" role="document">
							    	<div class="modal-content">
							      		<div class="modal-header">
							        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        		<h4 class="modal-title" id="myModalLabel">Eliminar Registro</h4>
							      	</div>
							      	<div class="modal-body">
							        	<div class="">¿Esta seguro de eliminar este registro?</div>
							      	</div>
								      <div class="modal-footer">
								        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
								        	<a class="btn btn-danger" href="<?php echo $this->route;?>/delete?id=<?= $id ?>&csrf=<?= $this->csrf;?>" >Eliminar</a>
								      </div>
							    	</div>
							  	</div>
							</div>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<input type="hidden" id="csrf" value="<?php echo $this->csrf ?>">
	<input type="hidden" id="order-route" value="<?php echo $this->route; ?>/order">
	<div align="center">
		<ul class="pagination">
	    <?php
	    	$url = $this->route;
	        if ($this->totalpages > 1) {
	            if ($this->page != 1)
	                echo '<li><a href="'.$url.'?page='.($this->page-1).'"> &laquo; Anterior </a></li>';
	            for ($i=1;$i<=$this->totalpages;$i++) {
	                if ($this->page == $i)
	                    echo '<li class="active"><a class="paginaactual">'.$this->page.'</a></li>';
	                else
	                    echo '<li><a href="'.$url.'?page='.$i.'">'.$i.'</a></li>  ';
	            }
	            if ($this->page != $this->totalpages)
	                echo '<li><a href="'.$url.'?page='.($this->page+1).'">Siguiente &raquo;</a></li>';
	        }
	  ?>
	  </ul>
	</div>
</div>