﻿<div class="container-fluid">
<form class="text-left" enctype="multipart/form-data" method="post" action="<?php echo $this->routeform;?>" data-toggle="validator">
	<input type="hidden" name="csrf" value="<?php echo $this->csrf ?>">
	<?php if ($this->content->calendar_id) { ?>
		<input type="hidden" name="id" value="<?= $this->content->calendar_id; ?>" />
	<?php }?>
	<div class="row">
		<div class="col-xs-3 form-group">
			<label class="control-label">Tipo Calendario</label>
			<select class="form-control" name="type" required>
				<option value="">Seleccione...</option>
				<?php foreach ($this->types AS $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"calendar_type") == $value->type_calendar_id ){ echo "selected"; }?> value="<?php echo $value->type_calendar_id; ?>" /> <?= $value->type_calendar_name; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-3 form-group">
			<label class="control-label">Tipo Usuario</label>
			<select class="form-control" name="tipo" required>
				<option value="">Seleccione...</option>
				<?php foreach ($this->sections AS $key => $value ){?>
					<option <?php if($this->getObjectVariable($this->content,"calendar_type_user") == $key ){ echo "selected"; }?> value="<?php echo $key; ?>" /> <?= $value; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-xs-6 form-group">
			<label for="name"  class="control-label">Nombre</label>
			<input type="text" value="<?= $this->content->calendar_name; ?>" name="name" id="name" class="form-control" placeholder="Nombre" required>
		</div>
		<div class="col-xs-4 form-group">
			<label for="name"  class="control-label">Fecha Inicio</label>
			<input data-provide="datepicker" data-date-format="yyyy-mm-dd" type="text" value="<?php if ($this->content->calendar_startdate){?><?= $this->content->calendar_startdate; ?><?php } else { echo date("Y-m-d"); } ?>" name="startdate" id="startdate" class="form-control"  required>
		</div>
		<div class="col-xs-2 form-group">
			<label for="name"  class="control-label">Hora Inicio</label>
			<div class="input-group bootstrap-timepicker timepicker">
	            <input value="<?php if ($this->content->calendar_starttime){?><?= $this->content->calendar_starttime; ?><?php } ?>" name="starttime" id="starttime" class="form-control timepickerform"  required>
	            <span class="input-group-addon" style="cursor:pointer;"><i class="glyphicon glyphicon-time"></i></span>
        	</div>
		</div>
		<div class="col-xs-4 form-group">
			<label for="name" class="control-label">Fecha Fin</label>
			<input data-provide="datepicker" data-date-format="yyyy-mm-dd" type="text" value="<?php if ($this->content->calendar_enddate){?><?= $this->content->calendar_enddate; ?><?php } else { echo date("Y-m-d"); } ?>" name="enddate" id="enddate" class="form-control"  required>
		</div>
		<div class="col-xs-2 form-group">
			<label for="name"  class="control-label">Hora Fin</label>
			<div class="input-group bootstrap-timepicker timepicker">
	            <input value="<?php if ($this->content->calendar_endtime){?><?= $this->content->calendar_endtime; ?><?php } ?>" name="endtime" id="endtime" class="form-control timepickerform"  required>
	            <span class="input-group-addon" style="cursor:pointer;"><i class="glyphicon glyphicon-time"></i></span>
        	</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12 form-group">
			<label for="description" >Descripción</label>
			<textarea name="description" id="description"  placeholder="Descripción" class="form-control" rows="10" ><?= $this->content->calendar_description; ?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6 form-group">
			<label for="image" >Imagen</label>
			<input type="file" name="image" id="image" class="form-control  filestyle" data-buttonName="btn-primary">
			<?php if($this->content->calendar_image) { ?>
				<img src="/images/<?= $this->content->calendar_image; ?>" class="img-thumbnail thumbnail-administrator" />
			<?php } ?>
		</div>
		<div class="col-xs-6 form-group">
			<label for="banner" >Banner</label>
			<input type="file" name="banner" id="banner" class="form-control"/>
			<?php if($this->content->calendar_banner) { ?>
				<img src="/images/<?= $this->content->calendar_banner; ?>" class="img-thumbnail thumbnail-administrator" />
			<?php } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<button class="btn btn-success btn-block" type="submit">Guardar</button>
		</div>
		<div class="col-xs-6">
			<a class="btn btn-primary btn-block" href="<?php echo $this->route;?>">Cancelar</a>
		</div>
	</div>
</form>
</div>
<div class="modal hide fade">
	<div class="modal-header">
		<h1>Timepicker inside a modal</h1>
	</div>
	<div class="modal-body">
		<div class="input-group bootstrap-timepicker timepicker">
				<input id="timepicker4" type="text" value="10:35 AM" class="form-control input-small">
				<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
		</div>
	</div>
</div>
<!-- tiny -->
<script language="javascript" type="text/javascript">
tinyMCE.init({
  mode : "textareas",
  theme: "modern",
  language_url: "/scripts/tinymce/langs/es.js",
  language: "es",
  plugins:"link , responsivefilemanager, table ,  visualblocks, code,paste" ,
  external_filemanager_path:"/scripts/tinymce/plugins/filemanager/",
  filemanager_name:"Responsive Filemanager" ,
  external_plugins: {
        "filemanager": "/scripts/tinymce/plugins/filemanager/plugin.min.js",
        "responsivefilemanager": "/scripts/tinymce/plugins/responsivefilemanager/plugin.min.js"
  },
  theme_modern_toolbar_location : "bottom",
  paste_auto_cleanup_on_paste : true,
  toolbar: "bold,italic,underline,|,alignleft, aligncenter, alignright, alignjustify, |,bullist,numlist,|,link,unlink,|,table,|,responsivefilemanager,visualblocks,|,removeformat,code",
  menubar: false,
  resize: true,
  browser_spellcheck : true ,
  statusbar: true,
  relative_urls: false
});
$(":file").filestyle({buttonName: "btn-primary",buttonText:' Cargar Archivo'});
$('.timepickerform').timepicker({
	minuteStep: 1,
    showInputs: true,
   
});
</script>

<!-- Fin tiny -->