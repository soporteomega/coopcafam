<?php 
			/**
			* clase que genera la consulta del trigger en la base de datos
			*/
			class Administracion_Model_DbTable_Triggercursos extends Db_Table
			{
				/**
				 * [ nombre de la tabla actual]
				 * @var string
				 */
				protected $_name = 'trigger_cursos';

				/**
				 * [ identificador de la tabla actual en la base de datos]
				 * @var string
				 */
				protected $_id = '';

				/**
				 * insert recibe la informacion de un  y la inserta en la base de datos
				 * @param  array  array con la informacion con la cual se va a realizar la insercion en la base de datos
				 * @return integer      identificador del  registro que se inserto
				 */
				public function getList($filters,$order)
			    {
			        $filter = '';
			        if($filters != ''){
			            $filter = ' WHERE '.$filters;
			        }
			        $orders ='';
			        if($order != ''){
			            $orders = ' ORDER BY '.$order;
			        }
			        $select = 'SELECT * FROM '.$this->_name.' '.$filter.' '.$orders;
			        $res = $this->_conn->query( $select )->fetchAsObject();
			        return $res;
			    }
			}