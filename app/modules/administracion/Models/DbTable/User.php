<?php

/**
*
*/

class Administracion_Model_DbTable_User extends Db_Table
{
	protected $_name = 'user';
    protected $_id = "user_id";

    public function insert($data)
    {
        $user_names = $data['names'];
        $user_lastnames = $data['lastnames'];
        $user_email = $data['email'];
        $user_idnumber = $data['idnumber'];
        $user_city = $data['city'];
        $user_country = $data['country'];
        $user_phone = $data['phone'];
        $user_address = $data['address'];
        $user_position = $data['position'];
        $user_level = $data['level'];
        $user_state = $data['state'];
        $user_area = $data['area'];
        $user_user = $data['user'];
        $user_date = $data['date'];
        $user_password = password_hash($data['password'], PASSWORD_DEFAULT);
        $user_current_user = $data['current_user'];
        echo $query = "INSERT INTO user(user_names, user_lastnames, user_email, user_idnumber, user_city, user_country, user_phone, user_address, user_position, user_level, user_state, user_user, user_password,user_current_user, user_date) VALUES ('$user_names','$user_lastnames','$user_email','$user_idnumber','$user_city','$user_country','$user_phone','$user_address', '$user_position','$user_level','$user_state','$user_user','$user_password','$user_current_user','$user_date')";
        $res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
    }

    public function update($data,$id){
        $user_names = $data['names'];
        $user_lastnames = $data['lastnames'];
        $user_email = $data['email'];
        $user_idnumber = $data['idnumber'];
        $user_city = $data['city'];
        $user_country = $data['country'];
        $user_phone = $data['phone'];
        $user_address = $data['address'];
        $user_position = $data['position'];
        $user_level = $data['level'];
        $user_state = $data['state'];
        $user_area = $data['area'];
        $user_user = $data['user'];
        if($data['date'] == ''){
            $user_date = "0000-00-00";
        } else {
             $user_date = $data['date'];
        }
        $changepasword = '';
        if($data['password']!=''){
            $user_password = password_hash($data['password'], PASSWORD_DEFAULT);
            $changepasword = " , user_password = '$user_password'";
        }
        $user_current_user = $data['current_user'];
        $query = "UPDATE user SET user_names = '$user_names',user_lastnames = '$user_lastnames',user_email = '$user_email',user_idnumber = '$user_idnumber',user_city = '$user_city',user_country = '$user_country',user_phone = '$user_phone',user_address = '$user_address', user_position = '$user_position', user_level = '$user_level',user_state = '$user_state',user_user = '$user_user',user_current_user = '$user_current_user', user_date = '$user_date' $changepasword WHERE user_id= '$id' ";
        $res = $this->_conn->query($query);
    }

	public function searchUser($user)
    {
        $res = $this->_conn->query('SELECT * FROM '.$this->_name.' WHERE user_id = "'.$user.'"')->fetchAsObject();
        return $res[0];
    }

    public function loginUser($user,$password)
    {
        $res = $this->_conn->query('SELECT * FROM '.$this->_name.' WHERE user_id = "'.$user.'"')->fetchAsObject();
        return $res[0];
    }

    public function userCalendar($fecha,$fecha2)
    {
       $query = "SELECT user.* FROM user LEFT JOIN calendar ON calendar_user = user_id WHERE NOT EXISTS (SELECT * FROM calendar WHERE calendar_user = user_id AND calendar_type = 4 AND calendar_startdate >= '$fecha' AND  calendar_startdate <= '$fecha2' ) AND user_date IS NOT NULL ORDER BY user_date ASC";
        $res = $this->_conn->query($query)->fetchAsObject();
        return $res;
    }

    public function useridCalendar($fecha,$id)
    {
        $query = "SELECT user.*, calendar.* FROM user  WHEREWHERE NOT EXISTS (SELECT * FROM calendar WHERE calendar_user = user_id AND calendar_type = 4 AND calendar_startdate >= '$fecha' AND  calendar_startdate <= '$fecha2' ) AND user_date IS NOT NULL AND user_id ='$id'";
        $res = $this->_conn->query($query)->fetchAsObject();
        return $res;
    }




}