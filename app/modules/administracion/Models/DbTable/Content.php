<?php 
/**
* 
*/
class Administracion_Model_DbTable_Content extends Db_Table
{
	protected $_name = 'content';
	protected $_id = 'content_id';


	public function insert($data){
		$date = $data['date'];
		$title = $data['title'];
		$subtitle = $data['subtitle'];
		$introduction = $data['introduction'];
		$description = $data['description'];
		$section = $data['section'];
		$link = $data['link'];
		$image = $data['image'];
		$banner = $data['banner'];
		$content_disenio = $data['disenio'];
		$query = "INSERT INTO content(content_date,content_title, content_subtitle, content_introduction, content_description, content_image, content_banner, content_section, content_link, content_disenio) VALUES ('$date','$title','$subtitle','$introduction','$description','$image','$banner','$section','$link', '$content_disenio')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}
	public function update($data,$id){
		$date = $data['date'];
		$title = $data['title'];
		$subtitle = $data['subtitle'];
		$introduction = $data['introduction'];
		$description = $data['description'];
		$section = $data['section'];
		$link = $data['link'];
		$image = $data['image'];
		$banner = $data['banner'];
		$content_disenio = $data['disenio'];
		$query = "UPDATE content SET content_date ='$date', content_title='$title', content_subtitle='$subtitle', content_introduction='$introduction', content_description ='$description', content_image ='$image', content_banner='$banner', content_section ='$section', content_link = '$link', content_disenio = '$content_disenio' WHERE content_id = '".$id."'";
		$res = $this->_conn->query($query);
	}
	public function editFieldContent($id,$field,$value){
		$query =' UPDATE '.$this->_name.' SET '.$field.' = "'.$value.' WHERE content_id = "'.$id.'"';
		$res = $this->_conn->query($query);
	}
}