<?php 
/**
* 
*/
class Administracion_Model_DbTable_Calendar extends Db_Table
{
	protected $_name = 'calendar';
	protected $_id = 'calendar_id';


	public function insert($data){
		$calendar_name = $data['name'];
		$calendar_startdate = $data['startdate'];
		$calendar_starttime = $data['starttime'];
		$calendar_enddate = $data['enddate'];
		if($calendar_enddate == ''){
			$calendar_enddate = 'null';
		}
		$calendar_endtime = $data['endtime'];
		$calendar_description = $data['description'];
		$calendar_type = $data['type'];
		$calendar_type_user = $data['tipo'];
		$calendar_createdby = $data['createdby'];
		$calendar_banner = $data['banner'];
		$calendar_image = $data['image'];
		$calendar_user = $data['user'];
		$query = "INSERT INTO calendar( calendar_name, calendar_startdate, calendar_starttime, calendar_enddate, calendar_endtime, calendar_description, calendar_type, calendar_type_user, calendar_createdby, calendar_banner, calendar_image, calendar_user) VALUES ('$calendar_name', '$calendar_startdate', '$calendar_starttime', $calendar_enddate, '$calendar_endtime', '$calendar_description', '$calendar_type','$calendar_type_user', '$calendar_createdby', '$calendar_banner', '$calendar_image', '$calendar_user')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	public function update($data,$id){
		$calendar_name = $data['name'];
		$calendar_startdate = $data['startdate'];
		$calendar_starttime = $data['starttime'];
		$calendar_enddate = $data['enddate'];
		if($calendar_enddate == ''){
			$calendar_enddate = 'null';
		}
		$calendar_endtime = $data['endtime'];
		$calendar_description = $data['description'];
		$calendar_type = $data['type'];
		$calendar_type_user = $data['tipo'];
		$calendar_createdby = $data['createdby'];
		$calendar_banner = $data['banner'];
		$calendar_image = $data['image'];
		$calendar_user = $data['user'];
		$query = "UPDATE calendar SET calendar_name = '$calendar_name', calendar_startdate = '$calendar_startdate', calendar_starttime = '$calendar_starttime', calendar_enddate = $calendar_enddate,  calendar_endtime = '$calendar_endtime', calendar_description = '$calendar_description', calendar_type = '$calendar_type', calendar_type_user = '$calendar_type_user', calendar_createdby = '$calendar_createdby', calendar_banner = '$calendar_banner', calendar_image = '$calendar_image', calendar_user = '$calendar_user' WHERE calendar_id = '".$id."'";
		$res = $this->_conn->query($query);
	}


	public function getList($filters,$order)
    {
       $filter = '';
        if($filters != ''){
            $filter = ' WHERE '.$filters;
        }
        $orders ="";
        if($order != ''){
            $orders = ' ORDER BY '.$order;
        }
        $select = 'SELECT * FROM '.$this->_name.' LEFT JOIN  type_calendar ON type_calendar_id = calendar_type '.$filter.' '.$orders;
        $res = $this->_conn->query($select)->fetchAsObject();
        return $res;
    }

    public function getListPages($filters,$order,$page,$amount)
    {
       $filter = '';
        if($filters != ''){
            $filter = ' WHERE '.$filters;
        }
        $orders ="";
        if($order != ''){
            $orders = ' ORDER BY '.$order;
        }
        $select = 'SELECT * FROM '.$this->_name.' LEFT JOIN  type_calendar ON type_calendar_id = calendar_type '.$filter.' '.$orders.' LIMIT '.$page.' , '.$amount;
        $res = $this->_conn->query($select)->fetchAsObject();
        return $res;
    }

}