<?php 
/**
* 
*/
class Administracion_Model_DbTable_Respuesta extends Db_Table
{
	protected $_name = 'respuesta';
	protected $_id = 'respuesta_id';

	public function respuestas($modulo)
	{
		$query = "SELECT respuesta.* , preguntas.*, user.* FROM (respuesta LEFT JOIN preguntas ON preguntas.pregunta_id = respuesta.pregunta_id) LEFT JOIN user ON respuesta.usuario_id = user.user_id WHERE preguntas.modulo_id = '$modulo' AND pregunta_activo='1' GROUP BY respuesta.usuario_id ";
		$res = $this->_conn->query($query)->fetchAsObject();
		//echo $query;
        return $res;
	}

	public function respondio($usuario,$modulo)
	{
		$query = "SELECT respuesta.* , preguntas.* FROM respuesta LEFT JOIN preguntas ON preguntas.pregunta_id = respuesta.pregunta_id WHERE preguntas.modulo_id = '$modulo' AND pregunta_activo='1' AND respuesta.usuario_id = '$usuario'";
		$res = $this->_conn->query($query)->fetchAsObject();
        return $res;
	}


}