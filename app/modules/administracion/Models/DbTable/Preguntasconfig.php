<?php 
/**
* clase que genera la insercion y edicion  de configuracion evaluaci&oacute;n en la base de datos
*/
class Administracion_Model_DbTable_Preguntasconfig extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'preguntas_config';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'preguntas_config_id';

	/**
	 * insert recibe la informacion de un configuraci&oacute;n y la inserta en la base de datos
	 * @param  array Array array con la informacion con la cual se va a realizar la insercion en la base de datos
	 * @return integer      identificador del  registro que se inserto
	 */
	public function insert($data){
		$preguntas_config_modulo_id = $data['preguntas_config_modulo_id'];
		$preguntas_config_orden = $data['preguntas_config_orden'];
		$preguntas_config_cantidad = $data['preguntas_config_cantidad'];
		$preguntas_config_tiempo = $data['preguntas_config_tiempo'];
		$preguntas_config_oportunidades = $data['preguntas_config_oportunidades'];
		$preguntas_mostrar_certificado = $data['preguntas_mostrar_certificado'];
		$preguntas_config_gif_aprobado = $data['preguntas_config_gif_aprobado'];
		$preguntas_config_gif_reprobado = $data['preguntas_config_gif_reprobado'];
		$preguntas_config_gif_reloj = $data['preguntas_config_gif_reloj'];
		$preguntas_config_orden_respuestas = $data['preguntas_config_orden_respuestas'];

		$query = "INSERT INTO preguntas_config( preguntas_config_modulo_id, preguntas_config_orden, preguntas_config_cantidad, preguntas_config_tiempo, preguntas_config_oportunidades,preguntas_mostrar_certificado, preguntas_config_gif_aprobado, preguntas_config_gif_reprobado, preguntas_config_gif_reloj, preguntas_config_orden_respuestas) VALUES ( '$preguntas_config_modulo_id', '$preguntas_config_orden', '$preguntas_config_cantidad', '$preguntas_config_tiempo','$preguntas_config_oportunidades','$preguntas_mostrar_certificado', '$preguntas_config_gif_aprobado', '$preguntas_config_gif_reprobado', '$preguntas_config_gif_reloj','$preguntas_config_orden_respuestas')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	/**
	 * update Recibe la informacion de un configuraci&oacute;n  y actualiza la informacion en la base de datos
	 * @param  array Array Array con la informacion con la cual se va a realizar la actualizacion en la base de datos
	 * @param  integer    identificador al cual se le va a realizar la actualizacion
	 * @return void
	 */
	public function update($data,$id){

		$preguntas_config_modulo_id = $data['preguntas_config_modulo_id'];
		$preguntas_config_orden = $data['preguntas_config_orden'];
		$preguntas_config_cantidad = $data['preguntas_config_cantidad'];
		$preguntas_config_tiempo = $data['preguntas_config_tiempo'];
		$preguntas_config_oportunidades = $data['preguntas_config_oportunidades'];
		$preguntas_mostrar_certificado = $data['preguntas_mostrar_certificado'];
		$preguntas_config_gif_aprobado = $data['preguntas_config_gif_aprobado'];
		$preguntas_config_gif_reprobado = $data['preguntas_config_gif_reprobado'];
		$preguntas_config_gif_reloj = $data['preguntas_config_gif_reloj'];
		$preguntas_config_orden_respuestas = $data['preguntas_config_orden_respuestas'];


		$query = "UPDATE preguntas_config SET  preguntas_config_modulo_id = '$preguntas_config_modulo_id', preguntas_config_orden = '$preguntas_config_orden', preguntas_config_cantidad = '$preguntas_config_cantidad', preguntas_config_tiempo = '$preguntas_config_tiempo', preguntas_config_oportunidades='$preguntas_config_oportunidades', preguntas_mostrar_certificado='$preguntas_mostrar_certificado', preguntas_config_gif_aprobado = '$preguntas_config_gif_aprobado', preguntas_config_gif_reprobado = '$preguntas_config_gif_reprobado', preguntas_config_gif_reloj = '$preguntas_config_gif_reloj', preguntas_config_orden_respuestas='$preguntas_config_orden_respuestas' WHERE preguntas_config_id = '".$id."'";
		//echo $query;
		$res = $this->_conn->query($query);
	}
}