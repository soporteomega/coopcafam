<?php 
/**
* 
*/
class Administracion_Model_DbTable_Categoriadocumento extends Db_Table
{
	protected $_name = 'categoria_documento';
	protected $_id = 'categoria_documento_id';


	public function insert($data){
		$categoria_documento_nombre = $data['nombre'];
		$categoria_documento_descripcion = $data['descripcion'];
		$categoria_documento_imagen = $data['imagen'];
		$categoria_documento_tipo = $data['tipo'];
		$categoria_documento_padre = $data['padre'];
		$query = "INSERT INTO categoria_documento(categoria_documento_nombre, categoria_documento_descripcion, categoria_documento_imagen, categoria_documento_tipo, categoria_documento_padre ) VALUES ('$categoria_documento_nombre', '$categoria_documento_descripcion', '$categoria_documento_imagen','$categoria_documento_tipo','$categoria_documento_padre')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	public function update($data,$id){
		$categoria_documento_nombre = $data['nombre'];
		$categoria_documento_descripcion = $data['descripcion'];
		$categoria_documento_imagen = $data['imagen'];
		$categoria_documento_tipo = $data['tipo'];
		$categoria_documento_padre = $data['padre'];
		$query = "UPDATE categoria_documento SET categoria_documento_nombre = '$categoria_documento_nombre', categoria_documento_descripcion = '$categoria_documento_descripcion', categoria_documento_imagen = '$categoria_documento_imagen', categoria_documento_tipo ='$categoria_documento_tipo' WHERE categoria_documento_id = '".$id."'";
		$res = $this->_conn->query($query);
	}
}