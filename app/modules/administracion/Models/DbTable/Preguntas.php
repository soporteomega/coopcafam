<?php
/**
*
*/
class Administracion_Model_DbTable_Preguntas extends Db_Table
{
	protected $_name = 'preguntas';
	protected $_id = 'pregunta_id';

	function insert($data){
		$pregunta_pregunta = $data['pregunta'];
		$pregunta_respuesta1 = $data['respuesta1'];
		$pregunta_respuesta2 = $data['respuesta2'];
		$pregunta_respuesta3 = $data['respuesta3'];
		$pregunta_respuesta4 = $data['respuesta4'];
		$pregunta_respuesta5 = $data['respuesta5'];
		$pregunta_respuesta6 = $data['respuesta6'];
		$pregunta_respuesta7 = $data['respuesta7'];
		$pregunta_respuesta8 = $data['respuesta8'];
		$pregunta_respuesta9 = $data['respuesta9'];
		$pregunta_respuesta10 = $data['respuesta10'];
		$pregunta_respuesta11 = $data['respuesta11'];
		$pregunta_respuesta12 = $data['respuesta12'];
		$pregunta_respuesta13 = $data['respuesta13'];
		$pregunta_respuesta14 = $data['respuesta14'];
		$modulo_id = $data['modulo'];
		$pregunta_activo = $data['activo'];

		$pregunta_correcta1 = $data['correcta1'];
		$pregunta_correcta2 = $data['correcta2'];
		$pregunta_correcta3 = $data['correcta3'];
		$pregunta_correcta4 = $data['correcta4'];
		$pregunta_correcta5 = $data['correcta5'];
		$pregunta_correcta6 = $data['correcta6'];
		$pregunta_correcta7 = $data['correcta7'];
		$pregunta_correcta8 = $data['correcta8'];
		$pregunta_correcta9 = $data['correcta9'];
		$pregunta_correcta10 = $data['correcta10'];
		$pregunta_correcta11 = $data['correcta11'];
		$pregunta_correcta12 = $data['correcta12'];
		$pregunta_correcta13 = $data['correcta13'];
		$pregunta_correcta14 = $data['correcta14'];
		$pregunta_tipo = $data['tipo'];

		$query = "INSERT INTO preguntas( pregunta_pregunta, pregunta_respuesta1, pregunta_respuesta2, pregunta_respuesta3, pregunta_respuesta4, pregunta_respuesta5,pregunta_respuesta6,pregunta_respuesta7,pregunta_respuesta8,pregunta_respuesta9,pregunta_respuesta10,pregunta_respuesta11,pregunta_respuesta12,pregunta_respuesta13,pregunta_respuesta14, modulo_id, pregunta_activo, pregunta_correcta1,pregunta_correcta2,pregunta_correcta3,pregunta_correcta4,pregunta_correcta5,pregunta_tipo ) VALUES ('$pregunta_pregunta', '$pregunta_respuesta1', '$pregunta_respuesta2', '$pregunta_respuesta3', '$pregunta_respuesta4', '$pregunta_respuesta5','$pregunta_respuesta6','$pregunta_respuesta7','$pregunta_respuesta8','$pregunta_respuesta9','$pregunta_respuesta10','$pregunta_respuesta11','$pregunta_respuesta12','$pregunta_respuesta13','$pregunta_respuesta14', '$modulo_id', '$pregunta_activo','$pregunta_correcta1','$pregunta_correcta2','$pregunta_correcta3','$pregunta_correcta4','$pregunta_correcta5','$pregunta_correcta6','$pregunta_correcta7','$pregunta_correcta8','$pregunta_correcta9','$pregunta_correcta10','$pregunta_correcta11','$pregunta_correcta12','$pregunta_correcta13','$pregunta_correcta14','$pregunta_tipo')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	function update($data,$id){
		$pregunta_pregunta = $data['pregunta'];
		$pregunta_respuesta1 = $data['respuesta1'];
		$pregunta_respuesta2 = $data['respuesta2'];
		$pregunta_respuesta3 = $data['respuesta3'];
		$pregunta_respuesta4 = $data['respuesta4'];
		$pregunta_respuesta5 = $data['respuesta5'];
		$pregunta_respuesta6 = $data['respuesta6'];
		$pregunta_respuesta7 = $data['respuesta7'];
		$pregunta_respuesta8 = $data['respuesta8'];
		$pregunta_respuesta9 = $data['respuesta9'];
		$pregunta_respuesta10 = $data['respuesta10'];
		$pregunta_respuesta11 = $data['respuesta11'];
		$pregunta_respuesta12 = $data['respuesta12'];
		$pregunta_respuesta13 = $data['respuesta13'];
		$pregunta_respuesta14 = $data['respuesta14'];
		$modulo_id = $data['modulo'];
		$pregunta_activo = $data['activo'];

		$pregunta_correcta1 = $data['correcta1'];
		$pregunta_correcta2 = $data['correcta2'];
		$pregunta_correcta3 = $data['correcta3'];
		$pregunta_correcta4 = $data['correcta4'];
		$pregunta_correcta5 = $data['correcta5'];
		$pregunta_correcta6 = $data['correcta6'];
		$pregunta_correcta7 = $data['correcta7'];
		$pregunta_correcta8 = $data['correcta8'];
		$pregunta_correcta9 = $data['correcta9'];
		$pregunta_correcta10 = $data['correcta10'];
		$pregunta_correcta11 = $data['correcta11'];
		$pregunta_correcta12 = $data['correcta12'];
		$pregunta_correcta13 = $data['correcta13'];
		$pregunta_correcta14 = $data['correcta14'];
		$pregunta_tipo = $data['tipo'];

		$query = "UPDATE ".$this->_name." SET pregunta_pregunta = '$pregunta_pregunta', pregunta_respuesta1 = '$pregunta_respuesta1', pregunta_respuesta2 = '$pregunta_respuesta2',
			pregunta_respuesta3 = '$pregunta_respuesta3',
			pregunta_respuesta4 = '$pregunta_respuesta4',
			pregunta_respuesta5 = '$pregunta_respuesta5',
			pregunta_respuesta6 = '$pregunta_respuesta6',
			pregunta_respuesta7 = '$pregunta_respuesta7',
			pregunta_respuesta8 = '$pregunta_respuesta8',
			pregunta_respuesta9 = '$pregunta_respuesta9',
			pregunta_respuesta10 = '$pregunta_respuesta10',
			pregunta_respuesta11 = '$pregunta_respuesta11',
			pregunta_respuesta12 = '$pregunta_respuesta12',
			pregunta_respuesta13 = '$pregunta_respuesta13',
			pregunta_respuesta14 = '$pregunta_respuesta14',
			modulo_id = '$modulo_id', pregunta_activo='$pregunta_activo',
			pregunta_correcta1='$pregunta_correcta1',
			pregunta_correcta2='$pregunta_correcta2',
			pregunta_correcta3='$pregunta_correcta3',
			pregunta_correcta4='$pregunta_correcta4',
			pregunta_correcta5='$pregunta_correcta5',
			pregunta_correcta6='$pregunta_correcta6',
			pregunta_correcta7='$pregunta_correcta7',
			pregunta_correcta8='$pregunta_correcta8',
			pregunta_correcta9='$pregunta_correcta9',
			pregunta_correcta10='$pregunta_correcta10',
			pregunta_correcta11='$pregunta_correcta11',
			pregunta_correcta12='$pregunta_correcta12',
			pregunta_correcta13='$pregunta_correcta13',
			pregunta_correcta14='$pregunta_correcta14',
			pregunta_tipo='$pregunta_tipo' WHERE ".$this->_id." = '$id' ";
		$res = $this->_conn->query($query);
	}

	function limpiar_seleccion($modulo_id){
		$query = "UPDATE ".$this->_name." SET pregunta_activo='0' WHERE modulo_id = '$modulo_id' ";
		$res = $this->_conn->query($query);
	}

}