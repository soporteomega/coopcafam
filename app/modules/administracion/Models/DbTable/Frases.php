<?php 
/**
* clase que genera la insercion y edicion  de frases en la base de datos
*/
class Administracion_Model_DbTable_Frases extends Db_Table
{
	/**
	 * [ nombre de la tabla actual]
	 * @var string
	 */
	protected $_name = 'cursos_mensajes';

	/**
	 * [ identificador de la tabla actual en la base de datos]
	 * @var string
	 */
	protected $_id = 'cursos_mensajes_id';

	/**
	 * insert recibe la informacion de un frase y la inserta en la base de datos
	 * @param  array Array array con la informacion con la cual se va a realizar la insercion en la base de datos
	 * @return integer      identificador del  registro que se inserto
	 */
	public function insert($data){
		$cursos_mensajes_mensaje = $data['cursos_mensajes_mensaje'];
		$cursos_mensajes_tipo = $data['cursos_mensajes_tipo'];
		$cursos_mensajes_color = $data['cursos_mensajes_color'];
		$cursos_mensajes_color2 = $data['cursos_mensajes_color2'];
		$query = "INSERT INTO cursos_mensajes( cursos_mensajes_mensaje, cursos_mensajes_tipo,cursos_mensajes_color,cursos_mensajes_color2) VALUES ( '$cursos_mensajes_mensaje', '$cursos_mensajes_tipo','$cursos_mensajes_color','$cursos_mensajes_color2')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	/**
	 * update Recibe la informacion de un frase  y actualiza la informacion en la base de datos
	 * @param  array Array Array con la informacion con la cual se va a realizar la actualizacion en la base de datos
	 * @param  integer    identificador al cual se le va a realizar la actualizacion
	 * @return void
	 */
	public function update($data,$id){

		$cursos_mensajes_mensaje = $data['cursos_mensajes_mensaje'];
		$cursos_mensajes_tipo = $data['cursos_mensajes_tipo'];
		$cursos_mensajes_color = $data['cursos_mensajes_color'];
		$cursos_mensajes_color2 = $data['cursos_mensajes_color2'];
		$query = "UPDATE cursos_mensajes SET  cursos_mensajes_mensaje = '$cursos_mensajes_mensaje', cursos_mensajes_tipo = '$cursos_mensajes_tipo', cursos_mensajes_color='$cursos_mensajes_color', cursos_mensajes_color2='$cursos_mensajes_color2' WHERE cursos_mensajes_id = '".$id."'";
		$res = $this->_conn->query($query);
	}
}