<?php 
/**
* 
*/
class Administracion_Model_DbTable_Solicitudes extends Db_Table
{
	protected $_name = 'solicitud';
	protected $_id = 'solicitud_id';

	function insert($data){
		$solicitud_fecha = $data['fecha'];
		$solicitud_idusuario = $data['usuario'];
		$solicitud_nombre = $data['nombre'];
		$solicitud_correo = $data['correo'];
		$solicitud_tipo = $data['tipo'];
		$solicitud_direccion = $data['direccion'];
		$solicitud_ciudad = $data['ciudad'];
		$solicitud_telefono = $data['telefono'];
		$solicitud_celular = $data['celular'];
		$solicitud_solicitud = $data['solicitud'];
		$solicitud_estado = $data['estado'];
		$query = "INSERT INTO solicitud(solicitud_fecha, solicitud_idusuario, solicitud_nombre, solicitud_correo, solicitud_tipo, solicitud_direccion, solicitud_ciudad, solicitud_telefono, solicitud_celular, solicitud_solicitud, solicitud_estado) VALUES ('$solicitud_fecha', '$solicitud_idusuario', '$solicitud_nombre', '$solicitud_correo', '$solicitud_tipo', '$solicitud_direccion', '$solicitud_ciudad', '$solicitud_telefono', '$solicitud_celular', '$solicitud_solicitud', '$solicitud_estado')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	function update($data,$id){
		$solicitud_fecha = $data['fecha'];
		$solicitud_idusuario = $data['usuario'];
		$solicitud_nombre = $data['nombre'];
		$solicitud_correo = $data['correo'];
		$solicitud_tipo = $data['tipo'];
		$solicitud_direccion = $data['direccion'];
		$solicitud_ciudad = $data['ciudad'];
		$solicitud_telefono = $data['telefono'];
		$solicitud_celular = $data['celular'];
		$solicitud_solicitud = $data['solicitud'];
		$solicitud_estado = $data['estado'];
		$query = "UPDATE ".$this->_name." SET solicitud_fecha = '$solicitud_fecha', solicitud_idusuario = '$solicitud_idusuario', solicitud_nombre = '$solicitud_nombre', solicitud_correo = '$solicitud_correo', solicitud_tipo = '$solicitud_tipo', solicitud_direccion = '$solicitud_direccion', solicitud_ciudad = '$solicitud_ciudad', solicitud_telefono = '$solicitud_telefono', solicitud_celular = '$solicitud_celular', solicitud_solicitud = '$solicitud_solicitud', solicitud_estado = '$solicitud_estado' WHERE ".$this->_id." = '$id' ";
		$res = $this->_conn->query($query);
	}

	public function getList($filters,$order)
    {
        $filter = '';
        if($filters != ''){
            $filter = ' WHERE '.$filters;
        }
        $orders ="";
        if($order != ''){
            $orders = ' ORDER BY '.$order;
        }
        $select = 'SELECT * FROM '.$this->_name.' LEFT JOIN tipo_gestion ON tipo_gestion_id = solicitud_tipo '.$filter.' '.$orders;
        $res = $this->_conn->query( $select )->fetchAsObject();
        return $res;
    }

    public function getListPages($filters,$order,$page,$amount)
    {
       $filter = '';
        if($filters != ''){
            $filter = ' WHERE '.$filters;
        }
        $orders ="";
        if($order != ''){
            $orders = ' ORDER BY '.$order;
        }
        $select = 'SELECT * FROM '.$this->_name.' LEFT JOIN tipo_gestion ON tipo_gestion_id = solicitud_tipo '.$filter.' '.$orders.' LIMIT '.$page.' , '.$amount;
        $res = $this->_conn->query($select)->fetchAsObject();
        return $res;
    }

}