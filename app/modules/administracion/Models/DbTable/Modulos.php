<?php 
/**
* 
*/
class Administracion_Model_DbTable_Modulos extends Db_Table
{
	protected $_name = 'modulo';
	protected $_id = 'modulo_id';

	function insert($data){
		$modulo_titulo = $data['titulo'];
		$modulo_descripcion = $data['descripcion'];
		$cursos_id = $data['curso'];
		$modulo_columnas = $data['columnas'];

		$query = "INSERT INTO ".$this->_name." (modulo_titulo, modulo_descripcion, cursos_id, modulo_columnas) VALUES ('$modulo_titulo', '$modulo_descripcion', '$cursos_id','$modulo_columnas')";
		$res = $this->_conn->query($query);
        return mysqli_insert_id($this->_conn->getConnection());
	}

	function update($data,$id){
		$modulo_titulo = $data['titulo'];
		$modulo_descripcion = $data['descripcion'];
		$cursos_id = $data['curso'];
		$modulo_columnas = $data['columnas'];
		$query = "UPDATE ".$this->_name." SET modulo_titulo = '$modulo_titulo' , modulo_descripcion = '$modulo_descripcion', cursos_id = '$cursos_id', modulo_columnas='$modulo_columnas' WHERE ".$this->_id." = '$id' ";
		$res = $this->_conn->query($query);
	}

}