<?php
/**
* Controlador de Preguntasconfig que permite la  creacion, edicion  y eliminacion de los configuracion evaluaci&oacute;n del Sistema
*/
class Administracion_preguntasconfigController extends Administracion_mainController
{
	/**
	 * $mainModel  instancia del modelo de  base de datos configuracion evaluaci&oacute;n
	 * @var modeloContenidos
	 */
	private $mainModel;

	/**
	 * $route  url del controlador base
	 * @var string
	 */
	private $route;

	/**
	 * $pages cantidad de registros a mostrar por pagina]
	 * @var integer
	 */
	private $pages ;

	/**
	 * $namefilter nombre de la variable a la fual se le van a guardar los filtros
	 * @var string
	 */
	protected $namefilter;

	/**
	 * $_csrf_section  nombre de la variable general csrf  que se va a almacenar en la session
	 * @var string
	 */
	protected $_csrf_section = "administracion_preguntasconfig";

	/**
	 * $namepages nombre de la pvariable en la cual se va a guardar  el numero de seccion en la paginacion del controlador
	 * @var string
	 */
	protected $namepages;



	/**
     * Inicializa las variables principales del controlador preguntasconfig .
     *
     * @return void.
     */
	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Preguntasconfig();
		$this->namefilter = "parametersfilterpreguntasconfig";
		$this->route = "/administracion/preguntasconfig";
		$this->namepages ="pages_preguntasconfig";
		$this->_view->route = $this->route;
		if(Session::getInstance()->get($this->namepages)){
			$this->pages = Session::getInstance()->get($this->namepages);
		} else {
			$this->pages = 20;
		}
		parent::init();
	}


	/**
     * Recibe la informacion y  muestra un listado de  configuracion evaluaci&oacute;n con sus respectivos filtros.
     *
     * @return void.
     */
	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar configuracion evaluaci&oacute;n");
		$this->filters();
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$filters = $this->getFilter();
		$order = "";
		$list = $this->mainModel->getList($filters,$order);
		$amount = $this->pages;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->register_number = count($list);
		$this->_view->pages = $this->pages;
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->list_preguntas_config_orden = $this->getPreguntasconfigorden();
	}

	/**
     * Genera la Informacion necesaria para editar o crear un  configuraci&oacute;n  y muestra su formulario
     *
     * @return void.
     */
	public function manageAction()
	{
		$this->_view->route = $this->route;
		$this->setLayout('administracion_panel');
		$this->_csrf_section = "manage_preguntasconfig_".date("YmdHis");
		//$this->_csrf->generateCode($this->_csrf_section);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$this->_view->list_preguntas_config_orden = $this->getPreguntasconfigorden();

		$modulo = $this->_getSanitizedParam("modulo");
		$existe = $this->mainModel->getList(" preguntas_config_modulo_id='$modulo' ","");

		//$id = $this->_getSanitizedParam("id");
		$id = $existe[0]->preguntas_config_id;

		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->preguntas_config_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar configuraci&oacute;n");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear configuraci&oacute;n");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear configuraci&oacute;n");
		}
	}

	/**
     * Inserta la informacion de un configuraci&oacute;n  y redirecciona al listado de configuracion evaluaci&oacute;n.
     *
     * @return void.
     */
	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf or 1==1) {
			$data = $this->getData();
			$uploadImage =  new Core_Model_Upload_Image();
			if($_FILES['preguntas_config_gif_aprobado']['name'] != ''){
				$data['preguntas_config_gif_aprobado'] = $uploadImage->upload("preguntas_config_gif_aprobado");
			}
			if($_FILES['preguntas_config_gif_reprobado']['name'] != ''){
				$data['preguntas_config_gif_reprobado'] = $uploadImage->upload("preguntas_config_gif_reprobado");
			}
			if($_FILES['preguntas_config_gif_reloj']['name'] != ''){
				$data['preguntas_config_gif_reloj'] = $uploadImage->upload("preguntas_config_gif_reloj");
			}
			$id = $this->mainModel->insert($data);
		}
		//header('Location: '.$this->route);
		$curso = $this->_getSanitizedParam("curso");
		header("Location: /administracion/modulos/?curso=".$curso);
	}

	/**
     * Recibe un identificador  y Actualiza la informacion de un configuraci&oacute;n  y redirecciona al listado de configuracion evaluaci&oacute;n.
     *
     * @return void.
     */
	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf or 1==1) {
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->preguntas_config_id) {
				$data = $this->getData();
				$uploadImage =  new Core_Model_Upload_Image();
				if($_FILES['preguntas_config_gif_aprobado']['name'] != ''){
					if($content->preguntas_config_gif_aprobado){
						$uploadImage->delete($content->preguntas_config_gif_aprobado);
					}
					$data['preguntas_config_gif_aprobado'] = $uploadImage->upload("preguntas_config_gif_aprobado");
				} else {
					$data['preguntas_config_gif_aprobado'] = $content->preguntas_config_gif_aprobado;
				}
			
				if($_FILES['preguntas_config_gif_reprobado']['name'] != ''){
					if($content->preguntas_config_gif_reprobado){
						$uploadImage->delete($content->preguntas_config_gif_reprobado);
					}
					$data['preguntas_config_gif_reprobado'] = $uploadImage->upload("preguntas_config_gif_reprobado");
				} else {
					$data['preguntas_config_gif_reprobado'] = $content->preguntas_config_gif_reprobado;
				}
			
				if($_FILES['preguntas_config_gif_reloj']['name'] != ''){
					if($content->preguntas_config_gif_reloj){
						$uploadImage->delete($content->preguntas_config_gif_reloj);
					}
					$data['preguntas_config_gif_reloj'] = $uploadImage->upload("preguntas_config_gif_reloj");
				} else {
					$data['preguntas_config_gif_reloj'] = $content->preguntas_config_gif_reloj;
				}
				$this->mainModel->update($data,$id);
			}
		}
		//header('Location: '.$this->route);
		$curso = $this->_getSanitizedParam("curso");
		header("Location: /administracion/modulos/?curso=".$curso);
	}

	/**
     * Recibe un identificador  y elimina un configuraci&oacute;n  y redirecciona al listado de configuracion evaluaci&oacute;n.
     *
     * @return void.
     */
	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_csrf_section] == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$uploadImage =  new Core_Model_Upload_Image();
					if (isset($content->preguntas_config_gif_aprobado) && $content->preguntas_config_gif_aprobado != '') {
						$uploadImage->delete($content->preguntas_config_gif_aprobado);
					}
					
					if (isset($content->preguntas_config_gif_reprobado) && $content->preguntas_config_gif_reprobado != '') {
						$uploadImage->delete($content->preguntas_config_gif_reprobado);
					}
					
					if (isset($content->preguntas_config_gif_reloj) && $content->preguntas_config_gif_reloj != '') {
						$uploadImage->delete($content->preguntas_config_gif_reloj);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	/**
     * Recibe la informacion del formulario y la retorna en forma de array para la edicion y creacion de Preguntasconfig.
     *
     * @return array con toda la informacion recibida del formulario.
     */
	private function getData()
	{
		$data = array();
		if($this->_getSanitizedParam("preguntas_config_modulo_id") == '' ) {
			$data['preguntas_config_modulo_id'] = '0';
		} else {
			$data['preguntas_config_modulo_id'] = $this->_getSanitizedParam("preguntas_config_modulo_id");
		}
		if($this->_getSanitizedParam("preguntas_config_orden") == '' ) {
			$data['preguntas_config_orden'] = '0';
		} else {
			$data['preguntas_config_orden'] = $this->_getSanitizedParam("preguntas_config_orden");
		}
		if($this->_getSanitizedParam("preguntas_config_orden_respuestas") == '' ) {
			$data['preguntas_config_orden_respuestas'] = '0';
		} else {
			$data['preguntas_config_orden_respuestas'] = $this->_getSanitizedParam("preguntas_config_orden_respuestas");
		}
		if($this->_getSanitizedParam("preguntas_config_cantidad") == '' ) {
			$data['preguntas_config_cantidad'] = '0';
		} else {
			$data['preguntas_config_cantidad'] = $this->_getSanitizedParam("preguntas_config_cantidad");
		}
		$data['preguntas_config_tiempo'] = $this->_getSanitizedParam("preguntas_config_tiempo")*1;
		$data['preguntas_config_oportunidades'] = $this->_getSanitizedParam("preguntas_config_oportunidades")*1;
		$data['preguntas_mostrar_certificado'] = $this->_getSanitizedParam("preguntas_mostrar_certificado")*1;
		$data['preguntas_config_gif_aprobado'] = "";
		$data['preguntas_config_gif_reprobado'] = "";
		$data['preguntas_config_gif_reloj'] = "";
		return $data;
	}

	/**
     * Genera los valores del campo orden.
     *
     * @return array cadena con los valores del campo orden.
     */
	private function getPreguntasconfigorden()
	{
		$array = array();
		$array['1'] = 'Definido';
		$array['2'] = 'Aleatorio';
		return $array;
	}

	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {

        $filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);
            if ($filters->preguntas_config_modulo_id != '') {
                $filtros = $filtros." AND preguntas_config_modulo_id LIKE '%".$filters->preguntas_config_modulo_id."%'";
            }
            if ($filters->preguntas_config_orden != '') {
                $filtros = $filtros." AND preguntas_config_orden ='".$filters->preguntas_config_orden."'";
            }
            if ($filters->preguntas_config_cantidad != '') {
                $filtros = $filtros." AND preguntas_config_cantidad LIKE '%".$filters->preguntas_config_cantidad."%'";
            }
            if ($filters->preguntas_config_tiempo != '') {
                $filtros = $filtros." AND preguntas_config_tiempo LIKE '%".$filters->preguntas_config_tiempo."%'";
            }
		}
        return $filtros;
    }

 /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();$parramsfilter['preguntas_config_modulo_id'] =  $this->_getSanitizedParam("preguntas_config_modulo_id");$parramsfilter['preguntas_config_orden'] =  $this->_getSanitizedParam("preguntas_config_orden");$parramsfilter['preguntas_config_cantidad'] =  $this->_getSanitizedParam("preguntas_config_cantidad");$parramsfilter['preguntas_config_tiempo'] =  $this->_getSanitizedParam("preguntas_config_tiempo");Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }
}