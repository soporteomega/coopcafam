<?php

/**
*
*/

class Administracion_seccionmoduloController extends Administracion_mainController
{
	private $mainModel;
	private $route;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Seccionmodulos();
		$this->route = "/administracion/seccionmodulo";
		$this->_view->route = $this->route;
		$this->_view->csrf = Session::getInstance()->get('csrf');
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$modelmodulo = new Administracion_Model_DbTable_Modulos();
		$idmodulo = $this->_getSanitizedParam("modulo");
		$this->_view->modulo = $modelmodulo->getById($idmodulo);
		$this->getLayout()->setTitle("Listar Secciones ".$this->_view->modulo->modulo_titulo);
		$filters = " modulo_id = '$idmodulo' ";
		$order = " orden ASC ";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}

	public function manageAction()
	{
		$modelmodulo = new Administracion_Model_DbTable_Modulos();
		$idmodulo = $this->_getSanitizedParam("modulo");
		$this->_view->modulo = $modelmodulo->getById($idmodulo);
		$this->setLayout('administracion_panel');
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if(isset($content->modulo_id)){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Seccion ".$this->_view->modulo->modulo_titulo);
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Seccion ".$this->_view->modulo->modulo_titulo);
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Seccion ".$this->_view->modulo->modulo_titulo);
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$data = $this->getData();
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
		}
		header('Location: '.$this->route."?modulo=".$data['modulo']);
	}
	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->modulo_id) {
				$data = $this->getData();
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route."?modulo=".$data['modulo']);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		$idmodulo = $this->_getSanitizedParam("modulo");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route.'?modulo='.$idmodulo);
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['nombre'] = $this->_getSanitizedParam("nombre");
		$data['descripcion'] = $this->_getSanitizedParamHtml("descripcion");
		$data['modulo'] = $this->_getSanitizedParam("modulo");
		return $data;
	}



}