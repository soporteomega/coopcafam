<?php

/**
*
*/
class Administracion_adicionalconsejeroController extends Administracion_mainController
{
	private $mainModel;
	private $route;
	protected $namefilter;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Adicionalconsejero();
		$this->namefilter = "parametersfilteradicionalconsejero";
		$this->route = "/administracion/adicionalconsejero";
		$this->_view->route = $this->route;
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Contenido Adicional Consejero");
		$this->filters();
		$idcontent = $this->_getSanitizedParam("content");
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$filters = "content_id = '$idcontent' AND ".$this->getFilter();
		$order = " orden ASC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->id_content = $idcontent;
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}
	public function manageAction()
	{
		$this->setLayout('administracion_panel');
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$this->_view->sections = $this->getSections();
		$this->_view->id_content = $this->_getSanitizedParam("content");
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->adicional_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Contenido Adicional Consejero");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Contenido Adicional Consejero");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Contenido Adicional Consejero");
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1 ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$uploadDocument =  new Core_Model_Upload_Document();
			$data = $this->getData();
			if($_FILES['documento']['name'] != ''){
				$data['documento'] = $uploadDocument->upload("documento");
			}
			if($_FILES['imagen']['name'] != ''){
				$data['imagen'] = $uploadImage->upload("imagen");
			}
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
		}
		$idcontent = $this->_getSanitizedParam("content");
		header('Location: '.$this->route.'?content='.$idcontent);
	}

	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1) {
			$uploadImage =  new Core_Model_Upload_Image();
			$uploadDocument =  new Core_Model_Upload_Document();
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->adicional_id) {
				$data = $this->getData();
				if($_FILES['documento']['name'] != ''){
					if($content->documento_documento){
						$uploadDocument->delete($content->adicional_documento);
					}
					$data['documento'] = $uploadDocument->upload("documento");
				} else {
					$data['documento'] = $content->adicional_documento;
				}
				if($_FILES['imagen']['name'] != ''){
					if($content->adicional_imagen){
						$uploadImage->delete($content->adicional_imagen);
					}
					$data['imagen'] = $uploadImage->upload("imagen");
				} else {
					$data['imagen'] = $content->adicional_imagen;
				}
				$this->mainModel->update($data,$id);
			}
		}
		$idcontent = $this->_getSanitizedParam("content");
		header('Location: '.$this->route.'?content='.$idcontent);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$uploadDocument =  new Core_Model_Upload_Document();
					if (isset($content->adicional_documento) && $content->adicional_documento != '') {
						$uploadDocument->delete($content->adicional_documento);
					}
					$modelUploadImage= new Core_Model_Upload_Image();
					if (isset($content->adicional_imagen) && $content->adicional_imagen != '') {
						$modelUploadImage->delete($content->adicional_imagen);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		$idcontent = $this->_getSanitizedParam("content");
		header('Location: '.$this->route.'?content='.$idcontent);
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['titulo'] = $this->_getSanitizedParam('titulo');
		$data['descripcion'] = $this->_getSanitizedParamHtml('descripcion');
		$data['imagen'] = "";
		$data['tipo'] = $this->_getSanitizedParam('tipo');
		$data['content'] = $this->_getSanitizedParam('content');
		$data['documento'] = '';
		return $data;
	}

	private function getSections()
	{
		$sections = array();
		$sections['Texto'] = "Texto";
		$sections['Acordeon'] = "Acordeon";
		$sections['Carrousel'] = "Carrousel";
		return $sections;
	}


	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {
        $filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);

            if ($filters->titulo != '') {
                $filtros = $filtros." AND adicional_titulo LIKE '%".$filters->titulo."%'";
            }
            if ($filters->seccion != '') {
                $filtros = $filtros." AND adicional_tipo ='".$filters->seccion."'";
            }
        }
        return $filtros;
    }

    /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        $this->_view->sections = $this->getSections();
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();
            $parramsfilter['titulo'] =  $this->_getSanitizedParam("titulo");
            $parramsfilter['seccion'] =  $this->_getSanitizedParam("seccion");
            Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }

}