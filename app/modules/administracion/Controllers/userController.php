<?php

/**
*
*/

class Administracion_userController extends Controllers_Abstract
{

	private $mainModel;
	private $route;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_User();
		$this->route = "/administracion/users";
		$this->_view->route = $this->route;
		parent::init();
	}

	public function indexAction()
	{
		# code...
	}

	public function changepasswordAction()
	{
		# code...
	}

	public function editprofileAction()
	{
		# code...
	}

	public function forgotpasswordAction()
	{
		# code...
	}

	public function loginAction()
	{
		header('Content-Type:application/json');
		$isPost = $this->getRequest()->isPost();
		$user= $this->_getSanitizedParam("user");
		$password = $this->_getSanitizedParam("password");
		$csrf = $this->_getSanitizedParam("csrf");
		$isError = false;
		$busco = "no";
		if($isPost == true && $user && $password && Session::getInstance()->get('csrf') == $csrf  ){
			$userModel = new core_Model_DbTable_User();
			$busco = "si";
			if ($userModel->autenticateUser($user,$password) == true) {
				$resUser = $userModel->searchUserByUser($user);
				Session::getInstance()->set("kt_login_id",$resUser->user_id);
				Session::getInstance()->set("kt_login_level",$resUser->user_level);
				Session::getInstance()->set("kt_login_user",$resUser->user_user);
				Session::getInstance()->set("kt_login_name",$resUser->user_names." ".$resUser->user_lastnames);
				$logModel = new Core_Model_DbTable_Log();
				$logModel->log($resUser->user_id);
			} else {

				$isError = true;
			}
		} else {
			$isError = true;
		}
		$this->_view->array = json_encode(array(
			"isError" => $isError,
			"Busco"  => $busco
		));
	}

	public function logoutAction()
	{
		Session::getInstance()->set("kt_login_id","");
		Session::getInstance()->set("kt_login_level","");
		Session::getInstance()->set("kt_login_user","");
		Session::getInstance()->set("kt_login_name","");
		header('Location: /administracion/');
	}

	

}