<?php 

/**
*
*/

class Administracion_registrogeneratorController extends Controllers_Abstract
{
	public function indexAction()
	{
		$modelTables = new Administracion_Model_DbTable_Tables();
		$namedatabase = Config_Config::getInstance()->getValue('db/name');
		$nombrevariable = "Tables_in_".$namedatabase;
		$tablas = $modelTables->getTables();
		foreach ($tablas as $key => $tabla) {
			$nombretabla = $tabla->$nombrevariable;
			if (strpos($nombretabla,"trigger")!== false || strpos($nombretabla,"log")!== false ) {
				//echo $nombretabla."<br>";
				$campos = $modelTables->getCampos($nombretabla);
				$this->crearModelo($nombretabla,$campos);
				$this->crearVista($nombretabla,$campos);
				$this->crearAcciones($nombretabla);
			}
		}
	}

	public function crearAcciones($nombre){
		$controlador = str_replace("_","",strtolower($nombre));
		$php  = "public function ".$controlador."Action()<br>
		{<br>
			header('Content-Type: application/vnd.ms-excel');<br>
			header('Content-Disposition: attachment; filename=".$nombre.".xls');<br>
			header('Cache-Control: max-age=0')<br>
			\$triggerModel = new Administracion_Model_DbTable_".ucwords($controlador)."();<br>
			\$this->_view->lists = \$triggerModel->getList('','trigger_".$nombre."_date DESC');<br>
		} <br><br><br><br>";
		echo $php;

	}

	public function crearVista($nombre,$campos){
		$controlador = str_replace("_","",strtolower($nombre));
		$table = $nombre;
		$identificador = $this->getKey($campos)->Field;
		$ruta = APP_PATH."/modules/administracion/Views/registros/".$controlador.".php";
		$nuevoarchivo = fopen($ruta, "w+");
		$php = "<table border='1'>
	<thead>
		<tr>";
		foreach ($campos as $key => $value) {

			$php = $php."
			<td>".$value->Field."</td>";
		}
		$php = $php."
		</tr>
	</thead>
	<tbody>
		<?php foreach(\$this->lists as \$content){ ?>
			<tr>";
				foreach ($campos as $key => $value) {
					$php = $php."
				<td><?=\$content->".$value->Field.";?></td>";
				}
			$php = $php."
			</tr>
		<?php } ?>
	</tbody>
</table>
";
		fwrite($nuevoarchivo,$php);
		fclose($nuevoarchivo);
	}

	public function crearModelo($nombre,$campos){
		$controlador = str_replace("_","",strtolower($nombre));
		$table = $nombre;
		$identificador = $this->getKey($campos)->Field;
		$ruta = APP_PATH."/modules/Administracion/Models/DbTable/".ucwords($controlador).".php";
		$nuevoarchivo = fopen($ruta, "w+");

		$php = "<?php 
			/**
			* clase que genera la consulta del trigger en la base de datos
			*/
			class Administracion_Model_DbTable_".ucwords($controlador)." extends Db_Table
			{
				/**
				 * [$_name nombre de la tabla actual]
				 * @var string
				 */
				protected \$_name = '".$table."';

				/**
				 * [$_id identificador de la tabla actual en la base de datos]
				 * @var string
				 */
				protected \$_id = '".$identificador."';

				/**
				 * insert recibe la informacion de un ".$tituloeditar." y la inserta en la base de datos
				 * @param  array $data array con la informacion con la cual se va a realizar la insercion en la base de datos
				 * @return integer      identificador del  registro que se inserto
				 */
				public function getList(\$filters,\$order)
			    {
			        \$filter = '';
			        if(\$filters != ''){
			            \$filter = ' WHERE '.\$filters;
			        }
			        \$orders ='';
			        if(\$order != ''){
			            \$orders = ' ORDER BY '.\$order;
			        }
			        \$select = 'SELECT * FROM '.\$this->_name.' '.\$filter.' '.\$orders;
			        \$res = \$this->_conn->query( \$select )->fetchAsObject();
			        return \$res;
			    }
			}";
		fwrite($nuevoarchivo,$php);
		fclose($nuevoarchivo);
	}
	public function getKey($campos){
		foreach ($campos as $key => $campo) {
			if($campo->Key == 'PRI'){
				return $campo;
			} 
		}
		return false;
	}
}