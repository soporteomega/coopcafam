<?php

/**
*
*/
class Administracion_documentoController extends Administracion_mainController
{
	private $mainModel;
	private $route;
	protected $namefilter;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Documento();
		$this->namefilter = "parametersfilterdocumento";
		$this->route = "/administracion/documento";
		$this->_view->route = $this->route;
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Documentos");
		$this->filters();
		$idcategoria = $this->_getSanitizedParam("categoria");
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$filters = "documento_categoria = '$idcategoria' AND ".$this->getFilter();
		$order = "documento_nombre ASC, orden ASC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 40;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->id_categoria = $idcategoria;
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}
	public function manageAction()
	{
		$this->setLayout('administracion_panel');
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$this->_view->sections = $this->getSections();
		$this->_view->id_categoria = $this->_getSanitizedParam("categoria");
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->documento_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Documento");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Documento");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Documento");
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1 ) {
			$uploadDocument =  new Core_Model_Upload_Document();
			$data = $this->getData();
			if($_FILES['archivo']['name'] != ''){
				$data['archivo'] = $uploadDocument->upload("archivo");
			}
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
		}
		$idcategoria = $this->_getSanitizedParam("categoria");
		header('Location: '.$this->route.'?categoria='.$idcategoria);
	}

	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1) {
			$uploadDocument =  new Core_Model_Upload_Document();
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->documento_id) {
				$data = $this->getData();
				if($_FILES['archivo']['name'] != ''){
					if($content->documento_archivo){
						$uploadDocument->delete($content->documento_documento);
					}
					$data['archivo'] = $uploadDocument->upload("archivo");
				} else {
					$data['archivo'] = $content->documento_documento;
				}
				$this->mainModel->update($data,$id);
			}
		}
		$idcategoria = $this->_getSanitizedParam("categoria");
		header('Location: '.$this->route.'?categoria='.$idcategoria);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$uploadDocument = new Core_Model_Upload_Document();
					if (isset($content->documento_archivo) && $content->documento_archivo != '') {
						$uploadDocument->delete($content->documento_archivo);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		$idcategoria = $this->_getSanitizedParam("categoria");
		header('Location: '.$this->route.'?categoria='.$idcategoria);
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['nombre'] = $this->_getSanitizedParam('nombre');
		$data['archivo'] = "";
		$data['categoria'] = $this->_getSanitizedParam('categoria');
		$data['descarga'] = $this->_getSanitizedParam('descarga');
		$data['tipo'] = $this->_getSanitizedParam('tipo');
		$data['proceso'] = $this->_getSanitizedParam('proceso');
		$data['consecutivo'] = $this->_getSanitizedParam('consecutivo');
		$data['origen'] = $this->_getSanitizedParam('origen');
		$data['ubicacion'] = $this->_getSanitizedParam('ubicacion');
		$data['cargo'] = $this->_getSanitizedParam('cargo');
		$data['version'] = $this->_getSanitizedParam('version');
		if($data['descarga']!=1){
			$data['descarga']= '0';
		}
		return $data;
	}

	private function getSections()
	{
		$sections = array();
		$sections['Texto'] = "Texto";
		$sections['Acordeon'] = "Acordeon";
		$sections['Carrousel'] = "Carrousel";
		return $sections;
	}


	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {
        $filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);

            if ($filters->titulo != '') {
                $filtros = $filtros." AND documento_nombre LIKE '%".$filters->titulo."%'";
            }
            if ($filters->seccion != '') {
                $filtros = $filtros." AND documento_tipo ='".$filters->seccion."'";
            }
        }
        return $filtros;
    }

    /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        $this->_view->sections = $this->getSections();
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();
            $parramsfilter['titulo'] =  $this->_getSanitizedParam("titulo");
            $parramsfilter['seccion'] =  $this->_getSanitizedParam("seccion");
            Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }

}