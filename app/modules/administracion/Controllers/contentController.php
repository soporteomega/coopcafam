<?php

/**
*
*/
class Administracion_contentController extends Administracion_mainController
{
	private $mainModel;
	private $route;
	protected $namefilter;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Content();
		$this->namefilter = "parametersfiltercontent";
		$this->route = "/administracion/content";
		$this->_view->route = $this->route;
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Contenidos");
		$this->filters();
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$filters = $this->getFilter();
		$order = " orden ASC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}
	public function manageAction()
	{
		$this->setLayout('administracion_panel');
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$this->_view->sections = $this->getSections();
		$this->_view->disenios = $this->getDisenio();
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->content_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Contenido");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Contenido");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Contenido");
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1 ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$data = $this->getData();
			if($_FILES['image']['name'] != ''){
				$data['image'] = $uploadImage->upload("image");
			}
			if($_FILES['banner']['name'] != ''){
				$data['banner'] = $uploadImage->upload("banner");
			}
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);

		}
		header('Location: '.$this->route);
	}

	public function enviarnoticiaAction(){
		$id = $this->_getSanitizedParam("id");
		$email = new Core_Model_Sendingemail($this->_view);
		$email->noticia($id);
		header('Location: '.$this->route);
	}

	public function enviarnoticiasAction(){
		$email = new Core_Model_Sendingemail($this->_view);
		$email->noticias();
		header('Location: '.$this->route);
	}

	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1) {
			$uploadImage =  new Core_Model_Upload_Image();
			$id = $this->_getSanitizedParam("id_content");
			$content = $this->mainModel->getById($id);
			if ($content->content_id) {
				$data = $this->getData();
				if($_FILES['image']['name'] != ''){
					if($content->content_image){
						$uploadImage->delete($content->content_image);
					}
					$data['image'] = $uploadImage->upload("image");
				} else {
					$data['image'] = $content->content_image;
				}
				if($_FILES['banner']['name'] != ''){
					if($content->content_banner){
						$uploadImage->delete($content->content_banner);
					}
					$data['banner'] = $uploadImage->upload("banner");
				} else {
					$data['banner'] = $content->content_banner;
				}
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$modelUploadImage= new Core_Model_Upload_Image();
					if (isset($content->content_image) && $content->content_image != '') {
						$modelUploadImage->delete($content->content_image);
					}
					if (isset($content->content_banner) && $content->content_banner != '') {
						$modelUploadImage->delete($content->content_banner);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['date'] = $this->_getSanitizedParam("date");
		$data['title'] = $this->_getSanitizedParam("title");
		$data['subtitle'] = $this->_getSanitizedParam("subtitle");
		$data['introduction'] = $this->_getSanitizedParamHtml("introduction");
		$data['description'] = $this->_getSanitizedParamHtml("description");
		$data['section'] = $this->_getSanitizedParam("section");
		$data['link'] = $this->_getSanitizedParam("link");
		$data['image'] = '';
		$data['banner'] = '';
		$data['disenio'] = $this->_getSanitizedParam('disenio');
		return $data;
	}

	private function getSections()
	{
		$sections = array();
		$sections['Banner Home'] = "Banner Home";
		$sections['Introduccion Home'] = "Introduccion Home";
		$sections['Introduccion Cooperativa'] = "Introduccion Cooperativa";
		$sections['Nuestra Cooperativa'] = "Nuestra Cooperativa";
		$sections['Introduccion Gestion'] = "Introduccion Gestion";
		$sections['Introduccion Beneficios'] = "Introduccion Beneficios";
		$sections['Beneficios'] = "Beneficios";
		$sections['Introduccion Campanias'] = "Introduccion Campanias";
		$sections['Campanias'] = "Campanias";
		$sections['Introduccion Galeria'] = "Introduccion Galeria";
		$sections['Introduccion SGC'] = "Introduccion SGC";
		$sections['Introduccion Mapa'] = "Introduccion Mapa";
		$sections['Introduccion Formacion'] = "Introduccion Formacion";
		$sections['Introduccion Solicitud'] = "Introduccion Solicitud";
		$sections['Introduccion Asociados'] = "Introduccion Asociados";
		$sections['Asociados'] = "Asociados";
		$sections['Introduccion Noticias'] = "Introduccion Noticias";
		$sections['Noticias'] = "Noticias";
		$sections['Directorio'] = "Directorio";
		$sections['Sedes'] = "Sedes";
		$sections['Enlaces'] = "Enlaces";
		$sections['Direccionamiento'] = "Direccionamiento";
		$sections['Red Social'] = "Red Social";
		$sections['Tarjetas'] = "Tarjetas";
		$sections['Introduccion Coopcafam en Cifras'] = "Introduccion Coopcafam en Cifras";
		$sections['Coopcafam en Cifras'] = "Coopcafam en Cifras";

		return $sections;
	}

	private function getDisenio()
	{
		$disenio = array();
		$disenio['Default Gris'] = "Default Gris";
		$disenio['Dos Columnas'] = "Dos Columnas";
		$disenio['Dos Columnas Gris'] = "Dos Columnas Gris";
		$disenio['Banner'] = "Banner";
		return $disenio;
	}

	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {
        $filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);

            if ($filters->titulo != '') {
                $filtros = $filtros." AND content.content_title LIKE '%".$filters->titulo."%'";
            }
            if ($filters->seccion != '') {
                $filtros = $filtros." AND content.content_section ='".$filters->seccion."'";
            }
        }
        return $filtros;
    }

    /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        $this->_view->sections = $this->getSections();
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();
            $parramsfilter['titulo'] =  $this->_getSanitizedParam("titulo");
            $parramsfilter['seccion'] =  $this->_getSanitizedParam("seccion");
            Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }

}