<?php

/**
*
*/
class Administracion_registrosController extends Administracion_mainController
{

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Registro de Acceso y Modificaciones");
		$hola ="";
	}

	public function logAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=log.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Log();
		$this->_view->lists = $triggerModel->getList('','log_fecha DESC , log_hora DESC');
	}

	public function triggeradicionalAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_adicional.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggeradicional();
		$this->_view->lists = $triggerModel->getList('','trigger_adicional_date DESC');
	}

	public function triggeralbumAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_album.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggeralbum();
		$this->_view->lists = $triggerModel->getList('','trigger_album_date DESC');
	} 

	public function triggercalendarAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_calendar.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggercalendar();
		$this->_view->lists = $triggerModel->getList('','trigger_calendar_date DESC');
	}

	public function triggercategoriadocumentoAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_categoria_documento.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggercategoriadocumento();
		$this->_view->lists = $triggerModel->getList('','trigger_categoria_documento_date DESC');
	}

	public function triggercontentAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_content.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggercontent();
		$this->_view->lists = $triggerModel->getList('','trigger_content_date DESC');
	}

	public function triggercursosAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_cursos.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggercursos();
		$this->_view->lists = $triggerModel->getList('','trigger_cursos_date DESC');
	}

	public function triggerdocumentoAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_documento.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggerdocumento();
		$this->_view->lists = $triggerModel->getList('','trigger_documento_date DESC');
	}

	public function triggerfelicitacionAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_felicitacion.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggerfelicitacion();
		$this->_view->lists = $triggerModel->getList('','trigger_felicitacion_date DESC');
	}

	public function triggerfotoAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_foto.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggerfoto();
		$this->_view->lists = $triggerModel->getList('','trigger_foto_date DESC');
	}

	public function triggermoduloAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_modulo.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggermodulo();
		$this->_view->lists = $triggerModel->getList('','trigger_modulo_date DESC');
	}

	public function triggerpreguntasAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_preguntas.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggerpreguntas();
		$this->_view->lists = $triggerModel->getList('','trigger_preguntas_date DESC');
	}

	public function triggerredesAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_redes.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggerredes();
		$this->_view->lists = $triggerModel->getList('','trigger_redes_date DESC');
	}

	public function triggerrespuestaAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_respuesta.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggerrespuesta();
		$this->_view->lists = $triggerModel->getList('','trigger_respuesta_date DESC');
	}

	public function triggerseccionmoduloAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_seccion_modulo.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggerseccionmodulo();
		$this->_view->lists = $triggerModel->getList('','trigger_seccion_modulo_date DESC');
	}

	public function triggersolicitudAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_solicitud.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggersolicitud();
		$this->_view->lists = $triggerModel->getList('','trigger_solicitud_date DESC');
	}

	public function triggertipogestionAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_tipo_gestion.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggertipogestion();
		$this->_view->lists = $triggerModel->getList('','trigger_tipo_gestion_date DESC');
	}

	public function triggertypecalendarAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_type_calendar.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggertypecalendar();
		$this->_view->lists = $triggerModel->getList('','trigger_type_calendar_date DESC');
	}

	public function triggeruserAction()
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=trigger_user.xls');
		header('Cache-Control: max-age=0');
		$triggerModel = new Administracion_Model_DbTable_Triggeruser();
		$this->_view->lists = $triggerModel->getList('','trigger_user_date DESC');
	}

}