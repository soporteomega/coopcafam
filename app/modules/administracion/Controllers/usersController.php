<?php

/**
*
*/

class Administracion_usersController extends Administracion_mainController
{
	private $mainModel;
	private $route;
	protected $namefilter;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_User();
		$this->route = "/administracion/users";
		$this->_view->route = $this->route;
		$this->namefilter = "parametersfilterUsers";
		parent::init();
	}

	public function indexAction()
	{
		$cargoModel = new Administracion_Model_DbTable_Cargo();
		$this->_view->cargos = $cargoModel->getList("","");
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Usuarios");
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$this->_view->levels = $this->getLevels();
		$this->filters();
        $filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
       	$filters = $this->getFilter();
		$order = " user_user ASC ";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}
	public function manageAction()
	{
		$cargoModel = new Administracion_Model_DbTable_Cargo();
		$this->_view->cargos = $cargoModel->getList("","");
		$this->setLayout('administracion_panel');
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$this->_view->levels = $this->getLevels();
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->user_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Usuario");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Usuario");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Usuario");
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$data = $this->getData();
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
			$this->actualizarcumpleaniosAction();
		}
		header('Location: '.$this->route);
	}
	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->user_id) {
				$data = $this->getData();
				$this->mainModel->update($data,$id);
				$this->actualizarfecha($id);
				$this->actualizarcumpleaniosAction();
			}
		}
		header('Location: '.$this->route);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	private function getData()
	{
		$data = array();
		$data['names'] = $this->_getSanitizedParam("names");
		$data['lastnames'] = $this->_getSanitizedParam("lastnames");
		$data['email'] = $this->_getSanitizedParam("email");
		$data['idnumber'] = $this->_getSanitizedParam("idnumber");
		$data['city'] = $this->_getSanitizedParam("city");
		$data['country'] = $this->_getSanitizedParam("country");
		$data['phone'] = $this->_getSanitizedParam("phone");
		$data['address'] = $this->_getSanitizedParam("address");
		$data['position'] = $this->_getSanitizedParam("position");
		$data['level'] = $this->_getSanitizedParam("level");
		$data['state'] = $this->_getSanitizedParam("state");
		$data['date'] = $this->_getSanitizedParam("date");
		if($data['state']!=1){
			$data['state'] = '0';
		}
		$data['user'] = $this->_getSanitizedParam("user");
		$data['password'] = $this->_getSanitizedParam("password");
		$data['current_user'] = Session::getInstance()->get("kt_login_id");
		return $data;
	}


	private function getLevels()
	{
		$levels  = array();
		$levels[1] = "Administrador";
		$levels[2] = "Consultor";
		$levels[3] = "Consejero";
		return $levels;
	}

	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {
        $filtros = "";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);
            $filtros = " 1 = 1 ";
            if ($filters->state == 1) {
                $filtros = $filtros." AND user.user_state = 1 ";
            } elseif ($filters->state == 2) {
                $filtros = $filtros." AND user.user_state <> 1";
            }
            if ($filters->nombre != '') {
                $filtros = $filtros." AND concat_ws('',user.user_names,user.user_lastnames) LIKE '%".$filters->nombre."%'";
            }
            if ($filters->level > 0) {
                $filtros = $filtros." AND user.user_level = '".$filters->level."'";
            }
            if ($filters->cargo != '') {
                $filtros = $filtros." AND user.user_position = '".$filters->cargo."'";
            }
        }
        return $filtros;
    }

    /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();
            $parramsfilter['state'] =  $this->_getSanitizedParam("state");
            $parramsfilter['nombre'] =  $this->_getSanitizedParam("nombre");
            $parramsfilter['level'] =  $this->_getSanitizedParam("level");
            $parramsfilter['cargo'] =  $this->_getSanitizedParam("cargo");
            Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }

    public function insertallAction()
	{
		$archivo = fopen( "archivo.csv", "rb" );
	    // Recorremos el archivo completo:
	     while( feof($archivo) == false )
	     {
	        $aDatos = fgetcsv( $archivo, 999999, ";");
	        $data = array();
			$data['names'] = $aDatos[1];
			$data['lastnames'] = $aDatos[2];
			$data['email'] = $aDatos[4];
			$data['idnumber'] = $aDatos[0];
			$data['city'] = "";
			$data['country'] = "";
			$data['phone'] = $aDatos[5];
			$data['address'] = "";
			$data['position'] = $aDatos[3];
			$data['level'] = 2;
			$data['state'] = 1;
			$data['user'] = $aDatos[0];;
			$data['password'] = $aDatos[0];;
			$data['current_user'] = Session::getInstance()->get("kt_login_id");
			$this->mainModel->insert($data);
	    }
    	fclose( $archivo );
	}

	public function actualizarfecha($id){
		$calendarModel = new Administracion_Model_DbTable_Calendar();
		 $fecha = date("Y-m-d",strtotime ( '-1 month' , strtotime (date('Y-m-d'))));
        $nuevafecha = strtotime ( '+1 year' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
		$calendar = $calendarModel->getList("calendar_type = 4 AND calendar_startdate >= '$fecha' AND calendar_startdate <= '$nuevafecha' AND calendar_user = '$id' ","");
		if(count($calendar)>0){
			foreach ($calendar as $key => $value) {
				$calendarModel->deleteRegister($value->calendar_id);
			}
		}
	}

	public function actualizarcumpleaniosAction()
    {
        $userModel = new Administracion_Model_DbTable_User();
        $fecha = date("Y-m-d",strtotime ( '-1 month' , strtotime (date('Y-m-d'))));
        $nuevafecha = strtotime ( '+1 year' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
        $usercalendar = $userModel->userCalendar($fecha,$nuevafecha);
        $calendarMoldel = new Administracion_Model_DbTable_Calendar();
        foreach ($usercalendar as $key => $user) {
            if($user->user_date != '0000-00-00'){
                $data = array();
                $data['name'] = $user->user_names." ".$user->user_lastnames;
                $data['startdate'] = $this->fechacumpleanos($user->user_date);
                $data['starttime'] = "";
                $data['enddate'] = "";
                $data['endtime'] = "";
                $data['user'] = $user->user_id;
                $data['description'] ="<p>Coopcafam te felicita por tu cumpleaños</p>";
                $data['type'] = 4;
                $data['tipo'] = "Empleados";
                $data['createdby'] = 0;
                $data['banner'] = "";
                $data['image'] = "";
            }
            $calendarMoldel->insert($data);
        }
    }

    public function  fechacumpleanos($fecha){
        $arrayfecha = explode("-",$fecha);
        $fechaactual = date("Y")."-".$arrayfecha[1]."-".$arrayfecha[2];
        if(date("Y-m-d",strtotime($fechaactual)) < date("Y-m-d",strtotime ( '-1 month' , strtotime (date('Y-m-d'))))){
            $fechaactual = (date("Y")+1)."-".$arrayfecha[1]."-".$arrayfecha[2];
        }
        return $fechaactual;
    }
}