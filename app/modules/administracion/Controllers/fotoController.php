<?php

/**
*
*/

class Administracion_fotoController extends Administracion_mainController
{
	private $mainModel;
	private $route;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Foto();
		$this->route = "/administracion/foto";
		$this->_view->route = $this->route;
		$this->_view->csrf = Session::getInstance()->get('csrf');
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Fotos");
		$album = $this->_getSanitizedParam("album");
		$this->_view->album = $album;
		$filters = " album_id='$album'";
		$order = "";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}

	public function manageAction()
	{
		$album = $this->_getSanitizedParam("album");
		$this->_view->album = $album;
		$this->setLayout('administracion_panel');
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if(isset($content->foto_id)){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Foto");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Foto");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Foto");
		}
	}

	public function insertAction(){
		$album = $this->_getSanitizedParam("album");
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$data = $this->getData();
			$uploadImage =  new Core_Model_Upload_Image();
			$imagenes =$uploadImage->uploadmultiple("imagen");
			foreach ($imagenes as $key => $imagen) {
				$data['imagen'] = $imagen;
				$id = $this->mainModel->insert($data);
				$this->mainModel->changeOrder($id,$id);
			}
		}
		header('Location: '.$this->route.'?album='.$album);
	}
	public function updateAction(){
		$album = $this->_getSanitizedParam("album");
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->foto_id) {
				$data = $this->getData();
				if($_FILES['imagen']['name'] != ''){
					if($content->foto_imagen){
						$uploadImage->delete($content->foto_imagen);
					}
					$data['imagen'] = $uploadImage->upload("imagen");
				} else {
					$data['imagen'] = $content->foto_imagen;
				}
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route.'?album='.$album);
	}

	public function deleteAction()
	{
		$album = $this->_getSanitizedParam("album");
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$modelUploadImage= new Core_Model_Upload_Image();
					if (isset($content->foto_imagen) && $content->foto_imagen != '') {
						$modelUploadImage->delete($content->foto_imagen);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route.'?album='.$album);
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['titulo'] = $this->_getSanitizedParam("titulo");
		$data['descripcion'] = $this->_getSanitizedParamHtml("descripcion");
		$data['imagen'] = '';
		$data['album'] = $this->_getSanitizedParam("album");;
		return $data;
	}



}