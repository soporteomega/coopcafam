<?php

/**
*
*/
class Administracion_solicitudesController extends Administracion_mainController
{
	private $mainModel;
	private $route;
	protected $namefilter;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Solicitudes();
		$this->namefilter = "parametersfiltersolicitudes";
		$this->route = "/administracion/solicitudes";
		$this->_view->route = $this->route;
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Contenidos");
		$this->filters();
		$modelTipogestion = new Administracion_Model_DbTable_Tipogestion();
		$this->_view->gestiones = $modelTipogestion->getList("","tipo_gestion_nombre ASC");
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$filters = $this->getFilter();
		$order = " solicitud_fecha DESC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}
	public function manageAction()
	{
		$this->setLayout('administracion_panel');
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$modelTipogestion = new Administracion_Model_DbTable_Tipogestion();
		$this->_view->gestiones = $modelTipogestion->getList("","tipo_gestion_nombre ASC");
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->solicitud_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Contenido");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Contenido");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Contenido");
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1 ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$data = $this->getData();
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
		}
		header('Location: '.$this->route);
	}

	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1) {
			$uploadImage =  new Core_Model_Upload_Image();
			$id = $this->_getSanitizedParam("id_content");
			$content = $this->mainModel->getById($id);
			if ($content->solicitud_id) {
				$data = $this->getData();
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['fecha'] = $this->_getSanitizedParam("fecha");
		$data['usuario'] = $this->_getSanitizedParam("usuario");
		$data['nombre'] = $this->_getSanitizedParam("nombre");
		$data['correo'] = $this->_getSanitizedParam("correo");
		$data['tipo'] = $this->_getSanitizedParam("tipo");
		$data['direccion'] = $this->_getSanitizedParam("direccion");
		$data['ciudad'] = $this->_getSanitizedParam("ciudad");
		$data['telefono'] = $this->_getSanitizedParam("telefono");
		$data['celular'] = $this->_getSanitizedParam("celular");
		$data['solicitud'] = $this->_getSanitizedParam("solicitud");
		$data['estado'] = $this->_getSanitizedParam("estado");
		return $data;
	}

	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {
        $filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);

            if ($filters->titulo != '') {
                $filtros = $filtros." AND solicitud_nombre LIKE '%".$filters->titulo."%'";
            }
            if ($filters->seccion != '') {
                $filtros = $filtros." AND solicitud_tipo ='".$filters->seccion."'";
            }
            if ($filters->fecha != '') {
                $filtros = $filtros." AND solicitud_fecha ='".$filters->fecha."'";
            }
            if ($filters->estado != '') {
            	if($filters->estado == 1 ){
            		$filtros = $filtros." AND solicitud_estado = '1' ";
            	} else if( $filters->estado == 2 ) {
            		$filtros = $filtros." AND solicitud_estado <> '1' ";
            	}
            }
        }
        return $filtros;
    }

    /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();
            $parramsfilter['titulo'] =  $this->_getSanitizedParam("nombre");
            $parramsfilter['seccion'] =  $this->_getSanitizedParam("seccion");
            $parramsfilter['fecha'] =  $this->_getSanitizedParam("fecha");
            $parramsfilter['estado'] =  $this->_getSanitizedParam("estado");
            Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }

}