<?php

/**
*
*/

class Administracion_felicitacionController extends Administracion_mainController
{
	private $mainModel;
	private $route;
	protected $namefilter;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Calendar();
		$this->route = "/administracion/felicitacion";
		$this->namefilter = "parametersfilterfelicitacion";
		$this->_view->route = $this->route;
		parent::init();
	}

	public function indexAction()
	{
		$this->filters();
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Felicitaciones");
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$filters = $this->getFilter();
		$order = " calendar_startdate ASC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}
	public function manageAction()
	{
		$modelType = new Administracion_Model_DbTable_TypeCalendar();
		$modelUser = new Administracion_Model_DbTable_User();
		$this->_view->users = $modelUser->getList(" user_level = '2' ","user_names ASC");
		$this->_view->sections = $this->getSections();
		$this->_view->types = $modelType->getList("type_calendar_type = 'Felicitacion' "," type_calendar_name ASC");
		$this->setLayout('administracion_panel');
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->calendar_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar felicitacion");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear felicitacion");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear felicitacion");
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$data = $this->getData();
			if($_FILES['image']['name'] != ''){
				$data['image'] = $uploadImage->upload("image");
			}
			if($_FILES['banner']['name'] != ''){
				$data['banner'] = $uploadImage->upload("banner");
			}
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
		}
		header('Location: '.$this->route);
	}
	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->calendar_id) {
				$data = $this->getData();
				if($_FILES['image']['name'] != ''){
					if($content->calendar_image){
						$uploadImage->delete($content->calendar_image);
					}
					$data['image'] = $uploadImage->upload("image");
				} else {
					$data['image'] = $content->calendar_image;
				}
				if($_FILES['banner']['name'] != ''){
					if($content->calendar_banner){
						$uploadImage->delete($content->calendar_banner);
					}
					$data['banner'] = $uploadImage->upload("banner");
				} else {
					$data['banner'] = $content->calendar_banner;
				}
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$modelUploadImage= new Core_Model_Upload_Image();
					if (isset($content->calendar_image) && $content->calendar_image != '') {
						$modelUploadImage->delete($content->calendar_image);
					}
					if (isset($content->calendar_banner) && $content->calendar_banner != '') {
						$modelUploadImage->delete($content->calendar_banner);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	private function getData()
	{
		$data = array();
		$data['name'] = $this->_getSanitizedParam("name");
		$data['startdate'] = $this->_getSanitizedParam("startdate");
		$data['starttime'] = "";
		$data['enddate'] = "";
		$data['endtime'] = "";
		$data['user'] = $this->_getSanitizedParam("user");
		$data['description'] = $this->_getSanitizedParamHtml("description");
		$data['type'] = $this->_getSanitizedParam("type");
		$data['tipo'] = $this->_getSanitizedParam("tipo");
		$data['createdby'] = Session::getInstance()->get("kt_login_id");;
		$data['banner'] = "";
		$data['image'] = "";
		return $data;
	}

	private function getSections()
	{
		$sections = array();
		$sections['Empleados'] = "Empleados";
		$sections['Asociados'] = "Asociados";
		return $sections;
	}

	private function getTypecalendario()
	{
		$modelType = new Administracion_Model_DbTable_TypeCalendar();
		$tipos = $modelType->getList("type_calendar_type = 'Felicitacion' "," type_calendar_name ASC");
		$sections = array();
		foreach ($tipos as $key => $value) {
			$sections[$value->type_calendar_id] = $value->type_calendar_name;
		}
		return $sections;
	}

	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {
        $filtros = " type_calendar_type = 'Felicitacion' ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);

            if ($filters->titulo != '') {
                $filtros = $filtros." AND calendar_name LIKE '%".$filters->titulo."%'";
            }
            if ($filters->type != '') {
                $filtros = $filtros." AND calendar_type = '".$filters->type."' ";
            }
            if ($filters->typeuser != '') {
                $filtros = $filtros." AND calendar_type_user ='".$filters->typeuser."'";
            }
            if ($filters->fecha != '') {
                $filtros = $filtros." AND calendar_startdate ='".$filters->fecha."'";
            }
        }
        return $filtros;
    }

    /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        $this->_view->sections = $this->getSections();
        $this->_view->typecalendarios = $this->getTypecalendario();
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();
            $parramsfilter['titulo'] =  $this->_getSanitizedParam("titulo");
            $parramsfilter['type'] =  $this->_getSanitizedParam("type");
            $parramsfilter['typeuser'] =  $this->_getSanitizedParam("typeuser");
            $parramsfilter['fecha'] =  $this->_getSanitizedParam("fecha");
            Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }


}