<?php

/**
*
*/
class Administracion_triggersgeneratorController extends Controllers_Abstract
{

	public function indexAction()
	{
		$modelTables = new Administracion_Model_DbTable_Tables();
		$namedatabase = Config_Config::getInstance()->getValue('db/name');
		$nombrevariable = "Tables_in_".$namedatabase;
		$tablas = $modelTables->getTables();
		foreach ($tablas as $key => $tabla) {
			$nombretabla = $tabla->$nombrevariable;
			//echo $nombretabla."<br><br>";
			$campos = $modelTables->getCampos($nombretabla);
			//print_r($campos);
			$this->generatetrigger($nombretabla,$campos);
		}
		
	}
	

	public function generatetrigger($name,$campos){
		$insert1 = "";
		$insert2 = "";
		$values1 = "";
		$values2 = "";
		$db1 = "";
		$db2 = "";
		foreach ($campos as $key => $value) {
			$campo = $value->Field;
			$tipo = $value->Type;
			$insert1 = $insert1."trigger_".$name."_".$campo."_old,";
			$insert2 = $insert2."trigger_".$name."_".$campo."_new,";
			$values1 = $values1."OLD.".$campo.",";
			$values2 = $values2."NEW.".$campo.",";
			$db1 = $db1."trigger_".$name."_".$campo."_old ".$tipo." DEFAULT NULL,";
			$db2 = $db2."trigger_".$name."_".$campo."_new ".$tipo." DEFAULT NULL,";
		}
		echo "DELIMITER $$ <br>";
		echo "CREATE TRIGGER tg_insert_".$name." BEFORE INSERT ON ".$name." FOR EACH ROW INSERT INTO trigger_".$name."(".$insert2." trigger_".$name."_date,trigger_".$name."_action) VALUES ( ".$values2."NOW(),'INSERT')";
		echo "<br> $$ DELIMITER ;<br><br>";


		echo "DELIMITER $$ <br>";
		echo "CREATE TRIGGER tg_update_".$name." BEFORE UPDATE ON ".$name." FOR EACH ROW INSERT INTO trigger_".$name."(".$insert1."".$insert2."trigger_".$name."_date,trigger_".$name."_action) VALUES (".$values1."".$values2." NOW(),'UPDATE')";
		echo "<br> $$ DELIMITER ;<br><br>";

		echo "DELIMITER $$ <br>";
		echo "CREATE TRIGGER tg_delete_".$name." BEFORE DELETE ON ".$name." FOR EACH ROW INSERT INTO trigger_".$name."(".$insert1."trigger_".$name."_date,trigger_".$name."_action) VALUES (".$values1." NOW(),'DELETE')";
		echo "<br> $$ DELIMITER ;<br><br>";

		echo "CREATE TABLE trigger_".$name." (
		  `trigger_".$name."_id` bigint(100) NOT NULL,
		  ".$db1."".$db2."
		  `trigger_".$name."_action` varchar(255) DEFAULT NULL,
		  `trigger_".$name."_date` datetime DEFAULT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		";
		echo"<br><br>";
	}

}