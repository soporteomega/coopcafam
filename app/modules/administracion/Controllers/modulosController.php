<?php

/**
*
*/

class Administracion_modulosController extends Administracion_mainController
{
	private $mainModel;
	private $route;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Modulos();
		$this->route = "/administracion/modulos";
		$this->_view->route = $this->route;
		$this->_view->csrf = Session::getInstance()->get('csrf');
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$modelCurso = new Administracion_Model_DbTable_Cursos();
		$idcurso = $this->_getSanitizedParam("curso");
		$this->_view->curso = $modelCurso->getById($idcurso);
		$this->getLayout()->setTitle("Listar Modulos ".$this->_view->curso->curso_titulo);
		$filters = " cursos_id ='$idcurso' ";
		$order = " orden ASC ";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}

	public function manageAction()
	{
		$modelCurso = new Administracion_Model_DbTable_Cursos();
		$idcurso = $this->_getSanitizedParam("curso");
		$this->_view->curso = $modelCurso->getById($idcurso);
		$this->setLayout('administracion_panel');
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if(isset($content->modulo_id)){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Modulo ".$this->_view->curso->curso_titulo);
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Modulo ".$this->_view->curso->curso_titulo);
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Modulo ".$this->_view->curso->curso_titulo);
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$data = $this->getData();
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
		}
		header('Location: '.$this->route."?curso=".$data['curso']);
	}
	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->modulo_id) {
				$data = $this->getData();
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route."?curso=".$data['curso']);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		$idcurso = $this->_getSanitizedParam("curso");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route.'?curso='.$idcurso);
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['titulo'] = $this->_getSanitizedParam("titulo");
		$data['descripcion'] = $this->_getSanitizedParamHtml("descripcion");
		$data['curso'] = $this->_getSanitizedParam("curso");
		$data['columnas'] = $this->_getSanitizedParam("columnas");
		return $data;
	}

	public function reporteAction(){
		$modulo = $this->_getSanitizedParam('modulo');
		$this->_view->modulo = $modulo;
		$content = $this->mainModel->getById($modulo);
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Reporte ".$content->modulo_titulo);

		$respuestaModel = new Administracion_Model_DbTable_Respuesta();
		$respuestas = $respuestaModel->respuestas($modulo);
		//print_r($respuestas);
		$arrayRespuestas = array();
		foreach ($respuestas as $key => $respuesta) {
			$arrayRespuestas[$key] = array();
			$arrayRespuestas[$key]['detalle'] = $respuesta;
			$arrayRespuestas[$key]["bien"]= 0;
			$arrayRespuestas[$key]["mal"]= 0;
			$resultados = $respuestaModel->respondio($respuesta->user_id,$modulo);
			foreach ($resultados as  $res) {
				if($res->respuesta_correcto == 1){
					$arrayRespuestas[$key]["bien"] = $arrayRespuestas[$key]["bien"]+1;
				} else {
					$arrayRespuestas[$key]["mal"] = $arrayRespuestas[$key]["mal"]+1;
				}
			}
		}
		$this->_view->contenido = $arrayRespuestas;
	}

	public function reportemesAction(){
		$modulo = $this->_getSanitizedParam('modulo');
		$this->_view->modulo = $modulo;
		$content = $this->mainModel->getById($modulo);
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Reporte ".$content->modulo_titulo);

		$respuestaModel = new Administracion_Model_DbTable_Respuesta();
		$respuestas = $respuestaModel->respuestas($modulo);
		//print_r($respuestas);
		$arrayRespuestas = array();
		foreach ($respuestas as $key => $respuesta) {
			$arrayRespuestas[$key] = array();
			$arrayRespuestas[$key]['detalle'] = $respuesta;
			$arrayRespuestas[$key]["bien"]= 0;
			$arrayRespuestas[$key]["mal"]= 0;
			$resultados = $respuestaModel->respondio($respuesta->user_id,$modulo);
			foreach ($resultados as  $res) {
				if($res->respuesta_correcto == 1){
					$arrayRespuestas[$key]["bien"] = $arrayRespuestas[$key]["bien"]+1;
				} else {
					$arrayRespuestas[$key]["mal"] = $arrayRespuestas[$key]["mal"]+1;
				}
			}
		}
		$this->_view->contenido = $arrayRespuestas;
	}

	public function reportepreguntasAction(){
		$this->setLayout('administracion_panel');
		$modelmodulo = new Administracion_Model_DbTable_Modulos();
		$idmodulo = $this->_getSanitizedParam("modulo");
		$this->_view->modulo = $modelmodulo->getById($idmodulo);
		$this->getLayout()->setTitle("Reporte preguntas ".$this->_view->modulo->modulo_titulo);
		$filters = " modulo_id = '$idmodulo' ";
		$order = " orden ASC ";

		$modeloPreguntas = new Administracion_Model_DbTable_Preguntas();
		$preguntas = $modeloPreguntas->getList($filters,$order);
		$this->_view->preguntas = $preguntas;

		$respuestaModel = new Administracion_Model_DbTable_Respuesta();
		$respuestas = $respuestaModel->getList("","");
		$this->_view->respuestas = $respuestas;

	}

	public function reporteexportarAction(){
		header("Content-type: application/vnd.ms-excel");

		$modulo = $this->_getSanitizedParam('modulo');
		$content = $this->mainModel->getById($modulo);
		$this->setLayout('blanco');
		$this->getLayout()->setTitle("Reporte ".$content->modulo_titulo);
		header("Content-disposition: attachment; filename=Reporte.xls");
		$respuestaModel = new Administracion_Model_DbTable_Respuesta();
		$respuestas = $respuestaModel->respuestas($modulo);
		$arrayRespuestas = array();
		foreach ($respuestas as $key => $respuesta) {
			$arrayRespuestas[$key] = array();
			$arrayRespuestas[$key]['detalle'] = $respuesta;
			$arrayRespuestas[$key]["bien"]= 0;
			$arrayRespuestas[$key]["mal"]= 0;
			$resultados = $respuestaModel->respondio($respuesta->user_id,$modulo);
			foreach ($resultados as  $res) {
				if($res->respuesta_correcto == 1){
					$arrayRespuestas[$key]["bien"] = $arrayRespuestas[$key]["bien"]+1;
				} else {
					$arrayRespuestas[$key]["mal"] = $arrayRespuestas[$key]["mal"]+1;
				}
			}
		}
		$this->_view->contenido = $arrayRespuestas;
	}



}