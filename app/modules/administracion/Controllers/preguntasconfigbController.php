<?php
/**
* Controlador de Preguntasconfigb que permite la  creacion, edicion  y eliminacion de los preguntas_config del Sistema
*/
class Administracion_preguntasconfigbController extends Administracion_mainController
{
	/**
	 * $mainModel  instancia del modelo de  base de datos preguntas_config
	 * @var modeloContenidos
	 */
	private $mainModel;

	/**
	 * $route  url del controlador base
	 * @var string
	 */
	private $route;

	/**
	 * $pages cantidad de registros a mostrar por pagina]
	 * @var integer
	 */
	private $pages ;

	/**
	 * $namefilter nombre de la variable a la fual se le van a guardar los filtros
	 * @var string
	 */
	protected $namefilter;

	/**
	 * $_csrf_section  nombre de la variable general csrf  que se va a almacenar en la session
	 * @var string
	 */
	protected $_csrf_section = "administracion_preguntasconfigb";

	/**
	 * $namepages nombre de la pvariable en la cual se va a guardar  el numero de seccion en la paginacion del controlador
	 * @var string
	 */
	protected $namepages;



	/**
     * Inicializa las variables principales del controlador preguntasconfigb .
     *
     * @return void.
     */
	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Preguntasconfigb();
		$this->namefilter = "parametersfilterpreguntasconfigb";
		$this->route = "/administracion/preguntasconfigb";
		$this->namepages ="pages_preguntasconfigb";
		$this->_view->route = $this->route;
		if(Session::getInstance()->get($this->namepages)){
			$this->pages = Session::getInstance()->get($this->namepages);
		} else {
			$this->pages = 20;
		}
		parent::init();
	}


	/**
     * Recibe la informacion y  muestra un listado de  preguntas_config con sus respectivos filtros.
     *
     * @return void.
     */
	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar preguntas_config");
		$this->filters();
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$filters = $this->getFilter();
		$order = "";
		$list = $this->mainModel->getList($filters,$order);
		$amount = $this->pages;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->register_number = count($list);
		$this->_view->pages = $this->pages;
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
		$this->_view->csrf_section = $this->_csrf_section;
	}

	/**
     * Genera la Informacion necesaria para editar o crear un  preguntas_config  y muestra su formulario
     *
     * @return void.
     */
	public function manageAction()
	{
		$this->_view->route = $this->route;
		$this->setLayout('administracion_panel');
		$this->_csrf_section = "manage_preguntasconfigb_".date("YmdHis");
		$this->_csrf->generateCode($this->_csrf_section);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->preguntas_config_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar preguntas_config");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear preguntas_config");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear preguntas_config");
		}
	}

	/**
     * Inserta la informacion de un preguntas_config  y redirecciona al listado de preguntas_config.
     *
     * @return void.
     */
	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf ) {	
			$data = $this->getData();
			$uploadImage =  new Core_Model_Upload_Image();
			if($_FILES['preguntas_config_gif_aprobado']['name'] != ''){
				$data['preguntas_config_gif_aprobado'] = $uploadImage->upload("preguntas_config_gif_aprobado");
			}
			if($_FILES['preguntas_config_gif_reprobado']['name'] != ''){
				$data['preguntas_config_gif_reprobado'] = $uploadImage->upload("preguntas_config_gif_reprobado");
			}
			if($_FILES['preguntas_config_gif_reloj']['name'] != ''){
				$data['preguntas_config_gif_reloj'] = $uploadImage->upload("preguntas_config_gif_reloj");
			}
			$id = $this->mainModel->insert($data);
			
		}
		header('Location: '.$this->route);
	}

	/**
     * Recibe un identificador  y Actualiza la informacion de un preguntas_config  y redirecciona al listado de preguntas_config.
     *
     * @return void.
     */
	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf ) {
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->preguntas_config_id) {
				$data = $this->getData();
				$uploadImage =  new Core_Model_Upload_Image();
				if($_FILES['preguntas_config_gif_aprobado']['name'] != ''){
					if($content->preguntas_config_gif_aprobado){
						$uploadImage->delete($content->preguntas_config_gif_aprobado);
					}
					$data['preguntas_config_gif_aprobado'] = $uploadImage->upload("preguntas_config_gif_aprobado");
				} else {
					$data['preguntas_config_gif_aprobado'] = $content->preguntas_config_gif_aprobado;
				}
			
				if($_FILES['preguntas_config_gif_reprobado']['name'] != ''){
					if($content->preguntas_config_gif_reprobado){
						$uploadImage->delete($content->preguntas_config_gif_reprobado);
					}
					$data['preguntas_config_gif_reprobado'] = $uploadImage->upload("preguntas_config_gif_reprobado");
				} else {
					$data['preguntas_config_gif_reprobado'] = $content->preguntas_config_gif_reprobado;
				}
			
				if($_FILES['preguntas_config_gif_reloj']['name'] != ''){
					if($content->preguntas_config_gif_reloj){
						$uploadImage->delete($content->preguntas_config_gif_reloj);
					}
					$data['preguntas_config_gif_reloj'] = $uploadImage->upload("preguntas_config_gif_reloj");
				} else {
					$data['preguntas_config_gif_reloj'] = $content->preguntas_config_gif_reloj;
				}
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route);
	}

	/**
     * Recibe un identificador  y elimina un preguntas_config  y redirecciona al listado de preguntas_config.
     *
     * @return void.
     */
	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_csrf_section] == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$uploadImage =  new Core_Model_Upload_Image();
					if (isset($content->preguntas_config_gif_aprobado) && $content->preguntas_config_gif_aprobado != '') {
						$uploadImage->delete($content->preguntas_config_gif_aprobado);
					}
					
					if (isset($content->preguntas_config_gif_reprobado) && $content->preguntas_config_gif_reprobado != '') {
						$uploadImage->delete($content->preguntas_config_gif_reprobado);
					}
					
					if (isset($content->preguntas_config_gif_reloj) && $content->preguntas_config_gif_reloj != '') {
						$uploadImage->delete($content->preguntas_config_gif_reloj);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	/**
     * Recibe la informacion del formulario y la retorna en forma de array para la edicion y creacion de Preguntasconfigb.
     *
     * @return array con toda la informacion recibida del formulario.
     */
	private function getData()
	{
		$data = array();
		if($this->_getSanitizedParam("preguntas_config_modulo_id") == '' ) {
			$data['preguntas_config_modulo_id'] = '0';
		} else {
			$data['preguntas_config_modulo_id'] = $this->_getSanitizedParam("preguntas_config_modulo_id");
		}
		if($this->_getSanitizedParam("preguntas_config_orden") == '' ) {
			$data['preguntas_config_orden'] = '0';
		} else {
			$data['preguntas_config_orden'] = $this->_getSanitizedParam("preguntas_config_orden");
		}
		if($this->_getSanitizedParam("preguntas_config_cantidad") == '' ) {
			$data['preguntas_config_cantidad'] = '0';
		} else {
			$data['preguntas_config_cantidad'] = $this->_getSanitizedParam("preguntas_config_cantidad");
		}
		$data['preguntas_config_tiempo'] = $this->_getSanitizedParam("preguntas_config_tiempo");
		if($this->_getSanitizedParam("preguntas_config_oportunidades") == '' ) {
			$data['preguntas_config_oportunidades'] = '0';
		} else {
			$data['preguntas_config_oportunidades'] = $this->_getSanitizedParam("preguntas_config_oportunidades");
		}
		if($this->_getSanitizedParam("preguntas_mostrar_certificado") == '' ) {
			$data['preguntas_mostrar_certificado'] = '0';
		} else {
			$data['preguntas_mostrar_certificado'] = $this->_getSanitizedParam("preguntas_mostrar_certificado");
		}
		$data['preguntas_config_gif_aprobado'] = "";
		$data['preguntas_config_gif_reprobado'] = "";
		$data['preguntas_config_gif_reloj'] = "";
		return $data;
	}
	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {

        $filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);
		}
        return $filtros;
    }

 /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }
}