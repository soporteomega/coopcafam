<?php

/**
*
*/

class Administracion_typecalendarController extends Administracion_mainController
{
	private $mainModel;
	private $route;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_TypeCalendar();
		$this->route = "/administracion/typecalendar";
		$this->_view->route = $this->route;
		$this->_view->csrf = Session::getInstance()->get('csrf');
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Tipos de Eventos Calendario");
		$filters = "";
		$order = "";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}

	public function manageAction()
	{
		$this->setLayout('administracion_panel');
		$id = $this->_getSanitizedParam("id");
		$this->_view->sections = $this->getSections();
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if(isset($content->type_calendar_id)){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Tipo de Evento Calendario");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Tipo de Evento Calendario");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Tipo de Evento Calendario");
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$data = $this->getData();
			if($_FILES['icono']['name'] != ''){
				$data['icono'] = $uploadImage->upload("icono");
			}
			$id = $this->mainModel->insert($data);
		}
		header('Location: '.$this->route);
	}
	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->type_calendar_id) {
				$uploadImage =  new Core_Model_Upload_Image();
				$data = $this->getData();
				if($_FILES['icono']['name'] != ''){
					if($content->type_calendar_icon){
						$uploadImage->delete($content->type_calendar_icon);
					}
					$data['icono'] = $uploadImage->upload("icono");
				} else {
					$data['icono'] = $content->type_calendar_icon;
				}
				
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$modelUploadImage= new Core_Model_Upload_Image();
					if (isset($content->type_calendar_icon) && $content->type_calendar_icon != '') {
						$modelUploadImage->delete($content->type_calendar_icon);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	private function getData()
	{
		$data = array();
		$data['name'] = $this->_getSanitizedParam("name");
		$data['color'] = $this->_getSanitizedParam("color");
		$data['tipo'] = $this->_getSanitizedParam("tipo");
		$data['logo'] = '';
		return $data;
	}

	private function getSections()
	{
		$sections = array();
		$sections['Calendario'] = "Calendario";
		$sections['Felicitacion'] = "Felicitacion";
		return $sections;
	}



}