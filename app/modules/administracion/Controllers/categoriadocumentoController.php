<?php

/**
*
*/
class Administracion_categoriadocumentoController extends Administracion_mainController
{
	private $mainModel;
	private $route;
	protected $namefilter;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Categoriadocumento();
		$this->namefilter = "parametersfiltercategoriadocumento";
		$this->route = "/administracion/categoriadocumento";
		$this->_view->route = $this->route;
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Categoria Documento");
		$this->filters();
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
        $padre = $this->_getSanitizedParam("padre");
        $this->_view->padre = $padre;
        if((int)$padre > 0){
        	$filters =" categoria_documento_padre = '".$padre."'";
        } else {
        	$filters =" ( categoria_documento_padre = 0 OR categoria_documento_padre IS NULL ) ";
        }
		$filters = $filters."AND ".$this->getFilter();
		$order = " orden ASC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 100;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->id_content = $idcontent;
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}
	public function manageAction()
	{
		$this->setLayout('administracion_panel');
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$this->_view->id_content = $this->_getSanitizedParam("content");
		$this->_view->sections = $this->getSections();
		$id = $this->_getSanitizedParam("id");
		$padre = $this->_getSanitizedParam("padre");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->categoria_documento_id){
				$this->_view->content = $content;
				$padre = $content->categoria_documento_padre;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Categoria Documento");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Categoria Documento");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Categoria Documento");
		}
		 $this->_view->padre = $padre;
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1 ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$data = $this->getData();
			if($_FILES['imagen']['name'] != ''){
				$data['imagen'] = $uploadImage->upload("imagen");
			}
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
		}
		if($this->_getSanitizedParamHtml('padre') > 0){
			header('Location: '.$this->route.'?padre='.$this->_getSanitizedParamHtml('padre'));
		} else {
			header('Location: '.$this->route);
		}
	}

	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1) {
			$uploadImage =  new Core_Model_Upload_Image();
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->categoria_documento_id) {
				$data = $this->getData();
				if($_FILES['imagen']['name'] != ''){
					if($content->categoria_documento_imagen){
						$uploadImage->delete($content->categoria_documento_imagen);
					}
					$data['imagen'] = $uploadImage->upload("imagen");
				} else {
					$data['imagen'] = $content->categoria_documento_imagen;
				}
				$this->mainModel->update($data,$id);
			}
		}
		if($this->_getSanitizedParamHtml('padre') > 0){
			header('Location: '.$this->route.'?padre='.$this->_getSanitizedParamHtml('padre'));
		} else {
			header('Location: '.$this->route);
		}
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$modelUploadImage= new Core_Model_Upload_Image();
					if (isset($content->categoria_documento_imagen) && $content->categoria_documento_imagen != '') {
						$modelUploadImage->delete($content->categoria_documento_imagen);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		if($this->_getSanitizedParamHtml('padre') > 0){
			header('Location: '.$this->route.'?padre='.$this->_getSanitizedParamHtml('padre'));
		} else {
			header('Location: '.$this->route);
		}
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['nombre'] = $this->_getSanitizedParam('nombre');
		$data['tipo'] = $this->_getSanitizedParam('tipo');
		$data['descripcion'] = $this->_getSanitizedParamHtml('descripcion');
		$data['padre'] = $this->_getSanitizedParamHtml('padre');
		$data['imagen'] = "";
		return $data;
	}

	private function getSections()
	{
		$sections = array();
		$sections['SGC General'] = "SGC General";
		$sections['Mapa - Procesos Estrategicos'] = "Mapa - Procesos Estrategicos";
		$sections['Mapa - Procesos Misionales'] = "Mapa - Procesos Misionales";
		$sections['Mapa - Procesos de Apoyo'] = "Mapa - Procesos de Apoyo";
		return $sections;
	}


	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {
        $filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);

            if ($filters->titulo != '') {
                $filtros = $filtros." AND categoria_documento_nombre LIKE '%".$filters->titulo."%'";
            }
            if ($filters->seccion != '') {
                $filtros = $filtros." AND categoria_documento_tipo ='".$filters->seccion."'";
            }
        }
        return $filtros;
    }

    /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
         $this->_view->sections = $this->getSections();
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();
            $parramsfilter['titulo'] =  $this->_getSanitizedParam("titulo");
            $parramsfilter['seccion'] =  $this->_getSanitizedParam("seccion");
            Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }

}