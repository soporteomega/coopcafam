<?php

/**
*
*/

class Administracion_calendarController extends Administracion_mainController
{
	private $mainModel;
	private $route;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Calendar();
		$this->route = "/administracion/calendar";
		$this->_view->route = $this->route;
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Calendarios");
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$filters = "type_calendar_type = 'Calendario'";
		$order = " calendar_startdate ASC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}
	public function manageAction()
	{
		$modelType = new Administracion_Model_DbTable_TypeCalendar();
		$this->_view->sections = $this->getSections();
		$this->_view->types = $modelType->getList(" type_calendar_type = 'Calendario' "," type_calendar_name ASC");
		$this->setLayout('administracion_panel');
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->calendar_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Calendario");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Calendario");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Calendario");
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$data = $this->getData();
			if($_FILES['image']['name'] != ''){
				$data['image'] = $uploadImage->upload("image");
			}
			if($_FILES['banner']['name'] != ''){
				$data['banner'] = $uploadImage->upload("banner");
			}
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
		}
		header('Location: '.$this->route);
	}

	public function enviareventoAction(){
		$id = $this->_getSanitizedParam('id');
		$email = new Core_Model_Sendingemail($this->_view);
		$email->evento($id);
		header('Location: '.$this->route);
	}

	public function enviareventosAction(){
		$email = new Core_Model_Sendingemail($this->_view);
		$email->eventos();
		header('Location: '.$this->route);
	}

	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->calendar_id) {
				$data = $this->getData();
				if($_FILES['image']['name'] != ''){
					if($content->calendar_image){
						$uploadImage->delete($content->calendar_image);
					}
					$data['image'] = $uploadImage->upload("image");
				} else {
					$data['image'] = $content->calendar_image;
				}
				if($_FILES['banner']['name'] != ''){
					if($content->calendar_banner){
						$uploadImage->delete($content->calendar_banner);
					}
					$data['banner'] = $uploadImage->upload("banner");
				} else {
					$data['banner'] = $content->calendar_banner;
				}
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$modelUploadImage= new Core_Model_Upload_Image();
					if (isset($content->calendar_image) && $content->calendar_image != '') {
						$modelUploadImage->delete($content->calendar_image);
					}
					if (isset($content->calendar_banner) && $content->calendar_banner != '') {
						$modelUploadImage->delete($content->calendar_banner);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	private function getData()
	{
		$data = array();
		$data['name'] = $this->_getSanitizedParam("name");
		$data['startdate'] = $this->_getSanitizedParam("startdate");
		$data['starttime'] = $this->_getSanitizedParam("starttime");
		$data['enddate'] = $this->_getSanitizedParam("enddate");
		$data['endtime'] = $this->_getSanitizedParam("endtime");
		$data['description'] = $this->_getSanitizedParamHtml("description");
		$data['type'] = $this->_getSanitizedParam("type");
		$data['tipo'] = $this->_getSanitizedParam("tipo");
		$data['createdby'] = Session::getInstance()->get("kt_login_id");;
		$data['banner'] = "";
		$data['user'] = "0";
		return $data;
	}

	private function getSections()
	{
		$sections = array();
		$sections['Empleados'] = "Empleados";
		$sections['Asociados'] = "Asociados";
		return $sections;
	}

}