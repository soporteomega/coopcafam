<?php

/**
*
*/
class Administracion_consejeroController extends Administracion_mainController
{
	private $mainModel;
	private $route;
	protected $namefilter;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Consejero();
		$this->namefilter = "parametersfilterconsejero";
		$this->route = "/administracion/consejero";
		$this->_view->route = $this->route;
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Contenidos Consejero");
		$this->filters();
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$filters = $this->getFilter();
		$order = "orden ASC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}
	public function manageAction()
	{
		$this->setLayout('administracion_panel');
		$this->_view->csrf = Session::getInstance()->get('csrf');
		$this->_view->sections = $this->getSections();
		$this->_view->disenios = $this->getDisenio();
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->content_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Contenido Consejero");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Contenido Consejero");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Contenido Consejero");
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1 ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$uploadDocument =  new Core_Model_Upload_Document();
			$data = $this->getData();
			if($_FILES['documento']['name'] != ''){
				$data['documento'] = $uploadDocument->upload("documento");
			}
			if($_FILES['image']['name'] != ''){
				$data['image'] = $uploadImage->upload("image");
			}
			if($_FILES['banner']['name'] != ''){
				$data['banner'] = $uploadImage->upload("banner");
			}
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);

		}
		header('Location: '.$this->route);
	}

	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf or 1==1) {
			$uploadImage =  new Core_Model_Upload_Image();
			$uploadDocument =  new Core_Model_Upload_Document();
			$id = $this->_getSanitizedParam("id_content");
			$content = $this->mainModel->getById($id);
			if ($content->content_id) {
				$data = $this->getData();
				if($_FILES['documento']['name'] != ''){
					if($content->documento_documento){
						$uploadDocument->delete($content->content_documento);
					}
					$data['documento'] = $uploadDocument->upload("documento");
				} else {
					$data['documento'] = $content->content_documento;
				}
				if($_FILES['image']['name'] != ''){
					if($content->content_image){
						$uploadImage->delete($content->content_image);
					}
					$data['image'] = $uploadImage->upload("image");
				} else {
					$data['image'] = $content->content_image;
				}
				if($_FILES['banner']['name'] != ''){
					if($content->content_banner){
						$uploadImage->delete($content->content_banner);
					}
					$data['banner'] = $uploadImage->upload("banner");
				} else {
					$data['banner'] = $content->content_banner;
				}
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$modelUploadImage= new Core_Model_Upload_Image();
					$uploadDocument =  new Core_Model_Upload_Document();
					if (isset($content->content_documento) && $content->content_documento != '') {
						$uploadDocument->delete($content->content_documento);
					}
					if (isset($content->content_image) && $content->content_image != '') {
						$modelUploadImage->delete($content->content_image);
					}
					if (isset($content->content_banner) && $content->content_banner != '') {
						$modelUploadImage->delete($content->content_banner);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['date'] = $this->_getSanitizedParam("date");
		$data['title'] = $this->_getSanitizedParam("title");
		$data['subtitle'] = $this->_getSanitizedParam("subtitle");
		$data['introduction'] = $this->_getSanitizedParamHtml("introduction");
		$data['description'] = $this->_getSanitizedParamHtml("description");
		$data['section'] = $this->_getSanitizedParam("section");
		$data['link'] = $this->_getSanitizedParam("link");
		$data['image'] = '';
		$data['banner'] = '';
		$data['documento'] = '';
		$data['disenio'] = $this->_getSanitizedParam('disenio');
		return $data;
	}

	private function getSections()
	{
		$sections = array();
		$sections['Introduccion'] = "Introduccion";
		$sections['Consejero'] = "Consejero";
		return $sections;
	}

	private function getDisenio()
	{
		$disenio = array();
		$disenio['Default Gris'] = "Default Gris";
		$disenio['Dos Columnas'] = "Dos Columnas";
		$disenio['Dos Columnas Gris'] = "Dos Columnas Gris";
		$disenio['Banner'] = "Banner";
		return $disenio;
	}

	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {
        $filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);

            if ($filters->titulo != '') {
                $filtros = $filtros." AND content.content_title LIKE '%".$filters->titulo."%'";
            }
            if ($filters->seccion != '') {
                $filtros = $filtros." AND content.content_section ='".$filters->seccion."'";
            }
        }
        return $filtros;
    }

    /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        $this->_view->sections = $this->getSections();
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();
            $parramsfilter['titulo'] =  $this->_getSanitizedParam("titulo");
            $parramsfilter['seccion'] =  $this->_getSanitizedParam("seccion");
            Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }

}