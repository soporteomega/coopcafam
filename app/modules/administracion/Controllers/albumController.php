<?php

/**
*
*/

class Administracion_albumController extends Administracion_mainController
{
	private $mainModel;
	private $route;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Album();
		$this->route = "/administracion/album";
		$this->_view->route = $this->route;
		$this->_view->csrf = Session::getInstance()->get('csrf');
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar Albumes");
		$filters = "";
		$order = "album_fecha DESC, orden ASC";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}

	public function manageAction()
	{
		$this->setLayout('administracion_panel');
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if(isset($content->album_id)){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar Album");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear Album");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear Album");
		}
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$data = $this->getData();
			if($_FILES['imagen']['name'] != ''){
				$data['imagen'] = $uploadImage->upload("imagen");
			}
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
		}
		header('Location: '.$this->route);
	}
	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$uploadImage =  new Core_Model_Upload_Image();
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->album_id) {
				$data = $this->getData();
				if($_FILES['imagen']['name'] != ''){
					if($content->album_imagen){
						$uploadImage->delete($content->album_imagen);
					}
					$data['imagen'] = $uploadImage->upload("imagen");
				} else {
					$data['imagen'] = $content->album_imagen;
				}
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$modelUploadImage= new Core_Model_Upload_Image();
					if (isset($content->album_imagen) && $content->album_imagen != '') {
						$modelUploadImage->delete($content->album_imagen);
					}
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['titulo'] = $this->_getSanitizedParam("titulo");
		$data['descripcion'] = $this->_getSanitizedParamHtml("descripcion");
		$data['imagen'] = '';
		$data['fecha'] = $this->_getSanitizedParam("fecha");
		return $data;
	}



}