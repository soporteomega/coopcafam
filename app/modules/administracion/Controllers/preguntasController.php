<?php

/**
*
*/

class Administracion_preguntasController extends Administracion_mainController
{
	private $mainModel;
	private $route;

	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Preguntas();
		$this->route = "/administracion/preguntas";
		$this->_view->route = $this->route;
		$this->_view->csrf = Session::getInstance()->get('csrf');
		parent::init();
	}

	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$modelmodulo = new Administracion_Model_DbTable_Modulos();
		$idmodulo = $this->_getSanitizedParam("modulo");
		$this->_view->modulo = $modelmodulo->getById($idmodulo);
		$this->getLayout()->setTitle("Listar preguntas ".$this->_view->modulo->modulo_titulo);
		$filters = " modulo_id = '$idmodulo' ";
		$order = " orden ASC ";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);

		$this->_view->tipos = $this->getTipos();
	}

	public function seleccionarAction()
	{
		$this->setLayout('administracion_panel');
		$modelmodulo = new Administracion_Model_DbTable_Modulos();
		$idmodulo = $this->_getSanitizedParam("modulo");
		$this->_view->modulo = $modelmodulo->getById($idmodulo);
		$this->getLayout()->setTitle("Seleccionar preguntas curso:  ".$this->_view->modulo->modulo_titulo);
		$filters = " modulo_id = '$idmodulo' ";
		$order = " orden ASC ";
		$list = $this->mainModel->getList($filters,$order);
		$amount = 20;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
	}

	public function manageAction()
	{
		$modelmodulo = new Administracion_Model_DbTable_Modulos();
		$idmodulo = $this->_getSanitizedParam("modulo");
		$this->_view->modulo = $modelmodulo->getById($idmodulo);
		$this->setLayout('administracion_panel');
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if(isset($content->modulo_id)){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar pregunta ".$this->_view->modulo->modulo_titulo);
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear pregunta ".$this->_view->modulo->modulo_titulo);
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear pregunta ".$this->_view->modulo->modulo_titulo);
		}
		$this->_view->tipos = $this->getTipos();
	}

	private function getTipos()
	{
		$tipos = array();
		$tipos[1] = 'Selección múltiple con única respuesta';
		$tipos[2] = 'Falso o Verdadero';
		$tipos[3] = 'Selección múltiple con múltiple respuesta';

		return $tipos;
	}

	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$data = $this->getData();
			$id = $this->mainModel->insert($data);
			$this->mainModel->changeOrder($id,$id);
		}
		header('Location: '.$this->route."?modulo=".$data['modulo']);
	}
	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->modulo_id) {
				$data = $this->getData();
				$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route."?modulo=".$data['modulo']);
	}

	public function guardarseleccionadasAction(){
		$curso = $this->_getSanitizedParam("curso");
		$modulo = $this->_getSanitizedParam("modulo");
		$i=0;
		$this->mainModel->limpiar_seleccion($modulo);
		for($i=0;$i<=1000;$i++){
			$pregunta = $_POST['activo'.$i];
			if($pregunta!=""){
				$this->mainModel->editField($i,"pregunta_activo","1");
			}
		}
		header('Location: '.$this->route."/seleccionar/?modulo=".$modulo."&curso=".$curso);
	}

	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		$idmodulo = $this->_getSanitizedParam("modulo");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route.'?modulo='.$idmodulo);
	}

	public function orderAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf') == $csrf ) {
			$id1 =  $this->_getSanitizedParam("id1");
			$id2 =  $this->_getSanitizedParam("id2");
			if (isset($id1) && $id1 > 0 && isset($id2) && $id2 > 0) {
				$content1 = $this->mainModel->getById($id1);
				$content2 = $this->mainModel->getById($id2);
				if (isset($content1) && isset($content2)) {
					$order1 = $content1->orden;
					$order2 = $content2->orden;
					$this->mainModel->changeOrder($order2,$id1);
					$this->mainModel->changeOrder($order1,$id2);
				}
			}
		}
	}

	private function getData()
	{
		$data = array();
		$data['pregunta'] = $this->_getSanitizedParamHtml("pregunta");
		$data['respuesta1'] = $this->_getSanitizedParam("respuesta1");
		$data['respuesta2'] = $this->_getSanitizedParam("respuesta2");
		$data['respuesta3'] = $this->_getSanitizedParam("respuesta3");
		$data['respuesta4'] = $this->_getSanitizedParam("respuesta4");
		$data['respuesta5'] = $this->_getSanitizedParam("respuesta5");
		$data['respuesta6'] = $this->_getSanitizedParam("respuesta6");
		$data['respuesta7'] = $this->_getSanitizedParam("respuesta7");
		$data['respuesta8'] = $this->_getSanitizedParam("respuesta8");
		$data['respuesta9'] = $this->_getSanitizedParam("respuesta9");
		$data['respuesta10'] = $this->_getSanitizedParam("respuesta10");
		$data['respuesta11'] = $this->_getSanitizedParam("respuesta11");
		$data['respuesta12'] = $this->_getSanitizedParam("respuesta12");
		$data['respuesta13'] = $this->_getSanitizedParam("respuesta13");
		$data['respuesta14'] = $this->_getSanitizedParam("respuesta14");
		$data['modulo'] = $this->_getSanitizedParam("modulo");
		$data['activo'] = $this->_getSanitizedParam("activo");
		$data['correcta1'] = $this->_getSanitizedParam("correcta1");
		$data['correcta2'] = $this->_getSanitizedParam("correcta2");
		$data['correcta3'] = $this->_getSanitizedParam("correcta3");
		$data['correcta4'] = $this->_getSanitizedParam("correcta4");
		$data['correcta5'] = $this->_getSanitizedParam("correcta5");
		$data['correcta6'] = $this->_getSanitizedParam("correcta6");
		$data['correcta7'] = $this->_getSanitizedParam("correcta7");
		$data['correcta8'] = $this->_getSanitizedParam("correcta8");
		$data['correcta9'] = $this->_getSanitizedParam("correcta9");
		$data['correcta10'] = $this->_getSanitizedParam("correcta10");
		$data['correcta11'] = $this->_getSanitizedParam("correcta11");
		$data['correcta12'] = $this->_getSanitizedParam("correcta12");
		$data['correcta13'] = $this->_getSanitizedParam("correcta13");
		$data['correcta14'] = $this->_getSanitizedParam("correcta14");
		$data['tipo'] = $this->_getSanitizedParam("tipo");

		return $data;
	}



}