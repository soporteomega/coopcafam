<?php
/**
* Controlador de Frases que permite la  creacion, edicion  y eliminacion de los frases del Sistema
*/
class Administracion_frasesController extends Administracion_mainController
{
	/**
	 * $mainModel  instancia del modelo de  base de datos frases
	 * @var modeloContenidos
	 */
	private $mainModel;

	/**
	 * $route  url del controlador base
	 * @var string
	 */
	private $route;

	/**
	 * $pages cantidad de registros a mostrar por pagina]
	 * @var integer
	 */
	private $pages ;

	/**
	 * $namefilter nombre de la variable a la fual se le van a guardar los filtros
	 * @var string
	 */
	protected $namefilter;

	/**
	 * $_csrf_section  nombre de la variable general csrf  que se va a almacenar en la session
	 * @var string
	 */
	protected $_csrf_section = "administracion_frases";

	/**
	 * $namepages nombre de la pvariable en la cual se va a guardar  el numero de seccion en la paginacion del controlador
	 * @var string
	 */
	protected $namepages;



	/**
     * Inicializa las variables principales del controlador frases .
     *
     * @return void.
     */
	public function init()
	{
		$this->mainModel = new Administracion_Model_DbTable_Frases();
		$this->namefilter = "parametersfilterfrases";
		$this->route = "/administracion/frases";
		$this->namepages ="pages_frases";
		$this->_view->route = $this->route;
		if(Session::getInstance()->get($this->namepages)){
			$this->pages = Session::getInstance()->get($this->namepages);
		} else {
			$this->pages = 20;
		}
		parent::init();
	}


	/**
     * Recibe la informacion y  muestra un listado de  frases con sus respectivos filtros.
     *
     * @return void.
     */
	public function indexAction()
	{
		$this->setLayout('administracion_panel');
		$this->getLayout()->setTitle("Listar frases");
		$this->filters();
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$filters =(object)Session::getInstance()->get($this->namefilter);
        $this->_view->filters = $filters;
		$filters = $this->getFilter();
		$order = "";
		$list = $this->mainModel->getList($filters,$order);
		$amount = $this->pages;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		   	$start = 0;
		   	$page=1;
		}
		else {
		   	$start = ($page - 1) * $amount;
		}
		$this->_view->register_number = count($list);
		$this->_view->pages = $this->pages;
		$this->_view->totalpages = ceil(count($list)/$amount);
		$this->_view->page = $page;
		$this->_view->lists = $this->mainModel->getListPages($filters,$order,$start,$amount);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->list_cursos_mensajes_tipo = $this->getCursosmensajestipo();
	}

	/**
     * Genera la Informacion necesaria para editar o crear un  frase  y muestra su formulario
     *
     * @return void.
     */
	public function manageAction()
	{
		$this->_view->route = $this->route;
		$this->setLayout('administracion_panel');
		$this->_csrf_section = "manage_frases_".date("YmdHis");
		//$this->_csrf->generateCode($this->_csrf_section);
		$this->_view->csrf_section = $this->_csrf_section;
		$this->_view->csrf = Session::getInstance()->get('csrf')[$this->_csrf_section];
		$this->_view->list_cursos_mensajes_tipo = $this->getCursosmensajestipo();
		$id = $this->_getSanitizedParam("id");
		if ($id > 0) {
			$content = $this->mainModel->getById($id);
			if($content->cursos_mensajes_id){
				$this->_view->content = $content;
				$this->_view->routeform = $this->route."/update";
				$this->getLayout()->setTitle("Actualizar frase");
			}else{
				$this->_view->routeform = $this->route."/insert";
				$this->getLayout()->setTitle("Crear frase");
			}
		} else {
			$this->_view->routeform = $this->route."/insert";
			$this->getLayout()->setTitle("Crear frase");
		}
	}

	/**
     * Inserta la informacion de un frase  y redirecciona al listado de frases.
     *
     * @return void.
     */
	public function insertAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf or 1==1) {	
			$data = $this->getData();
			$id = $this->mainModel->insert($data);
			
		}
		header('Location: '.$this->route);
	}

	/**
     * Recibe un identificador  y Actualiza la informacion de un frase  y redirecciona al listado de frases.
     *
     * @return void.
     */
	public function updateAction(){
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_getSanitizedParam("csrf_section")] == $csrf or 1==1) {
			$id = $this->_getSanitizedParam("id");
			$content = $this->mainModel->getById($id);
			if ($content->cursos_mensajes_id) {
				$data = $this->getData();
					$this->mainModel->update($data,$id);
			}
		}
		header('Location: '.$this->route);
	}

	/**
     * Recibe un identificador  y elimina un frase  y redirecciona al listado de frases.
     *
     * @return void.
     */
	public function deleteAction()
	{
		$csrf = $this->_getSanitizedParam("csrf");
		if (Session::getInstance()->get('csrf')[$this->_csrf_section] == $csrf ) {
			$id =  $this->_getSanitizedParam("id");
			if (isset($id) && $id > 0) {
				$content = $this->mainModel->getById($id);
				if (isset($content)) {
					$this->mainModel->deleteRegister($id);
				}
			}
		}
		header('Location: '.$this->route);
	}

	/**
     * Recibe la informacion del formulario y la retorna en forma de array para la edicion y creacion de Frases.
     *
     * @return array con toda la informacion recibida del formulario.
     */
	private function getData()
	{
		$data = array();
		$data['cursos_mensajes_mensaje'] = $this->_getSanitizedParamHtml("cursos_mensajes_mensaje");
		if($this->_getSanitizedParam("cursos_mensajes_tipo") == '' ) {
			$data['cursos_mensajes_tipo'] = '0';
		} else {
			$data['cursos_mensajes_tipo'] = $this->_getSanitizedParam("cursos_mensajes_tipo");
		}
		$data['cursos_mensajes_color'] = $this->_getSanitizedParam("cursos_mensajes_color");
		$data['cursos_mensajes_color2'] = $this->_getSanitizedParam("cursos_mensajes_color2");
		return $data;
	}

	/**
     * Genera los valores del campo tipo.
     *
     * @return array cadena con los valores del campo tipo.
     */
	private function getCursosmensajestipo()
	{
		$array = array();
		$array['1'] = 'Aprobado';
		$array['2'] = 'Reprobado';
		return $array;
	}

	/**
     * Genera la consulta con los filtros de este controlador.
     *
     * @return array cadena con los filtros que se van a asignar a la base de datos
     */
    protected function getFilter()
    {

        $filtros = " 1 = 1 ";
        if (Session::getInstance()->get($this->namefilter)!="") {
            $filters =(object)Session::getInstance()->get($this->namefilter);
            if ($filters->cursos_mensajes_mensaje != '') {
                $filtros = $filtros." AND cursos_mensajes_mensaje LIKE '%".$filters->cursos_mensajes_mensaje."%'";
            }
            if ($filters->cursos_mensajes_tipo != '') {
                $filtros = $filtros." AND cursos_mensajes_tipo ='".$filters->cursos_mensajes_tipo."'";
            }
		}
        return $filtros;
    }

 /**
     * Recibe y asigna los filtros de este controlador
     *
     * @return void
     */
    protected function filters()
    {
        if ($this->getRequest()->isPost()== true) {
            $parramsfilter = array();$parramsfilter['cursos_mensajes_mensaje'] =  $this->_getSanitizedParam("cursos_mensajes_mensaje");$parramsfilter['cursos_mensajes_tipo'] =  $this->_getSanitizedParam("cursos_mensajes_tipo");Session::getInstance()->set($this->namefilter, $parramsfilter);
        }
        if ($this->_getSanitizedParam("cleanfilter") == 1) {
             Session::getInstance()->set($this->namefilter, '');
        }
    }
}