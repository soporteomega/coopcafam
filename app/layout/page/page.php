<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title><?= $this->_titlepage ?></title>
	<script src="/components/jquery/dist/jquery.min.js"></script>
	<script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/scripts/carousel/carousel.js"></script>
	<script src="/components/Gallery/js/blueimp-gallery.min.js"></script>
	<script src="/components/bootstrap-filestyle/src/bootstrap-filestyle.min.js"></script>
	<script src="/components/bootstrap-fileinput/js/fileinput.min.js" charset="UTF-8"></script>
	<script src="/components/bootstrap-fileinput/js/locales/es.js"></script>
	<script src="/skins/page/js/main.js"></script>
	<link rel="stylesheet" href="/components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="/components/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" href="/skins/page/css/global.css">
	<link rel="stylesheet" type="text/css" href="/scripts/carousel/carousel.css">
	<link href="/components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>
<body>
	<header>
		<div class="container">
			
			<div class="row">
				
				<div class="col-xs-6 col-sm-6 col-md-3">
					<img src="/skins/page/images/logo.png" class="logo">
				</div>
				<div class=" col-xs-6 col-sm-6 abrir-menu" ><span onclick="menuresponsive();"><i class="glyphicon glyphicon-th-list"></i>Menú</span></div>
				<div class="col-md-9 nav-responsive">
					<div class="cerrar-menu" onclick="menuresponsive()"><i class="glyphicon glyphicon-remove"></i>Cerrar</div>
					<div class="perfil">
						<?= $this->_data['perfil']; ?>
					</div>
					<nav>
						<ul>
							<li><a href="/">Inicio<br><img src="/skins/page/images/home.png" alt="inicio"></a></li>
							<li><a href="/page/cooperativa">Nuestra Cooperativa<br><img src="/skins/page/images/nosotros.png" alt="Nuestra Cooperativa"></a></li>
							<li><a href="/page/gestionhumana">Gestión Humana<br><img src="/skins/page/images/gestion.png" alt="Gestión Humana"></a></li>
							<li><a href="/page/sgc">SGC<br><img src="/skins/page/images/sgc.png" alt="SGC"></a></li>
							<li><a href="/page/formacionenlinea">Formación en Línea<br><img src="/skins/page/images/formacion.png" alt="Formación en Línea"></a></li>
							<li><a href="/page/solicitudesenlinea">Solicitudes en Línea<br><img src="/skins/page/images/solicitudes.png" alt="Solicitudes en Línea"></a></li>
							<?php if($_SESSION['kt_login_level']!=2){ ?>
								<li class="oculto"><a href="/page/consejeros">Sección Consejo de Administración</a></li>
							<?php } ?>
						</ul>
					</nav>
					<?php if($_SESSION['kt_login_level']!=2){ ?>
						<a href="/page/consejeros" class="btn-consejeros">Sección Consejo de Administración</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</header>
	<div class="main-content"><?= $this->_content ?></div>
	<footer>
		<?= $this->_data['footer']; ?>
	</footer>
	<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
	    <div class="slides"></div>
	    <h3 class="title"></h3>
	    <a class="prev">‹</a>
	    <a class="next">›</a>
	    <a class="close">×</a>
	    <a class="play-pause"></a>
	    <ol class="indicator"></ol>
	</div> 
</body>
</html>