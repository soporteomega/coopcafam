<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<script src="/components/jquery/dist/jquery.min.js"></script>
    <script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="/skins/page/js/main.js"></script>
    <link rel="stylesheet" href="/components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/skins/page/css/global.css">
    <title><?= $this->_titlepage ?></title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>
<body class="fondo_login">
	<article class="cajalogin">
    	<img src="/skins/page/images/logo.png" />
        <?= $this->_content; ?>
        </div>
        <div class="footerLogin"></div>
    </article>
</body>
</html>