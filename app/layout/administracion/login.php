<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?= $this->_titlepage ?></title>
<script src="/components/jquery/dist/jquery.min.js"></script>
<script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/components/bootstrap-validator/dist/validator.min.js"></script>
<script src="/components/bootstrap-filestyle/src/bootstrap-filestyle.min.js"></script>
<script src="/components/tinymce/tinymce.min.js"></script>
<script src="/skins/administrador/js/main.js"></script>
<link rel="stylesheet" href="/components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/skins/administrador/css/global.css">
</head>
<body>
	<article  id="cajaindex">
    	<h1> content <strong>management</strong> system</h1>
        <img src="/skins/administrador/images/logo.png" id="logoindex"/>
        <form id="login_form" autocomplete="off">
                    <div class="form-group text-left" >
                        <label class="control-label">Usuario</label>
                        <input type="text" class="form-control" id="user" placeholder="Usuario">
                    </div>
                    <div class="form-group text-left">
                        <label class="control-label">Contraseña</label>
                        <input type="password" class="form-control " id="password" placeholder="Contraseña">
                    </div>
                    <div id="error_login"></div>
                        <input type="hidden" id="csrf" value="<?php echo $_SESSION['csrf']; ?>" />
                    <button  class="btn btn-block btn-success" onclick="return login()">Entrar</button>
        </form>
        <img src="/skins/administrador/images/figuras.png" width="100%;"/>
    </article>
    
    <aside id="informacionindex">
    	WHY CREATIVE SOLUTIONS S.A.S<br>
        Cra 53 No. 104B - 35<br>
        www.why.com.co
    </aside>
</body>
</html>
