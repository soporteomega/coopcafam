<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?= $this->_titlepage ?></title>
<script src="/components/jquery/dist/jquery.min.js"></script>
<script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js"></script>
<script src="/components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="/components/bootstrap-validator/dist/validator.min.js"></script>
<script src="/components/bootstrap-filestyle/src/bootstrap-filestyle.min.js"></script>
<script src="/components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="/components/tinymce/tinymce.min.js"></script>
<script src="/components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="/components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="/components/bootstrap-fileinput/js/fileinput.min.js" charset="UTF-8"></script>
<script src="/components/eonasdan-bootstrap-datetimepicker/build/js/moment.js"></script>
<script src="/components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="/skins/administrador/js/main.js"></script>
<link rel="stylesheet" href="/components/bootstrap/dist/css/bootstrap.min.css">
<link href="/components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet">
<link rel="stylesheet" href="/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="/components/bootstrap-select/dist/css/bootstrap-select.min.css">
<link rel="stylesheet" href="/components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
<link rel="stylesheet/less" type="text/css" href="/components/bootstrap-timepicker/css/timepicker.less" />
<link href="/components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet">
<link href="/components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href="/skins/administrador/css/global.css">
<style>
.border{
    border: 1px solid #000000;
}
</style>
</head>

<body>
<header id="barra_panel">
	<aside id="titulopanel"><?= $this->_titlepage ?></aside>
    <aside id="bienvenida">¡Bienvenido <?php echo $_SESSION['kt_login_user']; ?>!</aside>
    <h1> content <strong>management</strong> system</h1>
</header>
<nav id="bonones_panel">
	<a href="/administracion/panel"><img src="/skins/administrador/images/logo.png" class="logo_panel" /></a>
    <ul>
        <li><a href="/administracion/solicitudes">Administrar Solicitudes</a></li>
    	<li><a href="/administracion/tipogestion">Administrar Tipos de Gestion </a></li>
        <li><a href="/administracion/content">Administrar Contenidos</a></li>
        <li><a href="/administracion/calendario">Administrar Calendario</a></li>
        <li><a href="/administracion/categoriadocumento">Administrar Documentos</a></li>
        <li><a href="/administracion/cursos">Administrar Cursos</a></li>
        <li><a href="/administracion/frases">Frases Cursos</a></li>
        <li><a href="/administracion/album">Administrar Albumes</a></li>
        <li><a href="/administracion/cargo">Administrar Cargos</a></li>
        <li><a href="/administracion/users">Administrar Usuarios</a></li>
        <li><a href="/administracion/registros">Registro de Acceso y Modificaciones</a></li>
        <li><a href="/administracion/consejero">Sección Consejo de Administración</a></li>
    </ul>
</nav>
<article id="contenido_panel">
	<section id="contenido_general">
      <?= $this->_content ?>
    </section>
</article>
<nav id="botones2_panel">
	<ul>
    	<!--<li><img src="/skins/administrador/images/configuracion.png" width="80" height="80" alt=""/></li>
        <li><img src="/skins/administrador/images/manual.png" width="80" height="80" alt=""/></li>-->
        <li><a href="/administracion/user/logout"><img src="/skins/administrador/images/salida.png" width="80" height="80" alt=""/></a></li>
    </ul>
</nav>
</body>
</html>