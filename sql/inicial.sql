-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 24-05-2016 a las 10:37:36
-- Versión del servidor: 5.7.9
-- Versión de PHP: 5.5.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `framework`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `content`
--

CREATE TABLE `content` (
  `content_id` bigint(20) NOT NULL,
  `content_title` varchar(255) DEFAULT NULL,
  `content_subtitle` varchar(255) DEFAULT NULL,
  `content_introduction` text,
  `content_description` text,
  `content_image` varchar(255) DEFAULT NULL,
  `content_banner` varchar(255) DEFAULT NULL,
  `content_section` varchar(255) DEFAULT NULL,
  `orden` bigint(20) DEFAULT NULL,
  `content_delete` int(11) DEFAULT NULL,
  `content_current_user` bigint(20) DEFAULT NULL,
  `content_date` date DEFAULT NULL,
  `content_link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `content`
--
DELIMITER $$
CREATE TRIGGER `tg_insert_content` BEFORE INSERT ON `content` FOR EACH ROW INSERT INTO trigger_content(content_id, content_title_new, content_subtitle_new, content_introduction_new, content_description_new, content_image_new, content_banner_new, content_secion_new, content_order_new, content_delete_new, trigger_content_user, trigger_content_date,trigger_content_action) VALUES (NEW.content_id, NEW.content_title, NEW.content_subtitle, NEW.content_introduction, NEW.content_description, NEW.content_image, NEW.content_banner, NEW.content_section, NEW.orden, NEW.content_delete, NEW.content_current_user, NOW(),'INSERT')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trigger_content`
--

CREATE TABLE `trigger_content` (
  `trigger_content_id` bigint(20) NOT NULL,
  `content_id` bigint(20) DEFAULT NULL,
  `content_title_old` varchar(255) DEFAULT NULL,
  `content_subtitle_old` varchar(255) DEFAULT NULL,
  `content_introduction_old` text,
  `content_description_old` text,
  `content_image_old` varchar(255) DEFAULT NULL,
  `content_banner_old` varchar(255) DEFAULT NULL,
  `content_secion_old` varchar(255) DEFAULT NULL,
  `content_order_old` bit(20) DEFAULT NULL,
  `content_delete_old` int(11) DEFAULT NULL,
  `content_title_new` varchar(255) DEFAULT NULL,
  `content_subtitle_new` varchar(255) DEFAULT NULL,
  `content_introduction_new` text,
  `content_description_new` text,
  `content_image_new` varchar(255) DEFAULT NULL,
  `content_banner_new` varchar(255) DEFAULT NULL,
  `content_secion_new` varchar(255) DEFAULT NULL,
  `content_order_new` bit(20) DEFAULT NULL,
  `content_delete_new` int(11) DEFAULT NULL,
  `trigger_content_user` bigint(20) DEFAULT NULL,
  `trigger_content_date` datetime DEFAULT NULL,
  `trigger_content_action` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trigger_user`
--

CREATE TABLE `trigger_user` (
  `trigger_user_id` bigint(100) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_names_old` varchar(255) DEFAULT NULL,
  `user_lastnames_old` varchar(255) DEFAULT NULL,
  `user_email_old` varchar(255) DEFAULT NULL,
  `user_idnumber_old` varchar(255) DEFAULT NULL,
  `user_city_old` varchar(255) DEFAULT NULL,
  `user_country_old` varchar(255) DEFAULT NULL,
  `user_phone_old` varchar(255) DEFAULT NULL,
  `user_address_old` varchar(255) DEFAULT NULL,
  `user_user_old` varchar(255) DEFAULT NULL,
  `user_password_old` varchar(255) DEFAULT NULL,
  `user_delete_old` int(11) DEFAULT NULL,
  `user_names_new` varchar(255) DEFAULT NULL,
  `user_lastnames_new` varchar(255) DEFAULT NULL,
  `user_email_new` varchar(255) DEFAULT NULL,
  `user_idnumber_new` varchar(255) DEFAULT NULL,
  `user_city_new` varchar(255) DEFAULT NULL,
  `user_country_new` varchar(255) DEFAULT NULL,
  `user_phone_new` varchar(255) DEFAULT NULL,
  `user_address_new` varchar(255) DEFAULT NULL,
  `user_user_new` varchar(255) DEFAULT NULL,
  `user_password_new` varchar(255) DEFAULT NULL,
  `user_delete_new` int(11) DEFAULT NULL,
  `trigger_user_action` varchar(255) DEFAULT NULL,
  `trigger_user_user` bigint(20) DEFAULT NULL,
  `trigger_user_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL,
  `user_names` varchar(255) DEFAULT NULL,
  `user_lastnames` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_idnumber` varchar(255) DEFAULT NULL,
  `user_city` varchar(255) DEFAULT NULL,
  `user_country` varchar(255) DEFAULT NULL,
  `user_phone` varchar(255) DEFAULT NULL,
  `user_address` varchar(255) DEFAULT NULL,
  `user_level` int(11) DEFAULT NULL,
  `user_state` int(11) DEFAULT NULL,
  `user_user` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_delete` int(11) DEFAULT NULL,
  `user_current_user` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`user_id`, `user_names`, `user_lastnames`, `user_email`, `user_idnumber`, `user_city`, `user_country`, `user_phone`, `user_address`, `user_level`, `user_state`, `user_user`, `user_password`, `user_delete`, `user_current_user`) VALUES
(1, 'Administrador', 'Administrador', 'desarrollo4@omegawebsystems.com', 'Administrador', 'Administrador', 'Administrador', 'Administrador', 'Administrador', 1, 1, 'admin', '$2y$10$rwGrOr5lRNZzYfJKPpSMM.rwyTnbDc4uw6SvjisatGnudqBFmB66.', NULL, 1);

--
-- Disparadores `user`
--
DELIMITER $$
CREATE TRIGGER `tg_insert_user` BEFORE INSERT ON `user` FOR EACH ROW INSERT INTO trigger_user(user_id,user_names_new, user_lastnames_new, user_email_new, user_idnumber_new, user_city_new, user_country_new, user_phone_new, user_address_new, user_user_new, user_password_new, user_delete_new, trigger_user_action, trigger_user_user, trigger_user_date)
VALUES (NEW.user_id,NEW.user_names, NEW.user_lastnames, NEW.user_email, NEW.user_idnumber, NEW.user_city, NEW.user_country, NEW.user_phone, NEW.user_address, NEW.user_user, NEW.user_password, NEW.user_delete, 'INSERT',NEW.user_current_user,NOW())
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tg_update_user` BEFORE UPDATE ON `user` FOR EACH ROW INSERT INTO trigger_user(user_names_old, user_lastnames_old, user_email_old, user_idnumber_old, user_city_old, user_country_old, user_phone_old, user_address_old, user_user_old, user_password_old, user_delete_old, user_names_new, user_lastnames_new, user_email_new, user_idnumber_new, user_city_new, user_country_new, user_phone_new, user_address_new, user_user_new, user_password_new, user_delete_new, trigger_user_action, trigger_user_user, trigger_user_date) VALUES 
(OLD.user_names, OLD.user_lastnames, OLD.user_email, OLD.user_idnumber, OLD.user_city, OLD.user_country, OLD.user_phone, OLD.user_address, OLD.user_user, OLD.user_password, OLD.user_delete,NEW.user_names, NEW.user_lastnames, NEW.user_email, NEW.user_idnumber, NEW.user_city, NEW.user_country, NEW.user_phone, NEW.user_address, NEW.user_user, NEW.user_password, NEW.user_delete, 'UPDATE',NEW.user_current_user,NOW())
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`content_id`);

--
-- Indices de la tabla `trigger_content`
--
ALTER TABLE `trigger_content`
  ADD PRIMARY KEY (`trigger_content_id`);

--
-- Indices de la tabla `trigger_user`
--
ALTER TABLE `trigger_user`
  ADD PRIMARY KEY (`trigger_user_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `content`
--
ALTER TABLE `content`
  MODIFY `content_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `trigger_content`
--
ALTER TABLE `trigger_content`
  MODIFY `trigger_content_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `trigger_user`
--
ALTER TABLE `trigger_user`
  MODIFY `trigger_user_id` bigint(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;