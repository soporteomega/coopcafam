-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-10-2016 a las 21:43:46
-- Versión del servidor: 5.7.11
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `coopcafam`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adicional`
--

CREATE TABLE `adicional` (
  `adicional_id` int(11) NOT NULL,
  `adicional_titulo` varchar(255) DEFAULT NULL,
  `adicional_descripcion` text,
  `adicional_imagen` varchar(255) DEFAULT NULL,
  `adicional_tipo` varchar(255) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adicional`
--

INSERT INTO `adicional` (`adicional_id`, `adicional_titulo`, `adicional_descripcion`, `adicional_imagen`, `adicional_tipo`, `content_id`, `orden`) VALUES
(1, 'Solidaridad', '<p>Orientaremos nuestras acciones en un sentido moral y racional, teniendo como norte los objetivos que nos incluyen en este grupo humano llamado Coopcafam.</p>\r\n<p>Este valor se ve reflejado en los siguientes principios:</p>\r\n<ul>\r\n<li>Asertividad</li>\r\n<li>Hacer propia la causa ajena</li>\r\n<li>Responsabilidad social</li>\r\n<li>Sensibilidad</li>\r\n<li>&Eacute;tica</li>\r\n</ul>', '', 'Acordeon', 5, 1),
(2, '&Eacute;tica', '<p>Orientaremos nuestras acciones en un sentido moral y racional, teniendo como norte los objetivos que nos incluyen en este grupo humano llamado Coopcafam.</p>\r\n<p>Este valor se ve reflejado en los siguientes principios:</p>\r\n<ul>\r\n<li>Honestidad</li>\r\n<li>Integralidad</li>\r\n<li>Transparencia</li>\r\n<li>Lealtad</li>\r\n</ul>', '', 'Acordeon', 5, 2),
(3, 'Trabajo en Equipo', '<p>Es la actitud cotidiana con la que involucramos a quienes tienen relaci&oacute;n directa con los procesos de trabajo, generando un ambiente de confianza y compa&ntilde;erismo, que nos permita lograr los mejores resultados.</p>\r\n<p>Este valor se ve reflejado en los siguientes principios:</p>\r\n<ul>\r\n<li>Comunicaci&oacute;n</li>\r\n<li>Responsabilidad</li>\r\n<li>Liderazgo</li>\r\n</ul>', '', 'Acordeon', 5, 3),
(4, 'Actitud de Servicio', '<p>Es la calidad y calidez con la que servimos, la disposici&oacute;n para satisfacer las necesidades y expectativas del asociado, orientando nuestro actuar hacia el logro de los mejores resultados.</p>\r\n<p>Este valor se ve reflejado en los siguientes principios:</p>\r\n<ul>\r\n<li>Compromiso</li>\r\n<li>Ecuanimidad</li>\r\n<li>Respeto por las personas</li>\r\n<li>Empat&iacute;a</li>\r\n</ul>', '', 'Acordeon', 5, 4),
(5, 'Primer Principio: Membres&iacute;a abierta y voluntaria', '<p><span>Las cooperativas son organizaciones voluntarias abiertas para todas aquellas personas dispuestas a utilizar sus servicios y dispuestas a aceptar las responsabilidades que conlleva la membres&iacute;a sin discriminaci&oacute;n de g&eacute;nero, raza, clase social, posici&oacute;n pol&iacute;tica o religiosa.</span></p>', '', 'Acordeon', 6, 5),
(6, 'Segundo Principio: Control democr&aacute;tico de los miembros', '<p>Las cooperativas son organizaciones democr&aacute;ticas controladas por sus miembros quienes participan activamente en la definici&oacute;n de las pol&iacute;ticas y en la toma de decisiones.</p>\r\n<p>Los hombres y mujeres elegidos para representar a su cooperativa, responden ante los miembros.</p>\r\n<p>En las cooperativas de base los miembros tienen igual derecho de voto (un miembro, un voto), mientras en las cooperativas de otros niveles tambi&eacute;n se organizan con procedimientos democr&aacute;ticos.</p>', '', 'Acordeon', 6, 6),
(7, 'Tercer Principio: Participaci&oacute;n econ&oacute;mica de los miembros', '<p>Los miembros contribuyen de manera equitativa y controlan de manera democr&aacute;tica el capital de la cooperativa. Por lo menos una parte de ese capital es propiedad com&uacute;n de la cooperativa.</p>\r\n<p>Usualmente reciben una compensaci&oacute;n limitada, si es que la hay, sobre el capital suscrito como condici&oacute;n de membres&iacute;a.</p>\r\n<p>Los miembros asignan excedentes para cualquiera de los siguientes prop&oacute;sitos: El desarrollo de la cooperativa mediante la posible creaci&oacute;n de reservas, de la cual al menos una parte debe ser indivisible; los beneficios para los miembros en proporci&oacute;n con sus transacciones con la cooperativa; y el apoyo a otras actividades seg&uacute;n lo apruebe la membres&iacute;a.</p>', '', 'Acordeon', 6, 7),
(8, 'Cuarto Principio: Autonom&iacute;a e independencia', '<p>Las cooperativas son organizaciones aut&oacute;nomas de ayuda mutua, controladas por sus miembros.</p>\r\n<p>Si entran en acuerdos con otras organizaciones (incluyendo gobiernos) o tienen capital de fuentes externas, lo realizan en t&eacute;rminos que aseguren el control democr&aacute;tico por parte de sus miembros y mantengan la autonom&iacute;a de la cooperativa.</p>', '', 'Acordeon', 6, 8),
(9, 'Quinto Principio: Educaci&oacute;n, formaci&oacute;n e informaci&oacute;n', '<p>Las cooperativas brindan educaci&oacute;n y entrenamiento a sus miembros, a sus dirigentes electos, gerentes y empleados, de tal forma que contribuyan eficazmente al desarrollo de sus cooperativas.</p>\r\n<p>Las cooperativas informan al p&uacute;blico en general, particularmente a j&oacute;venes y creadores de opini&oacute;n, acerca de la naturaleza y beneficios del cooperativismo.</p>', '', 'Acordeon', 6, 9),
(10, 'Sexto Principio: Cooperaci&oacute;n entre cooperativas', '<p><span>Las cooperativas sirven a sus miembros m&aacute;s eficazmente y fortalecen el movimiento cooperativo trabajando de manera conjunta por medio de estructuras locales, nacionales, regionales e internacionales.</span></p>', '', 'Acordeon', 6, 10),
(11, 'S&eacute;ptimo Principio: Compromiso con la comunidad', '<p><span>La cooperativa trabaja para el desarrollo sostenible de su comunidad por medio de pol&iacute;ticas aceptadas por sus miembros</span></p>', '', 'Acordeon', 6, 11),
(12, 'Tomado de', '<div class="descripcion">\r\n<p>Tomado de: <a href="http://www.aciamericas.coop/Principios-y-Valores-Cooperativos">http://www.aciamericas.coop/Principios-y-Valores-Cooperativos</a></p>\r\n</div>', '', 'Texto', 6, 12),
(19, 'Becas', '', 'Becas1.JPG', 'Carrousel', 12, 19),
(20, 'Bellas Artes', '', 'Bellas Artes1.JPG', 'Carrousel', 12, 20),
(21, 'C.E.T', '', 'C.E.T.1.JPG', 'Carrousel', 12, 21),
(22, 'Cafam Floresta', '', 'Cafam Floresta1.JPG', 'Carrousel', 12, 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `album`
--

CREATE TABLE `album` (
  `album_id` int(11) NOT NULL,
  `album_titulo` varchar(255) DEFAULT NULL,
  `album_descripcion` text NOT NULL,
  `album_imagen` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `album_fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `album`
--

INSERT INTO `album` (`album_id`, `album_titulo`, `album_descripcion`, `album_imagen`, `orden`, `album_fecha`) VALUES
(1, 'Prueba', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus faucibus neque nec iaculis.&nbsp;</p>', 'images/unnamed1.jpg', 2, '2016-09-01'),
(2, 'Prueba 2', '<p>wertyuiop`23456`+</p>\r\n<p>&nbsp;</p>', 'images/cambodia-603394_1280.jpg', 3, '2016-09-23'),
(3, 'Prueba 3', '<p>23456uhgf sdfg</p>', 'images/cambodia-603404_1920.jpg', 1, '2016-09-23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_documento`
--

CREATE TABLE `categoria_documento` (
  `categoria_documento_id` int(11) NOT NULL,
  `categoria_documento_nombre` varchar(255) DEFAULT NULL,
  `categoria_documento_descripcion` text,
  `categoria_documento_imagen` varchar(255) DEFAULT NULL,
  `categoria_documento_tipo` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria_documento`
--

INSERT INTO `categoria_documento` (`categoria_documento_id`, `categoria_documento_nombre`, `categoria_documento_descripcion`, `categoria_documento_imagen`, `categoria_documento_tipo`, `orden`) VALUES
(1, 'Manual de la calidad', '', '', 'SGC General', 1),
(2, 'Estructura organizacional', '', '', 'SGC General', 2),
(3, 'Listado Maestro de Documentos', '', '', 'SGC General', 3),
(4, 'Tablas de Retenci&oacute;n Documental (TRD)', '', '', 'SGC General', 4),
(5, 'Otros manuales', '', '', 'SGC General', 5),
(6, 'SAM', '', '', 'SGC General', 6),
(7, 'Indicadores de procesos', '', '', 'SGC General', 7),
(8, 'Informes', '', '', 'SGC General', 8),
(9, 'Otros Documentos de Inter&eacute;s', '', '', 'SGC General', 9),
(10, 'DIRECCIONAMIENTO ESTRAT&Eacute;GICO', '', '', 'Mapa - Procesos Gerenciales', 10),
(11, 'SISTEMA DE INFORMACI&Oacute;N GERENCIAL', '', '', 'Mapa - Procesos Gerenciales', 11),
(12, 'BASE SOCIAL', '', '', 'Mapa - Procesos Clave', 12),
(13, 'CR&Eacute;DITO', '', '', 'Mapa - Procesos Clave', 13),
(14, 'BENEFICIOS COMPLEMENTARIOS', '', '', 'Mapa - Procesos Clave', 14),
(15, 'GESTI&Oacute;N DEMOCR&Aacute;TICA', '', '', 'Mapa - Procesos Clave', 15),
(16, 'DEP&Oacute;SITOS Y APORTES DE ASOCIADOS', '', '', 'Mapa - Procesos Clave', 16),
(17, 'SERVICIO AL CLIENTE', '', '', 'Mapa - Procesos de Apoyo', 17),
(18, 'SISTEMA DE GESTI&Oacute;N DE LA CALIDAD', '', '', 'Mapa - Procesos de Apoyo', 18),
(19, 'INFORM&Aacute;TICA COMPLEMENTARIOS', '', '', 'Mapa - Procesos de Apoyo', 19),
(20, 'RECURSOS HUMANOS', '', '', 'Mapa - Procesos de Apoyo', 20),
(21, 'TESORERIA Y CAJA', '', '', 'Mapa - Procesos de Apoyo', 21),
(22, 'PREVENCI&Oacute;N LA/FT', '', '', 'Mapa - Procesos de Apoyo', 22),
(23, 'PLANEACI&Oacute;N', '', '', 'Mapa - Procesos de Apoyo', 23),
(24, 'ARCHIVO Y CONTROL DOCUMENTAL', '', '', 'Mapa - Procesos de Apoyo', 24),
(25, 'CARTERA', '', '', 'Mapa - Procesos de Apoyo', 25),
(26, 'PRESUPUESTO', '', '', 'Mapa - Procesos de Apoyo', 26),
(27, 'CONTROL INTERNO', '', '', 'Mapa - Procesos de Apoyo', 27),
(28, 'COMPRAS DE BIENES Y SERVICIOS', '', '', 'Mapa - Procesos de Apoyo', 28),
(29, 'CONTABILIDAD', '', '', 'Mapa - Procesos de Apoyo', 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `content`
--

CREATE TABLE `content` (
  `content_id` bigint(20) NOT NULL,
  `content_title` varchar(255) DEFAULT NULL,
  `content_subtitle` varchar(255) DEFAULT NULL,
  `content_introduction` text,
  `content_description` text,
  `content_image` varchar(255) DEFAULT NULL,
  `content_banner` varchar(255) DEFAULT NULL,
  `content_section` varchar(255) DEFAULT NULL,
  `content_disenio` varchar(255) DEFAULT NULL,
  `orden` bigint(20) DEFAULT NULL,
  `content_delete` int(11) DEFAULT NULL,
  `content_current_user` bigint(20) DEFAULT NULL,
  `content_date` date DEFAULT NULL,
  `content_link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `content`
--

INSERT INTO `content` (`content_id`, `content_title`, `content_subtitle`, `content_introduction`, `content_description`, `content_image`, `content_banner`, `content_section`, `content_disenio`, `orden`, `content_delete`, `content_current_user`, `content_date`, `content_link`) VALUES
(1, 'Banner', '', '', '', '', 'banner.jpg', 'Banner Home', NULL, 1, NULL, NULL, '2016-08-09', ''),
(2, 'MISI&Oacute;N', '', '', '<p><strong>Nuestra raz&oacute;n de ser, para qu&eacute; existimos...</strong></p>\r\n<p>COOPCAFAM es una Cooperativa, que basada en la filosof&iacute;a solidaria y con un modelo administrativo y financiero s&oacute;lido, estimula el ahorro, brinda alternativas de cr&eacute;dito y otros servicios complementarios, para contribuir al bienestar y la calidad de vida del asociado y su grupo familiar.</p>', 'mision.jpeg', 'imagen.jpg', 'Nuestra Cooperativa', 'Dos Columnas Gris', 3, NULL, NULL, '2016-08-26', ''),
(3, 'VISI&Oacute;N', '', '', '<p><strong>Hacia d&oacute;nde vamos, c&oacute;mo esperamos vernos:</strong></p>\r\n<p>Ser reconocida como la primera opci&oacute;n de los asociados por la variedad y calidad de los servicios.</p>', 'vision.jpg', '', 'Nuestra Cooperativa', 'Dos Columnas Gris', 4, NULL, NULL, '2016-08-26', ''),
(4, 'POL&Iacute;TICA DE CALIDAD', '', '', '<p><strong>Nuestro Compromiso...</strong></p>\r\n<p>En COOPCAFAM buscamos la satisfacci&oacute;n de nuestros asociados, con el mejoramiento permanente de los procesos y un talento humano competente y comprometido.</p>', 'politicas.jpg', '', 'Nuestra Cooperativa', 'Banner', 5, NULL, NULL, '2016-08-26', ''),
(5, 'VALORES', '', '', '<p>Inspiran y fundamentan nuestro trabajo:</p>\r\n<ul>\r\n<li>Solidaridad</li>\r\n<li>&Eacute;tica</li>\r\n<li>Trabajo en Equipo</li>\r\n<li>Actitud de Servicio</li>\r\n</ul>', 'valores.png', '', 'Nuestra Cooperativa', 'Dos Columnas', 6, NULL, NULL, '2016-08-26', ''),
(6, 'PRINCIPIOS COOPERATIVOS', '', '', '', 'principios.jpg', '', 'Nuestra Cooperativa', 'Dos Columnas', 14, NULL, NULL, '2016-08-26', ''),
(7, 'Auxilios', '', '', '<p>Salud visual: Apoyo de salud visual para el personal que tenga m&aacute;s de un a&ntilde;o de servicio: el subsidio ser&aacute; equivalente a 9 SMLDV (salario m&iacute;nimo legal diario vigente). Este auxilio se otorgar&aacute; para cirug&iacute;a refractiva, lentes de montura y lentes de contacto formulados, no se otorgar&aacute; para casos de est&eacute;tica. En todos los casos se deber&aacute; acreditar la formula y el pago. Solo se podr&aacute; acceder a este beneficio una vez al a&ntilde;o.</p>\r\n<p>Nacimiento de tus hijos: Cada empleado tendr&aacute; derecho de un auxilio por valor de 12 SMLDV, por el nacimiento de cada hijo. Deben presentar acta de registro civil de nacimiento. Debe haber cumplido el periodo de prueba.</p>\r\n<p>Compra de vivienda: Cada empleado tendr&aacute; derecho por la compra de su vivienda a un bono o regalo por valor de un SMMLV. Este auxilio se entregar&aacute; por una &uacute;nica vez y se deber&aacute; presentar copia de las escrituras p&uacute;blicas del inmueble.</p>', 'unnamed.jpg', '', 'Beneficios', 'Dos Columnas Gris', 7, NULL, NULL, '2016-09-01', ''),
(8, 'Bonificaciones extralegales', '', '', '<p>Prima de vacaciones: 10 d&iacute;as de salario b&aacute;sico por cada a&ntilde;o de vacaciones causadas. Solo se otorgar&aacute; al momento de salir a disfrutarlas.</p>\r\n<p>Prima semestral: 5 d&iacute;as de salario b&aacute;sico el 30 junio de cada a&ntilde;o y 10 d&iacute;as de salario b&aacute;sico el 30 de noviembre de cada a&ntilde;o.</p>\r\n<p>Prima quinquenal: 20 d&iacute;as de salario b&aacute;sico por cada cinco a&ntilde;os de trabajo continuo cumplido.</p>', '', '', 'Beneficios', 'Dos Columnas Gris', 8, NULL, NULL, '2016-09-01', ''),
(9, 'Tu formaci&oacute;n', '', '', '<table style="margin-left: auto; margin-right: auto;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr>\r\n<td rowspan="2">\r\n<p style="text-align: center;">TIPO DE FORMACI&Oacute;N</p>\r\n</td>\r\n<td colspan="4">\r\n<p style="text-align: center;">REQUISITOS</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">\r\n<p>TIEMPO DE VINCULACION</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>No. VECES AL A&Ntilde;O</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>MONTO DEL SUBSIDIO</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>OBSERVACIONES</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">\r\n<p>ESPECIALIZACI&Oacute;N</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>3 a&ntilde;os continuos con contrato a t&eacute;rmino indefinido</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>2</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>3 SMMLV</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>Recibir&aacute; este subsidio por una sola especializaci&oacute;n.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">\r\n<p>PREGRADO</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>2 a&ntilde;os continuos con contrato a t&eacute;rmino indefinido</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>2</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>2 SMMLV</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>Recibir&aacute; este subsidio por una sola carrera profesional.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">\r\n<p>TECNOLOGIA</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>1 a&ntilde;os continuos con contrato a t&eacute;rmino indefinido</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>2</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>1 SMMLV</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>Recibir&aacute; este subsidio por una sola carrera tecnol&oacute;gica.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">\r\n<p>TECNICA</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>1 a&ntilde;os continuos con contrato a t&eacute;rmino indefinido</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>2</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>1 SMMLV</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>Recibir&aacute; este subsidio por una sola carrera t&eacute;cnica.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>Los subsidios se entregar&aacute;n de manera semestral y el valor ser&aacute; reembolsado al empleado despu&eacute;s de haber presentado el certificado de notas donde acredite un promedio igual o superior a 4.0 sobre 5.0 o su equivalente.</p>', '', '', 'Beneficios', 'Banner', 9, NULL, NULL, '2016-09-01', ''),
(10, 'La formaci&oacute;n de tus hijos', '', '', '<p>Coopcafam apoya la educaci&oacute;n de los hijos de los colaboradores mediante el reconocimiento de los siguientes auxilios por matr&iacute;cula, en entidades legalmente reconocidas:</p>\r\n<p>&nbsp;</p>\r\n<table style="margin-left: auto; margin-right: auto;">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p style="text-align: center;">TIPO</p>\r\n</td>\r\n<td>\r\n<p style="text-align: center;">VALOR SUBSIDIO SEMESTRAL</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style="text-align: center;">Guarder&iacute;a (ni&ntilde;os de 4 a 36 meses)</p>\r\n</td>\r\n<td>\r\n<p style="text-align: center;">5 SMLDV</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p style="text-align: center;">Educaci&oacute;n preescolar</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>8 SMLDV</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">\r\n<p>Educaci&oacute;n especial</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>20 SMLDV</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">\r\n<p>Educaci&oacute;n primaria</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>8 SMLDV</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">\r\n<p>Educaci&oacute;n secundaria</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>10 SMLDV</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center;">\r\n<p>Educaci&oacute;n superior</p>\r\n</td>\r\n<td style="text-align: center;">\r\n<p>20 SMLDV</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>El empleado deber&aacute; presentar solicitud escrita de reintegro dentro del mes siguiente a su cancelaci&oacute;n, de los hijos registrados en la Cooperativa, anexando copia del recibo de pago, donde conste el valor por matr&iacute;cula.</p>', '', '', 'Beneficios', 'Dos Columnas', 10, NULL, NULL, '2016-09-01', ''),
(11, 'Est&iacute;mulos', '', '', '<p>Beneficio por matrimonio: el empleado tendr&aacute; derecho a disfrutar de ocho d&iacute;as calendario con motivo de su matrimonio. El disfrute de este permiso iniciar&aacute; el d&iacute;a de la ceremonia matrimonial.</p>\r\n<p>Auxilio funerario: Los empleados al suscribir contrato a t&eacute;rmino indefinido, ser&aacute;n amparados por un auxilio exequial b&aacute;sico para su grupo familiar (empleado, su c&oacute;nyuge o compa&ntilde;ero permanente y sus hijos menos de 25 a&ntilde;os de edad). Coopcafam asumir&aacute; el 100% del valor de la prima o contribuci&oacute;n.</p>', 'estimulos.jpg', '', 'Beneficios', 'Dos Columnas', 11, NULL, NULL, '2016-09-01', ''),
(12, 'Beneficios Caja de Compensaci&oacute;n CAFAM', '', '', '', '', '', 'Beneficios', 'Default Gris', 12, NULL, NULL, '2016-09-01', ''),
(13, 'inroduccion Home', '', '', '<p style="text-align: center;"><strong>&iexcl;Somos una gran familia y t&uacute; haces parte de ella! </strong></p>\r\n<p style="text-align: center;">Gracias por hacer parte de Coopcafam, por dedicar tu tiempo y esfuerzo para que cada uno de nuestros proyectos salgan adelante.</p>', '', '', 'Introduccion Home', NULL, 13, NULL, NULL, '2016-09-14', ''),
(14, 'intro cooperativa', '', '', '<p style="text-align: center;">Aqu&iacute; conocer&aacute;s m&aacute;s acerca de nosotros, nuestro norte y hacia d&oacute;nde queremos ir, basados en nuestra filosof&iacute;a solidaria y cooperativista.</p>', 'foto_cooperativa.jpg', '', 'Introduccion Cooperativa', '', 2, NULL, NULL, '2016-09-14', ''),
(16, 'SEGUIMIENTO A CREACION Y/O ACTUALIZACI&Oacute;N DE FORMATOS O DOCUMENTOS', '', '', '<table style="width: 100%;">\r\n<tbody>\r\n<tr style="height: 31px;">\r\n<td style="width: 8.29208%; text-align: center; height: 31px;">No</td>\r\n<td style="width: 89.9752%; text-align: center; height: 31px;"><span>Nombre Documento o Formato</span></td>\r\n<td style="width: 0.49505%; text-align: center; height: 31px;"><span>&Aacute;rea Responsable</span></td>\r\n<td style="width: 10%; text-align: center; height: 31px;"><span>Estado</span></td>\r\n<td style="width: 0.618812%; text-align: center; height: 31px;"><span>Documento </span></td>\r\n</tr>\r\n<tr style="height: 1px;">\r\n<td style="width: 8.29208%; height: 1px;"></td>\r\n<td style="width: 89.9752%; height: 1px;"></td>\r\n<td style="width: 0.49505%; height: 1px;"></td>\r\n<td style="width: 10%; height: 1px;"></td>\r\n<td style="width: 0.618812%; height: 1px;"></td>\r\n</tr>\r\n<tr style="height: 1px;">\r\n<td style="width: 8.29208%; height: 1px;"></td>\r\n<td style="width: 89.9752%; height: 1px;"></td>\r\n<td style="width: 0.49505%; height: 1px;"></td>\r\n<td style="width: 10%; height: 1px;"></td>\r\n<td style="width: 0.618812%; height: 1px;"></td>\r\n</tr>\r\n<tr style="height: 1px;">\r\n<td style="width: 8.29208%; height: 1px;"></td>\r\n<td style="width: 89.9752%; height: 1px;"></td>\r\n<td style="width: 0.49505%; height: 1px;"></td>\r\n<td style="width: 10%; height: 1px;"></td>\r\n<td style="width: 0.618812%; height: 1px;"></td>\r\n</tr>\r\n<tr style="height: 1px;">\r\n<td style="width: 8.29208%; height: 1px;"></td>\r\n<td style="width: 89.9752%; height: 1px;"></td>\r\n<td style="width: 0.49505%; height: 1px;"></td>\r\n<td style="width: 10%; height: 1px;"></td>\r\n<td style="width: 0.618812%; height: 1px;"></td>\r\n</tr>\r\n<tr style="height: 1px;">\r\n<td style="width: 8.29208%; height: 1px;"></td>\r\n<td style="width: 89.9752%; height: 1px;"></td>\r\n<td style="width: 0.49505%; height: 1px;"></td>\r\n<td style="width: 10%; height: 1px;"></td>\r\n<td style="width: 0.618812%; height: 1px;"></td>\r\n</tr>\r\n</tbody>\r\n</table>', '', '', 'Control de Documentos', NULL, 16, NULL, NULL, '2016-09-15', ''),
(17, 'Campa&ntilde;as', '', '', '<p><strong>Lorem Ipsum</strong> es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us&oacute; una galer&iacute;a de textos y los mezcl&oacute; de tal manera que logr&oacute; hacer un libro de textos especimen. No s&oacute;lo sobrevivi&oacute; 500 a&ntilde;os, sino que tambien ingres&oacute; como texto de relleno en documentos electr&oacute;nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci&oacute;n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m&aacute;s recientemente con software de autoedici&oacute;n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>', '', '', 'Campanias', 'Default Gris', 17, NULL, NULL, '2016-09-23', ''),
(18, 'Comunicados', '', '', '<p><strong>Lorem Ipsum</strong> es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us&oacute; una galer&iacute;a de textos y los mezcl&oacute; de tal manera que logr&oacute; hacer un libro de textos especimen. No s&oacute;lo sobrevivi&oacute; 500 a&ntilde;os, sino que tambien ingres&oacute; como texto de relleno en documentos electr&oacute;nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci&oacute;n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m&aacute;s recientemente con software de autoedici&oacute;n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>', '', '', 'Comunicados', 'Default Gris', 18, NULL, NULL, '2016-09-23', ''),
(19, 'Mapa de procesos', '', '', '<p><strong>Lorem Ipsum</strong><span> es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us&oacute; una galer&iacute;a de textos y los mezcl&oacute; de tal manera que logr&oacute; hacer un libro de textos especimen. No s&oacute;lo sobrevivi&oacute; 500 a&ntilde;os, sino que tambien ingres&oacute; como texto de relleno en documentos electr&oacute;nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci&oacute;n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m&aacute;s recientemente con software de autoedici&oacute;n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</span></p>', '', '', 'Mapas de Proceso', NULL, 19, NULL, NULL, '2016-09-23', ''),
(20, 'Control', '', '', '<p><strong>Lorem Ipsum</strong><span> es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us&oacute; una galer&iacute;a de textos y los mezcl&oacute; de tal manera que logr&oacute; hacer un libro de textos especimen. No s&oacute;lo sobrevivi&oacute; 500 a&ntilde;os, sino que tambien ingres&oacute; como texto de relleno en documentos electr&oacute;nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci&oacute;n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m&aacute;s recientemente con software de autoedici&oacute;n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</span></p>', '', '', 'Control SAM', NULL, 20, NULL, NULL, '2016-09-23', ''),
(21, 'Documento 1', '', '', '<p><strong>Lorem Ipsum</strong><span> es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us&oacute; una galer&iacute;a de textos y los mezcl&oacute; de tal manera que logr&oacute; hacer un libro de textos especimen. No s&oacute;lo sobrevivi&oacute; 500 a&ntilde;os, sino que tambien ingres&oacute; como texto de relleno en documentos electr&oacute;nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci&oacute;n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m&aacute;s recientemente con software de autoedici&oacute;n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</span></p>', '', '', 'Control de Documentos', NULL, 21, NULL, NULL, '2016-09-23', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `cursos_id` int(11) NOT NULL,
  `curso_titulo` varchar(255) DEFAULT NULL,
  `curso_descripcion` text,
  `cursos_imagen` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`cursos_id`, `curso_titulo`, `curso_descripcion`, `cursos_imagen`, `orden`) VALUES
(1, 'Inducci&oacute;n', '<p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us&oacute; una galer&iacute;a de textos y los mezcl&oacute; de tal manera que logr&oacute; hacer un libro de textos especimen. No s&oacute;lo sobrevivi&oacute; 500 a&ntilde;os, sino que tambien ingres&oacute; como texto de relleno en documentos electr&oacute;nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci&oacute;n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m&aacute;s recientemente con software de autoedici&oacute;n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>', 'images/cursos-570x3801.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento`
--

CREATE TABLE `documento` (
  `documento_id` int(11) NOT NULL,
  `documento_nombre` varchar(255) DEFAULT NULL,
  `documento_documento` varchar(255) DEFAULT NULL,
  `documento_categoria` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foto`
--

CREATE TABLE `foto` (
  `foto_id` int(11) NOT NULL,
  `foto_titulo` varchar(255) DEFAULT NULL,
  `foto_descripcion` text,
  `foto_imagen` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `foto`
--

INSERT INTO `foto` (`foto_id`, `foto_titulo`, `foto_descripcion`, `foto_imagen`, `orden`, `album_id`) VALUES
(1, 'Fotos', 'dsadsadasd', 'images/estimulos1.jpg', 1, 1),
(2, 'Fotos', 'dsadsadasd', 'images/unnamed2.jpg', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulo`
--

CREATE TABLE `modulo` (
  `modulo_id` int(11) NOT NULL,
  `modulo_titulo` varchar(255) DEFAULT NULL,
  `modulo_descripcion` text,
  `cursos_id` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modulo`
--

INSERT INTO `modulo` (`modulo_id`, `modulo_titulo`, `modulo_descripcion`, `cursos_id`, `orden`) VALUES
(1, 'Modulo 1', '<p><strong>Lorem Ipsum</strong> es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us&oacute; una galer&iacute;a de textos y los mezcl&oacute; de tal manera que logr&oacute; hacer un libro de textos especimen. No s&oacute;lo sobrevivi&oacute; 500 a&ntilde;os, sino que tambien ingres&oacute; como texto de relleno en documentos electr&oacute;nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci&oacute;n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m&aacute;s recientemente con software de autoedici&oacute;n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>', 1, 1),
(2, 'Modulo 2', '<p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us&oacute; una galer&iacute;a de textos y los mezcl&oacute; de tal manera que logr&oacute; hacer un libro de textos especimen. No s&oacute;lo sobrevivi&oacute; 500 a&ntilde;os, sino que tambien ingres&oacute; como texto de relleno en documentos electr&oacute;nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci&oacute;n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m&aacute;s recientemente con software de autoedici&oacute;n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE `preguntas` (
  `pregunta_id` int(11) NOT NULL,
  `pregunta_pregunta` varchar(255) DEFAULT NULL,
  `pregunta_respuesta1` varchar(255) DEFAULT NULL,
  `pregunta_respuesta2` varchar(255) DEFAULT NULL,
  `pregunta_respuesta3` varchar(255) DEFAULT NULL,
  `pregunta_respuesta4` varchar(255) DEFAULT NULL,
  `pregunta_respuesta5` varchar(255) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `modulo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`pregunta_id`, `pregunta_pregunta`, `pregunta_respuesta1`, `pregunta_respuesta2`, `pregunta_respuesta3`, `pregunta_respuesta4`, `pregunta_respuesta5`, `orden`, `modulo_id`) VALUES
(1, '&iquest;Pregunta 1?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 1, 2),
(2, '&iquest; Pregunta 2?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 2, 2),
(3, '&iquest; Pregunta 3?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 3, 2),
(4, '&iquest; Pregunta 4?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 4, 2),
(5, '&iquest; Pregunta 5?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 5, 2),
(6, '&iquest; Pregunta 6?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 6, 2),
(7, '&iquest; Pregunta 7?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 7, 2),
(8, '&iquest; Pregunta 8?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 8, 2),
(9, '&iquest; Pregunta 9?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 9, 2),
(10, '&iquest; Pregunta 10?', 'Si', 'No', '', '', '', 10, 2),
(11, '&iquest; Pregunta 11?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', '', 11, 2),
(12, '&iquest;pregunta 12?', 'Si', 'No', '', '', '', 12, 2),
(13, '&iquest;pregunta 1?', 'Si', 'No', '', '', '', 13, 1),
(14, '&iquest; Pregunta 2?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 14, 1),
(15, '&iquest; Pregunta 3?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', '', 15, 1),
(16, '&iquest; Pregunta 4?', 'Respuesta bien', 'Respuesta 1', 'Respuesta 2', 'Respuesta 3', 'Respuesta 4', 16, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuesta`
--

CREATE TABLE `respuesta` (
  `respuesta_id` int(11) NOT NULL,
  `pregunta_id` int(11) DEFAULT NULL,
  `respuesta_respuesta` varchar(255) DEFAULT NULL,
  `respuesta_correcto` varchar(255) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_modulo`
--

CREATE TABLE `seccion_modulo` (
  `seccion_modulo_id` int(11) NOT NULL,
  `seccion_modulo_nombre` varchar(255) DEFAULT NULL,
  `seccion_modulo_descripcion` text,
  `orden` int(11) DEFAULT NULL,
  `modulo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `seccion_modulo`
--

INSERT INTO `seccion_modulo` (`seccion_modulo_id`, `seccion_modulo_nombre`, `seccion_modulo_descripcion`, `orden`, `modulo_id`) VALUES
(1, 'Tema 1 - TITULO DEL TEMA', '<p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us&oacute; una galer&iacute;a de textos y los mezcl&oacute; de tal manera que logr&oacute; hacer un libro de textos especimen. No s&oacute;lo sobrevivi&oacute; 500 a&ntilde;os, sino que tambien ingres&oacute; como texto de relleno en documentos electr&oacute;nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci&oacute;n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m&aacute;s recientemente con software de autoedici&oacute;n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>', 1, 1),
(2, 'Tema 2 - TITULO DEL TEMA', '<p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est&aacute;ndar de las industrias desde el a&ntilde;o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido us&oacute; una galer&iacute;a de textos y los mezcl&oacute; de tal manera que logr&oacute; hacer un libro de textos especimen. No s&oacute;lo sobrevivi&oacute; 500 a&ntilde;os, sino que tambien ingres&oacute; como texto de relleno en documentos electr&oacute;nicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creaci&oacute;n de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y m&aacute;s recientemente con software de autoedici&oacute;n, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>', 2, 1),
(3, 'Seccion 1 modulo 2', '<p>sdfdsfdsfds fdsfds fdsfds &nbsp;fds fdsf dsf dsrf ds fdsfds fds fdsf dsfds dsfds</p>', 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL,
  `user_names` varchar(255) DEFAULT NULL,
  `user_lastnames` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_idnumber` varchar(255) DEFAULT NULL,
  `user_city` varchar(255) DEFAULT NULL,
  `user_country` varchar(255) DEFAULT NULL,
  `user_phone` varchar(255) DEFAULT NULL,
  `user_address` varchar(255) DEFAULT NULL,
  `user_level` int(11) DEFAULT NULL,
  `user_state` int(11) DEFAULT NULL,
  `user_user` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_delete` int(11) DEFAULT NULL,
  `user_current_user` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`user_id`, `user_names`, `user_lastnames`, `user_email`, `user_idnumber`, `user_city`, `user_country`, `user_phone`, `user_address`, `user_level`, `user_state`, `user_user`, `user_password`, `user_delete`, `user_current_user`) VALUES
(1, 'Administrador', 'Administrador', 'desarrollo4@omegawebsystems.com', 'Administrador', 'Administrador', 'Administrador', 'Administrador', 'Administrador', 1, 1, 'admin', '$2y$10$rwGrOr5lRNZzYfJKPpSMM.rwyTnbDc4uw6SvjisatGnudqBFmB66.', NULL, 1);

--
-- Disparadores `user`
--
DELIMITER $$
CREATE TRIGGER `tg_insert_user` BEFORE INSERT ON `user` FOR EACH ROW INSERT INTO trigger_user(user_id,user_names_new, user_lastnames_new, user_email_new, user_idnumber_new, user_city_new, user_country_new, user_phone_new, user_address_new, user_user_new, user_password_new, user_delete_new, trigger_user_action, trigger_user_user, trigger_user_date)
VALUES (NEW.user_id,NEW.user_names, NEW.user_lastnames, NEW.user_email, NEW.user_idnumber, NEW.user_city, NEW.user_country, NEW.user_phone, NEW.user_address, NEW.user_user, NEW.user_password, NEW.user_delete, 'INSERT',NEW.user_current_user,NOW())
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tg_update_user` BEFORE UPDATE ON `user` FOR EACH ROW INSERT INTO trigger_user(user_names_old, user_lastnames_old, user_email_old, user_idnumber_old, user_city_old, user_country_old, user_phone_old, user_address_old, user_user_old, user_password_old, user_delete_old, user_names_new, user_lastnames_new, user_email_new, user_idnumber_new, user_city_new, user_country_new, user_phone_new, user_address_new, user_user_new, user_password_new, user_delete_new, trigger_user_action, trigger_user_user, trigger_user_date) VALUES 
(OLD.user_names, OLD.user_lastnames, OLD.user_email, OLD.user_idnumber, OLD.user_city, OLD.user_country, OLD.user_phone, OLD.user_address, OLD.user_user, OLD.user_password, OLD.user_delete,NEW.user_names, NEW.user_lastnames, NEW.user_email, NEW.user_idnumber, NEW.user_city, NEW.user_country, NEW.user_phone, NEW.user_address, NEW.user_user, NEW.user_password, NEW.user_delete, 'UPDATE',NEW.user_current_user,NOW())
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adicional`
--
ALTER TABLE `adicional`
  ADD PRIMARY KEY (`adicional_id`);

--
-- Indices de la tabla `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`album_id`);

--
-- Indices de la tabla `categoria_documento`
--
ALTER TABLE `categoria_documento`
  ADD PRIMARY KEY (`categoria_documento_id`);

--
-- Indices de la tabla `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`content_id`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`cursos_id`);

--
-- Indices de la tabla `documento`
--
ALTER TABLE `documento`
  ADD PRIMARY KEY (`documento_id`);

--
-- Indices de la tabla `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`foto_id`);

--
-- Indices de la tabla `modulo`
--
ALTER TABLE `modulo`
  ADD PRIMARY KEY (`modulo_id`);

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD PRIMARY KEY (`pregunta_id`);

--
-- Indices de la tabla `respuesta`
--
ALTER TABLE `respuesta`
  ADD PRIMARY KEY (`respuesta_id`);

--
-- Indices de la tabla `seccion_modulo`
--
ALTER TABLE `seccion_modulo`
  ADD PRIMARY KEY (`seccion_modulo_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adicional`
--
ALTER TABLE `adicional`
  MODIFY `adicional_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `album`
--
ALTER TABLE `album`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `categoria_documento`
--
ALTER TABLE `categoria_documento`
  MODIFY `categoria_documento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `content`
--
ALTER TABLE `content`
  MODIFY `content_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `cursos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `documento`
--
ALTER TABLE `documento`
  MODIFY `documento_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `foto`
--
ALTER TABLE `foto`
  MODIFY `foto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `modulo`
--
ALTER TABLE `modulo`
  MODIFY `modulo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  MODIFY `pregunta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `respuesta`
--
ALTER TABLE `respuesta`
  MODIFY `respuesta_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `seccion_modulo`
--
ALTER TABLE `seccion_modulo`
  MODIFY `seccion_modulo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
